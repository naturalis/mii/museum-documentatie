#!/bin/bash
echo 'Start updating docs.museum.naturalis.nl'

# Clone the documentation
cd /var/tmp/build
git checkout --quiet master
git submodule update --init --recursive --quiet
hugo --quiet -D --enableGitInfo --config config.yaml -s ./
mkdir -p /var/www/docs/
rsync -ah ./public/ /var/www/docs/

#echo rm -rf /tmp/$buildjob
echo 'Documentation has been built!'
