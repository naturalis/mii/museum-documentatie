var relearn_search_index = [
  {
    "content": "Met het oog op het beheer van het laboratorium hanteert Naturalis een uitgebreide set aan standaarden.\nDe standaarden zijn opgesteld door het ‘ICT Lab Team’ van Naturalis. Voor support in het lab op het gebied van ICT is dit team het aanspreekpunt voor vragen \u0026 overleg over het meer algemeen bewaken van deze standaarden.\n",
    "description": "",
    "tags": null,
    "title": "1. Standaarden",
    "uri": "/standaarden/index.html"
  },
  {
    "content": "",
    "description": "",
    "tags": null,
    "title": "2. Stack",
    "uri": "/stack/index.html"
  },
  {
    "content": "",
    "description": "",
    "tags": null,
    "title": "3. Labs",
    "uri": "/labs/index.html"
  },
  {
    "content": "",
    "description": "",
    "tags": null,
    "title": "4. Workstations",
    "uri": "/workstations/index.html"
  },
  {
    "content": "",
    "description": "",
    "tags": null,
    "title": "5. Software",
    "uri": "/software/index.html"
  },
  {
    "content": "",
    "description": "",
    "tags": null,
    "title": "6. Equipment",
    "uri": "/equipment/index.html"
  },
  {
    "content": "",
    "description": "",
    "tags": null,
    "title": "7. Compute ",
    "uri": "/compute/index.html"
  },
  {
    "content": "ICT Documentatie Lab Naturalis Note De inrichting van deze website is nog in concept status en dus nog niet in productie. Gelieve dus nog even te wachten met het gebruik er van…\nIndeling Algemeen nut Labs Work in progress Het documenteren is - en blijft voorlopig - work in progress. Als er zaken ontbreken op een pagina wordt dit aangegeven met een informatieblok als de onderstaande:\nDeze documentatiepagina bevat issues: Er stond geen koud bier klaar bij het documenteren van deze pagina Waar is Titus? Wat heeft Joep nu weer gedaan? David z’n goedkeuring - grom - krijgt het sowieso niet (er is altijd wel een Markdown syntax probleem). Hebben jullie ook een glitteronesie bij je voor vananavond? ",
    "description": "",
    "tags": null,
    "title": "ICT Documentatie Lab Naturalis",
    "uri": "/index.html"
  },
  {
    "content": "",
    "description": "",
    "tags": null,
    "title": "Categories",
    "uri": "/categories/index.html"
  },
  {
    "content": "",
    "description": "",
    "tags": null,
    "title": "Tags",
    "uri": "/tags/index.html"
  }
]
