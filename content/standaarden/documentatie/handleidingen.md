---
title: "Handleidingen"
date: 2018-05-23T14:32:57+02:00
draft: true
weight: 35
---

Bij het maken van een handleiding in HUGO wordt bij voorkeur gebruik gemaakt van het archetype handleidingen.

Je kunt een pagina aanmaken op basis van het handleidingen archetype met het commando:

```bash
hugo new handleidingen/naamnieuwehandleiding
```

De handleiding komt terecht in de folder handleidingen. Vervolgens kun je de handleiding toevoegen als bijlage en er naar linken in de desbetreffende documentatie pagina.  

Het archetype voorziet in een template dat bestaat uit de onderstaande onderdelen. Deze kan naar eigen inzicht worden aangepast maar bied een basisstructuur ter bevoordering van de leesbaarheid.

## Algemene informatie

In het onderstaande document geven we aan wat de gewenste document indeling is voor een handleiding en wat we hoe beschreven willen zien.

### Inhoud

## Omschrijving

Om schrijving van wat de lezer kan doen met het volgen van de handleiding.

## Benodigdheden

Een overzicht van alles wat nodig is voor het uitvoeren van de instructie.

### Personen

Hier wordt aangegeven met hoeveel mensen de handeling kan worden verricht. Hou hierbij ook rekening met de ARBO regels die op dit gebied van toepassing zijn.

### Kennis

Welke voorkennis is nodig voor het uitvoeren van de handelingen en waar is deze te vinden.

### Onderdelen

Een overzicht van de onderdelen die nodig zijn bij het uitvoeren van de handelingen indien nodig.

### Gereedschap

Een overzicht van het gereedschap dat nodig is voor het uitvoeren van de instructies.

## Instructies

Een tekstuele omschrijving van de verschillende stappen die moeten worden uitgevoerd met een afbeelding ter verduidelijking indien dit een toegevoegde waarde heeft. Waar nodig worden ook de risico's aangegeven zowel voor de personen als eventuele beschadiging van componenten.

## Tests

Hier wordt aangegeven welke tests er kunnen worden uitgevoerd om te testen of de instucties ook het gewenste resultaat hebben opgeleverd.
