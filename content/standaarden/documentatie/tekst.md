---
title: "Tekst"
date: 2018-05-23T14:34:29+02:00
draft: false
weight: 20
---

Er is gekozen voor [Markdown](https://www.markdownguide.org/), toelichting op de
keuze staat in [dit
document](https://gitlab.com/naturalis/mii/museum-documentatie/blob/master/content/ontwerpen/documentatie-platform.md).

{{% notice tip %}}
[Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#tables)
{{% /notice %}}

Op elke pagina kun je daarnaast de documentatiekwaliteit aangeven door het gebruik van de `pagequality` shortcode:

```markdown
{{%/* pagequality */%}}
* Er is een eerste issue
* En een tweede
{{%/* /pagequality */%}}
```
