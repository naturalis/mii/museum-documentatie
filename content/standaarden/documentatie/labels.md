---
title: "Labels"
date: 2018-06-04T14:31:20+02:00
draft: false
weight: 5
---

Een belangrijk onderdeel van de documentatie van het museum bestaat uit de
labeling van componenten. In dit document staan de algemene uitgangspunten en de
concrete richtlijnen op het vlak van labeling uitgelegd.

## Uitgangspunten

In z'n algemeenheid gelden voor de labeling de volgende uitgangspunten:

1. De labels sluiten qua inhoud aan op de standaard [naamgeving](./naamgeving.md).
1. Er wordt gebruik gemaakt van professionele, duurzame labels.
1. Bij het vervangen van een onderdeel dienen slechts een minimale hoeveelheid labels
   vervangen te worden, idealiter alleen de labels van het betreffende onderdeel.

Wat het eerste uitgangspunt betreft valt de notatie op labels in beginsel samen
met de wijze waarop [variabelen](./naamgeving.md#variabelen)
worden genoteerd. Dat betekent dat ze bestaan uit een combinatie van de naam van
een `Component`, de naam van een `Variabele` en de waarde van die `Variabele`.

Het is echter niet altijd praktisch mogelijk of van toegevoegde waarde om al die
informatie op een label te zetten. Om die reden staan verderop in dit document
per toepassing de richtlijnen voor zowel de notatie als de fysieke eisen van
labels gespecificeerd.

Voor het labelen van onderdelen in het museum wordt gebruik gemaakt van een
[Altec TTP-343 E Plus](http://www.altec.nl/ttp-300) labelprinter. In
onderstaande documentatie zijn verwijzingen naar specifieke labeltypes, maten en
bijbehorende templates opgenomen. De templates zijn te gebruiken in het
programma [Altec Dataprint](http://www.altec.nl/dataprint).

## Labeling van AV en overige kabels

Componenten in het museum zijn onderling verbonden door middel van
(verschillende typen) kabels. Afhankelijk van de toepassing dienen deze kabels
worden voorzien van labels.

Het doel van de labeling van kabels waarmee AV apparatuur wordt aangesloten
is dat ter plekke op basis van labels kan worden afgelezen van en naar welk
component een kabel loopt. De labeling van deze kabels dient bovendien de
vervanging van componenten te vergemakkelijken / versnellen.

De opbouw van de tekst op een `Label` is afhankelijk van de volgende situaties:

1. De connector van de kabel past maar op één poort op het apparaat.
1. De connector van de kabel past op meerdere poorten op het apparaat waarbij
   het praktisch niet uitmaakt op welke poort de kabel wordt aangesloten (bijv.:
   een powerkabel aansluiten op één van de poorten in een stekkerdoos).
1. De connector van de kabel past op meerdere poorten op het apparaat waarbij
   het in theorie wél uitmaakt op welke poort de kabel wordt aangesloten (bijv.:
   een videokabel aansluiten op één van de HDMI-poorten van een projector).

### Opbouw

#### Poortonafhankelijk

In de situatie waarbij het niet uitmaakt op welke specifieke poort een kabel
wordt aangesloten (situatie 2 en 3) bestaat de tekst op een `Label` uit:

`<naamcomponent>.<naamvariabele>`

Voor de teksten van deze `Labels` gelden de volgende regels:

* Heeft een maximale lengte van 56 karakters
* Bevat bij het uitschrijven op één regel altijd een punt (`.`). Wanneer de
  tekst te lang is voor één regel dan wordt de punt vervangen door een linebreak.
* Het deel voor de punt bestaat uit de naam van het `Component` waar naar wordt
  verwezen.
* Het deel na de punt bestaat uit het type poort waarop de kabel dient te worden
  aangesloten.

Voorbeeld: `stamboom-cmp-1.nic` (De netwerkinterface op een computer van `stamboom`)

#### Poortafhankelijk

In de vierde en laatste situatie, waarbij het wel uitmaakt op welke poort een
kabel wordt aangesloten bestaat de tekst uit:

`<naamcomponent>.<naamvariabele>:<waardevariabele>`

Voor de teksten van deze `Labels` gelden de volgende regels:

* Heeft een maximale lengte van 56 karakters
* Bevat bij het uitschrijven op één regel altijd een punt (`.`) en een dubbele
  punt (`:`). Wanneer de tekst te lang is voor één regel dan wordt de punt
  vervangen door een linebreak.
* Het deel voor de punt bestaat uit de naam van het `Component` waar naar wordt
  verwezen.
* Het deel tussen de punt en de dubbele punt bestaat uit het type poort waarop
  de kabel dient te worden aangesloten.
* Het deel na de dubbele punt bestaat uit een aanduiding van de specifieke poort
  waarop de kabel dient te worden aangesloten.

Voorbeeld: `stamboom-prj-1.hdmi:1` (HDMI poort 1 op een projector van `stamboom`)

Voor kabellabels die verwijzen naar de hierboven bedoelde 'overige componenten',
zoals `Wandcontactdozen` en `Outlets` gelden afwijkende regels:

* Heeft een maximale lengte van 56 karakters
* Bevat altijd een dubbele punt (`:`).
* Het deel voor de dubbele punt bestaat uit het type `Component`.
* Het deel na de dubbele punt bestaat uit de naam van het `Component`.

Voorbeeld: `outlet:B2.D24`

### Fysieke eisen

#### Locatie en aantal

Bij het bepalen van de locatie en het aantal labels voor alle AV kabels
geldt een onderscheid tussen twee situaties:

##### Zichtbaar

In deze situatie is vanaf het ene component zichtbaar naar welk ander component de
betreffende kabel loopt.

In deze gevallen voldoet het om aan de kant van component A de kabel te voorzien
van een label dat refereert aan dat component en vice versa bij component B.

##### Niet zichtbaar

In deze situatie is vanaf het ene component NIET zichtbaar naar welk ander
component de betreffende kabel loopt.

Kabels worden op de volgende plaatsen van labels voorzien:

* In de nabijheid van beide componenten
* Boven een eventuele doorvoer in de vloer

Op al deze plekken dient een kabel van twee labels te worden voorzien. Het label
dat zich het dichtst bij component A bevindt refereert aan dat component en het
label dat zich het dichtst bij component B bevindt refereert daaraan. In [dit
diagram](https://www.draw.io/?lightbox=1&highlight=0000ff&edit=_blank&layers=1&nav=1&title=Labels.html#Uhttps%3A%2F%2Fdrive.google.com%2Fa%2Fnaturalis.nl%2Fuc%3Fid%3D1yHieQT567oCd_ZBZ8qTfL7mGpYjQrG3g%26export%3Ddownload)
is deze werkwijze gevisualiseerd.

#### Labelmaten

Voor alle kabels met een beperkte dikte, waaronder in ieder geval UTP kabels en
reguliere power kabels, worden zogeheten
[wikkellabels](http://www.altec.nl/ttp-24x12x36mm-wikkellabel-4-rij) (of:
Self-Laminating Wire Wraps) gebruikt.

Voor wikkellabels hanteren we templates voor twee maten:

* [Wikkellabels voor
    kabels](https://drive.google.com/open?id=1zWi-J5wbXdTX4wCUc7_ADAm849YziD9V)
    ([productcode
    53001024](http://www.altec.nl/ttp-24x12x36mm-wikkellabel-4-rij))
* [Wikkellabels voor dikkere kabels](https://drive.google.com/open?id=121v6eNw8Ar1oIaFMx-fX0nhUywpZlwFL) ([productcode 53001026](http://www.altec.nl/ttp-24x19x57mm-wikkellabel-4-rij-477))

Voor kabels met een zeer beperkte dikte, zoals USB-kabels of sommige
speakerkabel worden in overleg zogeheten
[T-labels](http://www.altec.nl/ttpft-30x10mm-vlaglabel-t-model)
gebruikt.

Voor T-labels hanteren we het volgende template:

* [T-labels voor
    kabels](https://drive.google.com/open?id=1b0PbTYZgz30FDVfx0ZLL3WWGVm_FtBUd) ([productcode 53000522](http://www.altec.nl/ttpft-30x10mm-vlaglabel-t-model))

## Labeling van lichtkabels

De labeling van kabels van lichtapparatuur is anders van opzet dan die van AV
en overige kabels. Elke kabel krijgt daarbij een unieke naam die in de
documentatie wordt verwerkt. Deze opzet vergemakkelijkt het aansluiten van grote
hoeveelheden armaturen.

### Opbouw

Voor labels op lichtkabels bestaat de tekst uit:

`<afkortingtypekabel>-<lengte>m-<volgnummer>`

Voor de verschillende typen lichtkabels worden de volgende afkortingen gebruikt:

* Schuko: `shu`
* Powercon: `pcn`
* DMX: `dmx`
* Light-n-lok: `lnl`

Dit resulteert in dit soort labelteksten:

* `shu-02m-023`
* `pcn-01m-995`

### Fysieke eisen

#### Locatie en aantal

Voor lichtkabels geldt dat, ongeacht de lengte en toepassing van de kabel altijd
een label aan beide uiteinden van de kabel wordt aangebracht.

#### Labelmaten

Voor alle lichtkabels met een beperkte dikte, waaronder in ieder geval UTP kabels en
reguliere power kabels, worden zogeheten
[wikkellabels](http://www.altec.nl/ttp-24x12x36mm-wikkellabel-4-rij) (of:
Self-Laminating Wire Wraps) gebruikt.

Voor wikkellabels die voor lichtkabels worden gebruikt hanteren we templates voor twee maten:

* [Wikkellabels voor
    lichtkabels](https://files.museum.naturalis.nl/s/DHmkrs3Z8nkMf7K)
    ([productcode
    53001024](http://www.altec.nl/ttp-24x12x36mm-wikkellabel-4-rij))
* [Wikkellabels voor dikkere lichtkabels](https://files.museum.naturalis.nl/s/GEAzTxgQQqy4kZ3) ([productcode 53001026](http://www.altec.nl/ttp-24x19x57mm-wikkellabel-4-rij-477))

Voor kabels met een zeer beperkte dikte, zoals USB-kabels of sommige
speakerkabel worden in overleg zogeheten
[T-labels](http://www.altec.nl/ttpft-30x10mm-vlaglabel-t-model)
gebruikt.

Voor T-labels hanteren we het volgende template:

* [T-labels voor
    kabels](https://files.museum.naturalis.nl/s/6JMDzE4JfwJNT7H) ([productcode 53000522](http://www.altec.nl/ttpft-30x10mm-vlaglabel-t-model))

## Labeling van AV apparatuur

Op basis van bovengenoemde uitgangspunten dient bij de labeling van AV
apparatuur rekening gehouden te worden met de volgende zaken:

* De naam van het apparaat, als onderdeel van een functionele eenheid, moet
  duidelijk zijn.
* Het moet duidelijk zijn op welke poort een kabel moet worden aangesloten.
* Het individuele apparaat moet te identificeren zijn.

#### Opbouw

##### Serienummer

Om te beginnen met de laatste eis; apparaten zoals computers, monitoren en
projectoren worden zonder uitzondering door fabrikanten van een label voorzien
met daarop het serienummer (al dan niet met barcode) van het apparaat. Zoals
uitgelegd in [dit document](./asset-informatie.md) dient bij het
serienummer meegeleverd te worden bij de asset informatie en wordt dit nummer
geregistreerd in het asset management systeem. Het door de fabrikant
aangebrachte label met serienummer voldoet daarmee voor het identificeren van
het individuele apparaat.

Mocht een dergelijk label ontbreken dan dient de AV installateur een apparaat te
voorzien van een label met een eigen unieke serienummer. In deze gevallen dient
dit serienummer ook als onderdeel van de asset informatie te worden aangeleverd.

##### Naam

Zoals uitgebreid toegelicht in de [naamgevingsstandaard](./naamgeving.md) heeft elk apparaat in het museum een unieke naam. Behalve dat
deze naam wordt gebruikt in de configuratie management, documentatie en asset
management systemen moet ook ter plaatse duidelijk zijn om welk apparaat het
gaat. Om die reden wordt elk apparaat dat in het museum wordt ingezet voorzien
van een label met daarop uitsluitend de naam van dat component.

##### Poorten

Bij het aansluiten van een kabel op een apparaat hebben we te maken met de
volgende situatie's:

1. De connector van de kabel past maar op één poort op het apparaat.
1. De connector van de kabel past op meerdere poorten op het apparaat waarbij
   het praktisch niet uitmaakt op welke poort de kabel wordt aangesloten (bijv.:
   een powerkabel aansluiten op één van de poorten in een stekkerdoos).
1. De connector van de kabel past op meerdere poorten op het apparaat waarbij
   het in theorie wél uitmaakt op welke poort de kabel wordt aangesloten (bijv.:
   een videokabel aansluiten op één van de HDMI-poorten van een projector).

Alleen in dit laatste geval is er een noodzaak voor een duidelijke markering van
de betreffende poort. Op de apparaten waarop dit van toepassing is én waarop
niet al een bestaande markering (zoals HDMI-1 en HDMI-2) aanwezig is zal deze
moeten worden aangebracht. Stelregel daarbij is dat per type poort een nummer
wordt gezet. Aan dit nummer wordt op de labeling van de kabels gerefereerd
(bijv. `stamboom-cmp-001.hdmi:1`).

#### Fysieke eisen

##### Serienummer

Bij apparaten waar het serienummer niet door de fabrikant met een label is
aangebracht dient het apparaat van een label te worden voorzien. Dit label bevat
behalve het serienummer zelf ook een ([code
128](https://en.wikipedia.org/wiki/Code_128)) barcode te bevatten. Voor deze
labels worden [wit polyester labels voor
typeplaatjes](http://www.altec.nl/ttp-38x13mm-wit-polyester-2-rij) gebruikt.

Het serienummerlabel wordt in beginsel op de zijde van een apparaat geplakt waar
deze het minste risico loopt om schade op te lopen. Meestal is dat de onderzijde
van het apparaat.

##### Naam

Voor labels met de naam van het apparaat worden eveneens [wit polyester labels
voor typeplaatjes](http://www.altec.nl/ttp-38x13mm-wit-polyester-2-rij)
gebruikt. Voor deze labels hanteren we het volgende template:

* [Wit polyester label voor componenten
    ](https://drive.google.com/open?id=1gaBj0RiHVse-WTmipVsFrQQoxhdE6yTV)
    ([productcode
    53000290](http://www.altec.nl/ttp-38x13mm-wit-polyester-2-rij))

Het naamlabel wordt op een dusdanige plek geplakt zodat het label zodat de
zichtbaarheid voor beheerders is geoptimaliseerd.

##### Poorten

Voor het aanbrengen van markeringen van poorten is er afhankelijk van het
apparaat maar zeer beperkt ruimte. In de gevallen waar plaats is voor labels
worden voor poortlabels eveneens [wit polyester
labels](http://www.altec.nl/ttp-10x5mm-wit-polyester-4-rij) gebruikt. Waar dit
niet haalbaar is wordt een markering aangebracht met een [outdoor white of black
marker](https://www.123inkt.nl/Edding-8055-outdoor-marker-wit-4-8055-1-1049-i44449-t475064.html)

De nummering van poorten worden in beginsel per rij van links naar rechts aangebracht.

## Labeling van lichtapparatuur

### Opbouw

Lichtapparatuur als armaturen en LED drivers worden voorzien van een label met
de naam van het apparaat. De naam is als volgt opgebouwd:

`<drieletterigeafkortingmerk>-<drieletterigeafkortingtype>-<volgnummer>`

Voorbeeld: `cls-fcz-002`

Addresseerbare armaturen worden daarnaast voorzien van een label met het
geprogrammeerde adres. De label is als volgt opgebouwd:

`dmx:<universe>/<adres>`

Voorbeeld: `dmx:2/123`

#### Fysieke eisen

Zowel voor labels met de naam als die met het (DMX)adres worden [wit
polyester labels voor
typeplaatjes](http://www.altec.nl/ttp-38x13mm-wit-polyester-2-rij)
gebruikt. Voor deze labels hanteren we het volgende template:

* [Wit polyester label voor componenten
    ](https://files.museum.naturalis.nl/s/jqcDwDGWf7YKrRJ)
    ([productcode
    53000290](http://www.altec.nl/ttp-38x13mm-wit-polyester-2-rij))

De label worden op een dusdanige plek geplakt rekening houdend met de
zichtbaarheid voor beheerders en de onzichtbaarheid voor het publiek.

## Labeling van outlets

### Opbouw

De labels op outlets bevatten simpelweg de naam van die componenten.

Voorbeeld: `B2.D24`

### Fysieke eisen

Het labelen van `Outlets` wordt uitgevoerd door de bouwkundig installateur.
Hiervoor worden (naar verwachting) resopal graveerplaatjes gebruikt.

## Labeling van WCD's, PDU's en TCD's

### Opbouw

De labels op  outlets en wandcontactdozen bevatten simpelweg de naam van die
componenten.

Voorbeeld: `B2.D24`

### Fysieke eisen

De wandcontactdozen dienen te worden gelabeld met een qua uiterlijk en kwaliteit
betreft vergelijkbaar alternatief in de vorm van [EPREP (Engravement Plate
Replacement) labels](http://www.altec.nl/labels/alternatief-voor-resopal).

De PDU's worden van wit polyester labels met de naam voorzien. Indien de PDU en
WCD ver uit elkaar liggen dient aan het einde van de kabel, aan de kant van de
WCD eveneens een T-label te worden aangebracht met de aanduiding
`wcd: <wcdnaam>`.

De tafelcontactdozen (TCD's) worden eveneens van een wit polyester label met de
naam voorzien. Indien de TCD op een PDU wordt aangesloten dient een T-label
te worden aangebracht met de aanduiding van de PDU en de socket. In alle andere
gevallen dient - indien andere TCD's of een WCD en de betreffende TCD ver uit
elkaar liggen - een T-label moet worden aangebracht met daarop de naam van de
TCD of WCD waarop de stekker is aangesloten.
