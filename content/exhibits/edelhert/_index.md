---
title: "edelhert"
date: 2019-07-24T13:48:33Z
draft: true
status:
unid: 25a215d4-8f26-4259-8fb1-1cd3c77f9045
componenten:
tentoonstellingen:
- deijstijd
attachments:
- title: deijstijd_podiumedelhert_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/jiFpw2sMc5aqLys
---

![deijstijd_podiumedelhert_bouwtekening](https://files.museum.naturalis.nl/s/7wP8RMSWPEkcwYt/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
