---
title: "steppenwisentvitrine"
date: 2019-07-24T16:05:10Z
draft: true
status:
unid: c54fe2cc-6403-40ad-8ca8-ee75cc8ab4c8
componenten:
tentoonstellingen:
- deijstijd
attachments:
- title: deijstijd_vitrinetypeC_bouwtekening_neushoorn_en_steppenwisenten_onderkaken (pdf)
  src: https://files.museum.naturalis.nl/s/TXbskRKj8XfmyF8
---

![deijstijd_vitrinetypeC_bouwtekening_neushoorn_en_steppenwisenten_onderkaken](https://files.museum.naturalis.nl/s/CagrZ4XtHarNpZN/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
