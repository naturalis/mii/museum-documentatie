---
title: "mammoetvitrine"
date: 2019-07-24T13:48:37Z
draft: true
status:
unid: c8e04699-71e6-484e-8893-9a83e9ffcd23
componenten:
tentoonstellingen:
- deijstijd
attachments:
- title: deijstijd_vitrinetypeB_mammoet_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/Cd84PiRaSsy2sQo
---

![deijstijd_vitrinetypeB_mammoet_bouwtekening](https://files.museum.naturalis.nl/s/37N5mmSbmWN9wFx/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
