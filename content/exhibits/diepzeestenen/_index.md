---
title: "diepzeestenen"
date: 2019-07-24T13:48:32Z
draft: true
status:
unid: f6589f91-4a44-4f50-9e8f-57ff475eda7b
componenten:
tentoonstellingen:
- leven
attachments:
- title: leven_exhibits_diepzeestenen_schenkels_lagen_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/DbgQD52ippmrqX2
- title: leven_exhibits_diepzeestenen_schenkels_montage_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/3HanWzgYw3CDWPa
- title: leven_trap_diepzeestenen_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/47zYCD88DXAoyny
---

![diepzeestenen](./diepzeestenen.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
