---
title: "manenvrouw"
date: 2019-07-24T13:48:38Z
draft: true
status:
unid: be743501-f1f4-482a-929d-796ec203cbc6
componenten:
tentoonstellingen:
- deverleiding
attachments:
- title: kast samenstelling man en vrouw
  src: https://files.museum.naturalis.nl/s/79yf5TCXjQdQTWM
- title: 190224 verleiding_DO_E_manvrouw.pdf
  url: https://files.museum.naturalis.nl/s/JTBkXW7Knzr9PMx
---

![deverleiding_manenvrouw_bouwtekening](https://files.museum.naturalis.nl/s/ztnM8jgXkTrcQ8K/preview)

## Functionele omschrijving

Een aantal dieren en flink wat planten maakt het nietveel uit welk geslacht ze zijn. Sterker nog, ze kunnenzijn wat ze willen: een mannetje of een vrouwtje, net watuitkomt. Of gewoon allebei tegelijkertijd! Of eerst het eenen dan het ander, dat wordt sequentieel hermafroditismegenoemd.Bezoekers zien een beeld van Hermaphroditus metmannelijke en vrouwelijke geslachtskenmerken, met eromheen collectie van dieren en planten die ook zowelman als vrouw (kunnen) zijn. (7-10 stuks)

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

* Decor: [Bruns](https://bruns.nl)

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
