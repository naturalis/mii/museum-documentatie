---
title: "kameelenco"
date: 2019-07-24T13:48:36Z
draft: true
status:
unid: ac6f0738-274b-4fbc-8753-09639538cdd3
componenten:
tentoonstellingen:
- leven
attachments:
- title: leven_exhibits_kameelenco_basispodium_detail_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/G6Z7JFxtEpCGd4m
- title: leven_exhibits_kameelenco_laag3_randen_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/8KLPtWe5wqTPck7
---

![kameelenco](./kameelenco.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
