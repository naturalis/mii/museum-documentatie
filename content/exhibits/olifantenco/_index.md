---
title: "olifantenco"
date: 2019-07-24T16:05:07Z
draft: true
status:
unid: 67d8ac2d-a57f-433d-a871-8dbc583a3067
componenten:
tentoonstellingen:
- leven
attachments:
- title: leven_trap_olifant_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/YdzpeA3BR5dFYoG
---

<!-- Voeg een overzichtsafbeelding toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
