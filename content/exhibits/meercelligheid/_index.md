---
title: "meercelligheid"
draft: true
status:
componenten:
tentoonstellingen:
- evolutie
experiences:
attachments:
- title: evolutie_exhibit_meercelligheid_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/B7kt8KgKp9LE6Nb
- title: evolutie_exhibit_meercelligheid_bouwtekening.pdf
  url: https://files.museum.naturalis.nl/s/efGkC5GcfTo9T7x
---

<!-- Voeg een overzichtsafbeelding toe -->

![definitiefontwerp](./meercelligheid_ontwerp.png "definitief ontwerp")

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
