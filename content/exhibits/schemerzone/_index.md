---
title: "schemerzone"
date: 2019-07-24T16:05:09Z
draft: true
status:
unid: 8ca03c93-6adb-4fe5-840d-6a514b26b9cf
componenten:
tentoonstellingen:
- leven
attachments:
- title: leven_exhibits_schemerzone_kolom2_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/qdSE3G3jibXCjDD
- title: leven_exhibits_schemerzone_lichtsluis_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/2YfRTXESWp4WoK9
- title: leven_exhibits_schemerzone_MDFplaten_L3-5_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/fCyZSr5WAA9mZ9F
- title: leven_exhibits_schemerzone_onderconstructie_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/FDpNFeDgC3etMEo
- title: leven_exhibits_schemerzone_wanden_L1-2 detail_bouwtekeningpdf (pdf)
  url: https://files.museum.naturalis.nl/s/8xk4G58cp7GTWW5
---

![schemerzone](./schemerzone.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
