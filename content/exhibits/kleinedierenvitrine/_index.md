---
title: "kleinedierenvitrine"
date: 2019-07-24T13:48:37Z
draft: true
status:
unid: 9f948649-f69c-4240-b3b7-ebee6b1fce2a
componenten:
tentoonstellingen:
- deijstijd
attachments:
- title: deijstijd_vitrinetypeA_kleinedierenvitrine_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/BgCiy6yfiknnpFw
---

![deijstijd_vitrinetypeA_kleinedierenvitrine_bouwtekening](https://files.museum.naturalis.nl/s/KdgjAgWgSSGSBGy/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->



## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
