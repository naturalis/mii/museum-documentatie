---
title: "zeekoe"
draft: true
status:
componenten:
tentoonstellingen:
- evolutie
experiences:
attachments:
- title: evolutie_exhibit_zeekoe_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/66n8WSf4YEtaPtB
- title: evolutie_exhibit_zeekoe__definitiefontwerp2.pdf
  url: https://files.museum.naturalis.nl/s/qkQ7ACpXRd8tL4T
- title: evolutie_exhibit_zeekoe_ontwerptekening.pdf
  url: https://files.museum.naturalis.nl/s/4QQmytp5rrr8X22
- title: evolutie_exhibit_zeekoe_meubel_bouwtekening.pdf
  url: https://files.museum.naturalis.nl/s/gt96makfFPwCL3a
- title: evolutie_exhibit_zeekoe_glasbinnenl_tekening.pdf
  url: https://files.museum.naturalis.nl/s/TapqxC4CM4TNk8g
- title: evolutie_exhibit_zeekoe_glasbuiten_tekening.pdf
  url: https://files.museum.naturalis.nl/s/StKJDgBXHEEtyGA
---

<!-- Voeg een overzichtsafbeelding toe -->

![definitiefontwerp](./zeekoe_ontwerp.png "definitief ontwerp")

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
