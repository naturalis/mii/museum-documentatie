---
title: "ijstijdmaquette"
date: 2019-07-24T13:48:36Z
draft: true
status:
unid: 7b570713-10f2-45e8-8e40-00ef2b64dedb
componenten:
tentoonstellingen:
- deijstijd
attachments:
- title: plattegrond
  src: https://files.museum.naturalis.nl/s/GSyyrj3JgWwoQwo
- title: ontwerp wolk 1
  url: https://files.museum.naturalis.nl/s/3Wjf86ZbjfbjSA7
- title: ontwerp wolk 2
  url: https://files.museum.naturalis.nl/s/SDYG5QwmNpzqeAL
- title: officieel rapport
  url: https://files.museum.naturalis.nl/s/GAnBfdxp7kSTyPc
- title: technische informatieblad
  url: https://files.museum.naturalis.nl/s/dMpGJ2bCLyw4XxF
- title: deijstijd_ijstijdmaquette_brandveiligheid_1 (pdf)
  url: https://files.museum.naturalis.nl/s/D4GaBKpQrPNy3qm
- title: deijstijd_ijstijdmaquette_brandveiligheid_2 (pdf)
  url: https://files.museum.naturalis.nl/s/qZskNEmsom4DWrD
- title: deijstijd_ijstijdmaquette_hekwerk_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/LZdz4red7qbaReP
- title: deijstijd_ijstijdmaquette_hekwerk_serviceluik_bouwtekening_1 (jpg)
  url: https://files.museum.naturalis.nl/s/km8kezQ76aSKWTX
- title: deijstijd_ijstijdmaquette_hekwerk_serviceluik_bouwtekening_2 (jpg)
  url: https://files.museum.naturalis.nl/s/2AC9EmzXr8RD39j
- title: ijstijd_ijstijdmaquette_hekwerk_serviceluiktechniek_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/aHHCMp5ScZPxNie
- title: deijstijd_ijstijdmaquette_opdrachtomschrijving_diermodellen (pdf)
  url: https://files.museum.naturalis.nl/s/WT9NB2WPn25etNf
- title: deijstijd_ijstijdmaquette_ophangpunten wolken_bouwtekening (jpg)
  url: https://files.museum.naturalis.nl/s/BoNjcFArB9Y3t3D
---

![plattegrond](./plattegrond.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->


## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

* Projectleider: [Matthijs Vegter](mailto:matthijs.vegter@naturlais.nl)

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

* Decor: [Bruns](https://bruns.nl)

### Ontwerper

* Decor: [Daniel Ament](mailto:info@danielament.com)
* Inhoud:


## Bijlagen

{{< pagelist bijlagen >}}
