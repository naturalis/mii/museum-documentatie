---
title: "picknicktafeljapanhawaii"
date: 2019-07-24T13:48:37Z
draft: true
status:
unid: ad8ce344-a087-40f6-8983-481047431665
componenten:
tentoonstellingen:
- deaarde
attachments:
- title: lavatafel
  src: https://files.museum.naturalis.nl/s/eidJ8Nf6B7eEywS
- title: lavavormen (docx)
  url: https://files.museum.naturalis.nl/s/39EDfxCPBBgCQ4K
- title: lavaverzameling (docx)
  url: https://files.museum.naturalis.nl/s/jepmQ3jNKq4igHt
- title: geothermisch gesteente (docx)
  url: https://files.museum.naturalis.nl/s/THrFBDKrmMSt2HZ
---

![lavatafel](https://files.museum.naturalis.nl/s/eidJ8Nf6B7eEywS/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
