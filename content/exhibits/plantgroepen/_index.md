---
title: "plantgroepen"
draft: true
status:
componenten:
tentoonstellingen:
- evolutie
experiences:
attachments:
- title: evolutie_exhibit_plantgroepen_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/ACozmEgDiXfp5WW
- title: evolutie_exhibit_aanland_meubel_bouwtekening.pdf
  url: https://files.museum.naturalis.nl/s/2JFo3j3xCBwdxRH
- title: evolutie_exhibit_aanland_glasbinnen_bouwtekening.pdf
  url: https://files.museum.naturalis.nl/s/GXj2Nfbi44ReA7T
- title: evolutie_exhibit_aanland_glasbuiten_bouwtekening.pdf
  url: https://files.museum.naturalis.nl/s/EywqcWNbeADrsNZ
- title: evolutie_exhibit_aanland_meubelaanz+drsn_bouwtekening.pdf
  url: https://files.museum.naturalis.nl/s/4CNs7F3isK2R7AG
- title: evolutie_exhibit_aanland_meubeldrsn_detail_bouwtekening.pdf
  url: https://files.museum.naturalis.nl/s/9YwtYqWtnAimjSa
---

<!-- Voeg een overzichtsafbeelding toe -->

[![definitiefontwerp](https://files.museum.naturalis.nl/s/ACozmEgDiXfp5WW/preview)](https://files.museum.naturalis.nl/s/ACozmEgDiXfp5WW)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
