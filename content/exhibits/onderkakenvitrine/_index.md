---
title: "onderkakenvitrine"
date: 2019-07-24T16:05:08Z
draft: true
status:
unid: 32b2b41c-5bbe-4aab-8c2b-ebcd84ea7e6a
componenten:
tentoonstellingen:
- deijstijd
attachments:
- title: deijstijd_vitrinetypeC_bouwtekening_neushoorn_en_steppenwisenten_onderkaken (pdf)
  src: https://files.museum.naturalis.nl/s/7SA75Zmi2cofkgP
---

![deijstijd_vitrinetypeC_bouwtekening_neushoorn_en_steppenwisenten_onderkaken](https://files.museum.naturalis.nl/s/4wckCijEBFDDoBL/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->



## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
