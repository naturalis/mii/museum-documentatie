---
title: "slakken"
draft: true
status:
componenten:
tentoonstellingen:
- evolutie
experiences:
attachments:
- title: evolutie_exhibit_slakken_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/rJW945sLbsLzPGm
- title: evolutie_exhibit_slakken_meubelaanz+drsn_bouwtekening.pdf
  url: https://files.museum.naturalis.nl/s/fCNQy9m8gqew2in
- title: evolutie_exhibit_slakken_meubel_bouwtekening.pdf
  url: https://files.museum.naturalis.nl/s/2raPBYmoPQaXQRb
- title: evolutie_exhibit_slakken_meubelglas_bouwtekening.pdf
  url: https://files.museum.naturalis.nl/s/M6eTP2S8rJnBwNW
---

<!-- Voeg een overzichtsafbeelding toe -->

![definitiefontwerp](./slakken_ontwerp.png "definitief ontwerp")

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
