---
title: "stenenhandelaar"
date:
draft: true
status:
unid:
componenten:
tentoonstellingen:
- deaarde
attachments:
- title: stenenhandelaar
  src: https://files.museum.naturalis.nl/s/YX4ePFn4rE6yQnB
- title: stenenhandelaar sfeer
  src: https://files.museum.naturalis.nl/s/o4EmjfZFJnLe6yz
- title: stenenhandelaar (docx)
  scr: https://files.museum.naturalis.nl/s/HwnZtLCMWrR2Zdr
- title: amethyst
  scr: https://files.museum.naturalis.nl/s/6wSP4GNHtMgc9Kb
- title: amesthyst samenstelling
  scr: https://files.museum.naturalis.nl/s/6TPQpoz9nCWEqon
---

![stenenhandelaar](https://files.museum.naturalis.nl/s/YX4ePFn4rE6yQnB/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

* Decor: [Bruns](https://bruns.nl)

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
