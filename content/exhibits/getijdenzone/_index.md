---
title: "getijdenzone"
date: 2019-07-24T13:48:35Z
draft: true
status:
unid: 5e8b7c1f-936f-4aba-a749-fdce21712a44
componenten:
tentoonstellingen:
- leven
attachments:
- title: leven_exhibits_getijdenzone_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/TRepd7BbnPqeQoy
- title: leven_exhibits_getijdenzone_randen_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/oPpdSqR3jH5DK4G
---

![getijdenzone](./getijdenzone.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
