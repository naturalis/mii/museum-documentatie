---
title: "bananen"
draft: true
status:
componenten:
tentoonstellingen:
- evolutie
experiences:
attachments:
- title: evolutie_exhibit_bananen_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/tF5FgcT2mbEMzNn
- title: evolutie_exhibit_bananen_bouwtekening.pdf
  url: https://files.museum.naturalis.nl/s/fndyXCTY7ngLcCZ
- title: evolutie_exhibit_bananen_glasverticaalrevA_bouwtekening.pdf
  url: https://files.museum.naturalis.nl/s/qH6oLmaAX8M8Y9A
- title: evolutie_exhibit_bananen_glashoriztaalrevA_bouwtekening.pdf
  url: https://files.museum.naturalis.nl/s/obZTBHEHiowWNf8
---

<!-- Voeg een overzichtsafbeelding toe -->

![definitiefontwerp](./bananen_ontwerp.png "definitief ontwerp")

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
