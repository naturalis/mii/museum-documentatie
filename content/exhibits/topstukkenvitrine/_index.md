---
title: "topstukkenvitrine"
draft: false
status:
unid: c9d9f72d-bc14-470e-b74f-750ace16c659
tentoonstellingen:
- livescience
componenten:
- miniclima-ebc
attachments:
- title: ID Sheet (pdf)
  url: https://files.museum.naturalis.nl/s/bRPa74bjN3nGeKi
- title: livescience_exhibits_topstukkenvitrine_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/LbX4JqK7EMrN4mD
- title: lifescience_exhibits_topstukkenvitrine_ periodieke_controle_Miniclima_documentatie (doc)
  url: https://files.museum.naturalis.nl/s/aNWtbojWdNmiXD7
- title: lifescience_exhibits_topstukkenvitrine_Miniclima_documentatie (pdf)
  url: https://files.museum.naturalis.nl/s/TbojZ8NQ3PFaWQ9
- title: lifescience_exhibits_topstukkenvitrine_Miniclima_manual_documentatie (pdf)
  url: https://files.museum.naturalis.nl/s/q6kdDERdAKnq8bT
---

![tekening](https://files.museum.naturalis.nl/s/bQx5Z5ZDQiFXF3a/preview)

![schema](https://files.museum.naturalis.nl/s/A5cCQiR8mbympLj/preview)

## Functionele omschrijving

Vitrine met topstukken uit de collectie van Naturalis.

- Een vitrine die voldoet aan de hoogste eisen voor de collectie zodat hier
  (tijdelijk)topstukken kunnen worden getoond.
- Oog in oog staan met unieke, opmerkelijke collectiestukken die je nergens
  anders inNederland (of ter wereld) kunt zien.
- De inhoud varieert door de tijd heen, zodat verschillende topstukken aan bod
  komen en detopstukken geen schade ondervinden als gevolg van te lange
  expositie

Topstukken uit de Naturalis-collectie zijn bijvoorbeeld type-materiaal (exemplaren
die zijn gebruikt voor de soortbeschrijving) of andere bijzondere objecten,
belangrijk voor de wetenschap of met een bijzonder verhaal eraan vast.

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

* [Periodieke controle Miniclima](https://files.museum.naturalis.nl/s/aNWtbojWdNmiXD7)
* [Miniclima documentatie](https://files.museum.naturalis.nl/s/TbojZ8NQ3PFaWQ9)
* [Miniclima manual documentatie](https://files.museum.naturalis.nl/s/q6kdDERdAKnq8bT)

## Bijlagen

{{< pagelist bijlagen >}}
