---
title: "actualiteitenvitrine"
draft: false
status:
unid: 459cb975-91db-4579-acd7-e241959c3b98
componenten:
tentoonstellingen:
- livescience
attachments:
- title: ID sheet (pdf)
  src: https://files.museum.naturalis.nl/s/GGwJH8BWZEWbNym
- title: livescience_exhibits_actualiteitenvitrine_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/pGzLogRbLHeoo5C
---

![schema](https://files.museum.naturalis.nl/s/RGsKB7H2r6Gk4KD/preview)

## Functionele omschrijving

Vitrine toont een actueel onderwerp waarmee Naturalis het (landelijk) nieuws heeft gehaald of waarmee Naturalisaansluit op een actueel thema.

- Een vitrine met een echt object rond een actueel onderwerp, aansluitend op het werkveldvan Naturalis. Iets om speciaal voor naar het museum te komen.
- Mogelijkheid tot het tonen van een filmpje (zonder geluid, evt. met ondertiteling).
- Eventueel met foto(’s).
- De inhoud varieert door de tijd heen, afhankelijk van de actualiteit

Collectie die hoort bij de betreffende actualiteit. Kan elke vorm van collectie zijn, dier (droog, nat),plant, steen, fossiel, maar ook een tekening of boek.

## Bijlagen

{{< pagelist bijlagen >}}
