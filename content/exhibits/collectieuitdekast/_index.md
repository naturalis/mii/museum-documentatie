---
title: "collectieuitdekast"
draft: false
status:
unid: 48500f64-61b2-497e-ac64-f732ce36cf7b
componenten:
tentoonstellingen:
- livescience
attachments:
- title: ID Sheet (pdf)
  src: https://files.museum.naturalis.nl/s/79SnKEwfMQRyjxT
- title: lifescience_exhibits_collectieuitdekast_stellingtekeningenwand_bouwtekening
  url: https://files.museum.naturalis.nl/s/qBcPycA2F8nCwYE
---

![schema](https://files.museum.naturalis.nl/s/tie5iz2cSS8ZAbk/preview)

## Functionele omschrijving

De collectie van Naturalis, en alle spullen die erbij komen kijken om eraan te werken.

- Een eye-catcher *pur sang* met allerlei objecten uit de collectie en voorwerpen (materialen, apparaten, enz.) die nodig zijn om ermee te werken.-De objecten en ‘props’ staan ruwweg ingedeeld op grond van de thema’s *verzamelen*, *bewaren*, *onderzoeken* en *ontsluiten*
- De vier manieren waarop met de collectie wordtgewerkt - zonder dit nadrukkelijk te benoemen: de thema’s dienen om de veelzijdigheid en het belang te laten ervaren van het werk van Naturalis.

Een veelheid aan objecten, materialen en voorwerpen (‘props’). Het gros hiervan
wordt niet voorzienvan aanvullende informatie, behalve de labels die de
objecten al hebben. Het is er gewoon om je over te verwonderen. Uitlichting van
een aantal ‘highlights’ met korte verhalen over de betekenis van deze objecten
en props voor Naturalis.

## Technische omschrijving

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Bijlagen

{{< pagelist bijlagen >}}
