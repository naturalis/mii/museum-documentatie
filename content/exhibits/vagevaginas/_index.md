---
title: "vagevaginas"
date: 2019-07-24T16:05:11Z
draft: true
status:
unid: 9e2769c8-cc7f-4765-af50-bb790c94c6c5
componenten:
tentoonstellingen:
- deverleiding
attachments:
- title: stellingkast vage vaginas 1
  src: https://files.museum.naturalis.nl/s/axJjc59593RNY9W
- title: stellingkast vage vaginas 2
  url: https://files.museum.naturalis.nl/s/jrMRDcjLa2YDSWm
---

![deverleiding_vagevaginas_bouwtekening](https://files.museum.naturalis.nl/s/aHz8zSADWioj8S2/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

* Decor: [Bruns](https://bruns.nl)

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
