---
title: "kristal"
draft: false
status:
unid:
componenten:
tentoonstellingen:
- deaarde
attachments:
- title: plattegrond (pdf)
  src: https://files.museum.naturalis.nl/s/X9TwMg5HFzjHf3W
- title: draaiplateau_bovenaanzicht (pdf)
  url: https://files.museum.naturalis.nl/s/F3yJLb8JXYxzBAF
- title: sturing_draaischijf (pdf)
  url: https://files.museum.naturalis.nl/s/owtnpG2j3gjSF4y
- title: sturing_showcontrol (pdf)
  url: https://files.museum.naturalis.nl/s/tbLRedNzyQAaHS4
- title: SEWMotoren_specificatieblad (pdf)
  url: https://files.museum.naturalis.nl/s/Zgizf4kq7WaooSZ
- title: relais (pdf)
  url: https://files.museum.naturalis.nl/s/g3SJNWpoPRHjJsi
- title: DMX_relais (pdf)
  url: https://files.museum.naturalis.nl/s/FbgQx492tRCQpyy 
- title: lichtplan_28-8-2023_as-built
  url: https://files.museum.naturalis.nl/s/M2WZaLLq3qZ7mFE          
---

[![definitiefontwerp](./aarde_exhibit_kristal_draaiplateau_bovenaanzicht.png)](https://files.museum.naturalis.nl/s/F3yJLb8JXYxzBAF)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->
* Glas: https://www.glascom.eu, contactpersoon, Martin Weerheim
* Casco: Brandwacht & Meijer, contactpersoon Dirk Koen op ten Noort
* Draaischijf: Pre Motion, contactpersoon Pieter Koning

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
