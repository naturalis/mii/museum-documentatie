---
title: "vissengang"
date: 2019-07-24T16:05:11Z
draft: true
status:
unid: 1f2b03e1-0c2c-4c90-8682-b42d93b34a26
componenten:
tentoonstellingen:
- leven
attachments:
- title: leven_exhibits_vissengang_podiumgroot_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/RpzJjTiP7tSmANa
- title: leven_exhibits_vissengang_podiumklein_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/qb6AbqY9eznKe5L
- title: leven_exhibits_vissengang_wand_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/P2candfpBq9k6Ao
---

![vissengang](./vissengang.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
