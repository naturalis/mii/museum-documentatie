---
title: "favorietenvitrine"
date: 2019-07-24T13:48:34Z
draft: true
status:
unid: 1f527bb7-85ab-43a9-8260-12bc0486ba07
componenten:
tentoonstellingen:
- livescience
attachments:
- title: ID Sheet (pdf)
  url: https://files.museum.naturalis.nl/s/HLc9EKBPALrRnLk
- title: livescience_exhibits_favorietenvitrine_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/LxFmNLiJ3GdYqYs
---

![vitrine](https://files.museum.naturalis.nl/s/tgbcJDBE3E7GGa8/preview)

## Functionele omschrijving

Vitrine toont favoriete objecten van de medewerkers van Naturalis en de verhalen daarachter.

 - Een vitrine waarin medewerkers van Naturalis hun favoriete object laten
   zien, met een kortemotivatie waarom dit hun favoriet is.
 - Bij elk object komt een foto van de medewerker, bij voorkeur een selfie
   waarin demedewerker het favoriete collectiestuk in de hand heeft of eraan
   werkt. De persoonlijkerelatie tussen medewerker en object spat er vanaf!
 - De inhoud varieert door de tijd heen, zodat verschillende medewerkers aan
   bod komen

## Technische omschrijving

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Bijlagen

{{< pagelist bijlagen >}}
