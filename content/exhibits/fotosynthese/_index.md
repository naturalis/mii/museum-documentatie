---
title: "fotosynthese"
draft: true
status:
componenten:
tentoonstellingen:
- evolutie
experiences:
attachments:
- title: evolutie_exhibit_fotosynthese_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/piZG8c9sLsgRnJa
- title: evolutie_exhibit_fotosynthese_bouwtekening.pdf
  url: https://files.museum.naturalis.nl/s/tKqx6noa86A4LGS
- title: evolutie_exhibit_fotosynthese_glas1_bouwtekening.pdf
  url: https://files.museum.naturalis.nl/s/kQbcnNR36adC8Dd
- title: evolutie_exhibit_fotosynthese_glas2_bouwtekening.pdf
  url: https://files.museum.naturalis.nl/s/M8WsiB2fzkwEtR8
- title: evolutie_exhibit_fotosynthese_glas3_bouwtekening.pdf
  url: https://files.museum.naturalis.nl/s/DHLAjcXdoGs2kZe
---

<!-- Voeg een overzichtsafbeelding toe -->

![definitiefontwerp](./fotosynthese_ontwerp.png "definitief ontwerp")

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
