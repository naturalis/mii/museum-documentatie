---
title: werkplaatsontwerp
status:
componenten:
- ctouch_lasersky75
tentoonstellingen:
experiences:
attachments:
- title:
  src:
- title:
  url:
---

<!-- Voeg een overzichtsafbeelding toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de faciliteit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze faciliteit bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
faciliteit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

* Inrichting/meubulair: [Hypsos](https://hypsos.com/nl/)
* Licht: [Tim Blom](mailto:blomartt@live.nl)

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
