---
title: servicebaliekassa1
draft: false
status:
componenten:
tentoonstellingen:
- atrium
attachments:
- title: Aansluitschema Servicebalie Kassa1
  url: https://files.museum.naturalis.nl/f/53381

---



## Functionele omschrijving

In het atrium staan er bij de servicebalie 4 kassa's waarmee tickets aan bezoekers worden verkocht. Dit is 1 van die kassa's.

Recreatex is het ticketsysteem van de leverancier Gantner. Recreatex wordt gehost bij de leverancier en heeft een beheeromgeving die middels Citrix met een token toegankelijk is voor de medewerkers van Naturalis. De kassa’s aan de servicebalie draaien in Recreatex en de klantenservice werkt in Recreatex om vragen van bezoekers over hun tickets te kunnen beantwoorden. De handscanners voor de toegangscontrole checken de online en in het museum verkochte tickets in op basis van de data in de database van Recreatex.

Het online bestelproces is een webshop die zijn data uit de database van Recreatex haalt (zie voor meer informatie: [Beheer tickets.naturalis.nl (Recreatex Webshop))](https://docs.google.com/document/d/1ybAIaucUb_hk5Gl1ho9V80Uz3oQVyuZAU1dgu8i0KdE/edit?pli=1#).

In het atrium van het museum staan ticketzuilen die door bezoekers van ons museum worden gebruikt om tickets op locatie te kopen (zie voor meer informatie: [ticketzuil1](https://docs.museum.naturalis.nl/latest/faciliteiten/ticketzuil1/)

## Technische omschrijving
Applicatielandschap Ticketing Recreatex - Focus Recreatex & Scanners Toegangscontrole:

[![Applicatielandschap Ticketing Recreatex - Focus Recreatex & Scanners Toegangscontrole](https://files.museum.naturalis.nl/s/EgxxyJnEeaqe36J/preview)](https://files.museum.naturalis.nl/s/EgxxyJnEeaqe36J)

Hardwarelandschap Ticketing Recreatex - Focus Recreatex & Scanners Toegangscontrole:

[![Hardwarelandschap Ticketing Recreatex - Focus Recreatex & Scanners Toegangscontrole](https://files.museum.naturalis.nl/s/Zbprm9HfK9MTkxH/preview)](https://files.museum.naturalis.nl/s/Zbprm9HfK9MTkxH)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

### Contact

+ Projectleider: Denise de Haan
+ Applicatiebeheerder: Sander Bal, Marijn Prins of Bart den Dulk

### Leverancier
Contactgegevens Gantner

+ SLA’s en contracten
  + [Overzicht van alle SLA’s en contracten en de dekking van de verschillende contracten](https://docs.google.com/spreadsheets/d/1-ReV_mvUK54Ek4-PLElXtGBV4N4i-Oo5XQoJeiK4-g8/edit?pli=1#gid=0)
  + Alle contracten staan in de module Contractbeheer in AFAS/N=Personal. Eigenaar van deze contracten is het afdelingshoofd AOB (Wilfred Gerritsen). Meldingen over het naderen van de opzeg- dan wel verlengtermijn zullen als taak bij deze persoon binnenkomen.
+ Helpdesk Gantner (CSC/Customer Support Center)
  + https://portal.gantner.be/
  + +31(0)10-8106020
  + Gebruikersaccount helpdesk Gantner
    + Voor Applicatiebeheer IM
      + E-mailadres: informatiemanagement@naturalis.nl
      + Wachtwoord: Bitwarden AB
    + Voor Technisch Team
      + E-mailadres: technischteam@naturalis.nl
      + Wachtwoord: Bitwarden TT
    + Voor projectleider
      + E-mailadres: denise.dehaan@naturalis.nl
      + Wachtwoord: ...
  + Bereikbaarheid support Gantner (zinnen uit de SLA):
    + De klant kan iedere dag (ook tijdens zon- en feestdagen) 24 uur per dag terecht voor het melden van storingen, waarvan een actieve ondersteuning is voorzien tussen 7u00 en 18u00 via telefonische assistentie van het CSC.
    + GANTNER garandeert een 24/24, 7/7, 365/365 service support waarbij tijdens de kantooruren elk incident onmiddellijk wordt aangenomen en buiten kantooruren dit via een stand-by mechanisme verloopt tussen 18u00 en 07u00.
  + Instructie supportomgeving
    + 1) Tabblad Tickets
    + 2) Zet create from date naar 1-11-2018
    + 3) Selecteer application. Wij hebben:
      + Betaalautomaat (=onze ticketzuil)
      + Joya scanner
      + RCX Admission app (=applicatie op de joya scanners)
      + RCX webshop (=onze online bestelomgeving)
      + RCX WSDL (=iets met de webshop)
      + Recreatex
      + SYX cloud (=hosting omgeving)
    + 4) Klik op Search (hij update niet vanzelf, ook al lijkt dat zo!)
  + Supportcontract is ingegaan per 1 juli 2019 en vastgezet voor 2 jaar, met de mogelijkheid tot verlenging van nog eens 3 jaar. Voor support vanaf 1 juli 2021 moet een nieuw contract ondertekend worden.
  + CSC als tussenleverancier naar CCV
    + Bij opleveren van het project zijn de pinapparaten bij de servicebalie niet door Gantner geleverd; het zijn hergebruik apparaten die we bij De Haan hebben aangeschaft. De Haan heeft het support op deze apparaten overgedragen aan de productleverancier CCV. Omdat de pinapparaten gekoppeld zijn aan de kassa’s van Gantner, zullen storingen van de pinapparaten allereerst met Gantner CSC opgenomen worden. In overleg zal per incident bepaald worden of Gantner dan wel wijzelf contact opnemen met CCV.
      + Contactgegevens CCV Service
        + +31(0)88 228 9849 (24/7u)
  + Escalatiekanaal helpdesk Gantner bij calamiteiten (of als het niet lukt de helpdesk te bereiken in het weekend na 2x-3x bellen)
    + Thomas van Rie, verantwoordelijke voor de operationele zaken en volledige helpdesk,
      + Thomas.VanRie@gantner.be
      + +32 479945039
    + Wim Vandelannoote, service manager
      + Wim.Vandelannoote@gantner.be
      + +32 479945034
+ Gantner Projects
  + Voor de inhuur van consultants op offertebasis
    + Marc Lisman, hoofd van projectbureau/consultants
      + projects@gantner.nl
      + +31(0) 33 432 84 16
        + Vaste consultant voor Naturalis is:
          + Dennis van Dam
          + Dennis.vanDam@gantner.nl
          + +31(0) 33 432 84 16
          + +31(0) 6 13 13 14 33
+ Account Manager
  + Voor escalaties en roadmapbesprekingen en dat soort “hoog-over”-dingen
    + Stefan Peeters, Sales/Account manager
    + Stefan.Peeters@gantner.nl
    + +31(0) 33 432 84 16
    + +31(0) 6 13 13 14 40

## Opgeleverde onderdelen

### Producten

Hieronder worden alle projecteindproducten opgesomd. De *cursief geschreven* producten zijn voor de volledigheid genoemd, maar de beheerafspraken over die producten staan niet in het onderhavige document, maar in:
Beheer [tickets.naturalis.nl (Recreatex Webshop)](https://docs.google.com/document/d/1wgmm5oZUjglT0okS_gu1nt7zoT6rDBx8dT9eP8S3KRI/edit?ts=5ddffaef&pli=1#) of [Beheer Ticketzuilen (Vending Machine)](https://docs.google.com/document/d/1qGiHEppLy1iNhH6zGyHifznTtm-uOc--tjizMSKE-a4/edit#).


Systemen/Software

1. Systeem (Recreatex) om...
    - toegangstickets te kopen:
        - *online bestelproces met koppeling aan Payment Service Provider Ingenico*
            1. *Frontend productieomgeving: tickets.naturalis.nl*
                a. *Webshop van Gantner (productie)*
                b. *CSS styling layer van Swis (productie)*
            1. *Frontend testomgeving: naturalisbiodiversitycentertest.recreatex.be*
                a. *Webshop van Gantner (test)*
                b. *CSS styling layer van Swis (test)*
            1. *Backend (CMS) productieomgeving: tickets.naturalis.nl/manager*
                a. *Gebruikersnaam: Admin*
                b. *Wachtwoord: [zie Bitwarden AB]*
            1. *Backend (CMS) testomgeving: naturalisbiodiversitycentertest.recreatex.be/manager*
                a. *Gebruikernaam: Admin*
                b. *Wachtwoord: [zie Bitwarden AB]*
        - in het museum aan de servicebalie
            1. Op het bureaublad van de kassa-computers staat een snelkoppeling naar de back-end van Recreatex: https://login.syxcloud.com
        - *in het museum in een self-service automaat*
            1. *Windowsapplicatie Vending machine versie 5.1.0.1*
                - *Koppelingen met Recreatex Database:*
                    1. *Shop-ID voor testomgeving: A76375C8-BAD4-4B86-A88C-C5F0C6C58DAF*
                    1. *Shop-ID voor productieomgeving:*
                        - *Kiosk 1: 565A682C-5FA5-4CC6-AA32-A97ECE229157*
                        - *Kiosk 2: 264A9530-0B6B-4618-AD9E-67F1E03CB6BA*
                        - *Kiosk 3: F7EE8B4A-96C9-4CD3-BDFC-742168A4207F*
                        - *Kiosk 4: 33A7AFEA-53E4-4FB9-8EDF-8BF3725CAECB*
                - *Configuratiebestand (met instellingen zoals kleuren, betaalbewijs printen ja/nee etc.): ReCreateX.VendingMachine.WPF.exe.config*
                - *Achtergrond, resolutie 1280x1040*
            1. *ATOS Valina betaalterminal*
                - *ATOS software package*
    - toegangstickets en MK en BGL VIP kaarten in te checken
    - ondersteuning te bieden aan mensen bij de online aanschaf van tickets (helpdesk interface)
        - De klantenservice gebruikt hiervoor ook de back-end van Recreatex: https://login.syxcloud.com

2. *Tokens om in Recreatex in te loggen*

|*#*|*Tokennummer*|*Tokennaam*|*Wachtwoord*|*Single sign on*|*Houder van de token*|
|------|:-------------|:---------------:|---------------:|---------------:|---------------:|
|*1*|*2786445648*|*Naturalis-FA1*|*[zie Bitwarden AB]*|*nee*|*Financiële administratie*|
|*2*|*2786445631*|*Naturalis-IM1*|*[zie Bitwarden AB]*|*nee*|*Applicatiebeheer*|
|*3*|*2786445624*|*Naturalis-IM2*|*[zie Bitwarden AB]*|*nee*|*Applicatiebeheer*|
|*4*|*2786447253*|*Naturalis-Kassa1*|*[zie Bitwarden AB]*|*ja*|*Kassa 1 Servicebalie*|
|*5*|*2786447246*|*Naturalis-Kassa2*|*[zie Bitwarden AB]*|*ja*|*Kassa 2 Servicebalie*|
|*6*|*2786447239*|*Naturalis-Kassa3*|*[zie Bitwarden AB]*|*ja*|*Kassa 3 Servicebalie*|
|*7*|*2786447222*|*Naturalis-Kassa4*|*[zie Bitwarden AB]*|*ja*|*Kassa 4 Servicebalie*|
|*8*|*2786447215*|*Naturalis-Rsrv1*|*[zie Bitwarden AB]*|*ja*|*Karen Droffelaar*|
|*9*|*2786447307*|*Naturalis-Rsrv2*|*[zie Bitwarden AB]*|*ja*|*Casper van Harten*|
|*10*|*2786447291*|*Naturalis-Rsrv3*|*[zie Bitwarden AB]*|*ja*|*Erica Roos*|
|11*|*2786447260*|*Naturalis-PZ1*|*[zie Bitwarden AB]*|*nee*|*Kirsten van Hulsen*|
|*12*|*2786447277*|*Naturalis-PZ2*|*[zie Bitwarden AB]*|*nee*|*Shani Blom en Natasja van der Tiel*|
|*13*|*2786447284*|*Naturalis-PZ3*|*[zie Bitwarden AB]*|*nee*|*Klantenservicecomputer achter de winkel (bij Eijsink server)*|

3. Payment Service Provider software Ingenico
    - Frontend productieomgeving: toegankelijk na doorlopen van bestelproces op [tickets.naturalis.nl ](http://tickets.naturalis.nl )
        1. PSPID: NaturalisProd
    - Frontend testomgeving: toegankelijk na doorlopen van bestelproces op [naturalisbiodiversitycentertest.recreatex.be](http://naturalisbiodiversitycentertest.recreatex.be)
        1. PSPID: NaturalisTest
    - Backend (CMS) productieomgeving: [secure.ogone.com](https://secure.ogone.com/Ncol/Prod/Backoffice/login/index?branding=OGONE&CSRFSP=%2fncol%2fprod%2fbackoffice%2fhome%2findex&CSRFKEY=E99D3C01DC48D8B7BD6D7D24004B53B7AF81C24F&CSRFTS=20191211161842)
        1. Gebruikersnaam/PSPID: NaturalisProd
        1. Wachtwoord: [bekend bij FA]
    - Backend (CMS) testomgeving: [secure.ogone.com](https://secure.ogone.com/Ncol/Prod/Backoffice/login/index?branding=OGONE&CSRFSP=%2fncol%2fprod%2fbackoffice%2fhome%2findex&CSRFKEY=E99D3C01DC48D8B7BD6D7D24004B53B7AF81C24F&CSRFTS=20191211161842)
        1. Gebruikersnaam/PSPID: NaturalisTest
        1. Wachtwoord: [bekend bij FA]

*Hardware*

- *Kassa-computer’s met toebehoren bij de servicebalie*
    1. *[aansluitschema’s](https://drive.google.com/open?id=1MErY05_Rtbb18VhkgYZlTv4D0iV2wsvT)*
    1. *Geïmporteerd in Topdesk Asset Management: [Asset Management Ticketing Hardware](https://drive.google.com/open?id=1EbtkswQEpzycJa9z19J2-YAeKI6khw8bVH8eCuFdIeo)*
- *Pinapparaten bij de servicebalie*
- *Self-service automaten in het atrium, met ingebouwd pinapparaat*
    1. *Ticketzuil buitenkant*
    1. *Computer*
    1. *Beeldscherm*
    1. *Boca ticketprinter*
    1. *Bonprinter*
    1. *Scanner (uitgeschakeld)*
    1. *Betaalautomaat*
- *Handscanners voor toegangscontrole (zowel voor tickets als voor MK en BGL VIP)*
    1. *De app ‘Admission’ is geïnstalleerd op deze handscanners. De laatste versie van deze app kan in de Gantner Portal gedownload worden in de [tab ‘App Store’](https://portal.gantner.be/web/gantner-customer-portal/app-store)*
- *Kantoorcomputers uitgerust met Citrix Receiver:*
    1. *Klantenservice&Reserveringen*
    1. *Informatie Management*
    2. *Coördinatoren Publieksbegeleiding*
    1. *Computer t.b.v. Klantenservice in het weekend (PB-ruimte waar ook de server van de winkelkassa staat)*
- *Bedraad/Wifi-netwerk in museum voor Kernsystemen*
    1. *SSID: Kernsystemen*

### Ontwerper

## Richtlijnen/handleidingen

**_Voor de gebruikers_**

Servicebalie



1. [Handleiding Ticketing: Servicebalie/kassa](https://drive.google.com/open?id=1EtgjZkuZtP2Fvskzi7IR5KCeGVPK3vLQKCnHlp3pd6A)
2. [Cheat sheet Ticketing: Kassascherm](https://drive.google.com/open?id=0BztX4NRI4fE1a3g5eWJqbk8yd3pyMjR0YzRJY0g0dkNYSzdv)
3. [Cheat sheet Ticketing: Ticketzuil](https://drive.google.com/open?id=1C3ktpyXR8pABPMDXtgQdN-9Ele1NASxk)
4. [Manueel kaarten de-activeren](https://drive.google.com/open?id=0BztX4NRI4fE1NmhRcGlZVVJiUkJSaXRGZTJOX0hPbzZxQlpr)

Coordinatoren Publieksbegeleiding



5. [Handleiding Papier vervangen printers](https://docs.google.com/document/d/1j8BQoKNP7n_2NQOnvQjm-_-QvawCeXhalbgJYJW0vUA/edit#)
6. Handleiding aanmaken nieuwe kassiers

Toegangscontrole



7. [Handleiding Ticketing: Toegangscontrole](https://drive.google.com/open?id=1X4DArrLFBvX9vUHH3KLfq9NxuFA_WAp1DQBJLOuvZIM)
8. [BankGiro Loterij VIP-KAART Kassa infosheet 2019](https://drive.google.com/open?id=0BztX4NRI4fE1R05TcUJFemFYd2pWZkdzdVJTYWJLMFBXZVhF)

Klantenservice



9. [Handleiding Ticketing: Online bestelproces & klantenservice](https://drive.google.com/open?id=17ALdCMjpatApBvtvXl3ps1KUkv8ZjIurayCw0bamaAM)

Communicatie



10. [Template Wensen nieuwe kortingsactie](https://docs.google.com/document/d/1xNzMXyvVKT1G6B57T0qzcvfSP2V5sD0fxlg-QdjQs20/copy)
11. [Technische Actiemogelijkheden in Recreatex](https://docs.google.com/document/d/1uiJcgz-tp2nw1T_gzRihE4LmnWEIt71GiAdmnXEVO14/edit)

**_Voor de beheerders_**

Software



12. [UserManual-SyxCloud](https://drive.google.com/open?id=0BztX4NRI4fE1cmIwVThZOE43NzhKTXlrRE5kUGtlSTlSUDJF)
13. Na inloggen in de Recreatex Backend is er onder ‘help’ een uitgebreide beschrijving van de verschillende functionaliteiten.
14. [Handleiding/Tips Recreatex Backend](https://docs.google.com/document/d/1s9wvEzdjsUfQ8P_CGPscDWiQ7oVJAygCxrtQaaboZCU/edit#)
15. [Handleiding Relatiekaarten](https://docs.google.com/document/d/1K7IBMswH2u5USXys5i4YTk2ZWLtTQQm0CdYefyyJq0o/edit#heading=h.p9j33jt4l1i6)
16. [Overzicht Relatiekaarten](https://docs.google.com/spreadsheets/d/1rzlJjXXbPvw9HLpjJRR_KL83MixhhOlEjZRyzRujeCA/edit?pli=1#gid=98071423)
17. [ReCreateX - Kortingscodes](https://drive.google.com/open?id=0BztX4NRI4fE1RTJERlJGaFJkNHlmcDk5VXBFTkR4ZEVmZEZN)
18. [Handleiding Mailings](https://docs.google.com/document/d/168GXOuc8YM8WoEX2rOvo33XVLyWeXEbo7E2ctGLgiuQ/edit?pli=1)
19. [Dashboard and Pivots](https://drive.google.com/open?id=1pdu8y1vMg4K7oEEYuShWVUHpGQRzSqJV)
20. [Handige links voor het werken met de dashboards](https://docs.google.com/document/d/1QAn11PH2CUuh6Im6jbZDGmU50fMwqazcZzyCf7DxRs0/edit)
21. In de Gantner Portal staan ook veel handleidingen onder de [tab ‘Manuals’](https://portal.gantner.be/web/gantner-customer-portal/manuals?p_p_id=110_INSTANCE_NxQzldXWyiRE&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1), bijv. over mailings of promotieregels

Hardware



22. [Handleiding Joya Touch 3-Slot Cradle](https://drive.google.com/open?id=1hVaLsGu7yE-kx_LRKkJFFNOqKighCsa4)
23. [Handleiding Joya scanner update](https://drive.google.com/open?id=1gKk_5iWjU4cK2elgGGfAE-Z-DdIletJXZTnqVpLezhw)

Technische informatie



24. Technische informatie over de API: [RCX Web Services v6.4.0.0](https://drive.google.com/open?id=0BztX4NRI4fE1Sk15dEhBcWhJOVc1b3lTTE5iazRKR0pjSk5B)
25. [Gantner Roadmap 2019/2020](https://drive.google.com/open?id=0BztX4NRI4fE1M3pkSnFlR0xTWjJXa0w4cGlHRmx3ZjBsd184)
26. Beheerpagina’s op [docs.museum.naturalis.nl](https://docs.museum.naturalis.nl/latest/):
        - Tentoonstelling: atrium
        - [Faciliteit: servicebaliekassa1](https://docs.museum.naturalis.nl/latest/faciliteiten/servicebaliekassa1/)
        - [Faciliteit: servicebaliekassa2](https://docs.museum.naturalis.nl/latest/faciliteiten/servicebaliekassa2/)
        - [Faciliteit: servicebaliekassa3](https://docs.museum.naturalis.nl/latest/faciliteiten/servicebaliekassa3/)
        - [Faciliteit: servicebaliekassa4](https://docs.museum.naturalis.nl/latest/faciliteiten/servicebaliekassa4/)
        - [Faciliteit: toegangscontroleatrium](https://docs.museum.naturalis.nl/latest/faciliteiten/toegangscontroleatrium/)

## Documentatie over de overeenkomst met Gantner

1. [Overzicht van alle SLA’s en contracten en de dekking van de verschillende contracten](https://docs.google.com/spreadsheets/d/1-ReV_mvUK54Ek4-PLElXtGBV4N4i-Oo5XQoJeiK4-g8/edit?pli=1#gid=0)
met speciale aandacht voor:
        - [Service Level Agreement (SLA) Recreatex](https://drive.google.com/open?id=1-YT20eLiRPW-ZCr1X4AHc59sC4GStAbX)
        - [Service Level Agreement (SLA) Datacenter Connectivity](https://drive.google.com/open?id=1VhoojtKiYwtjkfwaWZMFMptVyX_Lzqg_)
        - [Verwerkingscontract](https://drive.google.com/open?id=1qJQssBpNJK9ZJLKlHwksC0zwbhyJtC-F)
1. [Gantner Overeenkomst Toegangsticketsysteem 30OKT2019 getekend](https://drive.google.com/open?id=0BztX4NRI4fE1cE5YekNBSVFxSncyUW1pdlRRTW9Fb2NjZTJN)
1. [Map met documenten bij de aanbestedingsinschrijving door Gantner](https://drive.google.com/open?id=1_EjNCOxDVhGSAEMCq7QUezLn4UCUSQe5)
1. [Agenda maandelijkse Gantner consultancy dagen](https://docs.google.com/spreadsheets/d/12ykq5KBQVacjacLalF7dMPBUVhaT20oHN6ei72a9UF0/edit#gid=0)


# Verantwoordelijkheden


## ICT Technisch Team



1. Eerste aanspreekpunt voor de organisatie; als gebruikers een **melding voor het museum** aanmaken, komt deze binnen bij het Technisch Team. Het TT bepaalt of de melding door henzelf zelf (eerstelijns) of door Applicatiebeheer (tweedelijns) opgepakt moet worden.
1. Zaken die het Technisch Team oppakt:
    - Recreatex & Servicebaliekassa’s
        1. Hardware in functionerende staat houden en wanneer vervanging nodig is, melding voor vervangen doorgeven aan applicatiebeheer
        2. Netwerkverbinding
        3. Ondersteuning m.b.t. tokens
        4. Ondersteuning bij papier vervangen in de printers
    - tickets.naturalis.nl
    - Handscanners
       -  Hardware in functionerende staat houden en wanneer vervanging nodig is, melding voor vervangen doorgeven aan applicatiebeheer
        - Wifiverbinding
    - Ticketzuilen
3. Alle overige zaken zijn onder verantwoordelijkheid van Applicatiebeheer
4. Indien er storingen **in het weekend** zijn: telefonisch contact met leverancier Gantner. Deze storingen moeten na het weekend doorgegeven worden aan Applicatiebeheer, zodat zij een totaalbeeld van de werking van Recreatex kunnen opbouwen.
5. Indien er storingen doordeweek zijn, die niet vallen onder de zaken van punt 2: storing melden bij Applicatiebeheer.


## ICT Support



1. Als gebruikers een melding voor support aanmaken (is niet de bedoeling, maar kan gebeuren), komt deze binnen bij Support. Support zet de melding door naar het Technisch Team (eerstelijns) of naar Applicatiebeheer (tweedelijns).


## Applicatiebeheer



1. Tweedelijns beheer.
2. Zaken die Applicatiebeheer oppakt:
    - Recreatex & Servicebaliekassa’s
        - Autorisatie- en gebruikersbeheer, incl. tokenbeheer
        -  Vragen vanuit de organisatie, bugs en wijzigingen in de software (updates en change requests) vanuit de organisatie oppakken.
        -  Elk half jaar nieuwe periodes (=dagen waarop tickets geboekt kunnen worden) in de expositie “Bezoek Naturalis” aanmaken.
            1. Werk een jaar vooruit, zodat er altijd minimaal 12 en maximaal 18 maanden aan periodes beschikbaar zijn.
            2. Stem met de gebruikers af of er in het half jaar dat aangemaakt gaat worden, nog dagen uitgesloten moeten worden wegens evenementen. Standaard worden de volgende dagen uitgesloten: 27 april, 3 oktober, 25 december. **Let op!** Dagen uiteindelijk nog openstellen is makkelijker dan dagen weer dichtzetten, zeker als er al tickets voor die dagen geboekt zijn.
        - tickets.naturalis.nl
        - Handscanners
            1. Vragen vanuit de organisatie, bugs en wijzigingen in de software (updates en change requests) vanuit de organisatie oppakken.
        - Ticketzuilen
3. Indien er storingen doordeweek zijn, of voor ondersteuning bij vragen, bugs en wijzigingen: contact met Gantner via de CSC of telefonisch.
4. Indien er nieuwe of vervangende tokens of hardware nodig zijn: bestelling plaatsen bij Gantner via de CSC. Contractbeheer van de nieuwe tokens wordt door het Afdelingshoofd Applicatie Ontwikkeling & Beheer gedaan, dus nauw contact hierin is belangrijk. Wanneer de noodzaak voor nieuwe hardware wordt geconstateerd door het Technisch Team, is nauw contact met hen gedurend het bestelproces ook belangrijk.
5. Indien er consultancy ingehuurd moet worden voor wijzigingen: offerte opvragen bij Gantner Projects.


## Afdelingshoofd Applicatie Ontwikkeling & Beheer
1. Contractbeheer voor hardware (afstemming met Applicatiebeheer).
2. Contractbeheer voor software (afstemming met Applicatiebeheer).


## Key-users (Coördinatoren Publieksbegeleiding)

1. Handleidingen voor gebruikers maken en up-to-date houden.
2. Aanspreekpunt voor Technisch Team en Applicatiebeheerders voor inhoudelijke vragen.
3. Papier vervangen in de printers.
4. Aanmaken van nieuwe kassiers in Recreatex.
5. Training aan nieuwe gebruikers van Recreatex.
6. In geval van storingen, bugs of vragen: een **melding voor het museum** maken in de Selfservicedesk.


## Gebruikers

_Publieksbegeleiding_

1. Gebruik van Recreatex op de kassa’s bij de servicebalie.
2. Gebruik van de handscanners bij de toegangscontrole.
3. In geval van storingen: een **melding voor het museum** maken in de Selfservicedesk.
4. In geval van bugs of vragen: geeft dit door aan een Coördinator Publieksbegeleiding.

_Klantenservice_

1. Gebruik van Recreatex bij de klantenservice.
2. In geval van storingen, bugs of vragen: een **melding voor het museum** maken in de Selfservicedesk.

_Financiële Administratie_

1. Gebruik van Recreatex voor financiële doeleinden.
2. Houder van de bankcontracten (CCV, Rabobank)
3. In geval van storingen, bugs of vragen: een **melding voor het museum** maken in de Selfservicedesk.

_Communicatie_

1. Gebruik van Recreatex t.b.v. rapportages en data-analyse.
2. In geval van storingen, bugs of vragen: een **melding voor het museum** maken in de Selfservicedesk.
3. In geval van nieuwe kortingsacties
    - De actie minimaal een maand voor de deadline (d.i.: het moment waarop de actie technisch ingeregeld moet zijn) bespreken met Applicatiebeheer: een **melding voor het museum** maken in de Selfservicedesk. Gezamenlijk met applicatiebeheer de actie in het systeem aanmaken.
    - De actie moet passen binnen de technische actiemogelijkheden.

_Business Development_

1. Wanneer een uitdraai van een rapportage over de donaties gewenst is, hiervoor een verzoek doen bij Applicatiebeheer: een **melding voor het museum** maken in de Selfservicedesk.


# Wijzigingsproces


## Doorontwikkelingen vanuit Gantner

*   2 of 3 keer per jaar brengt Gantner een nieuwe release van Recreatex uit.
    - In releases zijn grote aanpassingen doorgevoerd. Het is altijd belangrijk om goed de samenhang tussen back-end, ticketzuilen en de webshop goed te bespreken.
    - Releases gelden voor alle gebruikers van Gantner, ze zijn niet op maat gemaakt.
    - Releases zijn bijv. van versie 7.1 naar versie 7.2.
    - De uitrol van releases zijn door onszelf in te plannen, ze worden niet automatisch op een bepaalde dag uitgerold.
    - Zie de roadmap van [2019/2020](https://drive.google.com/open?id=0BztX4NRI4fE1M3pkSnFlR0xTWjJXa0w4cGlHRmx3ZjBsd184), of bekijk de meest recente versie in de [Gantner Portal](https://portal.gantner.be/web/gantner-customer-portal/documents-presentations).
*   Meerdere keren per jaar brengt Gantner patches uit op de versies.
    - In patches zijn kleinere aanpassingen doorgevoerd (zoals bugoplossingen).
    - Releases gelden voor alle gebruikers van Gantner, ze zijn niet op maat gemaakt.
    - Patches zijn bijv. van versie 7.1.5 naar 7.1.6.
    - De uitrol van patches worden automatisch op een bepaalde dag uitgerold.

## Indienen van wijzigingen binnen Naturalis

*   Alle coördinatoren, afdelingshoofden of sectordirecteuren mogen verzoeken tot wijziging indienen: een **melding voor het museum** maken in de Selfservicedesk. Applicatiebeheer zal de verzoeken in behandeling nemen.
*   Alle wijzigingen worden besproken op een wijzigingsoverleg dat zich speciaal richt op wijzigingen m.b.t. ticketing: het maandelijkse Recreatex Wijzigingsoverleg. Elke wijziging moet voor uitvoering akkoord bevonden worden door alle partijen uit dat overleg, te weten:
    - Applicatiebeheer
    - Coördinatoren Publieksbegeleiding
    - Klantenservice
    - Communicatie
    - Financiële Administratie
* Als er kosten mee gemoeid zijn, dan moet de systeemeigenaar (Koen Berkenbosch) hier akkoord op geven. Indien de wijziging niet betaald wordt vanuit het budget van de systeemeigenaar, zal hij akkoord verkrijgen van degene die de wijziging zal betalen.


## Doorvoeren van wijzigingen

*   Wijzigingen worden altijd eerst op de testomgeving doorgevoerd, en na een goedgekeurde test ook op de productieomgeving doorgevoerd.
*   Wanneer een wijziging alleen een aanpassing op de inrichting omvat, is er geen systeemupdate nodig. Applicatiebeheer zal deze wijziging inplannen en uitvoeren in overleg met de aanvrager.
*   Wanneer een wijziging een release of patch vanuit Gantner omvat, zal Applicatiebeheer dit in nauwe afstemming met Gantner inplannen en uitvoeren.
*   De aanvrager van de wijziging past, wanneer nodig, de handleiding(en) aan.


# Dienstverlening

_In het **weekend** wordt alle dienstverlening door het Technisch Team geleverd._
_**Doordeweekwordt** de dienstverlening door het Technisch Team en Applicatiebeheer geleverd. (Zie voor taakverdeling hoofdstuk 4.)_
{: .gitlab-purple}



*   Kassa’s bij de servicebalie: dienstverlening gedurende openingstijden van het museum, dus dagelijks (ook in het weekend) van 10-17u.
*   Handscanners bij de toegangscontrole: dienstverlening gedurende openingstijden van het museum, dus dagelijks (ook in het weekend) van 10-17u.
*   Klantenservice: dienstverlening gedurende openingstijden van het museum, dus dagelijks (ook in het weekend) van 10-17u.
    - Door de week wordt de klantenservice bemand door de afdeling Klantenservice&Reserveringen
    - In het weekend wordt de klantenservice bemand door publieksbegeleiders

# Overgedragen issues



1. Mailings inrichten, incl. rapportages automatisch laten mailen
    - Reden: dit is een complexe inrichting. De looptijd duurt te lang om afsluiting van het project daarvoor uit te stellen.
    - Overgedragen aan: vervolgproject Ticketing (nov 2019 - feb 2020). Topdeskmelding M19111317 zal ook in dit vervolgproject opgepakt worden.
2. Update Recreatex naar versie 7.2, incl. uitrol nieuwe ticketzuilapplicatie
    - Reden: dit is een complexe inrichting. De looptijd duurt te lang om afsluiting van het project daarvoor uit te stellen.
    - Overgedragen aan: vervolgproject Ticketing (nov 2019 - feb 2020).
3. Enviso inrichten.
     - Reden: zolang de afdeling Communicatie hier niet direct gebruik van gaat maken, is inrichting niet nodig.
    - Overgedragen aan: vervolgproject Ticketing (nov 2019 - feb 2020).
4. Pinapparaten vervangen bij de Servicebalie.
    - Reden: de huidige apparaten zijn hergebruikapparaten die ooit bij leverancier De Haan zijn aangeschaft. De Haan heeft het support contract ontbonden, waardoor we nu support van CCV zelf krijgen. Wanneer mogelijk willen we echter alle hardware bij Gantner afnemen, zodat er nooit door leverancier naar elkaar gewezen kan worden als er iets mis is.
    - Overgedragen aan: applicatiebeheer. Topdeskmelding M19111518
5. Tickets die uitstaan bij Gantner:
    - Geplande release: RCX 7.3.0.0
        1. 307083 - Webshop - Email bevestiging bevat een Copyright onderaan de mail, dit is niet gewenst en we willen dit uitschakelen
        2. 308141 - Optimalisatie gebruikt framework - Onthouden veelgebruikte vinkjes
    - Geplande release: RCX 7.4.0.0
        3. 315748 - Redo mailing improvement to use HTML
    - Geplande release onbekend:
        4. 295574 - `Webshop - Verplaatsen bezoek - Overzicht onduidelijk omdat er geen details staan.
        5. 296874 - Kortingscodes via promotionrules hebben geen controle op aantal keer dat een code is gebruikt
        6. 301050 - Webshop - Historiek, keuze lijst terwijl er maar één keuze is
        7. 301053 - Webshop - Exposities - Verplaatsen bezoek vanuit historiek vereenvoudigen
        8. 302033 - Webshop - Default taal van de webshop aanpassen aan de browser setting i.p.v. default Engels
        9. 304674 - Webshop - taalobjecten worden niet allemaal vernieuwd na wisseling van taal
        10. 304697 - Expositie tickets - niet gemakkelijk te herleiden tot welk bezoek
        11. 308140 - Exposities- Verplaatsen bezoek - kalender start op 00:00 uur, daardoor tijden niet zichtbaar
        12. 308414 - Afdrukken tickets - Ingeven verkoopnummer zo maken dat de overige selecties niet belangrijk zijn
        13. 310381 - Kassa - Modern lay-out - Blokkeren negatieve verkoop per medewerker werkt niet correct
        14. 310382 - Kassa - Modern lay-out - Aantallen na de komma blokkeren werkt niet juist
        15. 310383 - Kassa - Gekoppelde pinautomaten accepteren geen ? 0,- maar we sturen dit wel
        16. 310384 - ReCreateX Basis - Uitschakeling menubalk schakeld ook de systeembalk uit
        17. 310390 - Mailing - MS word documenten niet te gebruiken in hosting of door servers

## Bijlagen

{{< pagelist bijlagen >}}
