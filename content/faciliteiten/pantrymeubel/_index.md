---
title: pantrymeubel
date:
draft: false
componenten:
tentoonstellingen:
- livescience
attachments:
- title: livescience_faciliteiten_ pantrymeubel_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/F53tbZnNSjXsiC8

---

![pantrymeubel](https://files.museum.naturalis.nl/s/7FQ9iy86PXgLAAZ/preview)


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

### Bouwer

* [Bruns](https://bruns.nl)

## Bijlagen

{{< pagelist bijlagen >}}
