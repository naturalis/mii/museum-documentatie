---
title: "lanoptik-mc500wg1"
date: 2019-12-03T14:50:06+01:00
draft: false
merk: lantoptik
model: mc500wg1
leverancier:
attachments:
- title: lantoptik-mc500wg1_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/g8DoPqdH4dcrZXa
- title: lanoptik-mc500wg1_gegevensblad_details.pdf
  src: https://files.museum.naturalis.nl/s/P286occM3tcR2wz
---

![mc500wg1](./mc500wg1.png "mc500wg1")
5G WiFi Digital Microscope Camera

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte: 162 mm
  * Diameter: 63 mm

* Gewicht: 260 g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
