---
title: "eutrac-3frail"
date:
draft: false
merk: Eutrac
model: 3F rail
leverancier: CLS en Q-Cat
attachments:
- title: Eutrac 3F Rail Data (pdf)
  src: https://files.museum.naturalis.nl/s/n9GGfHm296wtzds

---

![3frail](./3frail.png)

## Eigenschappen


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
