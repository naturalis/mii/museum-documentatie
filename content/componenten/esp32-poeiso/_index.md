---
title: "esp32-poe-iso"
date: 2019-06-13T10:00:04+02:00
draft: false
weight: 32
attachments:
- title: ESP32-PoE_Rev_D.pdf
  src: https://files.museum.naturalis.nl/s/yffBMJwFB6oF57W
- title: ESP32-POE-connector.jpg
  src: https://files.museum.naturalis.nl/s/7moMTYpNMrws6Wi

---

![eps21-poeiso](./eps21-poeiso.png "eps21-poeiso")

Om de show controllers de mogelijkheid te geven om lichten en andere
minder 'slimme' aparaten te bedienen, gaan we gebruik maken van
ESP32 modules van het merk OLIMEX. Om precies te zijn ESP32-PoE-ISO.

Dit zijn relatief goedkope bordjes waarop diverse technieken gebruikt
kunnen worden waaronder:

 - ESP32-WROOM-32 WiFi/BLE module
 - Ethernet 100Mb interface with IEEE 802.3 PoE support
 - MicroSD card
 - LiPo battery charger
 - LiPo battery connector
 - UEXT connector
 - Micro USB with programmer for ESP32 programming
 - diverse digitale en analoge poorten
 - user button

Meer details over deze bordjes is te vinden bij de leverancier
Olimex [ESP32-POE](https://www.olimex.com/Products/IoT/ESP32/ESP32-POE/open-source-hardware).

## Arduino IDE

Het voordeel van deze bordjes is dat ze zijn te programmeren via de
standaard Arduino IDE. Om dit voor elkaat te krijgen moet je de
stappen volgen die beschreven staan op [dit github project](https://github.com/espressif/arduino-esp32/blob/master/docs/arduino-ide/debian_ubuntu.md).

Belangrijke dingen om rekening mee te houden is dat het bordje
herkent moet worden als je `lsusb` uitvoert als je het bordje
hebt aangesloten via microusb kabel. Het gaat dan om:

```
Bus 001 Device 005: ID 1a86:7523 QinHeng Electronics HL-340 USB-Serial adapter
```

Installeer daarna de officiele Arduino IDE via de site (let op! niet via
apt of snap). En volg de stappen voor de [installatie van de esp32](https://github.com/espressif/arduino-esp32/blob/master/docs/arduino-ide/debian_ubuntu.md).

Tot slot dien je de rechten te fixen voor het usb device waarmee je
communiceert. Dit staat goed beschreven in de [Arduino IDE uitleg](https://playground.arduino.cc/Linux/All/#Permission). Vergeet
niet opnieuw in en uit te loggen voordat je verder gaat.

## Hello world

Een goede manier om de basic functionaliteit van het bordje te testen
is door een van de voorbeelden in de Arduino IDE te gebruiken. Deze
zijn te vinden onder File > Examples > Examples for Olimex ESP32-PoE.
Een goede is de Simple Wifi Server.

## Bijlagen

{{< pagelist bijlagen >}}
