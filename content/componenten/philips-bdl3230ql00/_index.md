---
title: "philips-bdl3230ql00"
date: 2019-12-03T14:50:08+01:00
draft: false
merk: Philips
model: bdl3230ql00
interactives:
- sexystories
- troodonpeppersghost
- vroegemensportaal
leverancier:
attachments:
- title: philips-bdl3230ql00_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/PMrGS8SDHmB89Ne
---

![bdl3230ql00](./bdl3230ql00.png)
Q-line scherm 32 inch Direct LED-achtergrondverlichting, Full HD

## Eigenschappen

* Video-in:
  * DVI-D
  * HDMI
  * Component (RCA)
  * Composite (RCA)
  * VGA (analoog D-Sub)
* Bediening:
  * RS232
  * RJ-45
  * IR
* Schermdiagonaal: 31,5 inch /  80 cm
* Resolutie: 1920 x 1080p
* Beeldverhouding: 16:9
* Vermogen: 32 Watt
* Kijkhoek: (h /  v): 178 /  178 graad
* Afmetingen: 726,5x 425,4 x  63,6 mm
* Gewicht: 5,8 kg


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
