---
title: "molitor-via"
merk: molitor
model: VIA
leverancier: Kloosterboer
interactives:
- slangen
attachments:
- title: 190416_VIA-induktiv_flyer_ENG_Screen.pdf
  src: https://files.museum.naturalis.nl/s/XkGTHjPNLdtDC2S
- title: Molitor-VIA.pdf
  src: https://files.museum.naturalis.nl/s/RZFSKcD5eTzoHeH
---

![molitor-via](./via.jpg "molitor-via")
Molitor VIA luisterhoorn met schakelaar (Reed contact)

## Eigenschappen

* Max toegestaan vermogen: 0.1 Watt
* Impedantie: 32 Ohm
* Actief of passief: Passief
* Afmetingen: diameter 60 mm, hoogte 56 + 24 mm
* Gewicht: luisterhoorn met kabel (zonder magneetplaat) 170 gr
* Sensor: max 175V AC/DC/500mA.

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
