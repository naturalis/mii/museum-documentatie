---
title: "ctouch-lasersky75"
draft: false
merk: CTOUCH
model: Laser Sky
leverancier: Newcandle
faciliteiten:
- werkplaatsnu
- werkplaatstoen
- werkplaatsontwerp
attachments:
- title: ctouch_lasersky_manual.pdf
  src: https://files.museum.naturalis.nl/s/KtTRgDZ4RYsdb8A
- title: ctouch_lasersky_75_specs.pdf
  src: https://files.museum.naturalis.nl/s/93RriESNoNACbXT
---

![ctouchlasersky](./ctouchlasersky.png "ctouchlasersky")
75 inch touch monitor met Android en optionele OPS module.


## Eigenschappen

* Video-in:
  * 1 x Displayport(v1.2)
  * 3 x HDMI 2.0 (1x ARC)
  * 1x DVI(v1.0)
  * 1 x VGA
* Audio-in:
  * 1 x Mini Jack
* Audio-out:
  * 1 x S/PDIF Optical
  * 1 x Mini Jack
* Bediening:
  * RS232C: 1x DB-9
  * LAN port: 1x RJ45 10/100/1000 BaseT
  * IR 1x
* Schermdiagonaal: 75", 1893 mm
* Resolutie: 3840 x 2160 @ 60Hz
* Beeldverhouding: 16:9
* Vermogen: 115 W
* Kijkhoek: horizontal/verticaal: 178°/178°
* Afmetingen: 1745 x 1036 x 113 (mm)
* Gewicht: 67 kg

## Technische specificaties

Zie bijlage

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Decors

Dit component wordt gebruik in de decors:

{{< pagelist decors >}}

## Known issues

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
