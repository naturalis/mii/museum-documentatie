---
title: "esi-gigaportex"
draft: false
merk: ESI
model: Gigaport EX
leverancier:
interactives:
- darwinvinken
attachments:
- title: GIGAPORT_eX-English.pdf
  src: https://files.museum.naturalis.nl/s/DXKDaJxBmDSzCGr
---

![esi-gigaportex](./gigaportex.JPG "esi-gigaportex")
8-Kanaals USB 3.1 audio-interface

## Eigenschappen

* USB 2.0 compatibel
* 8 Analoge uitgangen (Cinch -10dBV)
* 24-Bit /192 kHz digitaal-naar-analoog converter met een dynamisch bereik van 114dB
* 2 Aparte koptelefoonuitgangen
* Afmetingen: 120 x 70 x 20 mm
* Gewicht: 0,184 kg

## Technische specificaties




## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
