---
title: "jbl-control1"
date:
draft: false
merk: JBL
model: Control 1
leverancier:
interactives:
- onschatbaar
attachments:
- title: jbl_ctr1pro (pdf)
  src: https://files.museum.naturalis.nl/s/YeMRZJgC6s5QxQC
- title: jbl-control-one-collectie (pdf)
  src: https://files.museum.naturalis.nl/s/pyxRCmgByxQpGjP
---

![control1](./control1.jpeg)
Two-Way Professional Compact Loudspeaker System

## Eigenschappen

* Afmetingen: 235 mm x 159 mm x 143 mm
* Gewicht: 1,8 KG
* Power Capacity: 150 W

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
