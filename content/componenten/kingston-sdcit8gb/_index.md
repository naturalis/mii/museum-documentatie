---
title: kingston-sdcit8gb
date: 2019-12-10T16:09:16+01:00
draft: false
merk: kingston
model: kingston
leverancier:
attachments:
- title: kingston-sdcit8gb_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/Bi3t5KfJcd2Gkra
---

![sdcit8gb](./sdcit8gb.png "sdcit8gb")
Industrial Temperature microSD UHS-I

## Eigenschappen

* Capacities:  8GB, 16GB, 32GB
* Performance UHS-I Speed Class 1 (U1):
  * 8GB: 90MB/s read and 20MB/s write
  * 16GB-32GB: 90MB/s read and 45MB/s write
* microSD Dimensions: 11mm x 15mm x 1mm
* SD Adapter Dimensions: 24mm x 32mm x 2.1mm
* Format: FAT32 (microSDHC 8GB-32GB)
* Operating & Storage Temperature: -40°C to 85°C
* Voltage: 3.3V
* Thermal Cycle Testing: interval testing completed at various extreme temperatures
* Vigorous Temperature Humidity Bias: several hundred hours of testing to ensure durability at varying levels of humidity
* Wide Temp Chamber Testing: completed on all SDCIT cards prior to production
* Warranty:  5 years

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
