---
title: "kissbox-io3cc"
date:
draft: false
merk: Kissbox
model: IO3CC
leverancier:
shows:
- showrexperience
attachments:
- title: rexperience_hardware_handleiding_kissbox (pdf)
  src: https://files.museum.naturalis.nl/s/yiFLLH5kFcLCdrm
---

![IO3CC](./IO3CC.jpg)

The KISS-BOX I/O3 CardCage can hold up to 3 standard Kiss-Box I/O Cards. Input signals received on input cards will be translated to Ethernet UDP/IP or TCP/IP and send to the network. Network messages received from the network are translated to output signals on the selected output cards. Any compliant 3th party software can be used to process these messages and thus "read" or "write" standard in and outputs (like relays, contact-closures, analog signals) via a standard Ethernet network. A range of I/O cards is available. The I/O3 CardCage can be controlled also over ArtNet and MIDI Show Control protocols.

## Eigenschappen

* 3 slots hold up to 24 versatile in and/or outputs
(CARDS PURCHASED SEPARATELY)
* Easy to implement IP (TCP or UDP) control protocol

* Controllable with DMX-512 over Ethernet. (with Artnet Firmware)

* Windows, Mac OS-X and Linux compatible

* Operating power over the network cable (PoE)

* Easy software configuration

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
