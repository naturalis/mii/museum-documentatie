---
title: "christie-dhd400s"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: Christie
model: DHD400S
leverancier:
interactives:
- letsdance
attachments:
- title: christie-dhd400s_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/td7BjRngLnfRbww
- title: projector_manual_8976.pdf
  src: https://files.museum.naturalis.nl/s/bSwtyEppndRtwao
---

![christie_captiva_highleft_black_](./christie_captiva_highleft_black_.png "christie_captiva_highleft_black_")
<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

* Levensduur lichtbron: 20.000 hrs
* Type lamp: 1-chip 0.65" DMD
* Projectie techniek: Solid state (Laser phosphor)
* ANSI lumen: 3100
* Resolutie: 1920 x 1080 (2,073,600 pixels)
* Beeldverhouding: 16:9
* Statische contrastverhouding: 1800:1 Full on/off
* Video-in:
  * HDMI x 2
  * VGA x 1 + monitor out or VGA x 2
  * Component video
* Bediening:
  * IR Remote
  * RS-232 in
  * Ethernet (100BaseT) RJ45
  * Built-in keypad
  * Compatible with Crestron RoomView, AMX, PJ Link

* Verbinding (Ethernet): RJ45 (100BaseT)
* Afmetingen: (LxWxH) 12.20 x 15.08 x 4.06" (310 x 383 x 103mm) w/o feet
* Vermogen: 335W max - 225W eco mode

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}


## Bijlagen

{{< pagelist bijlagen >}}
