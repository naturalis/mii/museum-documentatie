---
title: "builder-extreme-1500"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: Builders
model: Extreme 1500 Pro
leverancier:
attachments:
- title: builder-extreme1500_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/nN52DXta6a299MF
---

![builder-extreme-1500-pro](./builder-extreme-1500-pro.png "builder-extreme-1500-pro")
3D printer, maar dan heel groot. Deze 3D printer staat bij het Dinolab en wordt gebruikt om o.a. Dino botten te printen. We hebben op dit moment twee Duilder Extreme 1500 printers.

## Eigenschappen

* Merk: Builders
* Afmetingen:

  * Hoogte: 155cm
  * Breedte: 151cm
  * Diepte: 79cm

* Gewicht: 225 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
