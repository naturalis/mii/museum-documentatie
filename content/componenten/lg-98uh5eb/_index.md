---
title: "lg-98uh5eb"
date:
draft: false
merk: LG
model: 98UH5E-B
leverancier:
attachments:
- title: Specs (pdf)
  src: https://files.museum.naturalis.nl/s/HeAcyoLiGT5p2w8
---
![98uh5e-front](./98uh5e-front.jpg)
![98uh5e-back](./98uh5e-back.jpg)

Super vet groot beeldscherm met heel veel pixels. De bom.

## Eigenschappen

* Afmetingen: 2,191.8 x 1,246.8 x 69.4 mm (without Handle)
* Gewicht: 88kg
* Power Consumption (Typ./Max): 420W/560W

* PANEL
  * Panel Size: 98"
  * Panel Technology:	IPS
  * Resolution:	3840 x 2160 (UHD)
  * Contrast Ratio:
  * Response Time:	8ms (G to G)
  * Brightness:	500 nits
  * Aspect Ratio: 16:9
  * Viewing Angle (H x V):	178°/178°
  * Orientation:	Landscape & Portrait
  * Lifetime (Typ.):	50,000 Hrs
  * Operation Hours:	24 Hrs
  * Surface Treatment (Haze):	Haze 1%

* CONNECTIVITY
  * Input:
    * HDMI (3)
    * DP
    * DVI-D
    * Audio
    * USB 2.0 (2)
  * Output
    * DP
    * Audio (Off/Fixed/Variable)
    * External Speaker
  * External Control
    * RS232C In/out (4 Pin Phone-Jack)
    * RJ45 (LAN) In
    * IR In

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
