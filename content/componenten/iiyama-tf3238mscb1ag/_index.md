---
title: "iiyama-tf3238mscb1ag"
date: 2019-12-03T14:50:04+01:00
draft: false
merk: iiyama
model: ProLite TF3238MSC-B1AG
leverancier:
interactives:
  - greenporn
  - monitorenjungle
decors:
  - ontsmettingszone
  - wachtruimte
attachments:
- title: iiyama-tf3238mscb1ag_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/zpcY369TpBLjHKA
---

![tf3238mscb1ag](./tf3238mscb1ag.jpg "tf3238mscb1ag")
Open Frame PCAP 12 point touch screen voorzien van een schuimrubberen afdichting vooreen naadloze (kiosk) integratie


## Eigenschappen

* Video-in:
  * 1 x Displayport(v1.2)
  * 2 x HDMI(v1.4)
  * 1 x DVI(v1.0)
  * 1 x VGA
* Bediening:
  * RS-232c x1 (Get/Set Command)
  * RJ45 (LAN) x1
  * IR 1x
* Schermdiagonaal: 31.5", 80cm
* Resolutie: 1920 x 1080 (2.1 megapixel Full HD)
* Beeldverhouding: 16:9
* Vermogen: 60 W
* Kijkhoek: horizontal/verticaal: 178°/178°, rechts/links: 89°/89°, naar boven/onderen: 89°/89°
* Afmetingen: 748.5 x 450 x 68mm
* Gewicht: 14,5 KG

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Decors

Dit component wordt gebruik in de decors:

{{< pagelist decors >}}

## Bijlagen

{{< pagelist bijlagen >}}
