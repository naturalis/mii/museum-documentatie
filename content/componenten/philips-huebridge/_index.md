---
title: "philips-huebridge"
date:
draft: false
merk: Philips
model: Hue Bridge
leverancier: De Circel
attachments:
- title: Philips Hue Bridge (pdf)
  src: https://files.museum.naturalis.nl/s/FAbKfETPSyRmFXP

---

![huebridge](./huebridge.png)

## Eigenschappen


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
