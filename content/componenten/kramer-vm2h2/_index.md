---
title: "kramer-vm2h2"
date: 2019-12-03T14:50:05+01:00
draft: false
merk: kramer
model: vm2h2
leverancier:
interactives:
- studiolab
attachments:
- title: kramer-vm2h2_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/iAFRt7x87JL8KpQ
- title: VM-2H2 Datasheet.pdf
  src: https://files.museum.naturalis.nl/s/sfeRXEmtY5QpjNd
---

![vm-2h2](./vm-2h2.png "vm-2h2")
4K HDMI 2.0 Splitter (1:2)


## Eigenschappen

* Inputs:
  * 1 HDMI connector
* Outputs:
  * 2 HDMI connectors
* Poorten:
  * 1 RS−232 for firmware upgrade extension
* Power: 5V DC, 730mA
* Afmetingen: 18.75cm x 11.50cm x 2.54cm
* Gewicht: 0,4 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
