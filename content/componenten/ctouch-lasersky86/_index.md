---
title: "ctouch-lasersky86"
draft: false
merk: CTOUCH
model: Laser Sky
leverancier: Newcandle
faciliteiten:
- hetatelier
- detribune
attachments:
- title: ctouch_lasersky_manual.pdf
  src: https://files.museum.naturalis.nl/s/iQsMjkm4jm8JmCE
- title: ctouch_lasersky_86_specs.pdf
  src: https://files.museum.naturalis.nl/s/ZYatBk3KJaoqdTe
---

![ctouchlasersky](./ctouchlasersky.png "ctouchlasersky")
86 inch touch monitor met Android en optionele OPS module.


## Eigenschappen

* Video-in:
  * 1 x Displayport(v1.2)
  * 3 x HDMI 2.0 (1x ARC)
  * 1x DVI(v1.0)
  * 1 x VGA
* Audio-in:
  * 1 x Mini Jack
* Audio-out:
  * 1 x S/PDIF Optical
  * 1 x Mini Jack
* Bediening:
  * RS232C: 1x DB-9
  * LAN port: 1x RJ45 10/100/1000 BaseT
  * IR 1x
* Schermdiagonaal: 86", 2174 mm
* Resolutie: 3840 x 2160 @ 60Hz
* Beeldverhouding: 16:9
* Vermogen: 189 W
* Kijkhoek: horizontal/verticaal: 178°/178°
* Afmetingen: 1990.4 x 1173 x 113 (mm)
* Gewicht: 100 kg

## Technische specificaties

Zie bijlage

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Decors

Dit component wordt gebruik in de decors:

{{< pagelist decors >}}

## Known issues

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
