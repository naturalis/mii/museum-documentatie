---
title: "epson-tmt88vi"
date:
draft: false
merk: epson
model: tmt88vi
leverancier:
faciliteiten:
- servicebaliekassa1
- servicebaliekassa2
- servicebaliekassa3
- servicebaliekassa4
attachments:
- title: Epson TM-T88-VI Series Data Sheet
  src: https://files.museum.naturalis.nl/s/SasRttdX9E7QZmS
---

![tmt88vi](./tmt88vi.png)
Toekomstbestendige kassabonprinter

## Eigenschappen

* Afmetingen:

  * Hoogte: 148 mm
  * Breedte: 145 mm
  * Diepte: 195 mm

* Gewicht: 1,6 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Faciliteiten

Dit component wordt gebruik in de faciliteiten:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
