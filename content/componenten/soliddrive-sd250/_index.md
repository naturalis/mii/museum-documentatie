---
title: "soliddrive-sd250"
date:
draft: false
merk: soliddrive
model: sd250
leverancier:
shows:
- showrexperience
attachments:
- title: User Manual
  src: https://files.museum.naturalis.nl/s/T6RWy8JajMarrM3
- title: Datasheet
  src: https://files.museum.naturalis.nl/s/XwwyofarNscRABr
---

![sd250](./sd250.png)
SolidDrive SD-250 compacte versterker

## Eigenschappen

* Afmetingen:

  * Hoogte: 31 mm
  * Breedte: 100 mm
  * Diepte: 124,3 mm

* Gewicht: 0,26 kg

## Technische specificaties


## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
