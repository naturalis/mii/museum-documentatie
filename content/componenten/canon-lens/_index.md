---
title: "canon-lens"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: Canon
model: Lens
leverancier:
attachments:
- title: canon-lens_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/JdommfzQ5adtagE
---

![canon-lens](./canon-lens.jpg "canon-lens")
<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte:
  * Breedte:
  * Diepte:

* Gewicht:

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
