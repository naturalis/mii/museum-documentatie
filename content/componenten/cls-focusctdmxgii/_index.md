---
title: cls-focusctdmxgii
date:
draft: false
merk: CLS
model: Focus C T DMX GII
leverancier: CLS
attachments:
- title: CLS Focus C T DMX GII manual (pdf)
  src: https://files.museum.naturalis.nl/s/Sy2rTrd6998DDxa
- title: CLS Focus C T DMX GII (pdf)
  src: https://files.museum.naturalis.nl/s/YJyZip7M2Cn4qYW

---

![focusctdmxgii](./focusctdmxgii.png)
4 Watt, ultra compact zoom fixture.

## Eigenschappen

* Dimensions (LxWxH): 86 mm x 36 mm x 101 mm
* Weight: 160 gr

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
