---
title: "optoma-ml750st"
date: 2019-12-03T14:50:07+01:00
draft: false
merk: optoma
model: ml750st
leverancier:
interactives:
- handscanner
- timelapse
attachments:
- title: optoma-ml750st_gegevensblad (pdf)
  src: https://files.museum.naturalis.nl/s/bCPP4BysPFRtLpc
- title: optoma-ml750st_manual (pdf)
  src: https://files.museum.naturalis.nl/s/pRXo58zATQf6SSZ
---

![ml750st](ml750st.png)
Ultra-compact short throw LED projector

## Eigenschappen

* Levensduur lichtbron: 20.000 hours life
* Type lamp: LED
* Projectie techniek: DPL
* Lumen: 800
* Resolutie: 1280x800 (WXGA)
* Beeldverhouding: 16:10
* Statische contrastverhouding: 20.000:1
* Video-in:
  * 1 x HDMI 1.4a 3D support + MHL
  * 1 x microSDcard
  * 1 x USB-A reader
  * 1 x VGA (YPbPr/RGB)
* Bediening:
  * Card style (IR)

* Afmetingen: 112 x 123 x 57 mm
* Vermogen: 77W max
* Gewicht: 0,42KG

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
