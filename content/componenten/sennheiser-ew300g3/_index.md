---
title: "sennheiser-ew300g3"
date: 2019-12-03T14:50:08+01:00
draft: false
merk: sennheiser
model: ew300g3
leverancier:
attachments:
- title: sennheiser-ew300g3_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/aA9KP7rNbqeBKFj
---

![ew300g3](./ew300g3.png)
Sennheiser Evolution Wireless 300 series Generation 3

## Eigenschappen


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
