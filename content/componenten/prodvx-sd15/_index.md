---
title: "prodvx-sd15"
date:
draft: false
merk: provdx
model: sd15
leverancier:
shows:
- showrexperience
attachments:
- title: Techinsche Datasheet
  src: https://files.museum.naturalis.nl/s/esQB7pfyXWfZMQE
---

![sd15-front](./sd15-front.png)
![sd15-side](./sd15-side.png)
![sd15-back](./sd15-back.png)

## Eigenschappen

* Video-in:
  * HDMI
* Bediening:
  * Remote Control
* Schermdiagonaal: 15,6 inch
* Resolutie: 1920 x 1080
* Beeldverhouding: 16:9 wide
* Vermogen: 12 W
* Kijkhoek: H 160° / V 160°
* Afmetingen:

  * Hoogte: 245 mm
  * Breedte: 388 mm
  * Diepte: 27 mm

* Gewicht: 1,7 kg

## Technische specificaties

* Stroomvoorziening:  DC 12V

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
