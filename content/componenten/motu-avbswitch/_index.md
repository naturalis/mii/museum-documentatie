---
title: "motu-avbswitch"
date:
draft: false
merk: MOTU
model: AVB Switch
shows:
- showrexperience
leverancier:
attachments:
---

![avb](./avb.jpg)

Volgens mij is dit gewoon een dure manier om een simpele 5 poorts 1Gb/s switch te verkopen.

## Eigenschappen

* Afmetingen:

  * Hoogte: 2,7 cm
  * Breedte: 14 cm
  * Diepte: 9,2 cm

* AVM poorten: 5


## Known issues

<!-- Voeg hier known issues toe -->

Deze switch wordt waarschijnlijk niet gebruikt in de rexperience. Ze kregen het niet aan de praat en werken nu via USB.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
