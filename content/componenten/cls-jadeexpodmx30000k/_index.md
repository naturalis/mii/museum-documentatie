---
title: cls-jadeexpodmx30000k
date:
draft: false
merk: CLS
model: Jade Expo DMX 30000K
leverancier: CLS
attachments:
- title: CLS Jade Expo DMX 3000K manual (pdf)
  src: https://files.museum.naturalis.nl/s/LfiHS8n5Tn9XGd3
- title: CLS Jade Expo DMX 3000K (pdf)
  src: https://files.museum.naturalis.nl/s/DbBt95Ro5EE6EeJ

---

![jade-expe](./jade-expo.jpg)
30 Watt, high quality track fixture with a large choice of accessories.

## Eigenschappen

* Dimensions (LxWxH): 110 mm x 75 mm x 157,7 mm
  * Diameter: 100 mm
* Weight: 780 gr

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
