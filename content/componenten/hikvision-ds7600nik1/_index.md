---
title: "hikvision-ds7600nik1"
date: 2019-12-03T14:50:04+01:00
draft: false
merk: hikvision
model: ds7600nik1
leverancier:
shows:
- showjapanstheater
attachments:
- title: hikvision-ds7600nik1_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/CLb8WrKeWDLsQk9
- title: 10480_EUD00615BBaselineQuickStartGuideofNetworkVideoRecorderV3.4.620160328.pdf
  src: https://files.museum.naturalis.nl/s/cX3TREWN5Bx839H
---

![nvr-hikvision-ds-7604ni-k1](./nvr-hikvision-ds-7604ni-k1.png "nvr-hikvision-ds-7604ni-k1")
Network Video Recorder
## Eigenschappen

* Vermogen: (without hard disk and POE) less than 10 W
* Afmetingen:

  * Hoogte: 48 mm
  * Breedte: 315 mm
  * Diepte: 240 mm

* Gewicht: ≤ 1 kg

## Technische specificaties

* Video/Audio Input
  * Two-way audio input:		1-ch, RCA (2.0 Vp-p, 1kΩ)
  * IP video input:		4-ch
* Network
  * Incoming bandwidth:		40Mbps
  * Outgoing bandwidth:		80Mbps
* Video/Audio Output
  * HDMI Output:		1-ch, resolution:4K (3840 × 2160)/30Hz,1920 × 1080/60Hz, 1600 × 1200/60Hz, 1280 ×1024/60Hz, 1280 × 720/60Hz, 1024 × 768/60Hz.
  * Recording Resolution:		8MP/6MP/5MP/4MP/3MP/1080p/UXGA/720p/VGA/4CIF/DCIF/2CIF/CIF/QCIF
  * Audio Output:		1-ch, RCA (Linear, 1kΩ)
  * VGA Output:		1-ch, resolution: 1920*1080P/60Hz, 1280*1024/60Hz, 1280*720/60Hz, 1024*768/60Hz
* Decoding
  * Capability:		4-ch@1080P
  * Live view / Playback:		8MP/6MP/5MP/3MP/1080p/UXGA/720p/VGA/4CIF/DCIF/2CIF/CIF/QCIF
* Hard Disk
  * SATA:		1 SATA interface for 1 HDD
  * Capacity:		Up to 6TB capacity for each HDD
* External Interface
  * Network Interface:		1 RJ-45 10 / 100 Mbps selfadaptive
  * Ethernet interface
  * USB Interface:		2, USB2.0
* POE
  * Interface:		4 independent 10 /100 Mbps PoE
  * Ethernet interfaces
* General
  * Power Supply:		48 VDC
  * Working Temperature:		-10 oC ~ +55 oC
  * Working Humidity:		10 % ~ 90 %
  * Chassis:		315 mm chassis


## Known issues

Het zou natuurlijk vet cool zijn om de beelden ook op je eigen computer te bekijken.
Dit kan, alleen gebruikt Hikvision een oude techniek voor de video in je browser: NPAPI

NPAPI is eruit gesloopt bij alle moderne browsers dus de enige mogenlijkheid is
om het via Internet Explorer te proberen.

Ik heb ook een work-around gevonden om het via de Chrome Extensie "IE Tab"  te doen
maar bij mijn test (via VPN thuis) ging dat al mis.

De wachtwoorden staan in Bitwaren.

![Hivision in de browser](https://files.museum.naturalis.nl/s/SAsrLHgF9jMGqyG/preview)

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
