---
title: "ltech-lt8406a"
date:
draft: false
merk: LTECH
model: LT-840-6A
leverancier: de Cirkel
attachments:
- title: LTECH LT-840-6A (pdf)
  src: https://files.museum.naturalis.nl/s/DsEFRABbyBK2jgm

---

![lt8406a](./lt8406a.png)
DMX/RDM 4CH CV DECODER

## Eigenschappen

* Afmetingen:
  * Diepte: 78 mm
  * Hoogte: 40 mm
  * Breedte: 156 mm
* Gewicht: 445 gr


## Technische specificaties

LT-840-6A with the standard RDM remote device management protocol, supports DMX512 signal bi-directional communication, achieves remote management of reading and writing DMX address (DMX master controller must recognize the RDM protocol). Equipped with DMX standard XLR-3, RJ45, green terminal interface. Realize 0-100% dimming or different lighting effect; workable with single color, bi-color, RGB or RGBW LED lamps.

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Bijlagen

{{< pagelist bijlagen >}}
