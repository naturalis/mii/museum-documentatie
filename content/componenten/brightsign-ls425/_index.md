---
title: "brightsign-ls425"
date:
draft: false
merk: BrightSign
model: LS425
leverancier: TheNextShop
shows:
- leven
attachments:
- title: LS5 datasheet (pdf)
  src: https://files.museum.naturalis.nl/s/b6dRiFLyg8QYfKd
---

![ls425](./ls425.png)

Flawlessly runs Full HD video and core motion graphics with flexible USB engagement.

## Eigenschappen

* Afmetingen:

  * Hoogte:
  * Breedte:
  * Diepte:

* Gewicht:

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
