---
title: "slv-newtriamr16"
date:
draft: false
merk: SLV
model: New Tria MR16
leverancier: Q-Cat
attachments:
- title: SLV New Tria MR16 (pdf)
  src: https://files.museum.naturalis.nl/s/mzGFLe6atAQNnk8
---

![newtriamr16](./newtriamr16.png)


## Eigenschappen

* Afmetingen:

  * Hoogte: 110 mm
  * Diameter: 78 mm

* Gewicht: 154 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
