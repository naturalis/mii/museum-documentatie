---
title: "sonnet-xmacmsat3e"
date:
draft: false
merk: Sonnet
model: xMac mini Server Thunderbolt 3 Edition
leverancier: ATA tech
shows:
- showrexperience
attachments:
- title: xmac_mini_server_t3e-ug (pdf)
  src: https://files.museum.naturalis.nl/s/3mzYiXWT8iRsHzE
---

![xmac-front](./xmac-front.png)
![xmac-rear](./xmac-rear.png)
![xmac-inside](./xmac-inside.png)
Thunderbolt 3 to PCIe card expansion system.
1U rackmount enclosure for Mac mini with Thunderbolt 3 ports.

## Eigenschappen

* Afmetingen: 48.3 x 45.1 x 4.4 cm

* Gewicht: 7,26 kg
* Externe connectors:
  * Two Thunderbolt 3 ports (one open, one used)
  * One RJ-45 Gigabit/10 Gigabit Ethernet port extension
  * Two USB-A port extensions (one front-mount and 1 rear-mount)
  * One HDMI port extension
* Interne connectors:
  * 6-pin Mini-Fit Jr (provides auxiliary power for PCIe cards that require it)
  * One C-8 power
* PCI expantion slots:
  * One x8 (x4 electrical) PCIe 2.0 half-length
  * One x16 (x4 electrical) PCIe 2.0 full-length
## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
