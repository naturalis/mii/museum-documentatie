---
title: "ple-magnedledverticalmount"
date:
draft: false
merk: PLE
model: MagnedLED vertical mount
leverancier: ATA Tech
attachments:
- title: PLE MagnedLED vertical mount (pdf)
  src: https://files.museum.naturalis.nl/s/t8FmsQGWjyMFkAY

---

![magnedled](./magnedled.png)

## Eigenschappen


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
