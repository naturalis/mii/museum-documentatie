---
title: "neutrik-nbb75dfi"
date: 2019-12-03T14:50:07+01:00
draft: false
merk: Neurtix
model: nbb75dfi
leverancier:
attachments:
- title: neutrik-nbb75dfi_technischetekening.pdf
  src: https://files.museum.naturalis.nl/s/5DJDDp6Mby6WAcd
---

![nbb75dfi](nbb75dfi.png)
BNC kabel connector.

## Eigenschappen

* Afmeting: 26 x 31 x 34mm
* Impedantie: 75 Ohm

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
