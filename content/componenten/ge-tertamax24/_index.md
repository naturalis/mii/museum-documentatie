---
title: "ge-tertamax24"
date:
draft: false
merk: GE
model: Terta Max 24
leverancier: de Cirkel
attachments:
- title: GE Tetra Max 24 (pdf)
  src: https://files.museum.naturalis.nl/s/ZTTWcngGLRfCiQj
- title: GE Tetra Max 24 manual (pdf)
  src: https://files.museum.naturalis.nl/s/cyj3iKdPdYmmBY9
- title: GE Tetra Max 24 colour manual (pdf)
  src: https://files.museum.naturalis.nl/s/qp7jpozcdxrxMm2
---

![tertamax24](./tertamax24.png)
LED Lighting System

## Eigenschappen


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
