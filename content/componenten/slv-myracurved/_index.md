---
title: "slv-myracurved"
date:
draft: false
merk: SLV
model: Myra Curved
leverancier: Q-Cat
attachments:
- title: SLV Myra Curved (pdf)
  src: https://files.museum.naturalis.nl/s/HCqiw7sbgNFZRbT
- title: SLV Myra Curved Technical Specifications (pdf)
  src: https://files.museum.naturalis.nl/s/9PBNqjw3dTrQ2WM
---

![myracurved](./myracurved.png)
Outdoor displayspot

## Eigenschappen

* Afmetingen:

  * Hoogte: 230 mm
  * Breedte: 80 mm
  * Diepte: 820 mm

* Gewicht: 795 g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
