---
title: "startech-hb30c3a1cst"
date:
draft: false
merk: Startech
model: HB30C3A1CST
leverancier:
attachments:
- title: hb30c3a1cst_datasheet (pdf)
  src: https://files.museum.naturalis.nl/s/55dgSPjjfTg76S5
- title: hb30c3a1cst_4-port_metal_usb_hub_manual (pdf)
  src: https://files.museum.naturalis.nl/s/rH2bzA4bAb5amF6
---

![hb30c3a1cst](./hb30c3a1cst.jpg)

4-Port USB-C Hub - Metal - USB-C to 3x USB-A and 1x USB-C - USB 3.0

## Eigenschappen

* Afmetingen:

  * Hoogte: 95 mm
  * Breedte: 100 mm
  * Diepte: 150 mm

* Gewicht: 0,5 kg
* Connectors:
    * Connector A: USB Type-B (9 pin) USB 3.0
    * Connector B: USB Type-A (9 pin) USB 3.0 (5 Gbps)
    * Connector B: USB Type-C (24 pin) USB 3.0 (5 Gbps)
    * Connector B: USB 3.0 A (Fast-Charge, 9 pin)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
