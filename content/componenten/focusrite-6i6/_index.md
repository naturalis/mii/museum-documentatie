---
title: "focusrite-6i6"
date: 2019-12-03T14:50:04+01:00
draft: false
merk: Focusrite
model: Scarlett 6i6
leverancier:
interactives:
- studiolab
attachments:
- title: focusrite-6i6_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/f2G2y6cRyGt3bZy
- title: scarlett-6i6-2nd-gen-user-guide.pdf
  src: https://files.museum.naturalis.nl/s/CeJFjdf7Ac7qE8r
---

![focusrite_scarlett_6i6](./focusrite_scarlett_6i6.png "focusrite_scarlett_6i6")
Hoge kwaliteit voorversterker, eventueel te bedienen via software.

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte: 50 mm
  * Breedte: 195 mm
  * Diepte: 212 mm

* Gewicht: 1,4 KG

## Technische specificaties

* Configuration
  * Inputs: 6: analogue (4), S/PDIF (2)
  * Outputs: 6: analogue (4), S/PDIF (2)
  * Mixer: Fully assignable 6-in/12-out software mixer (Focusrite Control)
* Digital Performance
  * Supported sample rates: 44.1 kHz, 48 kHz, 88.2 kHz, 96 kHz, 176.4 kHz and 192 kHz
  * Clock jitter: <250 ps
* Microphone Inputs
  * Frequency Response: 20 Hz to 20 kHz, +0.5/-1.5 dB
  * Dynamic Range: 109 dB (‘A’-weighted)
  * THD+N: < 0.002%
  * Noise EIN: –127 dBu
  * Maximum Input Level: +8.5 dBu (without pad)
  * Gain Range: 50 dB
* Line Inputs 1 & 2
  * Frequency Response: 20 Hz to 20 kHz, +0.5/-1.5 dB
  * Dynamic Range: 109 dB (‘A’-weighted)
  * THD+N: < 0.003%
  * Maximum Input Level: +22 dB
  * Gain Range: 50 dB
* Line Inputs 3 & 4
  * Frequency Response: 20 Hz to 20 kHz, +0.5/-1.5 dB
  * Dynamic Range: 109 dB (‘A’-weighted)
  * THD+N: < 0.003%
  * Maximum Input Level: +16 dB
* Instrument Inputs
  * Frequency Response: 20 Hz to 20 kHz, +0.5/-1.5 dB
  * Dynamic Range: 109 dB (‘A’-weighted)
  * THD+N: < 0.003%
  * Maximum Input Level: +12 dBu
  * Gain Range: 50 dB
* Line Outputs 1 to 4
  * Frequency Response: 20 Hz to 20 kHz, +/-0.5 dB
  * Dynamic Range: 108 dB (‘A’-weighted)
  * THD+N: < 0.001%
  * Maximum Output Level (0 dBFS): +16 dBu, balanced
* Headphone Outputs
  * Frequency Response: 20 Hz to 20 kHz, +/-0.5 dB
  * Dynamic Range: 108 dB (‘A’-weighted)
  * THD+N: < 0.001%
  * Maximum Output Level (0 dBFS): +13 dBu, balanced

* Analogue Inputs 1 & 2
  * Connectors: XLR Combo type: Mic/Line/Inst, on front panel
  * Mic/Line switching: Automatic
  * Line/Instrument switching: Via software from Focusrite Control
  * Phantom power: Shared +48 V phantom power switch for inputs 1 and 2
* Analogue Inputs 3 & 4
  * Connectors: 2 x balanced 1⁄4” TRS jacks on rear panel
Analogue Outputs 1 to 4
  * Connectors: 4 x balanced 1⁄4” TRS jacks on rear panel
  * Stereo headphone outputs: 2 x 1⁄4” TRS jack on front panel
  * Main monitor output level control: On front panel
  * Headphones level controls: On front panel
* Other I/O
  * S/PDIF I/O: 2 x phono (RCA)
  * USB: 1 x USB 2.0 Type B connector
  * MIDI I/O: 2 x 5-pin DIN sockets

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
