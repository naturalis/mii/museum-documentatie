---
title: "eldoled-lineaedrive100a"
date:
draft: false
merk: eldoLED
model: LINEARdrive 100A
leverancier:
attachments:
- title: EldoLED LINEARdrive 100A (pdf)
  src: https://files.museum.naturalis.nl/s/C27Rcb9SmtoAdCp

---

![eldoled-lineaedrive100a](./eldoled-lineaedrive100a.png)
4x2A DMX/DALI Full-Colour Dimmable LED Drivers


## Eigenschappen

* Afmetingen:
  * 388x42x30 mm
* Gewicht: 705 gr


## Technische specificaties

LINEARdrive 100/A delivers 8A and is DALI and DMX compatible. The end caps function as wire restraints and make the driver perfectly suited for independent installations. LINEARdrive 100/A has four separately controllable LED outputs, which can be used for wide range of full-colour (RGBW), dynamic LED lighting applications.

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
