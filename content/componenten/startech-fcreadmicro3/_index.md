---
title: "startech-fcreadmicro3"
date:
draft: false
merk: Startech
model: FC READ Micro 3
leverancier:
attachments:
- title: fcreadmicro3 (pdf)
  src: https://files.museum.naturalis.nl/s/jXg483AfZaD9reb
- title: fcreadmicro3_datasheet-nl (pdf)
  src: https://files.museum.naturalis.nl/s/nGZtE87W5m26fZZ
---

![fcreadmicro3](./fcreadmicro3.jpg)

USB 3.0 externe Flash multimedia kaartlezer - SDHC / MicroSD

## Eigenschappen

* Afmetingen:

  * Hoogte: 1,5 cm
  * Breedte: 2,3 cm
  * Diepte: 7,2 cm

* Gewicht: 15 g
* Bus: USB 3.0

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
