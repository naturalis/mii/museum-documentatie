---
title: "brightsign-au335"
date:
draft: false
merk: BrightSign
model: AU335
leverancier: TheNextShop
shows:
- verleiding
attachments:
- title: AU5 datasheet (pdf)
  src: https://files.museum.naturalis.nl/s/3bmTbDbMbHZPTnm
---

![au335](./au335.png)

Elevates interactive audio experiences with cutting-edge sound technologies

## Eigenschappen

* Afmetingen:

  * Hoogte:
  * Breedte:
  * Diepte:

* Gewicht:

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
