---
title: "iiyama-xu2595wsub1"
date: 2019-12-03T14:50:05+01:00
draft: false
merk: IIYama
model: xu2595wsub1
leverancier:
interactives:
- expeditietent
attachments:
- title: iiyama-xu2595wsub1_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/dEGosRdmQasaYPs
- title: iiyama-xu2595wsub1_technischetekening.pdf
  src: https://files.museum.naturalis.nl/s/Ca8W9wX58pQbTRB
---

![xu2595wsub1](./xu2595wsub1.jpg "xu2595wsub1")
25” ultra-slim monitor met IPS panel-technologie en 16:10 beeldverhouding

## Eigenschappen

* Video-in:
  * 1 x Displayport
  * 1 x HDMI
  * 1 x VGA
* Bediening:
  * RS-232c x1 (Get/Set Command)
  * RJ45 (LAN) x1
  * IR 1x
* Schermdiagonaal: 25", 63.36cm
* Resolutie: 1920 x 1200 (2.3 megapixel WUXGA)
* Beeldverhouding: 16:10
* Vermogen: 28W typisch, 0.5W stand by, 0.5W uit modus
* Kijkhoek: horizontal/verticaal: 178°/178°, rechts/links: 89°/89°, naar boven/onderen: 89°/89°
* Afmetingen: 546.5 x 438.5 x 180mm
* Gewicht: 3.9kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
