---
title: "dell-latitude3590"
date: 2019-12-03T14:50:03+01:00
draft: false
merk: Dell
model: latitude 3590
leverancier:
interactives:
- camperjapan
attachments:
- title: latitude-15-3590-laptop_owners-manual3_nl-nl.pdf
  src: https://files.museum.naturalis.nl/s/rEXLLn6H8timPkw
- title: dell-latitude3590_startguide.pdf
  src: https://files.museum.naturalis.nl/s/m7sYDZ5pRsA2ZNr
---

![dell-latitude3590](./dell-latitude3590.jpeg "dell-latitude3590")
<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte: 22,7 mm
  * Breedte: 380,0 mm
  * Diepte: 258,0 mm

* Gewicht: vanaf 2,2 KG


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Interactives

  Dit component wordt gebruik in de interactives:

  {{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
