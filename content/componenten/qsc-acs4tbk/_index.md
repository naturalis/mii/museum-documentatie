---
title: "qsc-acs4tbk"
date: 2019-12-03T14:50:08+01:00
draft: false
merk: QSC
model: Acs4tbk
leverancier: Ata Tech
interactives:
 - naturalisdigitaal
 - cabinijsland
 - bewakingjapanstheater
shows:
 - showrexperience
attachments:
- title: qsc-acs4tbk_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/ERdJZFnQ39aK4ny
---

![Speaker](speaker.png)

Speaker van het merk QSC.

## Eigenschappen

* Vermogen:  16 watt
* Afmetingen:

  * Hoogte: 230 mm
  * Breedte: 160 mm
  * Diepte: 166 mm (inclusief beugel), 150 mm (zonder)

* Gewicht: 2.4 kg

## Interactives

{{< pagelist interactives >}}


## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}



## Bijlagen

{{< pagelist bijlagen >}}
