---
title: "presonus-nsb88"
draft: false
merk: presonos
model: nsb88
interactives:
- oersteen
attachments:
- title: NSB-series_StageBoxes_Owners_Manual_EN_18102022.pdf
  src: https://files.museum.naturalis.nl/s/8WnnyJs4b4w4XdE
---


![presonus-nsb88](./presonos_stagebox.JPG) "presonus-nsb88")
Stagebox. Acht analoge in- en uitgangen en twee RJ45-connecties zijn alles wat je nodig hebt, je verstuurt je hele signaal namelijk over een enkele Ethernetkabel. Zo heb je 16x8 kanalen die je kunt verzenden en ontvangen. Niet genoeg plek? Je kunt de NSB 8.8 doorlussen met een ander model.

## Eigenschappen

* 19 inch-rackformaat: 3U
* Aansluitingen:
  * 8x XLR/TRS combi (mic/line input)
  * 8x XLR (out)
  * 2x RJ45 (AVB A/B

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Als deze uitgeschakeld is terwijl de Mac mini opstart is er geen verbinding. De presonus zit om die reden op vaste spanning. Ze hangen rechts midden van de zaal in de kabelgoot. -->

Op dit moment zijn er geen known issues bekend.


## Bijlagen

{{< pagelist bijlagen >}}
