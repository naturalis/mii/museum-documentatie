---
title: "beetronics-15hdm"
date:
draft: false
merk: beetronics
model: 15hdm
leverancier:
Interactives:
- studiolab
attachments:
- title: User Manual
  src: https://files.museum.naturalis.nl/s/C4BNp89sgwXPg5k
- title: Specifications
  src: https://files.museum.naturalis.nl/s/tGBGjw7LgjpspMP
---

![15hdm](./15HDM.png)
15 inch monitor metaal

## Eigenschappen

* Video-in:
  * 1 x HDMI
  * 1 x VGA
  * 1 x BNC (VCBS)
* Bediening: IR remote
* Schermdiagonaal: 15.6 inch (39,6 cm)
* Resolutie: 1920 x 1080
* Beeldverhouding: 16:9
* Vermogen: 15 W
* Kijkhoek: 178° horizontaal, 178° verticaal
* Afmetingen: 419 x 239,5 x 38 mm (zonder poot)
* Gewicht: 3,6 kg


## Technische specificaties

* Ingangsspanning: DC 12V

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
