---
title: "showtec-stageblinder2"
date:
draft: false
merk: Showtec
model: Stage Blinder 2
leverancier: Flashlight
attachments:
- title: Showtec Stage Blinder 2 manual (pdf)
  src: https://files.museum.naturalis.nl/s/FN3RjeM4psa73L7
---

![stageblinder2](./stageblinder2.png)


## Eigenschappen

* Afmetingen:

  * Hoogte: 190 mm
  * Breedte: 380 mm
  * Diepte: 155 mm

* Gewicht: 3 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
