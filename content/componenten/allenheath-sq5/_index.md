---
title: "allenheath-sq5"
date:
draft: false
merk: allen & heath
model: SQ-5
leverancier:
attachments:
- title: SQ_ReferenceGuide_V1_4_0.pdf
  src: https://files.museum.naturalis.nl/s/NaY4ECXTTEY8EAd
- title: SQ-5-Technical-Datasheet_D.pdf
  src: https://files.museum.naturalis.nl/s/49GgmcPfLePoCHz
---

![sq5](./sq5.jpg)
Mixer

## Eigenschappen

* Vermogen:
* Afmetingen: 440 x 514.9 x 198 mm
* Gewicht: 10.5 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
