---
title: "kramer-tp580r"
date:
draft: false
merk: Kramer
model: TP-580 R
leverancier:
shows:
- showrexperience
attachments:
- title: TP-580R Datasheet (pdf)
  src: https://files.museum.naturalis.nl/s/Rm9C4HTkamgJ3W7
- title: rexperience_hardware_handleiding_kramer_extender_pt-580t_tp-580t_tp-580r (pdf)
  src: https://files.museum.naturalis.nl/s/mYFPjxNLAy6Gr6J
---

![tp580r](./tp580r.png)
TP580R is a high performance, longreach HDBase Treceiver for 4K60Hz (4:2:0) HDMI, RS232 and IRsignals over twisted pair.

## Eigenschappen

* Afmetingen: 12.00cm x 7.15cm x 2.44cm

* Gewicht: 0,2 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
