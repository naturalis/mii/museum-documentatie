---
title: "philips-55bdl4050d"
date: 2019-12-03T14:50:08+01:00
draft: false
merk: Philips
model: 55bdl4050d
leverancier: Ata Tech
interactives:
- spermarace
- readingroom
attachments:
- title: philips-55bdl4050d_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/2bnn47wQdKkyFaP
- title: 55bdl4050d-specs.pdf
  src: https://files.museum.naturalis.nl/s/xyKqLQA5XrxyGFw
---

![55bdl4050d](./55bdl4050d.png)
55 inch beeldscherm van Philips

## Eigenschappen

* Video-in:
  * 2 x Displayport
  * 2 x HDMI
  * 1 x DVI-I(DVI-D & VGA)
* Bediening:
  * RS232
  * RJ-45
  * IR
* Schermdiagonaal: 138 cm / 54.64 inch
* Resolutie: 1920 (H) x 1080 (V)
* Beeldverhouding: 16:9
* Vermogen: 77 Watt (112kWh per jaar)
* Kijkhoek:
* Afmetingen:

  * Hoogte: 710.3 mm
  * Breedte:  1233.4 mm
  * Diepte: 45.5 mm

* Gewicht: 18 kg

## Interactives

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
