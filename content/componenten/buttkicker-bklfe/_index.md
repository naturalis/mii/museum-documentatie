---
title: "buttkicker-bklfe"
date:
draft: false
merk: buttkicker
model: lfe
leverancier:
shows:
- showrexperience
attachments:
- title: User's Manual
  src: https://files.museum.naturalis.nl/s/ZM39XKnn7FKFNY6
---

![bklfe](./bklfe.jpg)


## Eigenschappen
* Vermogen: 400 W (min) - 1500 W (max)
* Afmetingen:

  * Hoogte: 136 mm
  * Breedte: 139 mm
  * Diepte: 136 mm

* Gewicht: 5 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
