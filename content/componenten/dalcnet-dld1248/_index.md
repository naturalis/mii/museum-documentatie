---
title: "dalcnet-dld1248"
date:
draft: false
merk: Dalcnet
model: DLD1248
leverancier: De Cirkel
attachments:
- title: Dalcnet DLD1248 4x5A
  src: https://files.museum.naturalis.nl/s/yMnAjAqzsbBtZZ3
---

![dalcnet-dld1248](./dalcnet-dld1248.png)
4 channel LED driver


## Eigenschappen

* Afmetingen:
  * Diepte: 62 mm
  * Hoogte: 90,5 mm
  * Breedte: 71 mm
* Gewicht: 125 gr


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
