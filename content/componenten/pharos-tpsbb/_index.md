---
title: "pharos-tpsbb"
date: 2019-12-03T14:50:07+01:00
draft: false
merk: Pharos
model: TPS
leverancier: Ata Tech
shows:
- showjapanstheater
- showrexperience
interactives:
- studiolab
attachments:
- title: pharos-tpsbb_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/zc9cCdKq9d4Mzc4
---

![pharos-tpc](TPC.png)
The Pharos TPC (Touch Panel Controller) is an elegant lighting controller with a customisable, 4.3″ capacitive touch screen, 512 channels of eDMX output and vast interfacing potential, all over a single Power-over-Ethernet (PoE) network connection.

## Eigenschappen

* Vermogen: PoE (IEEE802.3af, Class 2) 4W typical
* Afmetingen:

  * Hoogte: ?
  * Breedte: 86 mm
  * Diepte: 146 mm

* Gewicht: 0,25 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
