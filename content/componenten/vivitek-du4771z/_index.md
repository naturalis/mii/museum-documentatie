---
title: "vivitek-du4771z"
draft: true
merk: Vivitek
model: DU4771Z
interactives:
- projectieboomstronk
attachments:
- title: Gebruikers Handleiding
  src: https://files.museum.naturalis.nl/s/HbPGcS9iBx37xrc
- title: Technische Specificaties
  src: https://files.museum.naturalis.nl/s/ntPRoyg5rnRoCm3
---

![vivitek-du4771z](./vivitek-du4771z.png "vivitek-du4771z")
<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

* Levensduur lichtbron: 20,000 hours
* Type lamp: 	Lamp free design. Laser light source
* Projectie techniek: Single chip DLP® Technology by Texas Instruments
* ANSI lumen: 6000
* Resolutie: WUXGA (1920 x 1200)
* Beeldverhouding: 16:10
* Statische contrastverhouding: 20,000:1
* Video-in:
  * VGA-In (15pin D-Sub)
  * HDMI (x3: HDMI/MHLx1, HDMIx2)
  * Composite Video
* Bediening:
  * IR Remote
  * RJ45 (x2: HDBaseTx1, LANx1)
* Verbinding (Ethernet): RJ45 (100BaseT)
* Afmetingen: 360 x 451.5 x 151 mm
* Vermogen: 350W (Eco. Mode), 430W (Normal Mode), <0.5W (Standby)

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}


## Bijlagen

{{< pagelist bijlagen >}}
