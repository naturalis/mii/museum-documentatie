---
title: "genelec-7050cpm"
draft: true
merk: Genelec
model: 7050CPM
interactives:
- oersteen
attachments:
- title: Gebruikers Handleiding
  src: https://files.museum.naturalis.nl/s/stEx4S6m3yz9BN2
- title: Lijn Tekening PDF
  src: https://files.museum.naturalis.nl/s/o5LAP4mCrXNYe3e
- title: Lijn Tekening DWG
  src: https://files.museum.naturalis.nl/s/9n7fLpAAbaRzH53
- title: Technische Specificaties
  src: https://files.museum.naturalis.nl/s/a3abxDiTnd42D7k
- title: Brochure
  src: https://files.museum.naturalis.nl/s/pDnAGocGryexr3y
---

![genelec-7050cpm](./genelec-7050cpm.png "genelec-7050cpm")
![genelec-7050cpm-side](./genelec-7050cpm-side.png "genelec-7050cpm-side")

The Genelec 7050C active subwoofer is a very compact low frequency loudspeaker, designed to extend the bass reproduction of Genelec active loudspeakers in stereo or surround applications. Adding the 7050C to the system creates a compact nearfield monitoring system capable of a flat frequency response down to 24 Hz (-6 dB).

## Eigenschappen

* RMS vermogen: 130 W
* Frequentierespons: 24 Hz - 85 kHz (± 6 dB)
* Type connector:
 - 5 x input connectors XLR female, 10 kOhm balanced
 - 1 x LFE IN connector
 - XLR male output connectors
* Actief of passief: actief

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
