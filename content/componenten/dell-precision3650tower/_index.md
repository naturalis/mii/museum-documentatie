---
title: "dell-precision3650tower"
date:
draft: false
merk: Dell
model: Precision 3650 Tower
leverancier: Dell
attachments:
- title:
  src:
---

![](./precision.jpg)


## Eigenschappen

* CPI: i9-11900 8
* Storage: 1TB M.2 NVMe
* Grafical card: Quadro P2200
* Extra NIC: Intel X550-TA2 NIC (10GbE)


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
