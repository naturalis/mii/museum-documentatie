---
title: "phidget-encoderhighspeed4input"
draft: false
merk: phidget
model: encoderhighspeed4input
leverancier: Jipp
interactives:
- devisinjou
attachments:
- title: PhidgetEncoder HighSpeed 4-Input - 1047_1B at Phidgets.pdf
  src: https://files.museum.naturalis.nl/s/r57FYsfJo4pBBSY
- title: PhidgetEncoder HighSpeed 4-Input - 1047_1B at Phidgets_specs.pdf
  src: https://files.museum.naturalis.nl/s/PyoZF7pWwWf6oqD
---

![phidget-encoderhighspeed4input](./PhidgetEncoder_HighSpeed_4-Input.JPG)
The PhidgetEncoder Highspeed 4-Input can be used with a wide assortment of mechanical and optical encoders. The encoder should be of quadrature output type, indicating that there will be two quadrature output channels (usually labeled A and B) and an optional third output channel to signal when the index pin (a reference point for zero position or a complete revolution) has been reached.

The PhidgetEncoder Highspeed 4-Input is able to read four encoders simultaneously. Encoders are not powered up until all initialization of the device is complete. It is possible to enable some or all encoders, depending on how many of the channels are being used. This can also be used to reduce power consumption when certain encoders are not needed.

The PhidgetEncoder Highspeed 4-Input has the added ability to time the duration between a group of quadrature changes. The time is returned in microseconds. This time value can be used to calculate velocity and acceleration.

## Eigenschappen

* USB Voltage: 4.8 V DC min, 5.3 V DC max
* Number of Digital Inputs: 4

## Technische specificaties

Lighting Playback Controller 1: 1024 channels DMX/eDMX

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
