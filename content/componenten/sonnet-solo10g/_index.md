---
title: "sonnet-solo10g"
date:
draft: false
merk: Sonnet Technologies
model: Solo10G
leverancier: The Future Store
interactives:
- studiolab
attachments:
- title: Solo10G handleiding (pdf)
  src: https://files.museum.naturalis.nl/s/osCrSoFiCMkYfF8
---

![](./solo10g.png)

Tunderbolt 3 to 10GBASE-T / NBASE-T Adapter

## Eigenschappen


* One RJ45 socket: 10GbE Controller
* 10GBASE-T (100 meters using Cat 6A cabling; 55 meters using Cat 6 cabling)
* 5GBASE-T(2), 2.5GBASE-T(2), 1000BASE-T(2), 100BASE-TX (100 meters using Cat 5e [or better] cabling)
* Data Rates Supported: 10 Gb/s, 5 GB/s, 2.5 Gb/s, 1 Gb/s, 100 Mb/s

* Advanced Features
    * Flow control support
    * MSI (Message Signaled Interrupts)
    * RSS (Receive-Side Scaling)
    * Interrupt coalescing
    * Multicast filtering
    * Energy-Efficient Ethernet (IEEE 802.3az)
    * AVB (IEEE 802.1Qav, Audio Video Bridging)
    * 64-bit address support for systems using more than 4GB of physical memory
    * Wake-on-LAN support
    * Stateless Offloads

* Operating Temperature: 0ºC to 35ºC (32ºF to 95ºF)
* Dimensions (WxDxH): 79.5 x 114 x 27.2 mm
* Weight: 0.24 kg

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

{{< pagelist interactives >}}


## Bijlagen

{{< pagelist bijlagen >}}
