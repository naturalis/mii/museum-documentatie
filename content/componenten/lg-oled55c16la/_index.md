---
title: "lg-oled55c16la"
draft: true
merk: LG
model: oled55c16la
leverancier: Coolblue
interactives:
  - aquarium
attachments:
- title: lg-oled55c16la.pdf
  src: https://files.museum.naturalis.nl/s/3j3HDQfxCSJJk4M
---

![lg-oled55c16la_picture](./lg-oled55c16la_picture.jpg "lg-oled55c16la_picture")
Een 4K Ultra HD oled televisie met een schermdiagonaal van 139 cm (55 inch).

## Eigenschappen

* Schermdiagonaal:  55" | 139 cm
* Resolutie: 4K Ultra HD (3840 x 2160)
* Afmetingen: 1228 x 706 x 46.9 mm
* VESA: 300 x 200
* Gewicht: 18.9kg
* Verbruik: 151 kW
* Aansluitingen:
  * 4 x HDMI
  * 3 x USB
  * 1 x Ethernet
  * 1 x Optische uitgang audio
  * 1 x Koptelefoon aansluiting


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
