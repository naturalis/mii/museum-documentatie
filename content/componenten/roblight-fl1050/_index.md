---
title: "roblight-fl1050"
date:
draft: false
merk: Roblight
model: FL1050 4000K
leverancier: Q-Cat
attachments:
- title: Roblight FL1050 4000K (pdf)
  src: https://files.museum.naturalis.nl/s/4JB8k6rorMgSq5f

---

![fl1050](./fl1050.png)

## Eigenschappen

* Afmetingen:
  * Diepte: 248 mm
  * Hoogte: 73 mm
  * Breedte: 86 mm
* Gewicht: 1120 gr


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
