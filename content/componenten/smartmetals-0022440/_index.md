---
title: "smartmetals-0022440"
date: 2019-12-03T14:50:09+01:00
draft: false
merk: smartmetals
model: 0022440
leverancier:
attachments:
- title: smartmetals-0022440_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/iZ2qeSfes3NpESA
---

![smartmetals-0022440](./0022440.png)
L1: vaste lengte 130 mm

## Eigenschappen

* Vermogen: 25kg
* Afmetingen:

  * Hoogte: 130 mm
  * Breedte:
  * Diepte:

* Gewicht: 1,6kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
