---
title: "qsc-ks112"
date:
draft: false
merk: qsc
model: ks112
leverancier:
shows:
- showrexperience
attachments:
- title: Technische specificaties
  src: https://files.museum.naturalis.nl/s/fQE46esmAM8M7QL
- title: Handleiding
  src: https://files.museum.naturalis.nl/s/4y2MryjE3yDi3a4
- title: rexperience_hardware_avh_speakers_handleiding_qsc_ksseries (pdf)
  src: https://files.museum.naturalis.nl/s/ndE7pmqenw7jDm9
---

![ks112](./ks112.jpg)
![ks112-rear](./ks112-rear.jpg)
2000 watt ACtive Subwoofer

## Eigenschappen
* Vermogen: 2000 W
* Afmetingen:

  * Hoogte: 622 mm
  * Breedte: 394 mm
  * Diepte: 616 mm (incl wielen)

* Gewicht: 28,4 kg

## Technische specificaties

* Connectors:
  * 2 x locking XLR/F ¼” combo  
  * 2 x XLR/M (Loop-thru Output)
  * 1 x locking IEC power connector

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
