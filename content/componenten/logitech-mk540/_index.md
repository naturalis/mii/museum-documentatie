---
title: "logitech-mk540"
date: 2019-12-03T14:50:06+01:00
draft: false
merk: logitech
model: mk540
leverancier:
interactives:
- studiolab
attachments:
- title: logitech-mk540_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/cD7QiPd3Awb8ese
---

![mk540](./mk540.png "mk540")
Draadloos toetsenbord- en muiscombinatie

## Eigenschappen

* Vermogen: 3 AA-alkalinebatterijen
* Afmetingen:

  * Hoogte: 74 mm
  * Breedte: 201 mm
  * Diepte: 472 mm

* Gewicht: 1355 g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}



## Bijlagen

{{< pagelist bijlagen >}}
