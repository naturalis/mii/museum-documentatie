---
title: "jbl-control19cs"
date:
draft: false
merk: jbl
model: Control 19 CS
leverancier:
shows:
- showrexperience
attachments:
- title: Technische specificaties
  src: https://files.museum.naturalis.nl/s/3NttMipkms9c3po
- title: Technical Application Guide
  src: https://files.museum.naturalis.nl/s/84tgByZ7wZo5XZC
---

![19cs](./19cs.jpg)
In-Ceiling Subwoofer

## Eigenschappen

* Vermogen: 200 Watts Continuous Program Power, 100 Watts Continuous Pink Noise
* Imperdatie: 8 Ohms

* Afmetingen:

  * Hoogte: 345 mm
  * Diameter: 345 mm

* Gewicht: 5,5 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
