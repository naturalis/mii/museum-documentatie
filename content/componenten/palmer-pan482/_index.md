---
title: "palmer-pan482"
date:
draft: false
merk: Palmer
model: PAN 48.2
leverancier: ATAtech
attachments:
- title: pan48 technical specs (pdf)
  src: https://files.museum.naturalis.nl/s/6MTHNLxQMCF6j7W
---

![pan482](./pan482.png)
2 Channel Phantom power

## Eigenschappen

* Afmetingen: 110 x 110 x 45 mm

* Gewicht:  0.9 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
