---
title: "magewell-proconvertnditohdmi4k"
date:
draft: false
merk: Magewell
model: Pro Convert NDI to HDMI 4K
leverancier: Streaming Valley
interactives:
- studiolab
attachments:
- title: User Manual
  src: https://files.museum.naturalis.nl/s/C3Yw24yB75zJS6T
- title: FAQ
  src: https://files.museum.naturalis.nl/s/63is8pLP7d9GYtW
- title: Technical Specifications
  src: https://files.museum.naturalis.nl/s/JRQF3AxS7oXQn9A
---


![](./proconvert.jpeg)
![](./tekening.png)

NDI to HDMI converter

## Eigenschappen

* Afmetingen: 117.5mm (L) x 66.7mm (W) x 23.4mm (H)

* Power:
  * 5V max current: ~1.6A
  * Max power consumption: ~7.2W

* Output:
    * Max output: 4096x2160 60fps 4:4:4 8-bit
    * Delivering video at the preferred video format according to the EDID of the connected monitor. Supported output formats include:
      * 4096x2160p/3840x2160p 23.98/24/25/29.97/30/50/59.94/60
      * 2560x1440p 60/100/120/144
      * 2560x1080p 23.98/24/25/29.97/30/50/59.94/60
      * 1920x1080p 23.98/24/25/29.97/30/50/59.94/60/100/119.88/120
      * 1920x1080i 50/59.94/60
      * 1280x720p 23.98/24/25/29.97/30/50/59.94/60
      * 720x576p50
      * 720x576i50
      * 720x480p59.94/60
      * 720x480i59.94/60
      * Support 48KHz 8-channel audio output
      * HDMI interface: HDMI 1.4/2.0

* Decoding:
  * Decoding up to 4Kp60 streams of:
      * Full NDI
      * NDI|HX
      * NDI|HX2
      * RTSP
      * HTTP
      * HLS
      * RTMP Pull/Push
      * MPEG-TS over UDP/SRT/RTP protocols
  * H.264/H.265 video decoders
  * AAC/MP3 audio decoders
  * Support NDI 4.x

* Web UI:
  * Provide comprehensive information regarding the device and decoded signals in real-time
  * Allow to configure how a signal is decoded and output, including OSD settings, deinterlacing, resolutions, alpha blending, audio gain, sample rate, and channel mapping, as well as the network connection
  * Update firmware
  * Support USB RNDIS/ECM
  * Support IE/Edge/Firefox/Chrome/Safari/Opera web browsers
  * Provide HTTP APIs

* NIC:
  * 10/100/1000Mbps Ethernet
  * IEEE 802.3af PoE

* USB:
  * USB2.0 Type B
    * 5V/2.1A power supply
    * USB RNDIS/ECM
  * USB3.0 Type A
    * Connecting peripherals (keyboard and/or mouse) to customize the video and audio settings

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
