---
title: "proliad-600216002"
date:
draft: false
merk: Proliad
model: 600 216002
leverancier: Proliad
attachments:
- title: Proliad 600 216002 manual (pdf)
  src: https://files.museum.naturalis.nl/s/fpZwWYS6EebaA8W
- title: Proliad 600 216002 (pdf)
  src: https://files.museum.naturalis.nl/s/RzfoSpEw3Ek5z8k
---

![600216002](./600216002.png)

## Eigenschappen

* Afmetingen:
  * Diameter: 180 mm
  * Hoogte: 80 mm


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
