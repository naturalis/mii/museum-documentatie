---
title: "elation-sixpar200"
date:
draft: false
merk:
model:
leverancier: Flashlight
attachments:
- title: Elation Sixpar200 manual (pdf)
  src: https://files.museum.naturalis.nl/s/ZzAdK7fMHaidm6e
- title: Elation Sixpar200 (pdf)
  src: https://files.museum.naturalis.nl/s/FsPWaBatW5DJjy3
---

![elation-sixpar200](./elation-sixpar200.png)

## Eigenschappen

* Afmetingen:
  * Breedte: 125 mm
  * Hoogte: 313 mm
  * verticale lengte: 300 mm
* Gewicht: 5,7 kg


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
