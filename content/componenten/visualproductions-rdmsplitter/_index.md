---
title: "visualproductions-rdmsplitter"
date:
draft: false
merk: Visual Productions
model: RMD Splitter DIN RJ45
leverancier: ATA Tech
attachments:
- title: Visual Productions RDM Splitter DIN RJ45 (pdf)
  src: https://files.museum.naturalis.nl/s/Mpqjt9QXtjDsLSF
---

![rdmsplitter](./rdmsplitter.png)
RDM Splitter

## Eigenschappen

* Afmetingen:

  * Hoogte: 29,5 mm
  * Breedte: 104 mm
  * Diepte: 98 mm


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

De RDM Splitter boven vogelskeletten in de goot aan het plafond heeft een kapot poortje. Poortje OUT2 is stuk.
Zie [showdood](https://docs.museum.naturalis.nl/latest/shows/showdood/#known-issues).

## Bijlagen

{{< pagelist bijlagen >}}
