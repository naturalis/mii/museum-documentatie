---
title: "arduino-leonardo"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: Arduino
model: Leonardo
leverancier:
Interactives:
- datingquiz
- fipperkast
- geilewand
- handscanner
- hartbank
- spermarace
- timelapse
attachments:
- title: arduino-leonardo_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/YfSkzCtHB5WkRJ5
- title: arduino-leonardo-schematic_3b.pdf
  src: https://files.museum.naturalis.nl/s/7TCH7aLd4nC7bqC
---

![ArduinoLeonardo](./ArduinoLeonardo.jpg "ArduinoLeonardo")
<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

- Microcontroller: ATmega32u4Operating
- Voltage: 5V
- Input Voltage (recommended): 7-12V
- Input Voltage (limits): 6-20V
- Digital I/O Pins: 20
- PWM Channels: 7
- Analog Input Channels: 12
- DC Current per I/O Pin: 40 mA
- DC Current for 3.3V Pin: 50 mA
- Flash Memory: 32 KB (ATmega32u4) of which 4 KB used by bootloader
- SRAM: 2.5 KB (ATmega32u4)
- EEPROM: 1 KB (ATmega32u4)
- Clock Speed: 16 MHz

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
