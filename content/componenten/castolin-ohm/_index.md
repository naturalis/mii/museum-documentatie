---
title: castolin-ohm
date: 2019-12-09T10:48:30+01:00
draft: false
merk: Castolin
model: Oxy-Hydrogen Mobile Blowtortch
leverancier:
faciliteiten:
- demosteensmelten
attachments:
- title: castolin-ohm_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/cPg2K9sTXm523eZ
- title: Opstart procedure IHM 2.4
  src: https://files.museum.naturalis.nl/s/gJgaJz37KyLYNzB
---

![castolin-ohm](./castolin-ohm.jpg "castolin-ohm")
De Castolin Ohm is een Oxy-Hydrogen Mobile Blowtortch, ofwel een Mobiele zuurstof-waterstofbrander. Hiermee smelten ze steen in zaal Aarde.

## Eigenschappen

### Production of Hydrogen/Oxygen
* Service pressure in NORMAL mode: 1,4 bar
* Maximum flow : 550 l/h
* Autonomy: 1 hour between 2 consumables cartridge
* Temperature of the flame: > 2 500 °C

### Dimensions, weight and handling
* Dimensions (mm): L 395; W360, H 960
* Weight : 38kg

### Electrical data, Pure water and Additive
* Power supply: 230 VAC / 16A
* Maximum power: 2,4 kW
* Nominal power: 2,0 kW
* Consumption at nominal power:
    * Purewater: 0,25 l/h (cartridge of 0,25 litre for one hour autonomy)
    * Additive: 0,125 l/h (cartridge of 0,125 litre for one hour autonomy)


* Consumption at maximum power: Pure water: 0,3 l/h Additive: 0,13 l/h
* Time for cartridgeof consumables : ~2 minutes
* Start-up time : ~2 minutes

__(Pure water and Additive in accordance with CASTOLIN specifications)__

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Faciliteiten

Dit component wordt gebruik in de faciliteiten:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
