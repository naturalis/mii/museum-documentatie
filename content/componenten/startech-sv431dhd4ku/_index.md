---
title: "startech-sv431dhd4ku"
date:
draft: false
merk: Startech
model: SV431DHD4KU
leverancier:
attachments:
- title: sv431dhd4ku_datasheet-nl (pdf)
  src: https://files.museum.naturalis.nl/s/iXxxkmDmbPYYqHw
- title: sv431dhd4ku_manual (pdf)
  src: https://files.museum.naturalis.nl/s/o4Fk9AAmNqYEL8j
---

![SV431DHD4KU-front](./SV431DHD4KU-front.png)
![SV431DHD4KU-rear](./SV431DHD4KU-rear.png)
4 poorts HDMI KVM switch - 4K 30Hz - dual display

## Eigenschappen

* Afmetingen:

  * Hoogte: 6,3 cm
  * Breedte: 13 cm
  * Diepte: 22 cm

* Gewicht: 1,2 kg

* Audio: Yes
* KVM-poorten: 4
* Ondersteunde platforms: USB
* PC-videotype: HDMI
* Multi-monitor: 2
* Rack-monteerbaar: Optional


## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
