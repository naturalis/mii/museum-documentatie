---
title: "jbl-control14ct"
date:
draft: false
merk: jbl
model: control 14 CT
leverancier: ATA-tech
shows:
- showrexperience
attachments:
- title: Technische specificaties (pdf)
  src: https://files.museum.naturalis.nl/s/WepsKAHzRpw7p8A
- title: rexperience_hardware_avh_speakers_handleiding_jbl_c10 (pdf)
  src: https://files.museum.naturalis.nl/s/L7B6EkNoig2SKwa
---

![14ct](./14ct.jpg)
Two-Way 4” Coaxial Ceiling Loudspeaker

## Eigenschappen

* Vermogen: 60W Continuous Program Power, 30W Continuous Pink Noise
* Impedantie: 8 Ohms
* Afmetingen:

  * Hoogte: 196 mm
  * Diameter: 196 mm

* Gewicht: 2,2 kg

## Technische specificaties

* Input Connector: Removable locking 4-pin connector with screw-down terminals. Max wire size 12 AWG (2.5 mm2).

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
