---
title: "optoma-zu500uste"
date: 
draft: false
merk: optoma
model: zu500uste
leverancier:
interactives:
- projectiedans
attachments:
- title: optoma-zu500uste_gegevensblad (pdf)
  src: https://files.museum.naturalis.nl/s/mYRSJ7caRZ9Lsga
- title: optoma-zu500uste_manual (pdf)
  src: https://files.museum.naturalis.nl/s/ZLq2an7fGqnRr8a
---

![zu500uste](zu500uste.png)
High brightness WUXGA ultra short throw laser projector

## Eigenschappen

* Levensduur lichtbron: 30.000 hours life
* Type lamp: Laser
* Projectie techniek: DPL
* Lumen: 5000
* Resolutie: WUXGA (1920x1200)
* Beeldverhouding: 16:10
* Statische contrastverhouding: 100.000:1
* Video-in:
Inputs , , , 
Outputs 1 x VGA, 1 x Audio 3.5mm, 1 x USB-A power 1.5A
Control , , 
  * 2 x HDMI 2.0
  * 1 x VGA (YPbPr/RGB)
* Bediening:
  * 1 x RS232
  * 1 x RJ45  
  * 1 x USB-B service

* Afmetingen: 383 x 318 x 111 mm
* Vermogen: 400W max
* Gewicht: 7.8 KG

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
