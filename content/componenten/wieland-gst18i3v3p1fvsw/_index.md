---
title: "wieland-gst18i3v3p1fvsw"
date: 2019-12-03T14:50:11+01:00
draft: false
merk: wieland
model: gst18i3v3p1fvsw
leverancier:
attachments:
- title: wieland-gst18i3v3p1fvsw_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/q6C5jWXpY3N98bT
---

![gst18i3v3p1fvsw](./gst18i3v3p1fvsw.png)
Verdeelblok GST18i3, 3-polig

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte:
  * Breedte:
  * Diepte:

* Gewicht:

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
