---
title: "vivitek-dh4661z"
date:
draft: false
merk: Viviitek
model: DH4661Z
leverancier: ATAtech
decors:
- capsule
attachments:
- title: rexperience_hardware_avh_beamer_handleiding_vivitekbeamer (pdf)
  src: https://files.museum.naturalis.nl/s/emZ39CE5tHSbnPQ
- title: rexperience_hardware_avh_beamer_specsheet_vivitek_dh4661z (pdf)
  src: https://files.museum.naturalis.nl/s/t28J76T7eZHk98q
---

![dh4661z](./dh4661z.png)


## Eigenschappen

* Levensduur lichtbron: 5 years or 20.000h
* Type lamp: Laser
* Projectie techniek: Single chip DLP® Technology by Texas Instruments
* ANSI lumen: 5000
* Resolutie: 1080p (1920 x 1080)
* Beeldverhouding: 16:9
* Statische contrastverhouding: 20.000:1
* Video-in:
  * HDMI v.1.4b (x3: HDMI/MHLx1, HDMIx2)
  * Composite Video
  * Mini D-Sub 15-pin (VGA)

* Bediening:
  * RJ45 (x2: HDBaseTx1, LANx1)
  * RS-232
  * USB miniB (Service)

* Afmetingen: 360 x 451.5 x 151 mm
* Gewicht: 10.7 KG
* Vermogen: 50W (Eco. Mode), 430W (Normal Mode), <0.5W (Standby)

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

![foutmeldingen](https://files.museum.naturalis.nl/s/bsxBD8ow4FeqMXQ/preview)

## Decors

{{< pagelist decors >}}

## Bijlagen

{{< pagelist bijlagen >}}
