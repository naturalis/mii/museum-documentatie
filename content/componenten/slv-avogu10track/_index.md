---
title: "slv-avogu10track"
date:
draft: false
merk: SLV
model: Avo GU10 Track
leverancier: Q-Cat
attachments:
- title: SLV Avo GU10 Track
  src: https://files.museum.naturalis.nl/s/6TWbaskDdCBTmed
---

![avogu10track](./avogu10track.png)


## Eigenschappen

* Afmetingen:

  * Hoogte: 120 mm
  * Breedte: 108 mm
  * Diameter: 62 mm

* Gewicht: 204 g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
