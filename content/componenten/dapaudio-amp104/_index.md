---
title: "dapaudio-amp104"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: DAP Audio
model: AMP-104
leverancier:
attachments:
- title: dapaudio-amp104_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/jAqc2zC8QnMZdaB
---

![d1536](./d1536.png "d1536")
4 Channel Headphone Amplifier


## Eigenschappen

* Versterkerklasse: 4 Channel Headphone Amplifier
* Aantal speaker kanalen: 4 x stereo
* RMS vermogen:
* Broningangen:
  * 6,3 mm Jack, RCA
  * 1/4" TRS socket
* Speakeraansluitingen: 4 maal 2 x 1/4" TRS socket
* Signaal-ruisverhouding:
* Impedantie: 2 x 50 Ohm
* Beveiligingen:
* Vermogen: 2x 50mW @ 32Ohm
* Afmetingen: 53 x 158 x 135 mm

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
