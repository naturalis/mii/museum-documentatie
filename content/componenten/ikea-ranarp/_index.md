---
title: "ikea-ranarp"
date:
draft: false
merk: IKEA
model: Ranarp
leverancier: de Cirkel
attachments:
- title: IKEA Ranarp (pdf)
  src: https://files.museum.naturalis.nl/s/BZq9GpFsSMcFF9s

---

![ranarp](./ranarp.png)
Hanglamp, zwart, 23cm

## Eigenschappen

* Afmetingen:
  * Breedte: 23 mm



## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
