---
title: "dapaudio-pm160"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: DAP Audio
model: PM-160
leverancier:
attachments:
- title: dapaudio-pm160_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/KyYQtqrzYMYQnco
---

![dap-pm-160-vergadermicrofoon](./dap-pm-160-vergadermicrofoon.png "dap-pm-160-vergadermicrofoon")
![dap-pm-160-vergadermicrofoon-achter](./dap-pm-160-vergadermicrofoon-achter.png "dap-pm-160-vergadermicrofoon-achter")
Vergadermicrofoon

## Eigenschappen

* Microfoon type: Condensator
* Frequentiebereik: 22 Hz – 23 KHz
* Gevoeligheid: 29dB @ 1kHz
* Bebalanceerde uitgang: 230mV
* Talk-knop
* Ongebalanceerde uitgang: 110mV
* Afmetingen: 132 x 155 x380mm (LxHxB)
* Gewicht: 0,7kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
