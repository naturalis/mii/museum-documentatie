---
title: "denon-dn500bdmkii"
date:
draft: false
merk: denon
model: DN-500BD MKII
leverancier:
attachments:
- title: DN-500BD MKII Specifications
  src: https://files.museum.naturalis.nl/s/33JHJiXmeeQ5Zrr
- title: DN-500BD MKII User Guide
  src: https://files.museum.naturalis.nl/s/LEdMLP3yZa72oi3
---

![dn500bd](./dn500bd.png)
The Denon Professional DN-500BDMKII Blu-ray disc and media player is a compact, high performance unit that handles all popular optical video and audio CD playback formats for unrivaled capability from a single disc playback device. For even more versatility, front-loading SD and USB ports deliver access to video, audio and picture files such as AVI, MOV, MP4, MP3, WAV, FLAC, GIF, JPEG, PNG, and more.

## Eigenschappen

* Vermogen: <0,5 Watt (standby) tot 15 Watt (typical)
* Afmetingen:

  * Hoogte:  44mm
  * Breedte: 483mm
  * Diepte: 273mm

* Gewicht: 2,7kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
