---
title: "kramer-tp590rxr"
date: 2019-12-03T14:50:05+01:00
draft: false
merk: kramer
model: tp590rxr
leverancier:
interactives:
- showrexperience
- projectiekwallen
shows:
- rexperience
attachments:
- title: TP-590rxr Datasheet.pdf
  src: https://files.museum.naturalis.nl/s/8M6aT3izbH9GNtT
---

![tp-590rxr](./tp590rxr.jpg "tp-590rxr")
![tp-590rxr-schema](./tp590rxr-schema.jpg)
TP−590RXR is a high−performance, extended−reach HDBaseT 2.0 receiver for 4K60Hz (4:2:0) HDMI, USB, Ethernet, RS−232, IR and stereo audio signals over twisted pair. It extends video signals to up to 100m (330ft) over CAT copper cables at up to 4K@60Hz (4:2:0) 24bpp video resolution and provides even further reach for lower HD video resolutions.

## Eigenschappen

* Inputs:
  * 1 HDBT on a RJ-45 female connector
* Outputs:
    * 1 HDMI
    * 1 Stereo analog unbalanced audio on a 3.5mm mini jack
* Poorten:
    * 1 IR on a 3.5mm mini jack for IR link extension,
    * 4 USB 2.0 on a USB−A connector
    * 1 RS−232 on a 3−pin terminal block for serial link extension
    * 1 RS−232 on a 3−pin terminal block for device control
    * 1 10Base−T/100BaseTx Ethernet on an RJ−45 female connector for device control and network traffic extension extension
* Voeding: 48V DC, 1.36A
* Power: 48V DC, 800mA
* Afmetingen: 18.75cm x 11.50cm x 2.54cm
* Gewicht: 0,5 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
