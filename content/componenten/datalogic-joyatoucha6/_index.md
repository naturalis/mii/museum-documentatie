---
title: "datalogic-joyatoucha6"
date:
draft: false
merk: Datalogic
model: Joya Touch A6
leverancier:
attachments:
- title: User Manual
  src: https://files.museum.naturalis.nl/s/LGWaJRnXKWgMHfa
- title: Technical Specifications
  src: https://files.museum.naturalis.nl/s/dd6k9LoASjpLJYd
---

![JoyaTouchA6](./JoyaTouchA6.png)
The Joya™ Touch A6 is a state-of-the-art Android™-based mobile computer that is ideal for various retail applications.

## Eigenschappen

* Afmetingen:

    * Handheld: 14.5 x 7.7 x 3.4 cm
    * Pistol-Grip: 14.5 x 7.7 x 11.6 cm

* Gewicht:

    * Handheld: 275 g
    * Pistol-Grip: 305 g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
