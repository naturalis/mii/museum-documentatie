---
title: chauvet-ovationf265ww
date:
draft: false
merk: Chauvet
model: Ovation F-265WW
leverancier: Lichtpunt
attachments:
- title: Chauvet Ovation F-265WW DMX (pdf)
  src: https://files.museum.naturalis.nl/s/kHWEQf25BkqYiYY
- title: Chauvet Ovation F-265WW manual (pdf)
  src: https://files.museum.naturalis.nl/s/i3cK9dfGNDQ4Xzq
- title: Chauvet Ovation F-265WW Quick (pdf)
  src: https://files.museum.naturalis.nl/s/TBb4DFwsrjQ3ZF9

---

![ovationf265ww](./ovationf265ww.png)
The Ovation F-265WW is a warm white Fresnel-style LED lighting fixture with 16-bit dimming resolution for smooth fades. It delivers a beautiful flat, even field of light. Its motorized zoom, with a range between 27° and 68°, can be controlled either manually or via DMX. Simple and complex DMX channel profiles offer programming versatility. Virtually quiet operation makes it ideal for use in any situation.

## Eigenschappen

* Dimensions (LxWxH): 384 mm x 320 mm x 450 mm
* Weight: 7.9 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
