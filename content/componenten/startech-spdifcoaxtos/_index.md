---
title: "startech-spdifcoaxtos"
date:
draft: false
merk: Startech
model: SPDIF COAX TOS
leverancier:
attachments:
- title: spdifcoaxtos (pdf)
  src: https://files.museum.naturalis.nl/s/yrqpDCbj8JWpjcM
- title: spdifcoaxtos_datasheet (pdf)
  src: https://files.museum.naturalis.nl/s/J474bcHtEodidCq
---

![spdifcoaxtos](./spdifcoaxtos.jpg)

Two Way Digital Coax to Toslink Optical Audio Converter Repeater

## Eigenschappen

* Afmetingen:

  * Hoogte: 1,2 cm
  * Breedte: 1,5 cm
  * Diepte: 2,3 cm

* Gewicht: 25 g

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
