---
title: "iiyama-tf2415mc"
date: 2019-12-03T14:50:04+01:00
draft: false
merk: iiyama
model: ProLite tf2415mc
leverancier:
attachments:
- title: ProLite TF2415MC-B1.pdf
  src: https://files.museum.naturalis.nl/s/HgwCmKraGZHemMp
- title: iiyama-tf2415mc_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/qQozxXc9QecprTF

---

![tf2415mc](./tf2415mc.jpg "tf2415mc")
Open Frame PCAP 10 point touch screen voorzien van een schuimrubberen afdichting vooreen naadloze (kiosk) integratie

## Eigenschappen

* Video-in:
  * 1 x Displayport
  * 1 x HDMI
  * 1 x VGA
* Bediening: touch, externbedieningspaneel
* Schermdiagonaal: 23.8", 60.5cm
* Resolutie: 1920 x 1080 (2.1 megapixel Full HD)
* Beeldverhouding: 16:9
* Vermogen: 25 W
* Kijkhoek: horizontal/verticaal: 178°/178°, rechts/links: 89°/89°, naar boven/onderen: 89°/89°
* Afmetingen: 575 x 347 x 42.5mm
* Gewicht: 5,8 KG

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
