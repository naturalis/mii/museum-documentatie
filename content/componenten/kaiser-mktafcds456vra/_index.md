---
title: "kaiser-mktafcds456vra"
date: 2019-12-03T14:50:05+01:00
draft: false
merk: kaiser
model: mktafcds456vra
leverancier:
attachments:
- title: kaiser-mktafcds456vra_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/ZZGZadbNJmLyCwP
---

![kaiser-mktafcds456vra](./kaiser-mktafcds456vra.png "kaiser-mktafcds456vra")

Zespunt stekkerdoos met aarde en lichtgevende schakelaar.

## Eigenschappen


* Afmetingen:

  * Hoogte:
  * Breedte:
  * Diepte:

* Gewicht:

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
