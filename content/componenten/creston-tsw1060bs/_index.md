---
title: "creston-tsw1060bs"
date:
draft: false
merk: creston
model: tsw1060bs
leverancier:
attachments:
- title: quickstart
  src: https://files.museum.naturalis.nl/s/qgqCwJzs8qMn7fA
- title: supplemental guide
  src: https://files.museum.naturalis.nl/s/bibmFJJg4C2yCgK
- title: technical specifications
  src: https://files.museum.naturalis.nl/s/z5X9GGRRJjxFpTE
- title: IP Guidelines for the IT Professional
  src: https://files.museum.naturalis.nl/s/6KK3HjGirbfDQPg
---

![tsw1060bs](./tsw1060bs.png)
10.1" Touch Screen, Black Smooth

## Eigenschappen

* Afmetingen:

  * Hoogte: 168 mm
  * Breedte: 260 mm
  * Diepte: 39 mm

* Gewicht: 675 g

## Technische specificaties

* Display Type: TFT active matrix color LCD
* Size: 10.1 inch (257mm) diagonal
* Aspect Ratio: 16:10 WXGA
* Resolution: 1280x800 pixels
* Brightness: 400 nits (cd/m2)
* Contrast: 950:1
* Color Depth: 24bit, 16.7M colors
* Illumination: Edgelit LED with auto brightness control
* ViewingAngle±80°horizontal,±80°vertical
* Touch Screen: Projected capacitive, 5-point multitouch capable

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
