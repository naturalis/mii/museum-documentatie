---
title: "powersoft-ottocanali1204dsp"
date:
draft: false
merk: powersoft
model: Ottocanali 1204 DSP
leverancier:
shows:
- showrexperience
attachments:
- title: User Guide
  src: https://files.museum.naturalis.nl/s/AZHK4MaydcCgbX7
- title: Technical Specifications
  src: https://files.museum.naturalis.nl/s/AAd298TKiS3Gq8E
---

![Ottocanali_1204](./Ottocanali_1204.png)
8-Channel Power Amplifier for Lo-Z and Hi-Z transducers.
Ultimately flexible and safe, 8-channels power amplifier ideal for multi-zone applications in small to mid-scale installs.

## Eigenschappen

* Input impedantie: 10 KOhm gebalanceerd
* Afmetingen:

  * Hoogte: 44,45 mm
  * Breedte: 483 mm (19")
  * Diepte: 360 mm

* Gewicht: 5-11 Kg depending on the number of installed BatFormers®

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
