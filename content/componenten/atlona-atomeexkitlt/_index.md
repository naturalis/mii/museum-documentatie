---
title: "atlona-atomeexkitlt"
draft: false
merk: atlona
model: AT-OME-EX-KIT-LT_G
interactives:
- devisinjou
- dnaspel
- slangen
attachments:
- title: AT-OME-EX-KIT-LT_G.pdf
  src: https://files.museum.naturalis.nl/s/GQnayP8Sj6KeQAb
- title: AT-OME-EX-KIT-LT_Spec.pdf
  src: https://files.museum.naturalis.nl/s/NsXp3qKLAW2z9py
---

![atlona-atomeexkitlt](./atlona-at-ome-ex-kit-It.JPG "atlona-atomeexkitlt")
De Atlona AT-OME-EX-KIT-LT is een HDBaseT-extender voor video tot 4K, plus ingebouwde audio, bediening en USB over afstanden tot 40 meter voor 4K / UHD en hoger tot 70 meter voor 1080p video.

## Eigenschappen

* HDMI 2.0, HDCP 2.2
* USB 2.0, 2.5 W per USB device interface
* CEC
* Power: 48 V, 0.83 A DC
* Afmetingen: 26 mm x 109 mm x 127 mm
* Gewicht: 0,4 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
