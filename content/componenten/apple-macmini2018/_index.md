---
title: "apple-macmini2018"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: Apple
model: Mac Mini 2018
leverancier:
shows:
  - showrexperience
  - showjapanstheater
attachments:
- title: apple-macmini_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/XtEQax8RPTCFH7B
---

{{% notice warning %}}
Aandachtspunt:
Dit is ondertussen een verouderd model. Specs hieronder zijn w.s. niet meer te koop.
{{% /notice %}}

![macmini](./macmini.png "macmini")

## Eigenschappen

* ID: Macmini8,1
* Naam Mac Mini
* Merk: Apple
* Model: A1993

## Technische specificaties

* Finish:

  * Space Gray-colored aluminum (100% recycled)

* Processor:

  * 3.6GHz quad-core Intel Core i3 with 6MB shared L3 cache OR
  * 3.0GHz 6-core Intel Core i5 with Turbo Boost up to 4.1GHz and 9MB shared L3 cache
  * Configurable to 3.2GHz 6-core Intel Core i7 with Turbo Boost up to 4.6GHz and 12MB shared L3 cache

* Storage Capacity:

  * 128GB PCIe-based SSD OR
  * 256GB PCIe-based SSD
  * Configurable to 512GB, 1TB, or 2TB SSD

* Memory:

  * 8GB of 2666MHz DDR4 SO-DIMM memory
  * Configurable to 16GB, 32GB, or 64GB

* Graphics:

  * Integrated Intel UHD Graphics 630

* Size & Weight:

  * Length: 7.7 inches (19.7 cm)
  * Width: 7.7 inches (19.7 cm)
  * Depth: 1.4 inches (3.6 cm)
  * Weight: 2.9 pounds (1.3 kg)

* Connections & Expansion:

  * Four Thunderbolt 3 (USB-C) ports with support for:
    * DisplayPort
    * Thunderbolt (up to 40 Gbps)
    * USB 3.1 Gen 2 (up to 10 Gbps)
    * Thunderbolt 2, HDMI, DVI, and VGA supported using adapters (sold separately)
  * Two USB 3 ports (up to 5 Gbps)
  * HDMI 2.0 port
  * Gigabit Ethernet port (configurable to 10Gb Ethernet)
  * 3.5mm headphone jack

* Communications:

  * Wi-Fi
    * 802.11ac Wi-Fi wireless networking
    * IEEE 802.11a/b/g/n compatible
  * Bluetooth
    * Bluetooth 5.0 wireless technology
  * Ethernet
    * 10/100/1000BASE-T Gigabit Ethernet (RJ-45 connector)
    * Configurable to 10Gb Ethernet (Nbase-T Ethernet with support for 1Gb, 2.5Gb, 5Gb, and 10Gb Ethernet using RJ-45 connector)

* Video Support:

  * Support for the following combination of maximum concurrent display setups:
    * Up to three displays:
      * Two displays with 4096-by-2304 resolution at 60Hz connected via Thunderbolt 3 plus one display with 4096-by-2160 resolution at 60Hz connected via HDMI 2.0 OR
      * One display with 5120-by-2880 resolution at 60Hz connected via Thunderbolt 3 plus one display with 4096-by-2160 resolution at 60Hz connected via HDMI 2.0

    * Thunderbolt 3 digital video output supports:
      * Native DisplayPort output over USB-C
      * Thunderbolt 2, DVI, and VGA output supported using adapters (sold separately)
    * HDMI 2.0 display video output
      * Support for up to one display with 4096-by-2160 resolution at 60Hz
      * DVI output using HDMI to DVI Adapter (sold separately)

* Audio:

  * 3.5 mm headphone jack
  * HDMI 2.0 port supports multichannel audio output

* Electrical & Operating Requirements:

  * Line voltage: 100–240V AC
  * Frequency: 50Hz to 60Hz, single phase
  * Operating temperature: 50° to 95° F (10° to 35° C)
  * Storage temperature: −40° to 116° F (−40° to 47° C)
  * Relative humidity: 5% to 90% noncondensing
  * Operating altitude: tested up to 16,400 feet
  * Typical acoustical performance: Sound pressure level (operator position): 4 dBA at idle

* Operating System:

  * macOS Mojave - up to latest release of macOS

* In the Box:

  * Mac Mini
  * Power cord

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
