---
title: "powersoft-m28q"
date:
draft: false
merk: Powersoft
model: M28Q
leverancier: ATA tech
shows:
- showrexperience
attachments:
- title: powersoft_MSeries_uguide_mul_v0.0 (pdf)
  src: https://files.museum.naturalis.nl/s/ZAJW68T7nrzBpYD
- title: powersoft_m28q_data_en_2.6 (pdf)
  src: https://files.museum.naturalis.nl/s/s7jDXFirsmeWHaK
---

![M28Q](./M28Q.png)

4-Channels Power Amplifier for Touring and Installation

## Eigenschappen

* Afmetingen: W 483 mm, H 44.5 mm, D 358 mm

* Gewicht: 7,4 kg

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
