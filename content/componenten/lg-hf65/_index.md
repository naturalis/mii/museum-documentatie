---
title: "lg-hf65"
date: 2019-12-03T14:50:02+01:00
draft: true
merk: LG
model: HF65
leverancier:
interactives:
- cambrium
attachments:
- title: Gebruikers Handleiding
  src: https://files.museum.naturalis.nl/s/9W9DpQSeK9RgP9F
- title: Technische Specificaties
  src: https://files.museum.naturalis.nl/s/PyK4mQHQZyBRXjF
---

![lg-hf65](./lg-hf65.jpg "lg-hf65")
<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

* Levensduur lichtbron: 30,000 Hrs
* Type lamp: RGB LED
* Projectie techniek: Solid state (Laser phosphor)
* ANSI lumen: 1000
* Resolutie: Full HD(1920x1080)
* Beeldverhouding: 16:9
* Statische contrastverhouding: 150,000:1
* Video-in:
  * HDMI x 2
* Bediening:
  * IR Remote
  * RJ45
* Verbinding (Ethernet): RJ45 (100BaseT)
* Afmetingen: 131 x 309 x 128(voorkant)/89(achterkant)
* Vermogen: 100W

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}


## Bijlagen

{{< pagelist bijlagen >}}
