---
title: "proliad-x1"
date:
draft: false
merk: Proliad
model: X1
leverancier: Proliad
attachments:
- title: Proliad X1 (pdf)
  src: https://files.museum.naturalis.nl/s/33zD8LwzHpHjQ4y
- title: Proliad X1 manual (pdf)
  src: https://files.museum.naturalis.nl/s/NMa6336Q6p8DoSk
---

![x1](./x1.png)

## Eigenschappen

* Afmetingen:
  * Diameter: 29 mm
  * Hoogte: 37 mm




## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
