---
title: "thetamp-ta50"
draft: true
merk: the t.amp
model: TA50
leverancier: Thomann
interactives:
- darwinvinken
- devisinjou
- dnaspel
attachments:
- title: Gebruikers Handleiding
  src: https://files.museum.naturalis.nl/s/QgZbFtAy89LSLky
---

![thetamp-ta50](./thetamp-ta50.png "thetamp-ta50")

Mini-versterker module

## Eigenschappen

* Versterkerklasse: D
* Aantal speaker kanalen: 2
* RMS vermogen: 20 W
* Broningangen: mini-jack
* Speakeraansluitingen: phoeninx klemmen
* Impedantie: 4 Ohms
* Beveiligingen: Convection cooling
* Afmetingen: 45 mm x 145 mm x 95 mm (hoogte x breedte x diepte in mm)
* Gewicht: 0,6 kg

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
