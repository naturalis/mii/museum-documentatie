---
title: "cherry-mc1000"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: Cherry
model: MC1000
leverancier:
attachments:
- title: 1349530877_1_muizen-cherry-mc-1000-jm-0800-0.pfd
  src: https://files.museum.naturalis.nl/s/WWXP7K6mz5qtFeo

---

![cherry-mc-1000-muis-optisch](./cherry-mc-1000-muis-optisch.png "cherry-mc-1000-muis-optisch")
3-button mouse with scroll wheel

## Eigenschappen

* Kabel lengte: aprox. 1.80 m
* Resolutie: 1200 dpi
* Gewicht: 90 g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
