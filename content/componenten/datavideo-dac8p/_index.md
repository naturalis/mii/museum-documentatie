---
title: "datavideo-dac8p"
date: 2019-12-03T14:50:03+01:00
draft: false
merk: Datavideo
model: DAC-8P
leverancier:
interactives:
- studiolab
attachments:
- title: datavideo-dac8p_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/q5Wi4DqGQKxs6Kt
---

![dac-8p](./dac-8p.jpeg "dac-8p")
![dac-8p-rear](./dac-8p-rear.jpeg "dac-8p-rear")
HD/SD-SDI TO HDMI CONVERTER

## Eigenschappen

* Vermogen: 4 W
* Afmetingen:

  * Hoogte: 38mm
  * Breedte: 105mm
  * Diepte: 120mm

* Gewicht: 0,5 KG

## Technische specificaties

* Input: 1x HD/SD-SDI input
* Output:
  *  1x HD/SD-SDI output (Loop Thru)
  * 1x HDMI (V1.2) output



## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

  Dit component wordt gebruik in de interactives:

  {{< pagelist interactives >}}


## Bijlagen

{{< pagelist bijlagen >}}
