---
title: "figure53-qlab"
merk: Figure53
model: Qlab
leverancier: Rutger van Dijk
shows:
- showrexperience
- showjapanstheater
attachments:
- title: QLab Download  
  src: http://figure53.com/qlab/download/
---

![qlab-icon](./qlab-icon.png "qlab-icon")
QLab is sound, video and lighting control for macOS.

## Eigenschappen

* Handleiding: https://qlab.app/docs/v4/
* Systeemeisen: https://qlab.app/docs/v4/general/system-recommendations/

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
