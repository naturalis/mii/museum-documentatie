---
title: "iiyama-tf1634mcb8x"
date: 2019-12-03T14:50:05+01:00
draft: false
merk: Iiyama
model: ProLite Iiyama-TF1634MCB8X
leverancier:
interactives:
  - slangen
attachments:
- title: Gebruikers Handleiding
  src: https://files.museum.naturalis.nl/s/jHsMBw67b5gz8at
- title: Technische Specificaties
  src: https://files.museum.naturalis.nl/s/nZNnqJa8fwCseJM
---

![iiyama-tf1634mcb8x](./iiyama-tf1634mcb8x.png "iiyama-tf1634mcb8x")
15,6 inch Full HD 10-punts PCAP Open Frame touchscreen met IPS Paneeltechnologie

## Eigenschappen

* Video-in:
  * 1 x Displayport
  * 1 x HDMI
  * 1 x VGA
* Schermdiagonaal: 15.6", 39.5cm
* Resolutie: 1920 x 1080 @60Hz (2.1 megapixel)
* Beeldverhouding: 16:9
* Vermogen: 18 W
* Kijkhoek: horizontal/verticaal: 178°/178°, rechts/links: 89°/89°, naar boven/onderen: 89°/89°
* Afmetingen: 381 x 230.5 x 46mm
* Gewicht: 2,0 KG

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
