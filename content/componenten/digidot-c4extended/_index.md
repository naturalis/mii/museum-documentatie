---
title: "digidot-c4extended"
date:
draft: false
merk: DIGIdot
model: C4 Extended
leverancier:
interactives:
- oersteen
attachments:
- title: DiGidot C4 Extended (pdf)
  src: https://files.museum.naturalis.nl/s/6ktktSSxzaza5Gb

---

![c4extended](./c4extended.png)
Pixel Controller voor LED strips
## Eigenschappen

* Afmetingen: 153 x 74 x 28 mm
* Gewicht: 140 gr


## Technische specificaties

The DiGidot C4 Extended is a multi functional LED pixel controller that converts Art-Net and DMX to SPI protocols. The output capacity goes up to 8 universes, which is 4096 channels. (depending on the license) The internal DHCP server, Wi-Fi and free DiGidot App grand easy access of the built in web interface.  A built in network switch allows multiple DiGidot C4 controllers to be daisy chained to create an extensive control network. The DiGidot C4 Extended offers Art-Net & DMX recording, playback and triggering. Extensive triggering options such as analog and digital triggers as well as the built in calendar offer great flexibility for system integration. Countless extra features such as DMX in and out, Art-Net IP Address filter, Art-Net routing, HTP input signal merger, Art-Net & DMX Monitor, in-/output channel repeater, Gamma correction, adjustable fall back color, output limiters, make this product to the most powerful LED pixel controller on the market.

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
