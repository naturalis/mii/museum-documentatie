---
title: "ecler-ca120"
date:
draft: false
merk: Ecler
model: CA120
leverancier: ATA-tech
interactives:
- onschatbaar
attachments:
- title: Ecler_CA-NET_User_Manual (pdf)
  src: https://files.museum.naturalis.nl/s/oRy6e8GPMLmpDW8
- title: Ecler_CA120_User_Manual (pdf)
  src: https://files.museum.naturalis.nl/s/dEnRbkXKYszSPcp
---

![ca120 voorkant](./ca120voorkant.jpg)
![ca120 zijkant](./ca120zijkant.jpg)

60W 2 Channel RS232 Micro amplifier

## Eigenschappen


## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
