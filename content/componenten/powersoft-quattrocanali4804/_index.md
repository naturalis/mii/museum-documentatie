---
title: "powersoft-quattrocanali4804"
date:
draft: false
merk: powersoft
model: Quattrocanali 4804
leverancier:
shows:
- showrexperience
attachments:
- title: Brochure
  src: https://files.museum.naturalis.nl/s/sr7L2y8WpFsPRYo
- title: Technical Specifications
  src: https://files.museum.naturalis.nl/s/ccD2d3P4n8ayyFc
- title: rexperience_hardware_avh_versterker_handleiding_powersoftduecanali_quattrocanali (pdf)
  src: https://files.museum.naturalis.nl/s/ffKg3A34mNoawN3
---

![quattrocanali4804_dsp](./quattrocanali4804_dsp.png)


## Eigenschappen

* Input impedantie: 20 kΩ balanced
* Afmetingen:

  * Hoogte: 44,5 mm
  * Breedte: 483 mm (19")
  * Diepte: 358 mm

* Gewicht: 6,8 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
