---
title: "spacepole-spv1102nn"
date: 2019-12-03T14:50:09+01:00
draft: false
merk: Spacepole
model: spv1102nn
leverancier:
attachments:
- title: spacepole-spv1102nn_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/3T7EJ4zGnkbmp69
---

![spv1102nn](./spv1102nn.jpeg)
Ergonomic Solutions Display Mounts

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte:
  * Breedte:
  * Diepte:

* Gewicht: 1.30 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
