---
title: "epson-ebl630u"
date: 
draft: false
merk: Epson
model: EB-L630U
leverancier:
Interactives:
- projectiediepzee
attachments:
- title: EpsonEB-L630U.pdf
  src: https://files.museum.naturalis.nl/s/b84EJptNSSnykZm
---
![epson-ebl630u](./epson-ebl630u.png "epson-ebl630u")
<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

* Levensduur lichtbron: 20.000 hours life
* Type lamp: WUXGA, 6200 lumens
* Projectie techniek: Laser
* Resolutie: 1920 x 1200 (WUXGA)
* Beeldverhouding: 16:10
* Statische contrastverhouding: 2.500.000 : 1
* Video-in:
  * VGA in (2x)
  * HDBaseT
  * Miracast
  * HDMI (HDCP 2.3) (2x)

* Bediening:
  * IR Remote
  * Wifi
  * RS-232 in
  * HTTPS, IPv6, SNMP, ESC/VP.net, PJLink

* Verbinding:
  * USB 2.0-A (2x)
  * RS-232C
  * Ethernet-interface (100 Base-TX/10 Base-T)
  * Draadloos LAN IEEE 802.11a/b/g/n/ac
  * Draadloos LAN b/g/g 25 GHz
  * Draadloos LAN a/n (5 GHz)
* Afmetingen: 440 x 339 x 136 mm (breedte x diepte x hoogte)
* Vermogen: 287 Watt (Normal On-Mode), 187 Watt (Eco On-Mode), 345 Watt (Normal Peak-mode), 258 Watt (Eco Peak-Mode), 0,3 Watt (Energy saving standby)

## Technische specificaties

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
