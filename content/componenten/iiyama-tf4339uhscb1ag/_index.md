---
title: "iiyama-tf4339uhscb1ag"
date: 2019-12-03T14:50:04+01:00
draft: true
merk: Iiyama
model: ProLite TF4339UHSC-B1AG
leverancier:
interactives:
  - darwinvinken
attachments:
- title: Gebruikers Handleiding
  src: https://files.museum.naturalis.nl/s/RGKfYQre7KXTx5H
- title: Technische Specificaties
  src: https://files.museum.naturalis.nl/s/ZS8aSegZqEnidZ3
---

![iiyama-tf4339uhscb1ag](./iiyama-tf4339uhscb1ag.png "iiyama-tf4339uhscb1ag")
43-inch 12-punts open frame touch monitor met touch through-glass technologie


## Eigenschappen

* Video-in:
  * 1 x Displayport
  * 2 x HDMI
  * 1 x VGA
* Bediening:
  * RS-232c x1
  * RJ45 (LAN) x1
* Schermdiagonaal: 43", 108cm
* Resolutie: 1920 x 1080 @60Hz (2.1 megapixel Full HD)
* Beeldverhouding: 16:9
* Vermogen: 60 W
* Kijkhoek: horizontal/verticaal: 178°/178°, rechts/links: 89°/89°, naar boven/onderen: 89°/89°
* Afmetingen: 985 x 577.5 x 68mm
* Gewicht: 23 KG

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Decors

Dit component wordt gebruik in de decors:

{{< pagelist decors >}}

## Bijlagen

{{< pagelist bijlagen >}}
