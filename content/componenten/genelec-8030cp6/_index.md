---
title: "genelec-8030cp6"
draft: true
merk: Genelec
model: 8030CP6
interactives:
- oersteen
attachments:
- title: Gebruikers Handleiding
  src: https://files.museum.naturalis.nl/s/25FWnwetJCeBYLm
- title: Lijn Tekening PDF
  src: https://files.museum.naturalis.nl/s/pdRwPKQyA5nj3yK
- title: Lijn Tekening DWG
  src: https://files.museum.naturalis.nl/s/9mew5y9LQbzPtb6
- title: Technische Specificaties
  src: https://files.museum.naturalis.nl/s/AC9kDWLpk2i3p94
---

![genelec-8030cp6](./genelec-8030cp6.png "genelec-8030cp6")
![genelec-8030cp6-back](./genelec-8030cp6-back.png "genelec-8030cp6-back")

The bi-amplified GENELEC 8030C is a two way active monitoring loudspeaker designed to be small, but still have high output, low coloration, and broad bandwidth.

## Eigenschappen

* RMS vermogen: 50 W
* Frequentierespons: 54 Hz - 20 kHz (± 2 dB)
* Type connector: Analog signal input connector XLR female, balanced 10 kOhm
* Actief of passief: actief

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
