---
title: "visaton-fr10"
draft: true
merk: Visaton
model: FR10 - 8 Ohm
interactives:
- darwinvinken
- devisinjou
- dnaspel
attachments:
- title: Technische Specificaties
  src: https://files.museum.naturalis.nl/s/YrA7YDQTqgPzzsf
---

![visaton-fr10](./visaton-fr10.jpg "visaton-fr10")

10 cm (4“) fullrange speaker with good bass reproduction, balanced frequency response and high
efficiency. Especially suitable as built-in speaker for music reproduction and as driver for 100 V network
column speakers.

## Eigenschappen

* RMS vermogen: 30 W
* Nominale impedantie Z: 8 Ohm
* Frequentierespons: 80–20000 Hz
* Type connector: 5.2 x 0.5 mm (+)/2.8 x 0.5 mm (-)
* Actief of passief: passief

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
