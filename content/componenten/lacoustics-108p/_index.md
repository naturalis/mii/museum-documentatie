---
title: "lacoustics-108p"
date:
draft: false
merk: lacoustics
model: 108p
leverancier:
attachments:
- title: user manual
  src: https://files.museum.naturalis.nl/s/9zdnNmRpJwtSpM7
- title: specifications
  src: https://files.museum.naturalis.nl/s/nxZ8MR6LKA6Xq5Z
---

![108p](./108p.png)
Self-Powered Compact Coaxial

## Eigenschappen

* VErmogen en impedantie: The power amplifier provides 500W continuous (1 kHz, 0.5% THD) into 4 ohms and  250W continuous (1 kHz, 0.5% THD) into 8 ohms for powering low and high frequency transducers, respectively
* Type connector: XLR
* Type power: 2 x Powercon® 120 or 230 V selectable by switch
* Actief of passief: Actief
* Afmetingen: H x W x D: 421 x 250 x 299 mm
* Gewicht: 13 kg


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
