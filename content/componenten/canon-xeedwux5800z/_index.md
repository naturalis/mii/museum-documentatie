---
title: "canon-xeedwux5800z"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: Canon
model: xeed wux5800z
leverancier:
Interactives:
attachments:
- title: canon-xeedwux5800z_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/EGLZA5acfNzGnnC
---
![canon-xeed-wux5800](./canon-xeed-wux5800.png "canon-xeed-wux5800")
<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

* Levensduur lichtbron: 20.000 hours life
* Type lamp: WUXGA, 5800 lumens
* Projectie techniek: Laser
* ANSI lumen: 5800 / 4060 / 2900
* Resolutie: 1920 x 1200 (WUXGA)
* Beeldverhouding: 16:10
* Statische contrastverhouding: 4000:1 (full on / full off) when iris is set to close 9
* Video-in:
  * HDMI (incl Deep Colour)
  * DisplayPort
  * DVI-I 29-pin (VGA via adapter)
  * Mini D-Sub 15-pin
  * Component video (via adapter cable)
  * HDBaseT (RJ-45)

* Bediening:
  * IR Remote
  * Wifi
  * RS-232 in
  * Compatible with Crestron RoomView, AMX, PJ Link

* Verbinding (Ethernet): RJ45 HDBaseT input (1000BASE-T / 100BASE-TX / 10BASE-T)
* Afmetingen: 196 x 480 x 545 in mm
* Vermogen: (470W max)

## Technische specificaties

Infomatie over schoonmaken geleverd door [Berry Wijnen](mailto:B.Wijnen@canon.nl) van Canon Europe:

Het filter van de laser projectoren hoeft niet ieder jaar vervangen te worden. Het filter in de laserprojectoren gaat 20.000 uur mee. Het filter zit er in omdat hetzelfde chassis wordt gebruikt voor de lamp versies. Die lampversies hebben een airflow met filter. De laserversies niet maar toch zit het filter er in omdat het zelfde chassis wordt gebruikt.

De ruimte tussen de interne optiek en de panelen is stofdicht afgesloten, dus er kan eigenlijk nooit stof in beeld komen.

Filters vervangen en projectoren afstoffen kan natuurlijk nooit kwaad maar ga niet met stofzuigers aan de projector  aan de slag om beschadiging van ventilatoren en ophoping van stof te voorkomen.Die keuze is aan jullie.

Filters kun je bestellen via onze dealers zoals ATA-Tech. Onze distributeur levert niet aan eindgebruikers.

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
