---
title: "magewell-proconverthdmitondi4k"
date:
draft: false
merk: Magewell
model: Pro Convert HDMI to NDI 4K
leverancier: Streaming Valley
interactives:
- studiolab
attachments:
- title: User Manual
  src: https://files.museum.naturalis.nl/s/C3Yw24yB75zJS6T
- title: FAQ
  src: https://files.museum.naturalis.nl/s/63is8pLP7d9GYtW
- title: Technical Specifications
  src: https://files.museum.naturalis.nl/s/JRQF3AxS7oXQn9A
---

![](./proconvert.jpeg)
![](./tekening.png)

HDMI to NDI converter


## Eigenschappen

Pro Convert HDMI 4K Plus Specifications

* Afmetingen: 117.5mm (L) x 66.7mm (W) x 23.4mm (H)


* Input & Loop through Features
    * Max input & loop through signal: 4096x2160 60fps 4:4:4 8-bit
    * Input video at any format by custom EDID of input port
    * Video formats: RGB, YUV BT.601/709/2020
    * 8-channel HDMI-embedded audio
    * HDMI interfaces: HDMI 1.4/2.0, DVI-D

* NDI® Encoding Features
    * Encoding Full NDI real-time stream at up to
      * 4096x2160 60fps 4:2:2 8-bit
      * 1920x1080 240fps 4:2:2 8-bit
    * Output the same resolution as input or any custom resolution. Typical outputs include:
      * 4096x2160p/3840x2160p/2048x1080p 23.98/24/25/29.97/30/50/59.94/60
      * 1920x1080p/1280x720p 23.98/24/25/29.97/30/50/59.94/60/100/119.88/120
      * 1920x1080i 25/29.97/30/50/59.94/60
      * 720x480p/720x480i 59.94/60/119.88/120
      * 720x576p/720x576i 50/100
    * Output frame rate same as the original, 1/2, 1/3 or 1/4 of input frame rate
    * Encoding 8-channel NDI-embedded audio
    * Support NDI 4.x

* Web UI Management
    * Provide comprehensive information regarding the device and input signals in real-time
    * Allow to configure how an input is processed and encoded, including deinterlacing, EDID, up/down scaling, frame rate, aspect ratio, as well as the network connection
    * Update firmware
    * Support USB RNDIS/ECM
    * Support IE/Edge/Firefox/Chrome/Safari/Opera web browsers
    * Provide HTTP APIs

* On-board Control Buttons
    * 16 Position Rotary DIP Switch: set board-index from 0 to F

* PTZ Control
    * Control PTZ cameras using protocols of VISCA, Visca UDP, Visca UDP2rs232, PELCO-P and PELCO-D via NDI
    * Mini-DIN–8 jack (shared with Tally light)

* Network Interfaces
    * 10/100/1000Mbps Ethernet
    * IEEE 802.3af PoE

* USB Interface
    * USB2.0 Type B
      * 5V/2.1A power supply
      * USB RNDIS/ECM

* Supported Products
    * NDI Studio Monitor
    * OBS
    * vMix
    * Any other NDI-enabled product

* LED Indicators
    * Status LEDs indicate:
      * Power supply: on/off
      * Tally: preview/program
      * Input signal: locked/unlocked
      * Loop through signal: device connected and signal detected/undetected

* Power Consumption
    * 5V max current: ~2A
    * Max power consumption: ~10W

* Working Environment
    * Operating temperature: 0 to 45 deg C
    * Storage temperature: –20 to 70 deg C
    * Relative humidity: 5% to 90% non-condensing

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
