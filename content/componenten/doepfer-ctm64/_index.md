---
title: "doepfer-ctm64"
date: 2019-12-03T14:50:03+01:00
draft: false
merk: Doepfer
model: CTM64
leverancier:
interactives:
- orgel
attachments:
- title: doepfer-ctm64_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/dmDKi7jyJFYzZDd
---

![CTM64](./CTM64.png "CTM64")
Contact To Midi Converter

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte:
  * Breedte:
  * Diepte:

* Gewicht:

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
