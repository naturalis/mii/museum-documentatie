---
title: "pharos-lpc1"
date: 2019-12-03T14:50:07+01:00
draft: false
merk: Pharos
model: LPC1
leverancier: Ata Tech
shows:
- showdinotijd
- showjapanstheater
- showlivescience
- showrexperience
- showonschatbaar
interactives:
- studiolab
attachments:
- title: pharos-lpc1_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/oZkGaSddiPxFGdw
- title: Pharos Installation Guide (pdf)
  src: https://files.museum.naturalis.nl/s/PSqzQW5QbrLdTQT
---

![pharos-lpc](pharos_lpc.png)
The Pharos LPC (Lighting Playback Controller) is an award-winning, all-in-one control solution for the med entertainment and LED lighting installations.

## Eigenschappen

* Vermogen: 4W
* Afmetingen:

  * Hoogte: 58 mm
  * Breedte: 143,6 mm
  * Diepte: 90 mm

* Gewicht: 0,5 kg

## Technische specificaties

Lighting Playback Controller 1: 512 channels DMX/eDMX

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
