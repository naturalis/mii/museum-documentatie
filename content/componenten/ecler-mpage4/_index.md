---
title: "ecler-mpage4"
date:
draft: false
merk: Ecler
model: MPAGE4
leverancier:
shows:
- showrexperience
attachments:
- title: rexperience_hardware_avh_omroepinstallatie_speakers_handleiding_ecler_mpage4 (pdf)
  src: https://files.museum.naturalis.nl/s/iByCiYxS2AbMYP7
- title: Ecler_MPAGE4_Data_Sheet (pdf)
  src: https://files.museum.naturalis.nl/s/zYyaqLSxX3dxgrq
---

![page4](./page4.jpg)
ECLER MPAGE4 is a 4-zone desktop paging station compatible with DAM614, CA series, etc. It allows for real-time paging with zones' destination selection.

## Eigenschappen

* Afmetingen (zonder mic):

  * Hoogte: 43 mm
  * Breedte: 120 mm
  * Diepte: 126 mm

* Gewicht: 625 g
* Microfoon type: Non-removable gooseneck condenser microphone
* Connector: RJ45

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
