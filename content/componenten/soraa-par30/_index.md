---
title: "soraa-par30"
date:
draft: false
merk: Soraa
model: PAR30
leverancier: Q-Cat
attachments:
- title: Soraa PAR30
  src: https://files.museum.naturalis.nl/s/mE8gxnKxWmRTEqL
---

![par30](./par30.png)


## Eigenschappen



## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
