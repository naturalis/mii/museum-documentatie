---
title: "skaarhoj-ptzpro"
date:
draft: false
merk: Skaarhoj
model: PTZ Pro
leverancier: ALV
interactives:
- studiolab
attachments:
- title: Skaarhoj Manual
  src: https://files.museum.naturalis.nl/s/XwY5sMprp9q98fG
- title: Skaarhoj PTZ Control
  src: https://files.museum.naturalis.nl/s/Wswakp3qLLo5FyG
---

![](./PTZPro.jpeg)

PTZ controller
## Eigenschappen

* Afmetingen:

  * Hoogte: 72 mm
  * Breedte: 258 mm
  * Diepte: 178 mm

* Gewicht: 1.208 kg

* Power: 5.71 Watt

## Known issues

De PTZ Pro kan alleen communiceren op 100Mb/s - halfduplex. De huidige switches in het museum kunnen niet zo 'laag'. Althans, daar lijkt het op. Als tussen oplossing kan je een switchje er tussen hangen. Bijvoorbeeld een instap model Netgear.

Met Marten afgesproken dat het tussen switchje in beheer is bij TT, niet bij infra.

Software om hem te installeren: [firmware-updater](https://www.skaarhoj.com/support/firmware-updater)

## Interactives

Dit onderdeel wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
