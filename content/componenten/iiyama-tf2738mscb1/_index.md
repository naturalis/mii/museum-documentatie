---
title: "iiyama-tf2738mscb1"
date:
draft: false
merk: Iiyama
model: ProLite TF2738MSC-B1
leverancier: ATA-tech
interactives:
  - bewakingrexperience
attachments:
- title: Gebruikers Handleiding
  src: https://files.museum.naturalis.nl/s/yzQ5wba3XPdmfP4
- title: Technische Specificaties
  src: https://files.museum.naturalis.nl/s/cSr6SaFpg59nEC9
---

![tf2738mscb1](./tf2738mscb1.png "tf2738mscb1")
27” 10pt open frame touch monitor met volledig vlakke voorkant

## Eigenschappen

* Video-in:
  * 1 x DVI
  * 1 x HDMI
  * 1 x DisplayPort
* Bediening:
  * touch (on screen menu)
* Schermdiagonaal: 27", 68.6cm
* Resolutie: 1920 x 1080 (2.1 megapixel Full HD)
* Beeldverhouding: 16:9
* Vermogen: 30 W
* Kijkhoek: horizontal/verticaal: 178°/178°, rechts/links: 89°/89°, naar boven/onderen: 89°/89°
* Afmetingen:

  * Hoogte: 386,5 mm
  * Breedte: 648,5 mm
  * Diepte: 50 mm

* Gewicht: 7,7 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}


## Bijlagen

{{< pagelist bijlagen >}}
