---
title: "vistaton-frs8"
date: 2019-12-03T14:50:10+01:00
draft: false
merk: vistaton
model: frs8
leverancier:
interactives:
- camperhawaii
- datingquiz
- flipperkast
- greenporn
- knaagdierontvangsthal
- sexystories
- vogelsvoeren
- waternesten
attachments:
- title: vistaton-frs8_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/R3KekdBEaEdranz
---

![frs8](./frs8.png)
8 cm (3.3") HiFi fullrange driver.

## Eigenschappen

* Vermogen: 30 W / max 50 W
* Afmetingen:

  * Hoogte: 40 mm
  * diameter: 84 mm

* Gewicht: 0,28 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

{{< pagelist interactives >}}


## Bijlagen

{{< pagelist bijlagen >}}
