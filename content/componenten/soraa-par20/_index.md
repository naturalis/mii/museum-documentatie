---
title: "soraa-par20"
date:
draft: false
merk: Soraa
model: PAR20
leverancier: Q-Cat
attachments:
- title: Soraa PAR20
  src: https://files.museum.naturalis.nl/s/ZxQWAYQ62LRj9G9
---

![par20](./par20.png)


## Eigenschappen



## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
