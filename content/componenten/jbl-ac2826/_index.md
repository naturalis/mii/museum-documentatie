---
title: "jbl-ac2826"
date:
draft: false
merk: jbl
model: AC28/26
shows:
- showrexperience
leverancier:
attachments:
- title: Array Application Guide
  src: https://files.museum.naturalis.nl/s/qnKD42BdJCSMDjE
- title: Technische specificaties
  src: https://files.museum.naturalis.nl/s/BHpy5YQodBHHRRz
---

![ac2826](./ac2826.jpg)
Compact 2-way Loudspeaker System with 2 - 8” LF

## Eigenschappen
* Vermogen: 700 W
* Imperdatie: 8 Ohms
* Afmetingen:

  * Hoogte: 679,5 mm
  * Breedte: 238 mm
  * Diepte: 254 mm

* Gewicht: 18,6 kg

## Technische specificaties

* Input Connectors: Neutrik Speakon®

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
