---
title: "hikvision-ds1272zj110"
date: 2019-12-03T14:50:04+01:00
draft: false
merk: hikvision
model: ds2cd2125fwdis
leverancier:
attachments:
- title: gegevensblad
  src: https://files.museum.naturalis.nl/s/5JacbHkYYn4Nq2F
---

![hikvision-ds1272zj110](./hikvision-ds1272zj110.jpeg)
Wall Mounting Bracket for Dome Camera

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte: 112 mm
  * Breedte: 12 mm
  * Diepte: 169 mm

* Gewicht: 480 g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
