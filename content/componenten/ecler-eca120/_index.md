---
title: "ecler-eca120"
date: 2019-12-03T14:50:03+01:00
draft: false
merk: Ecler
model: Eca120
leverancier:
- Ata Tech
interactives:
- camarasaurus
- trex
- stegosaurus
- orgel
- plateosaurus
- letsdance
- introjapanstheater
- triceratops
- naturalisdigitaal
- edmontosaurus
- bewakingjapanstheater
- cabinijsland
- spermarace
shows:
- showleven
attachments:
- title: ecler-eca120_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/rCb2XEg6FEBgDGk
- title: ecler-eca120_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/eDitebx5dFM23AE
---

{{% notice warning %}}
Aandachtspunt:
In verband met de ondersteuning van externe bediening over RS-232 middels CA-NET is de Ecler CA120 het alternatief bij nieuwe aanschaf.
{{% /notice %}}

![Versterker](ecler-eca120.jpg)

Audio versterker van het merk Ecler.

## Eigenschappen

* Vermogen: 2.5 Watt (standby) tot 51 Watt (luid)
* Afmetingen:

  * Hoogte:  48mm
  * Breedte: 190mm
  * Diepte: 90mm

* Gewicht: 600g

## Technische specificaties

Zie bijlage.

## Known issues

Op dit moment zijn er geen known issues bekend.

## Interactives

Wordt gebruikt in deze interactives.

{{< pagelist interactives >}}

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
