---
title: "proled-diversen"
date:
draft: false
merk: PROLED
model: Diversen
leverancier: de Cirkel
attachments:
- title: Proled Aluminium Profile M-Line Corner (pdf)
  src: https://files.museum.naturalis.nl/s/YNR5o39sRyPyHca
- title: Proled Aluminium Profile M-Line Standard (pdf)
  src: https://files.museum.naturalis.nl/s/8pcADq7fKE2xC2b
- title: Proled Aluminium Profile S-Line Corner (pdf)
  src: https://files.museum.naturalis.nl/s/dPZ7EPkwQeA3zEL
- title: Proled Aluminium Profile S-Line Low (pdf)
  src: https://files.museum.naturalis.nl/s/m2Sb7DL96qjsXDd
- title: Proled DMX PWM dimmer 4x5A (pdf)
  src: https://files.museum.naturalis.nl/s/PQTdyJNfPrBbZDp
- title: Proled DMX PWM dimmer 32ch (pdf)
  src: https://files.museum.naturalis.nl/s/QGdB2CFetAcGp79
- title: Proled DMX PWM Dimmer RGB 3x3A (pdf)
  src: https://files.museum.naturalis.nl/s/NyHQCpaTKb2AwDG
- title: Proled Flex Strip 600 95 Extra Bright (pdf)
  src: https://files.museum.naturalis.nl/s/JAConRygfNGAaz5
- title: Proled Flex Strip 600 95 (pdf)
  src: https://files.museum.naturalis.nl/s/iHGnP6sa2iSgtAA
- title: Proled Flex Strip 600 Mono (pdf)
  src: https://files.museum.naturalis.nl/s/x4zS8orQyDbKzdn
- title: Proled Flex Strip HD 80 (pdf)
  src: https://files.museum.naturalis.nl/s/3sHwZC3cojDEW7T
- title: Proled Flex Strip RGBW 4in1 2700K (pdf)
  src: https://files.museum.naturalis.nl/s/Js5ZGrs8LgDmXbe
- title: Proled Panel RGBW (pdf)
  src: https://files.museum.naturalis.nl/s/QKkMKazz47fQaE6
---

[![Proled Aluminium Profile M-Line Corner](./corner.png)](https://files.museum.naturalis.nl/s/YNR5o39sRyPyHca)
Proled Aluminium Profile M-Line Corner
[![Proled Aluminium Profile M-Line Standard](./mstandard.png)](https://files.museum.naturalis.nl/s/8pcADq7fKE2xC2b)
Proled Aluminium Profile M-Line Standard
[![Proled Aluminium Profile S-Line Corner](./scorner.png)](https://files.museum.naturalis.nl/s/dPZ7EPkwQeA3zEL)
Proled Aluminium Profile S-Line Corner
[![Proled Aluminium Profile S-Line Low](./slow.png)](https://files.museum.naturalis.nl/s/m2Sb7DL96qjsXDd)
Proled Aluminium Profile S-Line Low
[![Proled DMX PWM dimmer 4x5A](./dimmer45.png)](https://files.museum.naturalis.nl/s/PQTdyJNfPrBbZDp)
Proled DMX PWM dimmer 4x5A
[![Proled DMX PWM dimmer 32ch](./dimmer32.png)](https://files.museum.naturalis.nl/s/QGdB2CFetAcGp79)
Proled DMX PWM dimmer 32ch
[![Proled DMX PWM Dimmer RGB 3x3A](./dimmer33.png)](https://files.museum.naturalis.nl/s/NyHQCpaTKb2AwDG)
Proled DMX PWM Dimmer RGB 3x3A
[![Proled Flex Strip 600 95 Extra Bright](./strip60095extra.png)](https://files.museum.naturalis.nl/s/JAConRygfNGAaz5)
Proled Flex Strip 600 95 Extra Bright
[![Proled Flex Strip 600 95](./strip60095.png)](https://files.museum.naturalis.nl/s/iHGnP6sa2iSgtAA)
Proled Flex Strip 600 95
[![Proled Flex Strip 600 Mono](./strip600mono.png)](https://files.museum.naturalis.nl/s/x4zS8orQyDbKzdn)
Proled Flex Strip 600 Mono
[![Proled Flex Strip HD 80](./striphd80.png)](https://files.museum.naturalis.nl/s/3sHwZC3cojDEW7T)
Proled Flex Strip HD 80
[![Proled Flex Strip RGBW 4in1 2700K](./striprgbw41.png)](https://files.museum.naturalis.nl/s/Js5ZGrs8LgDmXbe)
Proled Flex Strip RGBW 4in1 2700K
[![Proled Panel RGBW](./panel.png)](https://files.museum.naturalis.nl/s/QKkMKazz47fQaE6)
Proled Panel RGBW

## Eigenschappen


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
