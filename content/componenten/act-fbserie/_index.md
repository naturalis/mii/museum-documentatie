---
title: "act-fbserie"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: ACT
model:
leverancier:
attachments:
- title: act-fbserie_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/fL5c4WHWFasCp54
- title: act-fbserie_technischetekening.pdf
  src: https://files.museum.naturalis.nl/s/JtyfSfgaeF2FR68
---

![cast6a](./cat6a.png)

<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

* Merk: ACT
* Type: CAT6A S/FTP PiMF patch cable
* Connector: RJ45
* Lengte: 0,50cm - 30m
* Kabeldikte:
* Leverancier:

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
