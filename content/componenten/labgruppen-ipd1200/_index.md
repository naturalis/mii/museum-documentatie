---
title: labgruppen-ipd1200
date: 
draft: false
merk: Lab Gruppen
model: IPD 1200
leverancier: ALV Productions
faciliteiten:
- demosteensmelten
attachments:
- title: IPD1200_Data_Sheet.pdf
  src: https://files.museum.naturalis.nl/s/J9xQo8oSQkHsEqx
- title: IPD1200-Brochure.pdf
  url: https://files.museum.naturalis.nl/s/dS7cLbLPwPQEfGW
- title: IPD-Series_Operation_Manual.pdf
  url: https://files.museum.naturalis.nl/s/QcqtXpiPEoz3C3c
---

![IPD 1200](./IPD-1200_P0DNV_Front_XL.png)
Digitale eindtrap

## Eigenschappen

* Vermogen: 
* Afmetingen:

  * Hoogte: 
  * Breedte: 
  * Diepte: 

* Gewicht: 

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Faciliteiten

{{< pagelist faciliteiten >}}

## Bijlagen

{{< pagelist bijlagen >}}
