---
title: "smartmetals-0022070"
date: 2019-12-03T14:50:09+01:00
draft: false
merk: smartmetals
model: 0022070
leverancier:
attachments:
- title: smartmetals-0022070_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/iFCZtnXjHiYJjoi
---

![smartmetals-0022070](./0022070.png)
L5: 930 - 1580 mm

## Eigenschappen

* Vermogen: 25kg
* Afmetingen:

  * Hoogte: 930 - 1580 mm
  * Breedte:
  * Diepte:

* Gewicht: 3,85kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
