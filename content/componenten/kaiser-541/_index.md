---
title: "kaiser-541"
date: 2019-12-03T14:50:05+01:00
draft: false
merk: Kaiser
model: 541
leverancier: Ata Tech
interactives:
- naturalisdigitaal
attachments:
- title: kaiser-541_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/46oTBeg7JCbeQ4k
---

![kaiser-541](./kaiser-541.png "kaiser-541")
Vierpunt stekkerdoos met aarde.

## Eigenschappen

* Afmetingen:

  * Hoogte:  42 mm
  * Breedte: 185.5 mm
  * Diepte:  50.5 mm

* Gewicht: 143 g

## Technische specificaties

250 Volt wisselstroom, 16 Ampere

## Interactives

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
