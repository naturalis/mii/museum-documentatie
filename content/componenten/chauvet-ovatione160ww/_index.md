---
title: chauvet-ovatione160ww
date:
draft: false
merk: Chauvet
model: Ovation E-160WW
leverancier: Lichtpunt
attachments:
- title: Chauvet Ovation E-160WW (pdf)
  src: https://files.museum.naturalis.nl/s/YKr66AGzixx2ADr
- title: Chauvet Ovation E-160WW DMX (pdf)
  src: https://files.museum.naturalis.nl/s/c3Ax3ACKFMEyTfz
- title: Chauvet Ovation E-160WW manual (pdf)
  src: https://files.museum.naturalis.nl/s/6T7AzNRwrs4HsYo
- title: Chauvet Ovation E-160WW Quick (pdf)
  src: https://files.museum.naturalis.nl/s/95KzBsmSwY2HiJF
---

![ovatione160ww](./ovatione160ww.png)
The Ovation E-160WW delivers an excellent white light with a warm color temperature and a beautiful flat field. It also features standard beam shaping shutters, a gobo/effect slot and lens barrels that are interchangeable with other popular ERS fixtures. Selectable dimming curves ensure smooth fading cues that intermix well with the output of conventional theatre ellipsoidals.

## Eigenschappen

* Dimensions (LxWxH): 495 mm x 285 mm x 487 mm
* Weight: 6.4 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
