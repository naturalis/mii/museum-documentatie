---
title: "apple-macmini2020"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: Apple
model: Mac Mini 2020
leverancier:
interactives:
  - oersteen
attachments:
- title: apple-macmini_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/XtEQax8RPTCFH7B
---


{{% notice warning %}}
Aandachtspunt:
Dit is ondertussen een verouderd model. Specs hieronder zijn w.s. niet meer te koop.
{{% /notice %}}

![macmini2020](./macmini2020.png "macmini2020")

## Eigenschappen

* ID: Macmini9.1
* Naam Mac Mini
* Merk: Apple
* Model: A2348

## Technische specificaties

* Uitvoering
  * Zilver

* Chip
  * Apple M1‑chip
    8‑core CPU met 4 performance-cores en 4 efficiency-cores
    8‑core GPU
    16‑core Neural Engine

* Geheugen
  * 8 GB
    8 GB centraal geheugen
    Configureerbaar met: 16 GB

* Opslag

  * 256 GB
    SSD van 256 GB
    Configureerbaar met: SSD van 512 GB, 1 TB of 2 TB

* Video-ondersteuning

  * Gelijktijdige ondersteuning voor maximaal twee displays:
    Eén display met een resolutie tot 6K bij 60 Hz dat is aangesloten via Thunderbolt en één display met een resolutie tot 4K bij 60 Hz dat is aangesloten via HDMI
  * Digitale video-uitvoer via Thunderbolt 3 ondersteunt:
    Native DisplayPort-uitvoer via USB‑C
    Thunderbolt 2-, DVI- en VGA-uitvoer via adapters (afzonderlijk verkrijgbaar)
  * HDMI-display-uitvoer
    Ondersteuning voor één display met een resolutie tot 4K bij 60 Hz
    DVI-uitvoer via HDMI-naar-DVI-adapter (afzonderlijk verkrijgbaar)

* Audio

  * Ingebouwde speaker
  * Mini‑jack-aansluiting
  * HDMI-poort ondersteunt meerkanaalsaudio-uitvoer

* Aansluitingen en uitbreiding

  * Twee Thunderbolt/USB 4-poorten met ondersteuning voor:
    * DisplayPort
    * Thunderbolt 3 (tot 40 Gb/s)
    * USB 3.1, 2e gen. (tot 10 Gb/s)
    * Thunderbolt 2-, HDMI-, DVI- en VGA-uitvoer via adapters (afzonderlijk verkrijgbaar)
  * Twee USB‑A-poorten (tot 5 Gb/s)
  * HDMI-poort
  * Gigabit Ethernet-poort (configureerbaar met 10‑Gb Ethernet)
  * Mini‑jack-aansluiting

* Communicatie

  * Wifi
    * Draadloze 802.11ax wifi 6-voorziening
    * Compatibel met IEEE 802.11a/b/g/n/ac
  * Bluetooth
    * Draadloze Bluetooth 5.0-technologie
  * Ethernet
    * 10/100/1000BASE‑T Gigabit Ethernet (RJ‑45-connector)
    * Configureerbaar met 10‑Gb Ethernet (Nbase‑T Ethernet met ondersteuning voor 1‑Gb, 2,5‑Gb, 5‑Gb en 10‑Gb Ethernet via RJ‑45-connector)

* Afmetingen en gewicht

  * Hoogte: 3,6 cm
  * Breedte: 19,7 cm
  * Diepte: 19,7 cm
  * Gewicht: 1,2 kg

* Omgevingsvereisten

  * Netspanning: 100‑240 V wisselstroom
  * Frequentie: 50‑60 Hz, enkelfasig
  * Maximaal continu vermogen: 150 W
  * Temperatuur bij gebruik: 10 tot 35 °C
  * Temperatuur bij opslag: -40 tot 47 °C
  * Relatieve luchtvochtigheid: 5 tot 90%, niet‑condenserend
  * Hoogte bij gebruik: getest tot 5000 m
  * Standaard akoestische prestaties: Geluidsdrukniveau (positie van de bediener): 5 dBA (indien niet in gebruik)

* Inhoud verpakking

  * Mac mini
  * Netsnoer

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
