---
title: "zotaczbox-magnusone"
draft: true
merk: Zotac
model: Zbox magnus one
leverancier: Azerty
attachments: 
- title: zotac_zboxmagnusone.pdf
  src: https://files.museum.naturalis.nl/s/JPiX3r9SjZ9eTnM
---

![zotac_zbox](./zotac_zbox.jpg "zotac_zbox")
Mini-pc met ZOTAC GAMING GeForce® RTX 3060 grafische kaart

## Eigenschappen

* Afmetingen:

  * Hoogte: 249 mm
  * Breedte: 265.5 mm
  * Diepte: 126 mm


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
