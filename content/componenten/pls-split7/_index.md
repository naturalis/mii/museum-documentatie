---
title: "pls-split7"
date:
draft: false
merk: SLP
model: Split-7
leverancier:
attachments:
- title: PLS Split7 Usermanual
  src: https://files.museum.naturalis.nl/s/Gf48NzWBiXacjSa
- title: pls2017-v1-web
  src: https://files.museum.naturalis.nl/s/gTKJY6Tpyi3w6fE
---

![Split-7](./Split-7.png)
DMX splitter with seven optically isolated outputs with separate power supply.

## Eigenschappen

* Afmetingen:
  * Diepte: 160 mm
  * Hoogte: 44 mm
  * Breedte: 484 mm
* Weight: 2,7 gr


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
