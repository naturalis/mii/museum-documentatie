---
title: "eldoled-powerdrive562S"
date:
draft: false
merk: eldoLED
model: POWERdrive 562S
leverancier:
attachments:
- title: EldoLED POWERdrive 562S 3ch (pdf)
  src: https://files.museum.naturalis.nl/s/PtmfJ7Pcj5TAgyX

---

![powerdrive562S](./powerdrive562S.png)
50W DMX/RDM Full-Colour (RGB) Dimmable LED Driver

## Eigenschappen

* Afmetingen:
  * Diepte: 130 mm
  * Hoogte: 30 mm
  * Breedte: 76 mm
* Gewicht: 350 gr


## Technische specificaties

POWERdrive 561/S delivers 50W and is DMX compatible. POWERdrive 561 has four separately controllable LED outputs, which can be used for wide range of full-colour (RGBW), dynamic LED lighting applications.

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
