---
title: "martinaudio-cdd5"
date:
draft: false
merk: Martin Audio
model: CDD5
leverancier:
shows:
- showrexperience
attachments:
- title: rexperience_hardware_avh_speakers_handleiding_martinaudio_cdd (pdf)
  src: https://files.museum.naturalis.nl/s/EB5rd9YJnKmTYs4
- title: CDD5datasheet (pdf)
  src: https://files.museum.naturalis.nl/s/Wkj9waPnEtT9JCN
---

![cdd5](./cdd5.png)
Ultra-Compact Coaxial Differential Dispersion System
Passive luidspreker

## Eigenschappen

* Afmetingen: (W) 160mm x (H) 230mm x (D) 149mm

* Gewicht: 3 kg

* Rated Power: 100W AES, 400W peak

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
