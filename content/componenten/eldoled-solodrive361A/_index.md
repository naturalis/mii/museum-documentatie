---
title: "eldoled-solodrive361A"
date:
draft: false
merk: eldoLED
model: SOLOdrive 361A
leverancier:
attachments:
- title: EldoLED SOLOdrive 361A (pdf)
  src: https://files.museum.naturalis.nl/s/4py77ojrGdFkf8a

---

![solodrive361A](./solodrive361A.png)
30W 0-10V 'Dim to Dark' LED Driver

## Eigenschappen

* Afmetingen:
  * Diepte: 210 mm
  * Hoogte: 33,5 mm
  * Breedte: 40 mm
* Gewicht: 204 gr


## Technische specificaties

SOLOdrive 361/A delivers 30W and is 0-10V compatible. The end caps function as wire restraints and make the driver perfectly suited for independent installations. As all SOLOdrives, the 361/A smoothly dims all the way down to dark.

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
