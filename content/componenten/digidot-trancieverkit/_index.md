---
title: "digidot-trancieverkit"
date:
draft: false
merk: DIGIdot
model: Tranciever Kit 48
leverancier:
attachments:
- title: DiGidot Tranciever Kit 48 (pdf)
  src: https://files.museum.naturalis.nl/s/RtBYicd96XDxpR6
---
![trancieverkit](./trancieverkit.png)
Receiver module for LED strips

## Eigenschappen

* Afmetingen: 43.8 x 15.8 x 15.3 mm

* Gewicht: 6 gr

## Technische specificaties

SPI protocols are sensitive to data distortion and often only work safely up to 2 meters (6.5 ft) cable distance. The DiGidot TRxB s a small size receiver module that can be used in combination with DiGidot C4 and a DiGidot Transmitter module. It allows sensitive SPI protocols to be send over huge distances, up to 250 meters. Any single wire SPI protocol (Data only) that can be outputted from a DiGidot C4 can be received. The input can be connected easily with a RJ45 connector or it can be soldered on the underside. The output can directly be soldered to most industry standard SPI controlled LED strips.

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
