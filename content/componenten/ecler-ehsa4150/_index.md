---
title: "ecler-ehsa4150"
date:
draft: false
merk: Ecler
model: eHSA4-150
leverancier: ATA Tech
shows:
- showrexperience
attachments:
- title: Ecler_eHSA4-150_Data_Sheet (pdf)
  src: https://files.museum.naturalis.nl/s/DAxeXSrD2Zf8cY5
- title: Ecler_eHSA_2-4-150_User_Manual_EN (pdf)
  src: https://files.museum.naturalis.nl/s/nPzBNnsCEBSTBra
---

![Ecler-eHSA4-150](./Ecler-eHSA4-150.jpg)
![Ecler-eHSA4-150-rear](./Ecler-eHSA4-150-rear.jpg)
High Impedance Multichannel Amplifier
ECLER ESSENTIALS eHSA4-150 is a multichannel amplifier with high impedance powered outputs. A very high efficiency and its auto stand-by function provide this amplifier with a true green profile

## Eigenschappen

* Afmetingen: 440x44x341mm

* Gewicht: 10,5 kg
* Vermogen: 4 x 150 W RMS output (70/ 100V line level)
## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
