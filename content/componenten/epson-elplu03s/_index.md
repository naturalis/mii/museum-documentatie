---
title: "canon-lens"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: Epson
model: ELPLU03S
leverancier:
Interactives:
- cirkelprojectie
attachments:
- title: Epson ELPLU03S.pdf
  src: https://files.museum.naturalis.nl/s/md2pm5ny65FkHnd
---

![epson-elplu03s](./epson-elplu03s.png "epson-elplu03s")
<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

0.65 to 0.78 WUXGA/WXGA throw ratio; lens shift—vertical: -67 percent to +67 percent, horizontal: -30 percent to +30 percent

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
