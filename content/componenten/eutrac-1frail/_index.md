---
title: "eutrac-1frail"
date:
draft: false
merk: Eurtrac
model: 1F rail
leverancier: CLS en Q-Cat
attachments:
- title: Eutrac 1F Rail Data (pdf)
  src: https://files.museum.naturalis.nl/s/MN2wAs349CB5NNE

---

![1frail](./1frail.png)

## Eigenschappen




## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
