---
title: "barco-cse200"
date:
draft: false
merk:  barco
model: CSE-200
leverancier:
attachments:
- title: ClickShare CSE-200.pdf
  src: https://files.museum.naturalis.nl/s/kkc25EPtidXQgK4
---

![cse200](./cse200.png)


## Eigenschappen

* Vermogen: 6W (typisch) / 18W (max)
* Afmetingen (excl antenne):

  * Hoogte: 205 mm
  * Breedte: 115 mm
  * Diepte: 45 mm

* Gewicht: 600 g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
