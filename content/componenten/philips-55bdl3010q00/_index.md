---
title: "philips-55bdl3010q00"
date:
draft: false
merk: philips
model: 55bdl3010q00
leverancier: ATA-tech
faciliteiten:
- signageservicebalie
attachments:
- title: Technische specificaties
  src: https://files.museum.naturalis.nl/s/QsmmYLx8syA26QX
---

![55bdl3010q00](./55bdl3010q00.png)
Uitblinker
Eenvoudig in te stellen 18/7-display.

## Eigenschappen

* Video-in:
  * 1 x DVI-I
  * 4 x HDMI
* Bediening:
  * RJ45
  * RS232C
  * IR remote
* Schermdiagonaal: 54,6 inch /  138,7 cm
* Resolutie: 3840 x 2160
* Beeldverhouding: 16:9
* Vermogen: 125 W
* Kijkhoek:  178 /  178 graa
* Afmetingen:

  * Hoogte: 711,6 mm
  * Breedte: 1239,2 mm
  * Diepte:  61,71 mm

* Gewicht: 15,82 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
