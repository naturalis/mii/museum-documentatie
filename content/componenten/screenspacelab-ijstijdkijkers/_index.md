---
title: screenspacelab-ijstijdkijkers
date: 2019-12-10T14:33:18+01:00
draft: false
merk:
model:
interactives:
- kijkersbrabant
- kijkersdrenthe
- kijkersfriesland
- kijkersgelderland
- kijkersholland
leverancier:
attachments:
- title: screenspacelab-ijstijdkijkers_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/wx2xWEaN4Bk3Tpx
- title: bouwtekening ijstijdkijker (pdf)
  src: https://files.museum.naturalis.nl/s/TcYXg6zcnHjctAg
- title: Handleiding AV hardware software IJstijd (pdf)
  src: https://files.museum.naturalis.nl/s/tRNQnnFXoon5J9j
---

![ijstijdkijker](./ijstijdkijker.png)
## Eigenschappen


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

Er staat een firmware update klaar op de content server.
Bij het terugplaatsen van de palen na Coronatijd moeten we de update draaien.

Meer info over het updaten van de firmware: [Handleiding AV hardware software IJstijd](https://files.museum.naturalis.nl/s/tRNQnnFXoon5J9j)

Firmware: [staat hier](https://files.museum.naturalis.nl/s/wCqsS3WDBKJk82R)

## Interactives

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
