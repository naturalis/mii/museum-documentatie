---
title: "iiyama-tf3239mscb1ag"
date: 2019-12-03T14:50:04+01:00
draft: true
merk: Iiyama
model: ProLite TF3239MSC-B1AG
leverancier:
interactives:
  - dnaspel
attachments:
- title: Gebruikers Handleiding
  src: https://files.museum.naturalis.nl/s/w4KWYoi4SBx7e9i
- title: Technische Specificaties
  src: https://files.museum.naturalis.nl/s/fa6yGCRX7CFYAo8
---

![iiyama-tf3239mscb1ag](./iiyama-tf3239mscb1ag.png "iiyama-tf3239mscb1ag")
Open Frame PCAP 12 point touch screen voorzien van een schuimrubberen afdichting vooreen naadloze (kiosk) integratie


## Eigenschappen

* Video-in:
  * 1 x Displayport
  * 2 x HDMI
  * 1 x VGA
* Bediening:
  * RS-232c x1
  * RJ45 (LAN) x1
* Schermdiagonaal: 31.5", 80cm
* Resolutie: 1920 x 1080 (2.1 megapixel Full HD)
* Beeldverhouding: 16:9
* Vermogen: 60 W
* Kijkhoek: horizontal/verticaal: 178°/178°, rechts/links: 89°/89°, naar boven/onderen: 89°/89°
* Afmetingen: 738.7 x 439,9 x 67mm
* Gewicht: 13,4 KG

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Decors

Dit component wordt gebruik in de decors:

{{< pagelist decors >}}

## Bijlagen

{{< pagelist bijlagen >}}
