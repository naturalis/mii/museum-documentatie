---
title: "dataton-watchpax20"
date: 2019-12-03T14:50:03+01:00
draft: false
merk: Dataton
model: Watchpax 20
leverancier:
interactives:
- cirkelprojectie
- projectiewaterplaats
attachments:
- title: dataton-watchpax20_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/NAZB6qJtBDZB5Xi
- title: dataton-watchpax20_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/7MRD7gnSPz6ndLS
---

![watchpax20](./watchpax20.jpg "watchpax20")
Plug-and-play media server with built-in WATCHOUT.

## Eigenschappen

* Afmetingen:

  * Hoogte: 127 mm
  * Breedte: 127 mm
  * Diepte: 23 mm

## Features

  * 256 Gigabyte storage
  * 2 x Mini DisplayPort outputs
  * 3.5 mm stereo audio out
  * Gigabit Ethernet connector
  * USB-C port (compatible USB 3.0)

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

  Dit component wordt gebruik in de interactives:

  {{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
