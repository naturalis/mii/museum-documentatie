---
title: "iiyama-tf2738mscb2"
date:
draft: true
merk: Iiyama
model: ProLite TF2738MSC-B2
leverancier: Kloosterboer
interactives:
  - devisinjou
attachments:
- title: Gebruikers Handleiding
  src: https://files.museum.naturalis.nl/s/YFZoEiJ5e5bnzyn
- title: Technische Specificaties
  src: https://files.museum.naturalis.nl/s/iisWP5RfPwbQ9ST
---

![iiyama-tf2738mscb2](./iiyama-tf2738mscb2.png "iiyama-tf2738mscb2")
27” 10pt open frame touch monitor met IPS-paneel en edge-to-edge glas

## Eigenschappen

* Video-in:
  * 1 x DVI
  * 1 x HDMI
  * 1 x DisplayPort
* Bediening:
  * touch (on screen menu)
* Schermdiagonaal: 27", 68.6cm
* Resolutie: 1920 x 1080 (2.1 megapixel Full HD)
* Beeldverhouding: 16:9
* Vermogen: 25 W
* Kijkhoek: horizontal/verticaal: 178°/178°, rechts/links: 89°/89°, naar boven/onderen: 89°/89°
* Afmetingen:

  * Hoogte: 386,5 mm
  * Breedte: 648,5 mm
  * Diepte: 52 mm

* Gewicht: 8,3 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}


## Bijlagen

{{< pagelist bijlagen >}}
