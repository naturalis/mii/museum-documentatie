---
title: "iiyama-tf4938uhsc"
date: 2019-12-03T14:50:05+01:00
draft: false
merk: Iiyama
model: ProLite TF4938UHSC-B1AG
leverancier:
attachments:
- title: ProLite TF4938UHSC.pdf
  src: https://files.museum.naturalis.nl/s/K9RaXYpHWKcFP42
- title: iiyama-tf4938uhsc_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/2ssGoc6az46wtiq
---

![tf4938uhsc](./tf4938uhsc.jpg "tf4938uhsc")
49 inch 12P Open Frame PCAP touch monitor met een volledig vlakke voorkant


## Eigenschappen

* Video-in:
  * 1 x Displayport(v1.2)
  * 2 x HDMI(v1.4)
  * 1 x DVI(v1.0)
  * 1 x VGA
* Bediening:
  * RS-232c x1 (Get/Set Command)
  * RJ45 (LAN) x1
  * IR 1x
* Schermdiagonaal: 48.5", 123.2cm
* Resolutie: 3840 x 2160 @ 60Hz (8.3 megapixel 4K UHD)
* Beeldverhouding: 16:9
* Vermogen: 115 W
* Kijkhoek: horizontal/verticaal: 178°/178°, rechts/links: 89°/89°, naar boven/onderen: 89°/89°
* Afmetingen: 1154 x 684 x 73,5mm
* Gewicht: 30 KG

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
