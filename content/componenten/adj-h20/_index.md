---
title: "adj-h20"
date: 2020-06-03T14:50:02+01:00
draft: false
merk: ADJ
model: H20
leverancier: Flashlight
attachments:
- title: ADJ H20 DMX IR (pdf)
  src: https://files.museum.naturalis.nl/s/MyDYDigFdHYe4ow
- title: ADJ H20 DMX IR manual (pdf)
  src: https://files.museum.naturalis.nl/s/2xGBsqGnd9DysNH
---

![h20](./h20.png)
ADJ H20 DMX IR Simulated flowing water effect

## Eigenschappen

* Dimensions (LxWxH): 301 x 232 x 207mm
* Weight: 4 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
