---
title: "srs-dsr10n"
date: 2019-12-03T14:50:09+01:00
draft: false
merk: SRS
model: dsr10n
leverancier: ATA Tech
shows:
- showrexperience
attachments:
- title: srs-dsr10n_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/xeBMt2XnBxZ6997
---

![dsr10n](./dsr10n.png)
The DSR10 is a 19'' 1U rack-mount 10-channel splitter / booster of the DMX signal

## Eigenschappen

* Vermogen: 15 W
* Afmetingen: 19", 1U

* Gewicht: 2,75 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Shows

{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
