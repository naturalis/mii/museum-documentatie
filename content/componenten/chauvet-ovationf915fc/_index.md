---
title: chauvet-ovationf915fc
date:
draft: false
merk: Chauvet
model: Ovation F-915FC
leverancier: Lichtpunt
attachments:
- title: Chauvet Ovation F-915FC DMX (pdf)
  src: https://files.museum.naturalis.nl/s/od5FN8gFJKXEt6b
- title: Chauvet Ovation F-915FC manual (pdf)
  src: https://files.museum.naturalis.nl/s/WfkGmgLcbdbqPGt
- title: Chauvet Ovation F-915FC Quick (pdf)
  src: https://files.museum.naturalis.nl/s/MgjxLm78Aeme9DP

---

![ovationf915fc](./ovationf915fc.png)
The Ovation F-915FC is a full-color Fresnel-style LED lighting fixture with 16-bit dimming resolution for smooth fades. It delivers a beautiful flat, even field of light. Its motorized zoom, with a range between 30° to 85°, is controlled either manually or via DMX. Simple and complex DMX channel profiles offer programming versatility. Virtually silent operation makes it ideal for use in any situation.


## Eigenschappen

* Dimensions (LxWxH): 569.44 mm x 320 mm x 275 mm
* Weight: 8.6 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
