---
title: "philips-43bdl3010q00"
date:
draft: false
merk: philips
model: 43bdl3010q00
leverancier: ATA-tech
attachments:
- title: Technische specificaties
  src: https://files.museum.naturalis.nl/s/wDzRkbRakPzkHjx
---

![43bdl3010q00](./43bdl3010q00.png)
Uitblinker
Eenvoudig in te stellen 18/7-display.

## Eigenschappen

* Video-in:
  * 1 x DVI-I
  * 2 x HDMI
  * VGA (Via DVI)
* Bediening:
  * RJ45
  * RS232C
  * IR Remote
* Schermdiagonaal: 42,5 inch / 108 cm
* Resolutie: 1920 x 1080p
* Beeldverhouding: 16:9
* Vermogen: 75 W
* Kijkhoek: 178 / 178 graad
* Afmetingen:

  * Hoogte: 559,4 mm
  * Breedte: 968,2 mm
  * Diepte: 60,9 mm

* Gewicht: 8,8 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
