---
title: "soraa-sle30"
date:
draft: false
merk: Soraa
model: SORAA SLE30
leverancier: Q-Cat
attachments:
- title: Eclipse Engine Soraa SLE30 3000K 95 (pdf)
  src: https://files.museum.naturalis.nl/s/CMeJRDE9XsLzcPH
---

[![sle30-08-009d-930-03-01](./sle30-08-009d-930-03-01.png)](https://files.museum.naturalis.nl/s/CMeJRDE9XsLzcPH)
Eclipse Engine Soraa SLE30 3000K 95

## Eigenschappen


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
