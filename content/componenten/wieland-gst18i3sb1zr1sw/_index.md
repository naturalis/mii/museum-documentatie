---
title: "wieland-gst18i3sb1zr1sw"
date: 2019-12-03T14:50:10+01:00
draft: false
merk: wieland
model: gst18i3sb1zr1sw
leverancier:
attachments:
- title: wieland-gst18i3sb1zr1sw_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/YrjjcD8FXCgbm6x
---

![gst18i3sb1zr1sw](./gst18i3sb1zr1sw.png)
GST18i3 connector, 3 pole, female with screw connection

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte: 12.6 mm
  * Breedte: 28.2 mm
  * Diepte: 69.4 mm

* Gewicht:

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
