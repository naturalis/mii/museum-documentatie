---
title: "smartmetals-0021561"
date: 2019-12-03T14:50:09+01:00
draft: false
merk: smartmetals
model: 0021561
leverancier:
attachments:
- title: smartmetals-0021561_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/mk2Rx83o5wwH4Lc
---

![smartmetals-0021561](./0021561.png)
Haakse set 3: 600 - 1000 mm

## Eigenschappen

* Vermogen: 30kg
* Afmetingen:

  * Hoogte: 600 - 1000 mm
  * Breedte:
  * Diepte:

* Gewicht: 3,15kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
