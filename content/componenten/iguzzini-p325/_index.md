---
title: "iguzzini-p325"
date:
draft: false
merk: iGuzzini
model: P325
leverancier: de Cirkel
attachments:
- title: iGuzzini P325
  src: https://files.museum.naturalis.nl/s/JekZoLF4Fm3mXZB

---

![p325](./p325.png)

## Eigenschappen

* Afmetingen:
  * Diameter: 67 mm
  * Hoogte: 62 mm

* Gewicht: 13 gr


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
