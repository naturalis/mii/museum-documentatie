---
title: "akai-mpx16"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: Akai
model: MPX16
interactives:
- orgel
leverancier:
attachments:
- title: akai-mpx16_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/gzkcQojzQJwNde5
---

![akai_mpx16](./akai_mpx16.jpeg "akai_mpx16")
Desktop Sampler

## Eigenschappen

* Naam: Desktop Sampler
* Merk: AKAI
* Model: MPX16
* Input/Output:
  * 1 USB port
  * 2 1/4" (6.35mm) TRS recording inputs (left and right stereo pair)
  * 2 1/4" (6.35mm) TRS main outputs (left and right stereo pair)
  * 1 1/8" (3.5mm) TRS stereo headphone output
  * 1 5-pin MIDI in
  * 1 5-pin MIDI out
* Power: via power adapter (included): 5.8V DC, 860mA, center-positive via computer USB bus or USB charger (sold separately)

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
