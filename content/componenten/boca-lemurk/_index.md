---
title: "boca-lemurk"
date:
draft: false
merk: boca
model: lemur-K
leverancier:
facaliteiten:
- ticketzuil1
- ticketzuil2
- ticketzuil3
- ticketzuil4
attachments:
- title: Boca Lemur-K data
  src: https://files.museum.naturalis.nl/s/enTazttcZsM7aHz
---

![lemurk](./lemurk.png)
Kiosk Printer

## Eigenschappen

* Afmetingen:

  * Hoogte: 195 mm
  * Breedte: 185 mm
  * Diepte: 216 mm

* Gewicht: 2,81 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Faciliteiten

Dit component wordt gebruik in de faciliteiten:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
