---
title: "epson-ebpu1007b"
date: 
draft: false
merk: Epson
model: EB-PU1007B
leverancier: BeamSystems
Interactives:
- cirkelprojectie
- projectiebeweging
- projectiewaterplaats
- projectiegiraf
- projectieluchtdieren
- projectiegetijden
attachments:
- title: EpsonEB-PU1007B.pdf
  src: https://files.museum.naturalis.nl/s/3TWjtfXQsW6KQik
---
![epson-ebpu1007b](./epson-ebpu1007b.png "epson-ebpu1007b")
<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

* Levensduur lichtbron: 20.000 hours life
* Type lamp: WUXGA, 7000 lumen
* Projectie techniek: Laser
* Resolutie: 1920 x 1200 (WUXGA)
* Beeldverhouding: 16:10
* Statische contrastverhouding: 2.500.000 : 1
* Video-in:
  * HDBaseT
  * DVI-D
  * HDMI (HDCP 2.3)

* Bediening:
  * IR Remote
  * Wifi
  * RS-232 in
  * HTTPS, IPv6, SNMP, ESC/VP.net, PJLink

* Verbinding:
  * USB 2.0-A (2x)
  * RS-232C
  * Ethernet-interface (100 Base-TX/10 Base-T)
  * Draadloos LAN IEEE 802.11a/b/g/n/ac
  * Draadloos LAN b/g/g 25 GHz
  * Draadloos LAN a/n (5 GHz)
* Afmetingen: 545 x 436 x 189 mm (breedte x diepte x hoogte)
* Vermogen: 358 Watt (Normal On-Mode), 431 Watt (Normal Peak-mode), 366 Watt (Eco Peak-Mode), 0,5 Watt (Energy saving standby)

## Technische specificaties

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
