---
title: "dapaudio-pr32"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: DAP Audio
model: PR-32
leverancier:
interactives:
- handscanner
- vogelskeletten
attachments:
- title: dapaudio-pr32_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/BGpSmMwQZY62BsS
---

![PR-32](./PR-32.jpg "PR-32")
Passieve PA luidspreker

## Eigenschappen

* RMS vermogen: 40 W
* Impedantie: 16 Ohm
* Type connector: speaker cable connector
* Actief of passief: Passief
* Afmetingen: 153x 160 x 215 mm (LxWxH)
* Gewicht: 3,2 KG


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}
## Bijlagen

{{< pagelist bijlagen >}}
