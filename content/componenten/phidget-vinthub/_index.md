---
title: "phidget-vinthub"
draft: false
merk: phidget
model: vinthub
leverancier: Jipp
interactives:
- dnaspel
attachments:
- title: VINT Hub Phidget - HUB0001_0 at Phidgets.pdf
  src: https://files.museum.naturalis.nl/s/2s6B9kCRbA36grQ
- title: VINT Hub Phidget - HUB0001_0 at Phidgets_specs.pdf
  src: https://files.museum.naturalis.nl/s/Swz63cpieER6AwG
---

![phidget-vinthub](./Vinthub.JPG)
Connect up to 6 devices to your computer through a single USB port. Act as a digital output. Read switches as a digital input. Read a 0-5V Voltage or ratiometric sensor.

## Eigenschappen

* USB Voltage: 4.8 V DC min, 5.3 V DC max
* Number of VINT Ports: 6

## Technische specificaties

Lighting Playback Controller 1: 1024 channels DMX/eDMX

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
