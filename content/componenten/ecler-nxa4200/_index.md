---
title: "ecler-nxa4200"
date: 2019-12-03T14:50:03+01:00
draft: false
merk: Ecler
model: NXA4-200
leverancier:
attachments:
- title: ecler-nxa4200_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/yLqR6jkmyFnbaRT
---

![Ecler-NXA4-200-multichannel_self-powered_digital-manager-front](./Ecler-NXA4-200-multichannel_self-powered_digital-manager-front.png)
![Ecler-NXA4-200-multichannel_self-powered_digital-manager-rear](./Ecler-NXA4-200-multichannel_self-powered_digital-manager-rear.png)

ECLER NXA4-200 is a 4 channel self-powered digital manager that stays halfway between a digital matrix and a multichannel amplifier

## Eigenschappen

* Versterkerklasse:
* Aantal speaker kanalen: 4 powered audio outputs
* RMS vermogen:
  * 2 Bridged Channels @ 8Ohm (RMS)	383W
  * 1 Channel @ 4 Ohm (RMS)	202W
  * 1 Channel @ 8 Ohm (RMS)	121W
  * All Channels @ 4 Ohm (RMS)	168W
* Broningangen: 4 analogue audio inputs
* Speakeraansluitingen:
* Signaal-ruisverhouding:
* Impedantie:
* Beveiligingen:
* Vermogen:
* Afmetingen: 482,6 x 88 x 373 mm
* Gewicht: 12,6 kg

## Technische specificaties

* 4 analogue audio inputs x 4 powered audio outputs
* Class D amplifiers (eco friendly)
* Auto stand-by function (eco friendly)
* 100% silent (fanless convection cooling system)
* Health self-test mode function, with FAULT RELAY (for an external redundancy system)
* Integrated anti-clip system
* Integrated DSP processor. Main features:
  * Inputs mixer independent per channel (all inputs available)
  * VOLUME, MUTE, SOLO, PHASE INVERSION, MAX. VOL limit and MIN.VOL limit, LP and HP Crossover filters, parametric EQ filters bank, Ducker, Delay, Compressor and more settings configurable per channel.
 * Ethernet interface, compatible with EclerNet Manager platform and UCP remote control system
 * TP-NET third-party remote control (compatible with CRESTRON®, AMX®, RTI®, VITY®, etc.)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
