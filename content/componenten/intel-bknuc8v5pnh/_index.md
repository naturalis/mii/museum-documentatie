---
title: "intel-bknuc8v5pnh"
date:
draft: false
merk: Intel
model: BKNUC8v5PNH
leverancier: Azerty
interactives:
- studiolab
attachments:
- title: Technische specificaties (pdf)
  src: https://files.museum.naturalis.nl/s/mSmagq7tqTCpkQL
---

![](./bknuc8v5pnh.png)

i5 NUC met vpro (AMT)
## Eigenschappen

* Afmetingen:

  * Hoogte:
  * Breedte:
  * Diepte:

* Gewicht:

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
