---
title: "koenig-knmsf10"
date: 2019-12-03T14:50:05+01:00
draft: false
merk: koenig
model: knmsf10
leverancier:
attachments:
- title: koenig-knmsf10_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/R6tgjt3PJas8DFa
- title:
  url:
---

![knmsf10](./knmsf10.png "knmsf10")
TV Muurbeugel Vast 10 - 26 " 35 kg

## Eigenschappen

* Geschikt voor min. schermgrootte: 10
* Aantal schermen: 1
* Min. afstand tot de muur: 18 mm
* Max. gewicht:35 kgGeschikt voor schermafmeting van max.: 26
* Kleur: Zwart
* Materiaal: Staal
* Max. afstand tot de muur: 18 mm
* Toepassing: Muur
* Functionaliteit: Vast
* VESA-standaard: 50x50 (compatibel met VESA 50x50, 75x75 en 100x100 mm)


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
