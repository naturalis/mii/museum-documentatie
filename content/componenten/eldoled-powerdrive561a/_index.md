---
title: "eldoled-powerdrive561a"
date:
draft: false
merk: eldoLED
model: POWERdrive 561A
leverancier:
attachments:
- title: EldoLED POWERdrive 561A 4ch
  src: https://files.museum.naturalis.nl/s/ts5ZGGqfR8xDw5J

---

![powerdrive561a](./powerdrive561a.png)
50W DMX/RDM Full-Colour (RGBW) Dimmable LED Driver

## Eigenschappen

* Afmetingen:
  * Diepte: 152,2 mm
  * Hoogte: 30,1 mm
  * Breedte: 76 mm
* Gewicht: 372 gr


## Technische specificaties

POWERdrive 561/A delivers 50W and is DMX compatible. The end cap functions as a wire restraint and makes the driver perfectly suited for independent installations. POWERdrive 561 has four separately controllable LED outputs, which can be used for wide range of full-colour (RGBW), dynamic LED lighting applications.

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
