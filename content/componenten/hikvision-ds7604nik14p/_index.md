---
title: "hikvision-ds7604nik14p"
date:
draft: false
merk: Hikvision
model: DS-7604NI-K1/4P(B)
leverancier:
shows:
- showrexperience
attachments:
- title: Baseline_Quick-Start-Guide_DS-7600NI-Q-&-K_Network-Video-Recorder_V3.4.98_20180124 (pdf)
  src: https://files.museum.naturalis.nl/s/RYHGxAK8tE5zMZy
- title: Baseline_User-Manual_DS-7600NI-Q-&-K_Network-Video-Recorder_V3.4.98_20180124 (pdf)
  src: https://files.museum.naturalis.nl/s/trmsKN9pYm4BmkX
- title: Datasheet-of_DS-7600NI-K1_P-B-NVR_3.4.96_20181102 (pdf)
  src: https://files.museum.naturalis.nl/s/KGpfFwJL9XiBrA3
---

![ds7604nik14p](./ds7604nik14p.jpg)
4-ch 1U 4 PoE 4K Network Video Recorder

## Eigenschappen

* Afmetingen: 315 × 240 × 48 mm
* Gewicht: 1 kg

* IP Video Input: 4-ch Up to 8 MP resolution
* Remote Connections: 8 camera's
* Network Protocols: TCP/IP, DHCP, Hik-Connect, DNS, DDNS, NTP, SADP, SMTP, NFS, iSCSI, UPnP™, HTTPS


## Known issues

Het zou natuurlijk vet cool zijn om de beelden ook op je eigen computer te bekijken.
Dit kan, alleen gebruikt Hikvision een oude techniek voor de video in je browser: NPAPI

NPAPI is eruit gesloopt bij alle moderne browsers dus de enige mogenlijkheid is
om het via Internet Explorer te proberen.

Ik heb ook een work-around gevonden om het via de Chrome Extensie "IE Tab"  te doen
maar bij mijn test (via VPN thuis) ging dat al mis.

De wachtwoorden staan in Bitwaren.

![Hivision in de browser](https://files.museum.naturalis.nl/s/SAsrLHgF9jMGqyG/preview)

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
