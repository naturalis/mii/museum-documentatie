---
title: "digitus-ds55508"
draft: false
merk: digidus
model: ds55508
interactives:
- aquarium
- cambrium
- projectieboomstronk
attachments:
- title: DS-55508.pdf
  src: https://files.museum.naturalis.nl/s/wic9sQDHgocMWaf
---

![digitus-ds55508](./digidus.JPG "digitus-ds55508")
Digitus DS-55508 audio/video extender AV-zender & ontvanger

## Eigenschappen

* HDMI output op zender voor een monitor
* CEC
* Power: 24 V, 9,36 W
* Afmetingen: 65 x 140 x 18 mm
* Gewicht: 155 g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
