---
title: "stoplisten-gorillaaudiohandset"
date: 2019-12-03T14:50:10+01:00
draft: false
merk: stop and listen
model: gorillaaudiohandset
leverancier:
interactives:
- hartbank
- geilewand
attachments:
- title: stoplisten-gorillaaudiohandset_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/JA6jMzfsctsPPoN
---

![gorillaaudiohandset](./gorillaaudiohandset.png)
“It’s a Headphone on a Stick”

## Eigenschappen

* Nominal Input Power: 30mW (max 70mW)
* Impedantie: 32 Ohm
* Afmetingen:

  * Hoogte: 167 mm
  * Breedte: 205 mm
  * Diepte: 789 mm

* Gewicht:

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
