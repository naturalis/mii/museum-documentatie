---
title: "antari-af3effectsfan"
date:
draft: false
merk: Antari
model: AF3 Effects Fan
leverancier: Baxshop
attachments:
- title: AF-3 User Manual (pdf)
  src: https://files.museum.naturalis.nl/s/zMLjT9ncSpFFcbp
---

![af3](./af3.png)

Compact all-purpose stage fan. DMX aangestuurd.

## Eigenschappen

* Afmetingen: L 124 W 316 H 368 mm

* Gewicht: 6 kg

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
