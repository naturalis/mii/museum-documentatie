---
title: "miniclima-ebc"
date: 2019-12-03T14:50:06+01:00
draft: false
merk: miniClima
model: EBC
leverancier:
exhibits:
- topstukkenvitrine
attachments:
- title: miniclima-ebc_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/gr5ssNZiDgxMz7P
- title: miniclima-ebc_handleiding_onderhoud.pdf
  src: https://files.museum.naturalis.nl/s/cdDXPbXnFYGAJtj
- title: miniclima-eba_brochure.pdf
  src: https://files.museum.naturalis.nl/s/PqqQFJLrTqprjwq
---

![miniClima](./miniclima.jpg "miniClima")
miniClima Constant Humidity Devices

## Eigenschappen

* Vermogen:
  * EBC10 en EBC11: 100W
  * EBC12: 200W
* Afmetingen:

  * Hoogte:
  * Breedte:
  * Diepte:

* Gewicht:
  * EBC10 en EBC11: 6,6KG
  * EBC12: 8,8KG

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Exhibits

Dit scherm wordt gebruik in de exhibits:

{{< pagelist exhibits >}}


## Bijlagen

{{< pagelist bijlagen >}}
