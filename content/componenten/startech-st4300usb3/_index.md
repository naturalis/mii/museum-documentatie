---
title: "startech-st4300usb3"
date:
draft: false
merk: Startech
model: ST4300USB3
leverancier:
attachments:
- title: st4300usb3_datasheet (pdf)
  src: https://files.museum.naturalis.nl/s/Jxb8RqnecMpxtYw
- title: st4300usb3_quickstart (pdf)
  src: https://files.museum.naturalis.nl/s/Mwg2GZ2ZczZHFbP
---

![st4300usb3](./st4300usb3.png)
4 Port Black SuperSpeed USB 3.0 Hub

## Eigenschappen

* Afmetingen:

  * Hoogte: 82 mm
  * Breedte: 150 mm
  * Diepte: 205 mm

* Gewicht: 400 g

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
