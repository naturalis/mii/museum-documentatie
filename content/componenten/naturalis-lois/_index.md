---
title: "lois"
date: 2019-04-09T17:33:04+02:00
draft: false
weight: 32
Interactives:
- datingquiz
- fipperkast
- geilewand
- handscanner
- hartbank
- spermarace
- timelapse
---

Om IODevices op een universele manier aan te kunnen sluiten hebben we LOIS
(Local Output Input System) ontwikkelt.

LOIS is ontwikkeld op basis van de onderstaande eisen voor het aansluiten van
input devices middels een micro controller.

* Native USB HID
* IO screw terminal al dan niet midddels phoenix connector
* Bijvoorkeur Arduino IDE compatible
* Goed verkrijgbaar
* Zo min mogelijk of heel gemakkelijke assemblage
* Programmeerbaar over USB
* Voeding over USB
* Goed te bevestigen, bij voorkeur geleverd met behuizing

LOIS bestaat uit:

* Behuizing (IODoos)
* Usb aansluiting (USB-B)
* Input / output header (Phoenix Contact)
* Microcontroller (Arduino Leonardo)
* Input / output shield (IOShield)
* Bedrading
* Code

## Behuizing

De behuizing die we gebruiken is deze:

[Plastic Project Box, Grey, 190 x 140 x 60mm](https://ie.rs-online.com/web/p/instrument-cases/0315961/)

De behuizing is vrij ruim zodat de interne montage wordt vergemakkelijkt en het
formaat in ons geval geen bottleneck zou vormen. Mocht dit toch het geval zijn
dan is het maken van een aangepaste versie goed mogelijk.

Zie [hier voor een dimensional drawing](https://github.com/MakeExpose/LOIS/blob/master/dimensionaldrawingprojectbox.pdf).

Voor de behuizing en de benodigde connectoren hebben we een custom front panel
ontworden met [OpenSCAD](http://www.openscad.org/). Deze zijn vervolgens 3D te printen.

Het ontwerp, de STL bestanden en GCODE bestanden zijn te vinden op
[github.com/MakeExpose/LOIS](https://gitlab.com/naturalis/mii/lois/-/tree/master/iodoos)

Voor bevestiging van de behuizing op de montage plaat gebruiken we [3M Dual Lock
klittenband](https://www.3m.com/3M/en_US/company-us/all-3m-products/~/All-3M-Products/Adhesives-Tapes/Industrial-Adhesives-and-Tapes/Fasteners/3M-Dual-Lock-Reclosable-Fasteners/?N=5002385+8710676+8710815+8710962+8711017+8716423+3294857497&rt=r3)
(3M klittenband paddenstoel).

## USB aansluiting

LOIS is voorzien van een USB B aansluiting.

Intern is de USB aansluiting verbonden met de Micro USB aansluiting van de
Arduino. Dit door middel van een [Panel Mount USB B to Micro-B
cable](https://www.kiwi-electronics.nl/panel-mount-usb-b-naar-micro-b-kabel)

## Input / Output header

Voor het aansluiten van knoppen en sensoren is LOIS voorzien van een Phoenix
contact header.

Het formaat Phoenix Contact Header is afhankelijk van het aantal gewenste
aansluitingen. Ons ontwerp voorziet in de keuze tussen 4, 6 en 8 ingangen.

* 4 ingangen

  Product: [Phoenix Contact DFK-PC 4/4-GF-7.62 (Feed-through header - DFK-PC 4/
  4-GF-7,62 -
  1840573)](https://www.phoenixcontact.com/online/portal/us?uri=pxc-oc-itemdetail:pid=1840573&library=usen&tab=1)
  Leverancier:
  [Farnell](https://nl.farnell.com/phoenix-contact/dfk-pc-4-4-gf-7-62/terminal-block-hdr-4pos-screw/dp/2776313)
  Bijbehorende stekker (screw terminal): [Farnell](https://nl.farnell.com/phoenix-contact/pc-4-4-st-7-62/terminal-block-pluggable-4pos/dp/1793035)

* 6 ingangen

  Product: [Phoenix Contact DFK-PC 4/6-GF-7.62 (Feed-through header - DFK-PC 4/
  6-GF-7,62 -
  1840599)](https://www.phoenixcontact.com/online/portal/us?uri=pxc-oc-itemdetail:pid=1840599&library=usen&tab=1)
  Leverancier:
  [Farnell](https://nl.farnell.com/phoenix-contact/dfk-pc-4-6-gf-7-62/terminal-block-hdr-6pos-screw/dp/2776314)
  Bijbehorende stekker (screw terminal): [Farnell](https://nl.farnell.com/phoenix-contact/pc-4-6-st-7-62/terminal-block-pluggable-6pos/dp/1793037)

* 8 ingangen

  Product: [Phoenix Contact DFK-PC 4/8-GF-7.62 (Feed-through header - DFK-PC 4/
  8-GF-7,62 -
  1840612)](https://www.phoenixcontact.com/online/portal/us?uri=pxc-oc-itemdetail:pid=1840612&library=usen&tab=1)
  Leverancier:
  [Farnell](https://nl.farnell.com/phoenix-contact/dfk-pc-4-8-gf-7-62/terminal-block-hdr-8pos-screw/dp/2776315)
  Bijbehorende stekker (screw terminal): [Farnell](https://nl.farnell.com/phoenix-contact/pc-4-8-st-7-62/terminal-block-pluggable-8pos/dp/1793038)

De headers en het IOshield zijn uitgevoerd met een schroefverbinding. Deze
zijn onderling verbonden met male-male jumpwires.

## Microcontroller

De atmega32u4 is de aangewezen microcontroller als het gaat om native USB HID support.

De development boards die in deze een optie zijn:

* Arduino Leonardo
* Arduino Micro
* Sparkfun Pro Micro
* Adafruit Teensy
* Adafruit ItsyBitsy
* Adafruit Feather 32u4 Basic Proto

Onze huidige versie van LOIS is voorzien van een Arduino Leonardo R3. Dit in
verband met de leverbaarheid en de kosten.

De Arduino Leonardo R3 is onder andere verkrijgbaar bij
[IProtoyype](https://iprototype.nl/products/arduino/boards/leonardo-with-headers)
en [Kiwi
Electronics](https://www.kiwi-electronics.nl/arduino-platform/arduino-boards/originele-arduino-boards/arduino-leonardo-met-headers-ATmega32u4)

## IOShield

Om de input header makkelijk aan te kunnen sluiten op de Arduino maken we
gebruik van een intern ontwikkeld IOShield.

Zie de [ioshield
git-repo](https://github.com/MakeExpose/LOIS/tree/master/ioshield) voor meer
informatie.

## Bedrading

![interne bedrading LOIS](https://gitlab.com/naturalis/mii/lois/-/raw/ffeea820a076cc032f2a20ca236fd995b99b0b6d/LOIS_interne_bedrading.png)

De uiterst rechter ingang op het Phoenix contact (naast de USB B ingang) is
altijd de GND, vervolgens zijn de ingangen van rechts naar links verbonden met
GPIO 1, 2, 3, etc.

## Code

De code die we gebruiken voor het aansluiten van knoppen is te vinden op
[Gitlab](https://gitlab.com/naturalis/mii/arduino-lois).

Knoppen verbonden met GPIO pin 1 t/m 7 geven achtereenvolgens z t/m m op een
qwerty toetsenbord als output terug.

|GPIO pin|Output|
|--------|------|
|       1|     z|
|       3|     c|
|       4|     v|
|       5|     b|
|       6|     n|
|       7|     m|

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}
