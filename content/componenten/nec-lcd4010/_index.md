---
title: "nec-lcd4010"
date:
draft: false
merk: nec
model: LCD4010
leverancier:
interactives:
- studiolab
attachments:
- title: User Manual
  src: https://files.museum.naturalis.nl/s/jmgPS3BARPMgQ3p
- title: Specifications
  src: https://files.museum.naturalis.nl/s/j2FWKEb4cgX5bFj
---

![lcd4010](./lcd4010.png)
40" Large-Screen AV Display

## Eigenschappen

* Afmetingen:

* Video-in:
  * 1 x DVI-D
  * 1 x Analog BNC (VGA)
  * 1 x Composite RCA or S-Video
  * 1 x Component BNC
* Bediening:
  * IR remote
  * RS-232C
* Schermdiagonaal: 40", 885.2 x 497.6mm
* Resolutie: 1366 x 768
* Beeldverhouding: 16:9
* Vermogen: 230 W
* Kijkhoek: 170° Vert., 170° Hor.  (85U/85D/85L/85R)@ CR>10
* Afmetingen: 981.8 x 579.8 x 140mm
* Gewicht: 27,5 KG

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
