---
title: "roblight-roblonara5"
date:
draft: false
merk: Roblight
model: Roblon Ara 5
leverancier: Q-Cat
attachments:
- title: Roblon Ara 5
  src: https://files.museum.naturalis.nl/s/RYkSgBR2kkpFPpA

---

![ara5](./ara5.png)

## Eigenschappen

* Afmetingen:
  * Diameter: 29 mm
  * Diepte: 24 mm
  * Hoogte: 46 + 29 (diameter) mm
  * Breedte: 44 mm



## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
