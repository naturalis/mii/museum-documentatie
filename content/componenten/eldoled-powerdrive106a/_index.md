---
title: "eldoled-powerdrive106a"
date:
draft: false
merk: eldoLED
model: POWERdrive 106A
leverancier:
attachments:
- title: EldoLED POWERdrive 106A (pdf)
  src: https://files.museum.naturalis.nl/s/PxD35Ma2nmTatyL

---

![powerdrive106a](./powerdrive106a.png)
100W DMX/DALI Full-Colour Dimmable LED Drivers

## Eigenschappen

* Afmetingen:
  * 388x42x30 mm
* Gewicht: 681.5 gr


## Technische specificaties

POWERdrive 1060/A delivers 100W and is DALI and DMX compatible. The end caps function as wire restraints and make the driver perfectly suited for independent installations. POWERdrive 1060/A has four separately controllable LED outputs, which can be used for a wide range of full-colour (RGBW), dynamic LED lighting applications.

You can configure the driver over its intuitive, 3-button user interface with display: it lets you configure the number of channels, DMX or DALI settings for networked mode and show, colour and dim values for standalone operation.

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
