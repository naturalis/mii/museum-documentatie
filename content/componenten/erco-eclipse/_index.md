---
title: "erco-eclipse"
date:
draft: false
merk: Erco
model: Eclipse met Engine Soraa SLE30 3000K 95
leverancier: Erco
attachments:
- title: Erco Eclipse met Engine Soraa SLE30 3000K 95 (pdf)
  src: https://files.museum.naturalis.nl/s/wtfBGK59oxpoyyX

---

![eclipse-grijs](./eclipse-grijs.png)
![eclipse-zwart](./eclipse-zwart.png)
Erco Eclipse schijnwerpers omgebouwd naar LED (door Tim)

## Eigenschappen



## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
