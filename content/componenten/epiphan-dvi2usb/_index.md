---
title: "epiphan-dvi2usb"
date: 2019-12-03T14:50:03+01:00
draft: false
merk: Epiphan
model: DVI2USB3
leverancier:
interactive:
- studiolab
attachments:
- title: epiphan-dvi2usb_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/xQG5NBqa5fr8RjB
---

![DVI2USB-3](./DVI2USB-3.0-2.png "DVI2USB-3")
DVI2USB 3.0™ is Epiphan's professional-series external DVI-I video grabber. It captures images and video from VGA, HDMI and DVI-I video sources and digitizes them on your laptop or workstation via connection to a USB 3.0 port.

## Eigenschappen

* Interface: USB 3.0, USB 2.0
* Dimensions: 90 mm × 60 mm × 23 mm
* Connectors:	DVI-I (integrated, digital & analog) USB 3.0 B-Type connector
* Input:	VGA and DVI (HDMI video compatible)
* Video Sampling:	24 bits per pixel, 8:8:8 format 16 bits per pixel, 5:6:5 format 8 bits per pixel, 3:3:2, 3:2:3, 2:3:3 or 256-grey scale format 4 bits per pixel, 16-grey scale format
* Supported video modes:	Up to 1920×1200
* HDMI Audio: 16-bit PCM encoded audio at 32 kHz, 44.1 kHz, and 48 kHz sampling rates
* LED:	One LED to indicate the status of the DVI2USB 3.0 (power, readiness, and operation in progress)
* OS Support:Windows 10 (x64), Linux (x86, x86_64), DirectShow (Windows), and V4L (Linux) supported


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
