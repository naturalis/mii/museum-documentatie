---
title: "iiyama-tf5538uhscb1ag"
date: 2019-12-03T14:50:05+01:00
draft: false
merk: Iiyama
model: Prolite TF5538UHSC-B1AG
leverancier: Ata Tech
interactives:
- naturalisdigitaal
- cabinijsland
- gameover
decors:
- ontsmettingszone
attachments:
- title: iiyama-tf5538uhscb1ag_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/HLxXm6CCPj9GR8x
---

![tf5538uhscb1ag](./tf5538uhscb1ag.jpg "tf5538uhscb1ag")
52 inch 12P Open Frame PCAP touch monitor met een volledig vlakke voorkant


## Eigenschappen

* Video-in:
  * 1 x Displayport(v1.2)
  * 2 x HDMI(v1.4)
  * 1x DVI(v1.0)
  * 1 x VGA
* Bediening:
  * RS-232c x1 (Get/Set Command)
  * RJ45 (LAN) x1
  * IR 1x
* Schermdiagonaal: 55", 139cm
* Resolutie: 3840 x 2160 @ 60Hz (8.3 megapixel 4K UHD)
* Beeldverhouding: 16:9
* Vermogen: 115 W
* Kijkhoek: horizontal/verticaal: 178°/178°, rechts/links: 89°/89°, naar boven/onderen: 89°/89°
* Afmetingen: 1294,5 x 765,5 x 68mm
* Gewicht: 41 KG

## Technische specificaties

Zie bijlage

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Decors

Dit component wordt gebruik in de decors:

{{< pagelist decors >}}

## Known issues

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
