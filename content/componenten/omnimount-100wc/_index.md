---
title: "omnimount-100wc"
date: 2019-12-03T14:50:07+01:00
draft: false
merk: Omnimount
model: 100wc
leverancier:
attachments:
- title: omnimount-100wc_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/r8bcMbZY5DXfwtt
---

![100wc](100wc.png)
Bookshelf speakers mount system

## Eigenschappen


* Afmetingen:

  * Hoogte:
  * Breedte:
  * Diepte:

* Gewicht:

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
