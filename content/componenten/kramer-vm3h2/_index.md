---
title: "kramer-vm3h2"
date: 2019-12-03T14:50:05+01:00
draft: false
merk: kramer
model: vm2h2
leverancier:
decors:
- vertrekhal
shows:
- showrexperience
attachments:
- title: VM-3H2 Datasheet (pdf)
  src: https://files.museum.naturalis.nl/s/sC2rKSyATZjaanr
---

![vm-3h2](./vm3h2.jpg "vm-3h2")
4K HDMI 2.0 Splitter (1:3)


## Eigenschappen

* Inputs:
  * 1 HDMI connector
* Outputs:
  * 3 HDMI connectors
* Poorten:
  * 1 RS−232 for firmware upgrade extension
* Power: 5V DC, 730mA
* Afmetingen: 18.75cm x 11.50cm x 2.54cm
* Gewicht: 0,4 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}

## Decors

Dit scherm wordt gebruik in de decors:

{{< pagelist decors >}}

## Bijlagen

{{< pagelist bijlagen >}}
