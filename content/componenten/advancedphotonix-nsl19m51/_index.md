---
title: advancedphotonix-nsl19m51
date: 2019-12-12T10:30:43+01:00
draft: false
merk: Advanced Photonix
model:
leverancier:
attachments:
- title: advancedphotonix-nsl19m51_datasheet.pdf
  src: https://files.museum.naturalis.nl/s/Ee496f4AMsKSgqR
---

![photoconductive cell](./42248202.jpg "photoconductive cell")
<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

Van [de pagina van leverancier
Farnell](https://nl.farnell.com/advanced-photonix/nsl-19m51/light-dependent-resistor-550nm/dp/3168335#).

The NSL 19M51 from Advanced Photonix is a CdS photoconductive cell in TO-18
ceramic plastic encapsulated for moisture resistance.

* Maximum light resistance of 100kohm at 10lux
* Dark resistance of 20Mohm
* Power dissipation at 25°C is 50mW
* Voltage rating (peak AC or DC) is 100V
* Operating temperature range from -60°C to 75°C
* Spectral peak wavelength of 550nm

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
