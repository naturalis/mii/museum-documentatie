---
title: "Netwerk"
date: 2018-11-21T07:51:41+01:00
draft: true
---

Het datanetwerk op de Darwinweg wordt volledig verzorgd door Naturalis.
Naturalis heeft daarbij gekozen voor [Cumulus
Linux](https://cumulusnetworks.com/products/cumulus-linux/) en whitebox
switches. Omdat de switches met Cumulus Linux als reguliere computers (alleen
dan met veel meer netwerkpoorten) benaderd kunnen worden zijn ze goed te beheren
en configureren met de automatiseringstools die ook in de rest van de
infrastructuur worden gebruikt: Ansible en AWX.

## IP plan en DHCP

Het netwerk op de Darwinweg is op basis van VLAN's en bijbehorende subnets in
segmenten opgedeeld. Omdat elke tentoonstelling technisch gezien onafhankelijk
moet kunnen functioneren, is voor elke tentoonstelling een apart VLAN voorzien.

In het geval van de tentoonstellingen en experiences is sprake van veel
apparaten die - vanwege de onderlinge samenhang en aansturing vanuit een
showcontroller - van een voorspelbaar en vast IP-adres moeten worden voorzien.
Om dat te bereiken wordt gebruik gemaakt van DHCP reserveringen. Alle apparatuur
moet daartoe ingesteld worden zodat via DHCP het IP-adres wordt geconfigureerd.
Op basis van het MAC-adres geven de DHCP-servers vervolgens een DHCP lease uit
met het vaste IP adres.

In onderstaand overzicht staan alle voor het museum relevante VLAN's. In de
laatste drie kolommen staan de gebruikte ranges binnen elk subnet:

* Gereserveerd: range voor firewalls en servers.
* DHCP reserveringen: range voor apparaten met een vast IP adres op basis van
  DHCP reservering.
* DHCP overig: range voor apparaten zonder een vast IP adres.

| Doel                    | VLAN ID | Subnet          | Gateway      | NAT-adres            | Gereserveerd    | DHCP reserveringen          | DHCP overig       |
|-------------------------|---------|-----------------|--------------|----------------------|-----------------|-----------------------------|-------------------|
| Bezoekers museum        | `125`   | `10.125.0.0/16` | `10.125.0.1` | `145.136.247.125/32` | `10.125.0.0/24` | `10.125.1.0-10.125.127.255` | `10.125.128.0/17` |
| Kernsystemen museum     | `126`   | `10.126.0.0/16` | `10.126.0.1` | `145.136.247.126/32` | `10.126.0.0/24` | `10.126.1.0-10.126.127.255` | `10.126.128.0/17` |
| Kernsystemen restaurant | `127`   | `10.127.0.0/16` | `10.127.0.1` | `145.136.247.127/32` | `10.127.0.0/24` | `10.127.1.0-10.127.127.255` | `10.127.128.0/17` |
| LiveScience             | `128`   | `10.128.0.0/16` | `10.128.0.1` | `145.136.247.128/32` | `10.128.0.0/24` | `10.128.1.0-10.128.127.255` | `10.128.128.0/17` |
| Leven                   | `129`   | `10.129.0.0/16` | `10.129.0.1` | `145.136.247.129/32` | `10.129.0.0/24` | `10.129.1.0-10.129.127.255` | `10.129.128.0/17` |
| Verleiding              | `130`   | `10.130.0.0/16` | `10.130.0.1` | `145.136.247.130/32` | `10.130.0.0/24` | `10.130.1.0-10.130.127.255` | `10.130.128.0/17` |
| IJstijd                 | `131`   | `10.131.0.0/16` | `10.131.0.1` | `145.136.247.131/32` | `10.131.0.0/24` | `10.131.1.0-10.131.127.255` | `10.131.128.0/17` |
| Ontstaan van leven      | `132`   | `10.132.0.0/16` | `10.132.0.1` | `145.136.247.132/32` | `10.132.0.0/24` | `10.132.1.0-10.132.127.255` | `10.132.128.0/17` |
| Aarde                   | `133`   | `10.133.0.0/16` | `10.133.0.1` | `145.136.247.133/32` | `10.133.0.0/24` | `10.133.1.0-10.133.127.255` | `10.133.128.0/17` |
| De vroege mens          | `134`   | `10.134.0.0/16` | `10.134.0.1` | `145.136.247.134/32` | `10.134.0.0/24` | `10.134.1.0-10.134.127.255` | `10.134.128.0/17` |
| De dood                 | `137`   | `10.137.0.0/16` | `10.137.0.1` | `145.136.247.137/32` | `10.137.0.0/24` | `10.137.1.0-10.137.127.255` | `10.137.128.0/17` |
| REXperience             | `138`   | `10.138.0.0/16` | `10.138.0.1` | `145.136.247.138/32` | `10.138.0.0/24` | `10.138.1.0-10.138.127.255` | `10.138.128.0/17` |
| Dinotijd                | `139`   | `10.139.0.0/16` | `10.139.0.1` | `145.136.247.139/32` | `10.139.0.0/24` | `10.139.1.0-10.139.127.255` | `10.139.128.0/17` |
| Japans theater          | `140`   | `10.140.0.0/16` | `10.140.0.1` | `145.136.247.140/32` | `10.140.0.0/24` | `10.140.1.0-10.140.127.255` | `10.140.128.0/17` |
| Auditorium              | `141`   | `10.141.0.0/16` | `10.141.0.1` | `145.136.247.141/32` | `10.141.0.0/24` | `10.141.1.0-10.141.127.255` | `10.141.128.0/17` |
| Signage                 | `142`   | `10.142.0.0/16` | `10.142.0.1` | `145.136.247.142/32` | `10.142.0.0/24` | `10.142.1.0-10.142.127.255` | `10.142.128.0/17` |

Via DHCP zullen alle apparaten worden voorzien van informatie over de default
gateway, het broadcast adres, de DNS servers en het search domain.

## DNS

Alle apparaten in de tentoonstellingen en experiences krijgen, behalve een DHCP
reservering, ook een (interne) domeinnaam. Er wordt één subdomein / DNS-zone
gehanteerd voor de Darwinweg: `dw2.naturalis.nl`.

Een individueel apparaat krijgt op basis van de naam van dat apparaat (die
bijvoorbeeld ook op het label komt) een domeinnaam. Een voorbeeld van een full
qualified domain name is `stamboom-cmp-1.dw2.naturalis.nl`.

Mits apparaten in hetzelfde VLAN zitten of er een firewall regel is die
onderling verkeer toestaat. Apparaten kunnen elkaar bereiken via de full qualified

[Technische ruimten](https://docs.museum.naturalis.nl/latest/architectuur/ruimten/)
