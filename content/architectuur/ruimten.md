---
title: "Technische ruimten"
date: 2019-03-04T15:52:34+01:00
draft: true
---

Voor de ophanging van netwerk- en AV-apparatuur zijn er verspreid over de
locatie Darwinweg 2 zestien equipment rooms / technische ruimten. Elke ruimte is
voorzien van een eigen letter op basis waarvan onder meer
[outlets](/standaarden/documentatie/naamgeving/#outlet),
[switches](/standaarden/documentatie/naamgeving/#switches) en andere onderdelen
van het netwerk hun naam ontlenen.

| Aanduiding | Ruimtenummer | Opmerking                             |
|------------|--------------|---------------------------------------|
| A          | K.01.T1      | MER                                   |
| B          | T.01.T3      | SER                                   |
| C          | L.01.17      | SER                                   |
| D          | D.03.06      | SER                                   |
| E          | K.04.16      | SER                                   |
| F          | M.00.T2      | SER Livescience (Zaal 1)              |
| G          | M.02.T1      | SER Leven (Zaal 2)                    |
| H          | M.03.T1      | SER Aarde (Zaal 6)                    |
| I          | M.03.T2      | SER Dinotijd (Zaal 9)                 |
| J          | M.03.09      | SER Rexperience                       |
| K          | M.05.T1      | SER IJstijd (Zaal 4)                  |
| L          | M.05.13a     | SER De Vroege Mens (Zaal 7)           |
| M          | M.05.11a     | SER Ontstaan van Leven (Zaal 5)       |
| N          | M.07.T1      | SER Verleiding + De dood (Zaal 3 + 8) |
| O          | M.07.06a     | SER Wisselzaal (Zaal 10)              |
| P          | M.09.T4      | SER Algemeen                          |
