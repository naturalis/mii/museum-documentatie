---
title: "deijstijd"
date: 2019-07-24T13:48:49Z
draft: false
status:
unid: 80716689-b5dd-4663-ab12-4094289330c8
shows:
- showijstijd
interactives:
- kijkersbrabant
- kijkersdrenthe
- kijkersfriesland
- kijkersgelderland
- kijkersholland
exhibits:
- edelhert
- grottenbeer
- ijstijdmaquette
- kleinedierenvitrine
- mammoet
- mammoetvitrine
- neushoornvitrine
- onderkakenvitrine
- reuzenhert
- steppenwisentvitrine
decors:
- ijstijddepot
faciliteiten:
- infopuntijstijd
attachments:
- title: Definitief Ontwerp (pdf)
  url: https://files.museum.naturalis.nl/s/FdAwAG2QD3yRWPw
- title: Vlekkenplan AV (pdf)
  url: https://files.museum.naturalis.nl/s/Lqx4TKK4r4LawPB
- title: Vlekkenplan AV (vwx)
  url: https://files.museum.naturalis.nl/s/GTEnLDTdqs3x6kT
- title: Kabelschema AV (pdf)
  url: https://files.museum.naturalis.nl/s/J3dH4yWTc7FHLSS
- title: Kabelschema AV (vwx)
  url: https://files.museum.naturalis.nl/s/CDGf9Em4cSN4F8t
- title: Service Manual (pdf)
  url: https://files.museum.naturalis.nl/s/d7qBY37rKdX9jyz
- title: Bevestiging materiaal (directory)
  url: https://files.museum.naturalis.nl/s/EMeg5dLEkPYo7tk
- title: Glas (directory)
  url: https://files.museum.naturalis.nl/s/4Mp6eFzsmrKmHTp
- title: Hout (directory)
  url: https://files.museum.naturalis.nl/s/eydksE2xqFzqAee
- title: Kabelgoten (directory)
  url: https://files.museum.naturalis.nl/s/mria7WRWEBS6ekF
- title: Kit (directory)
  url: https://files.museum.naturalis.nl/s/G4nAssxF3rgyRwe
- title: Kunststof (directory)
  url: https://files.museum.naturalis.nl/s/io6M6PrfmBzAsQf
- title: Secomesh (directory)
  url: https://files.museum.naturalis.nl/s/aXr7nN9KaSP9C7Z
- title: Sloten (directory)
  url: https://files.museum.naturalis.nl/s/kDiDfWkCjZcyTGz
- title: Staal (directory)
  url: https://files.museum.naturalis.nl/s/egWH77MqgnYYEJB
- title: UV lijm (directory)
  url: https://files.museum.naturalis.nl/s/rwdGK5XRCMrkpHn
- title: Verf en Lak (directory)
  url: https://files.museum.naturalis.nl/s/5k9RRc7xTH83tMB
- title: X-lift (directory)
  url: https://files.museum.naturalis.nl/s/HzqjktqmYKaA3LJ
- title: Stelling gewicht belasting (xls)
  url: https://files.museum.naturalis.nl/s/i85LMmqnfC7mfEj
- title: deijstijd_ijstijdkijkers_bouwtekening (pfd)
  src: https://files.museum.naturalis.nl/s/TcYXg6zcnHjctAg

- title: NAT LX AsBuilt 04 de IJstijd 20200517 (pdf)
  url: https://files.museum.naturalis.nl/s/wFsrzwYi4KGQrkd
- title: NAT LX AsBuilt 04 de IJstijd overzicht op Data aansluiting (pdf)
  url: https://files.museum.naturalis.nl/s/giJ4G9553xGFk8C
- title: NAT LX AsBuilt 04 de IJstijd overzicht op DMX nummer (pdf)
  url: https://files.museum.naturalis.nl/s/2dCaGEJNQb8729H
- title: NAT LX AsBuilt 04 de IJstijd overzicht op Groepnummer (pdf)
  url: https://files.museum.naturalis.nl/s/SzDLBrjdc3zXLaN
- title: NAT LX AsBuilt 04 de IJstijd overzicht op Posities (pdf)
  url: https://files.museum.naturalis.nl/s/xRRbJpMXbYLNjE4
- title: NAT LX AsBuilt 04 de IJstijd overzicht op Unit nummer (pdf)
  url: https://files.museum.naturalis.nl/s/7ttWFFgQerBe8Jr
- title: NAT LX AsBuilt 04 de IJstijd (xls)
  url: https://files.museum.naturalis.nl/s/2WRWme3WrjNkmx2
- title: NAT LX AsBuilt 04 de IJstijd (lw6)
  url: https://files.museum.naturalis.nl/s/aySMbE8wSYJcARH
- title: NAT LX AsBuilt 04 de IJstijd (vwx)
  url: https://files.museum.naturalis.nl/s/wR5R9caCsMaKHHD
- title: NAT LX AsBuilt 04 de IJstijd (xml)
  url: https://files.museum.naturalis.nl/s/m6yDMTEPcJTiz2b

---



![Impressie](https://files.museum.naturalis.nl/s/zaciGKLGNQWgsQJ/preview)

![Plattegrond](https://files.museum.naturalis.nl/s/cjEY2G2N7F8TjMn/preview)

<!-- Voeg een overzichtsafbeelding toe -->
Bekijk hier
[het definitief ontwerp](https://files.museum.naturalis.nl/s/FdAwAG2QD3yRWPw )
van de tentoonstelling.

## Functionele omschrijving

De ijstijd zaal is een zaal op de vijfde verdieping van het museum.
In het midden staan een maquette van Nederland waarop dieren en planten
zijn afgebeeld. Om de maquette heen staan kijkers waardoor je de
dieren actief ziet. Aan alle kanten om de maquette grote skeletten
van de beesten die ooit voorkwamen in de ijstijd in de omgeving van
Nederland.

De ijstijd tentoonstelling bestaat uit een aantal hoofdelementen:

1. Het landschap: het statisch landschap toont diversiteit aan leven en toont
   de Big Five (en andere dieren) op een cruciaal moment in een verhaal;
1. Kijkers: elk van de Big Five wordt tot leven gewekt. De scenes verbeelden
   vijf verschillende thema’s.
1. Collectiewand: toont de omvang en de diversiteit van de ijstijd collectie
1. Fossielen Uitgifte Punt: is een hands-on ontdekkingstocht, waarbij een link
   gelegd word tussen vast te pakken fossielen en het landschap in de
   expositie.

Als je de zaal binnen komt blaast er koude lucht in je gezicht en hoor je de
wind suizen.

De diversiteit aan dieren en thema’s zijn onderling verbonden, zoals de
ecologie op het landschap, waardoor er geen sprake is van een lineaire
structuur in de vertellingen over de dieren en thema’s.  De hoeveelheid
collectie is enorm en zo ook de diversiteit aan leven dat te veel is om
allemaal te zien en mee te maken. Daarom licht de tentoonstelling “the big
five” uit: de wolharige mammoet, de wolharige neushoorn, de hyena, de
steppenwisent en de leeuw, eruit die je net als op safari tenminste gezien wilt
hebben. Deze vijf belangrijkste dieren lichten op hun beurt vijf grote thema’s
uit: Leven en Dood, Voortplanting, Jagen en Eten, Migratie en Vechten.

Op het landschap worden onder andere vijf scenes uitgespeeld waarin elk van
deze vijf dieren een hoofdrol speelt. Deze scène’s zijn door verrekijkers
vanuit verschillende perspectieven te zien waarin de bezoeker alleen door
visuele actie en geluidsdecor uitgenodigd wordt naar bepaalde situaties te
kijken. De bezoeker heeft de vrijheid zelf rond te kijken door de verrekijkers.
De bevroren moment opname van het leven in het landschap toont een veel grotere
diversiteit aan dieren dan dat men door de kijkers in de scene’s ziet.

Wanneer men bij drukte even moet wachten bij een kijker, dan hoeft de bezoeker
die bij het thema of dier wil blijven waar zij zich voor interesseert, zich
maar om te draaien om daar de collectie te zien. Ook kunnen mensen zich
vermaken met de fossielen uit het Fossielen Uitgifte Punt.

De bezoeker heeft doorzicht naar de omloop waar ze wetenschappers aan
het werk kan zien die collectie plaatsen of meenemen voor onderzoek.

De collectie is op een aantal specials na niet nadrukkelijk tentoongesteld maar
opgeslagen alsof de bezoeker in het depot is.  Deze wijze van tentoonstellen
toont de bezoeker dat de collectie van de ijstijd in Nederland gigantisch is.
Ook is er een infopunt aanwezig. Hier kan door een museummedewerker extra
uitleg worden gegeven.

## Technische omschrijving

![Technisch schema](https://files.museum.naturalis.nl/s/GoYjEo9ZyEdwcZb/preview)

Technisch bestaat deze zaal uit vijf groepen van drie *ijstijdkijkers* die
ieder op hun beurt bestaan uit een 3D kijker, een Arduino en een Intel Nuc.
Verder is er nog een soundscape die wordt verzorgd door één centrale computer
en natuurlijk een lichtshow. Meer detail over de technische werking van
deze onderdelen zijn te vinden bij de specifieke onderdelen.

Het VLAN van deijstijd heeft het subnet 10.131.0.0/16, de showcontroller (CueCore2) heeft het IP adres 10.131.1.1.

## Onderdelen

Een functionele en technische beschrijving van de specifieke onderdelen vind je op de onderstaande pagina's.

### Show

Meer informatie over de show in deze tentoonstelling vind je hier:

{{< pagelist shows >}}

### Interactives

In de tentoonstelling staan de volgende interactives:

{{< pagelist interactives >}}

### Exhibits

In de tentoonstelling staan de volgende exhibits:

{{< pagelist exhibits >}}

### Decors

De volgende decors maken onderdeel uit van de tentoonstelling:

{{< pagelist decors >}}

### faciliteiten

In de tentoonstelling staan de volgende facaliteiten:

{{< pagelist faciliteiten >}}

## Handleidingen en procedures

Bruns heeft een
[onderhoudshandleiding](https://files.museum.naturalis.nl/s/d7qBY37rKdX9jyz)
geleverd voor deze zaal.
Marijn heeft een [Handleiding AV hardware software IJstijd](https://files.museum.naturalis.nl/s/kkPJX7wzgN3W46Y) geleverd.

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues met betrekking tot de gehele
tentoonstelling bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

- Opdrachtgever: [Caroline Breunesse](mailto:Caroline.Breunesse@naturalis.nl)
- Projectleider: [Matthijs Vegter](mailto:matthijs.vegter@naturalis.nl)
- Technisch beheer: [Technisch Team](mailto:support@naturalis.nl)

### SLA

#### Garantie

Indien en geen documentatie te vinden is over garantiebepalingen, dan geldt de algemene overeengekomen regeling. Hieronder nog een keer de toelichting van Erik Jan Elderman op die regeling uit de overeenkomst.
Deze regeling geldt zowel voor de tentoonstellingsbouwers als voor Atatech:

#### Artikel 7 Garantie

- 7.1      Opdrachtnemer blijft aansprakelijk voor herstel door middel van reparatie of vervanging van het geleverde of onderdelen daarvan die binnen 12 maanden na datum van aflevering
gebreken vertonen. Opdrachtnemer is niet tot herstel gehouden indien hij aantoont dat het
gebrek niet is te wijten aan fouten van Opdrachtnemer of door haar geleverde materialen.

- 7.2      De in dit artikel geregelde garantie van Opdrachtnemer houdt in dat het geleverde van
deugdelijk materiaal en met goed vakmanschap is vervaardigd en de montagewerkzaamheden
vakkundig zijn uitgevoerd. Mochten zich binnen de garantietermijn materiaal-, fabricage- of ontwerpfouten voordoen in het geleverde, dan zal Opdrachtnemer zonder berekening van kosten zorgdragen voor reparatie daarvan. Naast de feitelijke herstelkosten zullen ook eventuele aan dit herstel verbonden kosten als kosten gemoeid met montage en demontage van apparatuur; transportkosten van apparatuur, alsmede reis- en verblijfskosten van personeel van Opdrachtnemer; extra kosten in verband met spoedeisend herstel van storingen, die als garantieverplichting moeten worden verholpen; aantoonbare technische gevolgschade, welke optreedt door een voor garantie in aanmerking komend defect onderdeel, door Opdrachtnemer worden vergoed.

- 7.3      Opdrachtnemer staat ervoor in dat het door hem geleverde blijft functioneren conform het Bestek. Mocht binnen een periode van 3 jaar na datum van ondertekening van het overname-protocol, zoals bepaald in artikel 6, aan de hand van systematisch optreden van identieke storingen, blijken dat het opgeleverde of delen daarvan toch niet blijken te voldoen aan het Bestek, zal Opdrachtnemer alsnog zorgdragen voor aanpassing van het geleverde. Opdrachtnemer zal in dat geval het in het kader van deze overeenkomst geleverde door middel van een (ontwerp)-aanpassing herstellen, zonder dat hier voor Naturalis kosten aan zijn verbonden.

Opleverdata verschillen per zaal en per leverancier, maar de meeste zalen/de meeste AVH zijn opgeleverd in juni/juli/augustus 2019. Let op: als binnen die garantietermijn een item vervangen wordt, gaat de garantie van 12 maanden voor dat onderdeel opnieuw in. Oplevering van vervangen onderdelen moet dus goed worden geadministreerd.

### Bouwer

- Decor: [Bruns](https://www.bruns.nl)
- Maquette landschap (hoofdaannemer = Bruns): [Impulz decorum](http://www.impulzdecorum.nl/)
- Diermodellen maquette: [Manimal works/Remie Bakker](https://www.manimalworks.com/)
- Software: [Screen Space Lab](https://www.screenspacelab.net/), [Marijn Eken](mailto:marijn@screenspacelab.com)
- Licht: [Tim Blom](mailto:blomartt@live.nl)
- Techniek: [Ata Tech](https://ata-tech.nl)
- Show: Rutger van Dijk ([SemMika](https://www.semmika.nl/))
- Geluid: [Ard Jansen](mailto:ard@walvisnest.nl) van [Walvisnest](http://walvisnest.nl)
- Leverancier X-lift: [Izi-steigers](https://www.izi-hoogwerkerhuren.nl/)

### Ontwerper

- Ontwerper: [Studio Daniel Ament](http://www.danielament.com/)
- Inhoud: [John de Vos](mailto:john.de.vos@naturalis.nl), [Anne Schulp](mailto:anne.schulp@naturalis.nl), [Natasja den Ouden](mailto:natasja.denouden@naturalis.nl)
- Educatie: [Krista Leusink](mailto:krista.leusink@naturalis.nl)
- Licht: [Tim Blom](mailto:blomartt@live.nl)
- Technisch lichtontwerp: [Lichtatelier Okx](http://www.okx.nl)
- Techniek: [Ata Tech](https://ata-tech.nl)
- Show: Rutger van Dijk ([SemMika](https://www.semmika.nl/))

## Bijlagen

{{< pagelist bijlagen >}}
