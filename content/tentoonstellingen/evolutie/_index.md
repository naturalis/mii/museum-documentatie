---
title: "evolutie"
draft: false
status:
unid: 
shows:
- showevolutie
interactives:
- aquarium
- cambrium
- darwinvinken
- devisinjou
- diergroepen
- dnaspel
- oersteen
- plantgroepen
- projectieboomstronk
- slangen
exhibits:
- aanland
- bananen
- eten
- fotosynthese
- meercelligheid
- slakken
- zeekoe
decors:
- lichtsluis
attachments:
- title: Plattegrond (pdf)
  url: https://files.museum.naturalis.nl/s/4c5rqeRcqbNJdFs
- title: Definitief Ontwerp (pdf)
  url: https://files.museum.naturalis.nl/s/8cbDcQe7FcN83qC
- title: Totaal kabelschema AV (pdf)
  url: https://files.museum.naturalis.nl/s/5eJ28qtwFmNNADq
- title: Hardwarelijst (pdf)
  url: https://files.museum.naturalis.nl/s/5eJ28qtwFmNNADq
- title: Overzicht bekabeling SER
  url: https://docs.google.com/spreadsheets/d/1GCHrqRr6IZRk99kIHyuI1kCCRykdYnznMeHQ4r82Kic/edit?usp=sharing
- title: Overzicht asset informatie
  url: https://docs.google.com/spreadsheets/d/1gB4-fPrG5AcXfLGqJlanXntcwen9xgvg9sgTwW-vV8Q/edit?usp=sharing
- title: LX Evolutie as-built
  url: https://files.museum.naturalis.nl/s/3cQwrfKfHB77gLM
---

![Plattegrond](./evolutie_zaalplattegrond.PNG "Plattegrond")

<!--

{{< embed-pdf renderPageNum="1" hidePaginator="true" url="./evolutie_zaalplattegrond.pdf" >}}

Note: Currently supports local file embed. If absolute URL from the remote server is provided, configure the CORS header on that server.

Note 2: hidePaginator werkt ook niet :s

-->

## Functionele omschrijving

Dieren, planten, schimmels… Al het leven is verweven. Want al het leven, van vroeger en nu, is verbonden door evolutie: het proces waarbij soorten ontstaan en veranderen. Deze zaal maakt op bijzondere wijze duidelijk dat we verbonden zijn met de oorsprong van het leven en familie zijn van al het leven om ons heen.

Wanneer je een 3,7 miljard jaar oude steen aanraakt, wordt een licht- en geluidsinstallatie geactiveerd. Jij bent op dat moment verbonden met deze steen die vrijwel even oud is als het leven zelf. Je staat in contact met eencelligen die de basis vormden van al het leven op aarde: een magisch moment.

Evolutie mag dan ingewikkeld lijken, eigenlijk gaat het om een eenvoudig proces. In de tentoonstelling verklaren we evolutionaire stappen die belangrijk waren voor het ontstaan van de enorme diversiteit om ons heen - van simpele cel tot complexe cel, het ontstaan van meercelligheid en fotosynthese. Bijzondere fossielen vertellen een bijzonder verhaal.

Klik [hier](https://files.museum.naturalis.nl/s/8cbDcQe7FcN83qC) voor het definitieve ontwerp.

## Technische omschrijving

<!-- Voeg hier een blokschema van de hele tentoonstelling toe -->

![Kabelschema](./evolutie_kabelschema.PNG "Kabelschema")

Technisch bestaat de zaal uit projectie per specimen, achtergrondsgeluid
en sfeerverlichting en een projectie aan het begin van de tentoonstelling.

Het VLAN van evolutie heeft het subnet 10.132.0.0/16, de showcontroller (Pharos) heeft het IP adres 10.132.1.1.

## Onderdelen

Een functionele en technische beschrijving van de specifieke onderdelen vind je
op de onderstaande pagina's.

### Show

Meer informatie over de show in deze tentoonstelling vind je hier:

{{< pagelist shows >}}

### Interactives

In de tentoonstelling staan de volgende interactives:

{{< pagelist interactives >}}

### Exhibits

In de tentoonstelling staan de volgende exhibits:

{{< pagelist exhibits >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

Op dit moment zijn er geen known issues met betrekking tot de gehele
tentoonstelling bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

* Technisch beheer: [Technisch Team](mailto:support@naturalis.nl)
* Projectleider: [Sjan Janssen](mailto:sjan.janssen@naturalis.nl)

### SLA

#### Garantie

Indien en geen documentatie te vinden is over garantiebepalingen, dan geldt de algemene overeengekomen regeling. Hieronder nog een keer de toelichting van Erik Jan Elderman op die regeling uit de overeenkomst.
Deze regeling geldt zowel voor de tentoonstellingsbouwers als voor Atatech:

#### Artikel 7 Garantie

* 7.1     Opdrachtnemer blijft aansprakelijk voor herstel door middel van reparatie of vervanging van het geleverde of onderdelen daarvan die binnen 12 maanden na datum van aflevering
gebreken vertonen. Opdrachtnemer is niet tot herstel gehouden indien hij aantoont dat het
gebrek niet is te wijten aan fouten van Opdrachtnemer of door haar geleverde materialen.

* 7.2 De in dit artikel geregelde garantie van Opdrachtnemer houdt in dat het geleverde van
deugdelijk materiaal en met goed vakmanschap is vervaardigd en de montagewerkzaamheden
vakkundig zijn uitgevoerd. Mochten zich binnen de garantietermijn materiaal-, fabricage- of ontwerpfouten voordoen in het geleverde, dan zal Opdrachtnemer zonder berekening van kosten zorgdragen voor reparatie daarvan. Naast de feitelijke herstelkosten zullen ook eventuele aan dit herstel verbonden kosten als kosten gemoeid met montage en demontage van apparatuur; transportkosten van apparatuur, alsmede reis- en verblijfskosten van personeel van Opdrachtnemer; extra kosten in verband met spoedeisend herstel van storingen, die als garantieverplichting moeten worden verholpen; aantoonbare technische gevolgschade, welke optreedt door een voor garantie in aanmerking komend defect onderdeel, door Opdrachtnemer worden vergoed.

* 7.3 Opdrachtnemer staat ervoor in dat het door hem geleverde blijft functioneren conform het Bestek. Mocht binnen een periode van 3 jaar na datum van ondertekening van het overname-protocol, zoals bepaald in artikel 6, aan de hand van systematisch optreden van identieke storingen, blijken dat het opgeleverde of delen daarvan toch niet blijken te voldoen aan het Bestek, zal Opdrachtnemer alsnog zorgdragen voor aanpassing van het geleverde. Opdrachtnemer zal in dat geval het in het kader van deze overeenkomst geleverde door middel van een (ontwerp)-aanpassing herstellen, zonder dat hier voor Naturalis kosten aan zijn verbonden.

Opleverdata verschillen per zaal en per leverancier, maar de meeste zalen/de meeste AVH zijn opgeleverd in juni/juli/augustus 2019. Let op: als binnen die garantietermijn een item vervangen wordt, gaat de garantie van 12 maanden voor dat onderdeel opnieuw in. Oplevering van vervangen onderdelen moet dus goed worden geadministreerd.

### Bouwer

* Projectleider: Sjan Janssen
* Decor & Hardware: [Kloosterboer](http://www.kloosterboer-decor.nl/)
* Show: Rutger van Dijk ([SemMika](https://www.semmika.nl/))
* Licht: [Tim Blom](mailto:blomartt@live.nl)
* Animatie: [Shosho](http://www.shosho.nl/)
* Digitubes: [181](https://oneeightyone.com/nl/)
* Geluid: [wiwo.studio](http://wiwo.studio/)
* Aquarium: [Dutch Igloo](https://dutchigloo.com/)

### Ontwerper

* Tentoonstelling: [Designwolf](http://designwolf.nl/contact/)
* Licht: [Tim Blom](mailto:blomartt@live.nl)
* Animatie: [Shosho](http://www.shosho.nl/)
* Geluid: [wiwo.studio](http://wiwo.studio/)
* Aquarium: [Dutch Igloo](https://dutchigloo.com/)

## Bijlagen

{{< pagelist bijlagen >}}
