---
title: "deverleiding"
date: 2019-07-24T13:48:49Z
draft: false
status:
unid: 45d0f5d2-9742-46e0-b902-0e5a4a771a05
shows:
- showverleiding
interactives:
- bhtjes
- datingquiz
- eekhoornspel
- etalagebeweging
- etalagegeur
- etalagetreintjes
- flipperkast
- geboorte
- geboortetaart
- geilewand
- greenporn
- harigeballen
- hartbank
- inthepikture
- kamasutra
- kijkjeindebuik
- kruipdoorsluipdoor
- letsdance
- magazijningang
- orgel
- projectiedans
- sexystories
- speelnest
- spermarace
- vogelsvoeren
- warenhuisbalie
- waternesten
- zaadverspreiders
- zonderseks
exhibits:
- eieren
- etalageopvallen
- etalagetreintjes
- etalagevechten
- fallusfestival
- geboren
- kaptafel
- lantaarnpaalwarenhuis
- manenvrouw
- monogamievitrine
- nestenvitrine
- vagevaginas
- wiezorgter
- zadenkast
decors:
- cascowarenhuis
- parkbank
- peperbus
- warenhuiskassa
faciliteiten:
attachments:
- title: deverleiding_vooraanzicht_definitiefontwerp_1920x1080.png
  src: https://files.museum.naturalis.nl/s/Ga4tEQYmqdD6Xr8
- title: deverleiding_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/d7mLRTfRBBrmoxS

- title: deverleiding_plattegrond_definitiefontwerp_1920x1080.png
  src: https://files.museum.naturalis.nl/s/xZWtNQaFEyo8L8r
- title: deverleiding_plattegrond_definitiefontwerp_pdf
  src: https://files.museum.naturalis.nl/s/qc3ioHPDNiGZrrn

- title: deverleiding_vlekkenplan_av.pdf
  src: https://files.museum.naturalis.nl/s/FcPJZa8T9iPY3Jm
- title: deverleiding_vlekkenplan_av.vwx
  src: https://files.museum.naturalis.nl/s/9pKkzeDzcf7p4CC
- title: deverleiding_vlekkenplan_av_1920x1080.png
  src: https://files.museum.naturalis.nl/s/ZxWfr9MHkbGcWSY

- title: deverleiding_kabelschema_av.pdf
  src: https://files.museum.naturalis.nl/s/CHGXsE4sffFMzyL
- title: deverleiding_kabelschema_av.vwx
  src: https://files.museum.naturalis.nl/s/tZnxqnqnf5xynLC
- title: deverleiding_kabelschema_av_1920x1080.png
  src: https://files.museum.naturalis.nl/s/drw23BHQw5m8rQi

- title: NAT LX AsBuilt 03 de Verleiding 20200529 (pdf)
  url: https://files.museum.naturalis.nl/s/bS2Cb2yr7BE8i3D
- title: NAT LX AsBuilt 03 de Verleiding overzicht op Data-aansluiting (pdf)
  url: https://files.museum.naturalis.nl/s/HR7GzNWekP63ZJP
- title: NAT LX AsBuilt 03 de Verleiding overzicht op DMX nummer (pdf)
  url: https://files.museum.naturalis.nl/s/EMPN8TAYep7WCgA
- title: NAT LX AsBuilt 03 de Verleiding overzicht op Groepnummer (pdf)
  url: https://files.museum.naturalis.nl/s/g5FKXQ7eSBBaTHb
- title: NAT LX AsBuilt 03 de Verleiding overzicht op Posities (pdf)
  url: https://files.museum.naturalis.nl/s/EYAAjaTfQEJX3ty
- title: NAT LX AsBuilt 03 de Verleiding overzicht op Unit nummer (pdf)
  url: https://files.museum.naturalis.nl/s/CnQHqtfT8xey2GA
- title: NAT LX AsBuilt 03 de Verleiding (xls)
  url: https://files.museum.naturalis.nl/s/PwEjZoJPQzBi9iM
- title: NAT LX AsBuilt 03 de Verleiding (lw6)
  url: https://files.museum.naturalis.nl/s/g5J8Zm3ad9pGKZY
- title: NAT LX AsBuilt 03 de Verleiding (vwx)
  url: https://files.museum.naturalis.nl/s/TjAQajdyBeKajnL
- title: NAT LX AsBuilt 03 de Verleiding (xml)
  url: https://files.museum.naturalis.nl/s/dSFt7CoLj5QbQs9


---

{{% pagequality %}}

* Handleidingen en procedures ontbreken
* Afspraken en verantwoordelijkheden ontbreken
* SLAs ontbreken

{{% /pagequality %}}

<!-- Voeg een overzichtsafbeelding toe -->

[![plattegrond](https://files.museum.naturalis.nl/s/xZWtNQaFEyo8L8r/preview)](https://files.museum.naturalis.nl/s/qc3ioHPDNiGZrrn)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de tentoonstelling toe -->

In De verleiding draait alles om voortplanting in de natuur. Elkaar het hof maken, paren en zorgen voor het nageslacht, komen speels aan de orde.

Kijk voor meer info op [naturalis.nl](https://www.naturalis.nl/museum/museumzalen/verleiding)

Klik [hier](https://files.museum.naturalis.nl/s/d7mLRTfRBBrmoxS) voor het definitieve ontwerp.

## Technische omschrijving

<!-- Voeg hier een blokschema van de hele tentoonstelling toe -->

[![kabelschema](https://files.museum.naturalis.nl/s/drw23BHQw5m8rQi/preview)](https://files.museum.naturalis.nl/s/CHGXsE4sffFMzyL)

Het VLAN van deverleiding heeft het subnet 10.130.0.0/16, de showcontroller (CueCore2) heeft het IP adres 10.130.1.1.

## Onderdelen

[![vlekkenplan](https://files.museum.naturalis.nl/s/ZxWfr9MHkbGcWSY/preview)](https://files.museum.naturalis.nl/s/FcPJZa8T9iPY3Jm)

Een functionele en technische beschrijving van de specifieke onderdelen vind je
op de onderstaande pagina's.

### Show

Meer informatie over de show in deze tentoonstelling vind je hier:

{{< pagelist shows >}}

### Interactives

In de tentoonstelling staan de volgende interactives:

{{< pagelist interactives >}}

### Exhibits

In de tentoonstelling staan de volgende exhibits:

{{< pagelist exhibits >}}

### Decors

De volgende decors maken onderdeel uit van de tentoonstelling:

{{< pagelist decors >}}

## Handleidingen en procedures

[Onderhoud algemeen](https://files.museum.naturalis.nl/s/zPsLKiHYKmZXaX6)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues met betrekking tot de gehele
tentoonstelling bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->
* Collectie: [Marnel Scherrenberg](mailto:marnel.scherrenberg@naturalis.nl)
* Ontwerp: [Caroline Breunesse](mailto:caroline.breunesse@naturalis.nl)
* Techniek: [Technisch Team](mailto:support@naturalis.nl)

### SLA

#### Garantie

Indien en geen documentatie te vinden is over garantiebepalingen, dan geldt de algemene overeengekomen regeling. Hieronder nog een keer de toelichting van Erik Jan Elderman op die regeling uit de overeenkomst.
Deze regeling geldt zowel voor de tentoonstellingsbouwers als voor Atatech:

#### Artikel 7 Garantie

* 7.1      Opdrachtnemer blijft aansprakelijk voor herstel door middel van reparatie of vervanging van het geleverde of onderdelen daarvan die binnen 12 maanden na datum van aflevering
gebreken vertonen. Opdrachtnemer is niet tot herstel gehouden indien hij aantoont dat het
gebrek niet is te wijten aan fouten van Opdrachtnemer of door haar geleverde materialen.

* 7.2      De in dit artikel geregelde garantie van Opdrachtnemer houdt in dat het geleverde van
deugdelijk materiaal en met goed vakmanschap is vervaardigd en de montagewerkzaamheden
vakkundig zijn uitgevoerd. Mochten zich binnen de garantietermijn materiaal-, fabricage- of ontwerpfouten voordoen in het geleverde, dan zal Opdrachtnemer zonder berekening van kosten zorgdragen voor reparatie daarvan. Naast de feitelijke herstelkosten zullen ook eventuele aan dit herstel verbonden kosten als kosten gemoeid met montage en demontage van apparatuur; transportkosten van apparatuur, alsmede reis- en verblijfskosten van personeel van Opdrachtnemer; extra kosten in verband met spoedeisend herstel van storingen, die als garantieverplichting moeten worden verholpen; aantoonbare technische gevolgschade, welke optreedt door een voor garantie in aanmerking komend defect onderdeel, door Opdrachtnemer worden vergoed.

* 7.3      Opdrachtnemer staat ervoor in dat het door hem geleverde blijft functioneren conform het Bestek. Mocht binnen een periode van 3 jaar na datum van ondertekening van het overname-protocol, zoals bepaald in artikel 6, aan de hand van systematisch optreden van identieke storingen, blijken dat het opgeleverde of delen daarvan toch niet blijken te voldoen aan het Bestek, zal Opdrachtnemer alsnog zorgdragen voor aanpassing van het geleverde. Opdrachtnemer zal in dat geval het in het kader van deze overeenkomst geleverde door middel van een (ontwerp)-aanpassing herstellen, zonder dat hier voor Naturalis kosten aan zijn verbonden.

Opleverdata verschillen per zaal en per leverancier, maar de meeste zalen/de meeste AVH zijn opgeleverd in juni/juli/augustus 2019. Let op: als binnen die garantietermijn een item vervangen wordt, gaat de garantie van 12 maanden voor dat onderdeel opnieuw in. Oplevering van vervangen onderdelen moet dus goed worden geadministreerd.

### Bouwer

* Software interactives: [YiPP](https://yipp.nl/)
* Hardware: [AtaTECH](https://www.ata-tech.nl/)
* Decor: [Bruns](https://www.bruns.nl/)
* Showcontrol: Rutger van Dijk ([SemMika](https://www.semmika.nl/))
* Licht: [Tim Blom](mailto:blomartt@live.nl)
* Grafisch: Martine
* AV Content: [Studio Kluut](https://aukeflorian.nl/)
* Voiceover opnames: [Bob Kommer Studio's](http://www.bobkommer.com/)
* Handlettering: [Letterhand](https://letterhand.com/)
* Geuren: [Aromaprime](https://aromaprime.com/)
* Glas in lood ramen aan het plafond: [Wat je ziet](https://www.watjeziet.nl/)
* Stressballen voor Hungry Birds: [Multigift](https://www.multigift.nl/nl/antistress-artikelen/ronde-stressballen/stressbal-%C3%B8-63-mm-9355)
* Penismodellen: [Manimal works/Remie Bakker](https://www.manimalworks.com/)
* Papieren vaginamodellen: [Madame Silhouette](https://www.madamesilhouette.nl/)
* Skippyballen: [Decathlon](https://www.decathlon.nl/p/springbal-resist-60-cm-voor-kinderen-van-6-12-jaar/_/R-p-152233?mc=8363520&c=ROZE&orderId=nl606149727)
* Lantaarnpaal: [Akker lantaarnpalen Schijndel](http://www.akkerlantaarnpalen.nl/)
* Etageres: [Fonq](https://www.fonq.nl/product/riviera-maison-soho-etagere/225775/)
* Eier-etagere: [Deens](https://deens.nl)

### Ontwerper

* Totaal: Onno Brouwer
* Ontwerp: [Designwolf](http://designwolf.nl/)
* Grafisch ontwerper: [GraciArtworks](http://graciaartigas.com/)
* Inhoud: [Ilse van Zeeland](mailto:ilse.vanzeeland@naturalis.nl), Monique Koopmans, [Sanne van Gammeren](mailto:sanne.vangammeren@naturalis.nl)
* AV Techniek: [AtaTECH](https://www.ata-tech.nl/)
* Decor: [Bruns](https://www.bruns.nl/)
* Licht: [Tom Verheijen](https://www.tomverheijen.nl/)
* Projecleiding: [Marianne Fokkens](mailto:marianne.fokkens@naturalis.nl)

## Bijlagen

{{< pagelist bijlagen >}}
