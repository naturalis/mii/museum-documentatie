---
title: "dedood"
draft: false
status:
unid: b1a2e00c-55b7-4170-9533-fc87b2f647e8
shows:
- showdood
interactives:
- bontjas
- braakbal
- cirkelprojectie
- gameover
- handscanner
- kruiptunnelstolp
- kruiptunnelzonderstolp
- lampenhemel
- timelapse
- veroudering
- vogelskeletten
- weegschaal
exhibits:
- allesgaatdood
- autobumper
- boomstronk
- buizerdfazantegel
- damhertkadaver
- diepvrieskip
- dinododo
- dodekonijnen
- dodepauw
- dodezwanen
- edelhertkadaver
- gestiktevis
- giercarmen
- gorilla
- jachttrofeeen
- jakhalzen
- muizenval
- oeiikgroei
- olievogel
- rattenkoning
- relikwieenmuseum
- rendiergeweien
- schedels
- slangarend
- slijtage
- stopgezet
- walviskadaver
decors:
- labyrinth
faciliteiten:
attachments:
- title: Definitief Ontwerp (pdf)
  url: https://files.museum.naturalis.nl/s/zxjaASMRcWX5oKt
- title: Vlekkenplan (pdf)
  url: https://files.museum.naturalis.nl/s/y5atkXStBxMz8yA
- title: Vlekkenplan (vwx)
  url: https://files.museum.naturalis.nl/s/qzLZ87Ti8NnANtw
- title: Totaal Kabelschema (pdf)
  url: https://files.museum.naturalis.nl/s/neA6RkdYN7imxiG
- title: Totaal Kabelschema (vwx)
  url: https://files.museum.naturalis.nl/s/p8NYNWP6obMdSpj
- title: Projector Ophanging (vwx)
  url: https://files.museum.naturalis.nl/s/8nnQqMXJ4SNpKeB
- title: 170526_PLTTGRND_eenvoudig_3D (stp)
  url: https://files.museum.naturalis.nl/s/2yt2s6TDwmrxE6c
- title: dood_exhibit_sokkel_g3703_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/Wd55kDj9nFccHp4
- title: dood_montagevoorzieningen_collectie_1 (pdf)
  url: https://files.museum.naturalis.nl/s/ZEF4ERpJyrys2of
- title: dood_montagevoorzieningen_collectie_2 (pdf)
  url: https://files.museum.naturalis.nl/s/WTSWSC8CCnAZjTP
- title: dood_principedetail_collectiemontage_op_staf_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/GNraonKHaideSZr
- title: dood_principetekening_koof_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/RGWgH59i3ZEXiZ4
- title: dood_sokkel_overzicht-bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/4W6br2qRnGgfAME

- title: NAT LX AsBuilt 08 de Dood 20200522 (pdf)
  url: https://files.museum.naturalis.nl/s/LLQPYJYQispQD7o
- title: NAT LX AsBuilt 08 de Dood overzicht op Data aansluiting (pdf)
  url: https://files.museum.naturalis.nl/s/bf9MEQjH5DfibeL
- title: NAT LX AsBuilt 08 de Dood overzicht op DMX nummer (pdf)
  url: https://files.museum.naturalis.nl/s/56rsAFLjKRk5DpG
- title: NAT LX AsBuilt 08 de Dood overzicht op Groepnummer (pdf)
  url: https://files.museum.naturalis.nl/s/6YqbwfzBQmtcrGx
- title: NAT LX AsBuilt 08 de Dood overzicht op Posities (pdf)
  url: https://files.museum.naturalis.nl/s/xiHPd6idkEEWAii
- title: NAT LX AsBuilt 08 de Dood overzicht op Unit nummer (pdf)
  url: https://files.museum.naturalis.nl/s/miAQ76qNgK33mGd
- title: NAT LX AsBuilt 08 de Dood (xls)
  url: https://files.museum.naturalis.nl/s/XRFZsSRw9kH54BT
- title: NAT LX AsBuilt 08 de Dood (lw6)
  url: https://files.museum.naturalis.nl/s/XiFQBEa6fDgdpZR
- title: NAT LX AsBuilt 08 de Dood (vwx)
  url: https://files.museum.naturalis.nl/s/RJmPA9nAFcGb6SF
- title: NAT LX AsBuilt 08 de Dood (xml)
  url: https://files.museum.naturalis.nl/s/FAoz6cwfxe2E8fb

---



![dedood](https://files.museum.naturalis.nl/s/PjdXs6PDm7WKPai/preview)

<!-- Voeg een overzichtsafbeelding toe -->
[Bekijk hier het
vlekkenplan](https://files.museum.naturalis.nl/s/y5atkXStBxMz8yA) van de
tentoonstelling.

## Functionele omschrijving

‘Welkom in het doolhof van de dood’. De bezoeker begrijpt meteen wat de
bedoeling is. Al dwalende zal hij langzaam zijn doel bereiken. Wat dat is,
blijft nog even spannend; een oplichtende cirkelvormige ruimte verderop. De
tentoonstelling is niet in één keer te overzien. Door een patroon in
cirkelvormige wanden valt licht naar buiten. Mooi aangelichte objecten bevinden
zich hierbinnen op verschillende hoogten. Elke bocht of nis geeft een nieuwe
verrassing. Onderweg komt de bezoeker van alles tegen. Kraaien, en met name hun
gekras dat af en toe te horen is, zorgen voor een theatrale toevoeging.

## Technische omschrijving

<!-- Voeg hier een blokschema van de hele tentoonstelling toe -->

Het VLAN van dedood heeft het subnet 10.137.0.0/16, de showcontroller (CureCore2) heeft het IP adres 10.137.1.1.

## Onderdelen

Een functionele en technische beschrijving van de specifieke onderdelen vind je
op de onderstaande pagina's.

### Show

Meer informatie over de show in deze tentoonstelling vind je hier:

{{< pagelist shows >}}

### Interactives

In de tentoonstelling staan de volgende interactives:

{{< pagelist interactives >}}

### Exhibits

In de tentoonstelling staan de volgende exhibits:

{{< pagelist exhibits >}}

### Decors

De volgende decors maken onderdeel uit van de tentoonstelling:

{{< pagelist decors >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues met betrekking tot de gehele
tentoonstelling bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->
- [Caroline Breunesse](mailto:caroline.breunesse@naturalis.nl)
- [Technisch Team](mailto:support@naturalis.nl)
- Projectleider: [Matthijs Vegter](mailto:matthijs.vegter@naturalis.nl)

### SLA

#### Garantie

Indien en geen documentatie te vinden is over garantiebepalingen, dan geldt de algemene overeengekomen regeling. Hieronder nog een keer de toelichting van Erik Jan Elderman op die regeling uit de overeenkomst.
Deze regeling geldt zowel voor de tentoonstellingsbouwers als voor Atatech:

#### Artikel 7 Garantie

- 7.1      Opdrachtnemer blijft aansprakelijk voor herstel door middel van reparatie of vervanging van het geleverde of onderdelen daarvan die binnen 12 maanden na datum van aflevering
gebreken vertonen. Opdrachtnemer is niet tot herstel gehouden indien hij aantoont dat het
gebrek niet is te wijten aan fouten van Opdrachtnemer of door haar geleverde materialen.

- 7.2      De in dit artikel geregelde garantie van Opdrachtnemer houdt in dat het geleverde van
deugdelijk materiaal en met goed vakmanschap is vervaardigd en de montagewerkzaamheden
vakkundig zijn uitgevoerd. Mochten zich binnen de garantietermijn materiaal-, fabricage- of ontwerpfouten voordoen in het geleverde, dan zal Opdrachtnemer zonder berekening van kosten zorgdragen voor reparatie daarvan. Naast de feitelijke herstelkosten zullen ook eventuele aan dit herstel verbonden kosten als kosten gemoeid met montage en demontage van apparatuur; transportkosten van apparatuur, alsmede reis- en verblijfskosten van personeel van Opdrachtnemer; extra kosten in verband met spoedeisend herstel van storingen, die als garantieverplichting moeten worden verholpen; aantoonbare technische gevolgschade, welke optreedt door een voor garantie in aanmerking komend defect onderdeel, door Opdrachtnemer worden vergoed.

- 7.3      Opdrachtnemer staat ervoor in dat het door hem geleverde blijft functioneren conform het Bestek. Mocht binnen een periode van 3 jaar na datum van ondertekening van het overname-protocol, zoals bepaald in artikel 6, aan de hand van systematisch optreden van identieke storingen, blijken dat het opgeleverde of delen daarvan toch niet blijken te voldoen aan het Bestek, zal Opdrachtnemer alsnog zorgdragen voor aanpassing van het geleverde. Opdrachtnemer zal in dat geval het in het kader van deze overeenkomst geleverde door middel van een (ontwerp)-aanpassing herstellen, zonder dat hier voor Naturalis kosten aan zijn verbonden.

Opleverdata verschillen per zaal en per leverancier, maar de meeste zalen/de meeste AVH zijn opgeleverd in juni/juli/augustus 2019. Let op: als binnen die garantietermijn een item vervangen wordt, gaat de garantie van 12 maanden voor dat onderdeel opnieuw in. Oplevering van vervangen onderdelen moet dus goed worden geadministreerd.

### Bouwer

- Techniek: [Ata Tech](https://ata-tech.nl)
- Decor: [Kloosterboer](http://kloosterboer-decor.nl)
- Show: Rutger van Dijk ([SemMika](https://www.semmika.nl/))
- Licht: [Tim Blom](mailto:blomartt@live.nl)

### Ontwerper

- Tentoonstelling: [Kossmann.DeJong](https://www.kossmanndejong.nl/)
- Animatie: [Shosho](http://www.shosho.nl/)
- Show: Rutger van Dijk ([SemMika](https://www.semmika.nl/))
- Filmprojectie & audio [Redrum](www.redrumbureau.com)
- Content animaties en interactives: [Shosho](http://www.shohos.nl)
- Lichtontwerp: [Heinz Loopstra Light Design](http://heinzloopstra.com/)
- Muziek: [Stijn Hosman](http://www.stijnhosman.com/)

## Bijlagen

{{< pagelist bijlagen >}}
