---
title: "dinotijd"
draft: false
status:
unid: 7065e48f-5a06-4bb3-9383-7cea70853e8f
shows:
- showdinotijd
interactives:
- camarasaurus
- dinovoetstappen
- edmontosaurus
- edmontosaurusspel
- plateosaurus
- stegosaurus
- trex
- triceratops
- troodonpeppersghost
- waterwand
exhibits:
- dinofossielen
- dinovogels
- evolutie
- mosasaurus
- pterodactylus
- troodon
faciliteiten:
- infopuntdinotijd
attachments:
- title: Vlekkenplan (pdf)
  url: https://files.museum.naturalis.nl/s/JbwQ5nzy6PmrR5o
- title: Vlekkenplan (vwx)
  url: https://files.museum.naturalis.nl/s/4nbz8x7XTEMm4Nw
- title: Zaalontwerp (pdf)
  url: https://files.museum.naturalis.nl/s/BscGKk7gTMr5nRj
- title: Totaal kabelschema AV (pdf)
  url: https://files.museum.naturalis.nl/s/xxAf4xCn7aqF7Mz
- title: Totaal kabelschema AV (vwx)
  url: https://files.museum.naturalis.nl/s/Y8933EmT637YjBR
- title: Conceptueel ontwerp (pdf)
  url: https://files.museum.naturalis.nl/s/kNsi5ErCyfZnPs2
- title: Grafisch ontwerp (pdf)
  url: https://files.museum.naturalis.nl/s/LkyE9bWy8xz7jft
- title: Glasplaten ontwerp (pdf)
  url: https://files.museum.naturalis.nl/s/yaGYxWdE3w3ty9T
- title: Ontwerp details (pdf)
  url: https://files.museum.naturalis.nl/s/dw4wR4mZywo5qWD
- title: Ontwerp details (dwg)
  url: https://files.museum.naturalis.nl/s/qqpwZ5nME79LCZp
- title: Vloer aansluitingen (pdf)
  url: https://files.museum.naturalis.nl/s/RwmkWqtnzrBXDwX
- title: Decor ontwerp (pdf)
  url: https://files.museum.naturalis.nl/s/BscGKk7gTMr5nRj
- title: dino_plattegrond_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/s5cdnwYCW8DgQSi
- title: dino_trussverdeling_dinozaal_plattegrond (pdf)
  url: https://files.museum.naturalis.nl/s/RzckGnJS3Z7z4cw
- title: NAT LX AsBuilt 03 Dinotijd 20200525 (pdf)
  url: https://files.museum.naturalis.nl/s/GmDABiBiH6Ww9oS
- title: NAT LX AsBuilt 03 Dinotijd overzicht op Data aansluiting (pdf)
  url: https://files.museum.naturalis.nl/s/LmEezZapakfFTXR
- title: NAT LX AsBuilt 03 Dinotijd overzicht op DMX nummer (pdf)
  url: https://files.museum.naturalis.nl/s/cygizmDdFwjS8yc
- title: NAT LX AsBuilt 03 Dinotijd overzicht op Groepnummer (pdf)
  url: https://files.museum.naturalis.nl/s/96kzsS8jrAqWYG7
- title: NAT LX AsBuilt 03 Dinotijd overzicht op Posities (pdf)
  url: https://files.museum.naturalis.nl/s/5on3mFtZZqgq5td
- title: NAT LX AsBuilt 03 Dinotijd overzicht op Unit nummer (pdf)
  url: https://files.museum.naturalis.nl/s/qqQPY2w9KQiGkwF
- title: NAT LX AsBuilt 03 Dinotijd (xls)
  url: https://files.museum.naturalis.nl/s/ZqckNspg2bk7rAr
- title: NAT LX AsBuilt 03 Dinotijd (lw6)
  url: https://files.museum.naturalis.nl/s/MZo6q8cpSaLJRxp
- title: NAT LX AsBuilt 03 Dinotijd (vwx)
  url: https://files.museum.naturalis.nl/s/cmETYoSH4pX7B39
- title: NAT LX AsBuilt 03 Dinotijd (xml)
  url: https://files.museum.naturalis.nl/s/3WToRSDGW4KBPjA

---


![Plattegrond](https://files.museum.naturalis.nl/s/mgZHQTsXMFCs7BH/preview)

## Functionele omschrijving

Dinotijd toont een aantal topstukken uit de Naturalis collectie in
combinatie met achtergrondprojectie, context en periodeinformatie. De
bezoeker wordt langs de diverse specimen geleid en leert al kijkende
meer over het onstaan van de dino's de perioden waarin zij leefden en
welke soorten bij welke periode horen. Ook wordt de T-Rex getoond en
een verband gelegd met de evolutie naar de vogels.

## Technische omschrijving

![Vlekkenplan](https://files.museum.naturalis.nl/s/iT8g5iZH32BRgBc/preview)

Technisch bestaat de zaal uit projectie per specimen, achtergrondsgeluid
en sfeerverlichting en een projectie aan het begin van de tentoonstelling.

Het VLAN van dinotijd heeft het subnet 10.139.0.0/16, de showcontroller (Pharos) heeft het IP adres 10.139.1.1.

## Onderdelen

Een functionele en technische beschrijving van de specifieke onderdelen vind je
op de onderstaande pagina's.

### Show

Meer informatie over de show in deze tentoonstelling vind je hier:

{{< pagelist shows >}}

### Interactives

In de tentoonstelling staan de volgende interactives:

{{< pagelist interactives >}}

### Exhibits

In de tentoonstelling staan de volgende exhibits:

{{< pagelist exhibits >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

Op dit moment zijn er geen known issues met betrekking tot de gehele
tentoonstelling bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

* Collectie:
* Technisch beheer: [Technisch Team](mailto:support@naturalis.nl)
* Projectleider: [Matthijs Vegter](mailto:matthijs.vegter@naturalis.nl)

### SLA

#### Garantie

Indien en geen documentatie te vinden is over garantiebepalingen, dan geldt de algemene overeengekomen regeling. Hieronder nog een keer de toelichting van Erik Jan Elderman op die regeling uit de overeenkomst.
Deze regeling geldt zowel voor de tentoonstellingsbouwers als voor Atatech:

#### Artikel 7 Garantie

* 7.1      Opdrachtnemer blijft aansprakelijk voor herstel door middel van reparatie of vervanging van het geleverde of onderdelen daarvan die binnen 12 maanden na datum van aflevering
gebreken vertonen. Opdrachtnemer is niet tot herstel gehouden indien hij aantoont dat het
gebrek niet is te wijten aan fouten van Opdrachtnemer of door haar geleverde materialen.

* 7.2      De in dit artikel geregelde garantie van Opdrachtnemer houdt in dat het geleverde van
deugdelijk materiaal en met goed vakmanschap is vervaardigd en de montagewerkzaamheden
vakkundig zijn uitgevoerd. Mochten zich binnen de garantietermijn materiaal-, fabricage- of ontwerpfouten voordoen in het geleverde, dan zal Opdrachtnemer zonder berekening van kosten zorgdragen voor reparatie daarvan. Naast de feitelijke herstelkosten zullen ook eventuele aan dit herstel verbonden kosten als kosten gemoeid met montage en demontage van apparatuur; transportkosten van apparatuur, alsmede reis- en verblijfskosten van personeel van Opdrachtnemer; extra kosten in verband met spoedeisend herstel van storingen, die als garantieverplichting moeten worden verholpen; aantoonbare technische gevolgschade, welke optreedt door een voor garantie in aanmerking komend defect onderdeel, door Opdrachtnemer worden vergoed.

* 7.3     Opdrachtnemer staat ervoor in dat het door hem geleverde blijft functioneren conform het Bestek. Mocht binnen een periode van 3 jaar na datum van ondertekening van het overname-protocol, zoals bepaald in artikel 6, aan de hand van systematisch optreden van identieke storingen, blijken dat het opgeleverde of delen daarvan toch niet blijken te voldoen aan het Bestek, zal Opdrachtnemer alsnog zorgdragen voor aanpassing van het geleverde. Opdrachtnemer zal in dat geval het in het kader van deze overeenkomst geleverde door middel van een (ontwerp)-aanpassing herstellen, zonder dat hier voor Naturalis kosten aan zijn verbonden.

Opleverdata verschillen per zaal en per leverancier, maar de meeste zalen/de meeste AVH zijn opgeleverd in juni/juli/augustus 2019. Let op: als binnen die garantietermijn een item vervangen wordt, gaat de garantie van 12 maanden voor dat onderdeel opnieuw in. Oplevering van vervangen onderdelen moet dus goed worden geadministreerd.

### Bouwer

* Projectleider: Wendy Rameckers
* Decor: [Kloosterboer](http://www.kloosterboer-decor.nl/) & Menno de Wit
* Show: Rutger van Dijk ([SemMika](https://www.semmika.nl/))
* Licht: [Tim Blom](mailto:blomartt@live.nl)
* Animatie: [JellyFish](https://jellyfishpictures.co.uk/)
* Geluid: [JellyFish](https://jellyfishpictures.co.uk/)
* Hardware: [AtaTECH](https://www.ata-tech.nl/)
* Prints doek: [PPS imaging (onderaannemer van Kloosterboer)](https://pps-imaging.nl/)
* Animatie evolutie gewervelden: [Redrum studio](https://www.redrumbureau.com/)
* Montage/constructie camarasaurus: LIS (geen andere info ontvangen)
* Staalwerk camarasaurus: [Hoogeveen Staalconstructies](https://www.hoogeveenstaalconstructie.nl/)
* Montage/constructie Triceratops: [Nichecraft (Remmert Schouten)](https://uk.linkedin.com/in/remmert-schouten-a69038a)
* Hijswerkzaamheden: [Witteveen](https://witkraan.nl/mini-hijskranen/)
* Sensors Beveiliging skeletten: [Euronova-nl](http://www.euronova-nl.nl/)

### Ontwerper

* Licht: [DHA light design / Adam Grater](http://dhadesigns.com/the-team)
* Tentoonstelling: [Event](http://eventcomm.com/)
* Animatie: [JellyFish](https://jellyfishpictures.co.uk/)
* Geluid: [JellyFish](https://jellyfishpictures.co.uk/)

## Bijlagen

{{< pagelist bijlagen >}}
