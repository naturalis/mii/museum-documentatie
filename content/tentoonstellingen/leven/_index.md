---
title: "leven"
draft: false
status:
unid: eb79de62-a79d-43d7-a4d4-8ca62f1b1a04
shows:
- showleven
interactives:
- projectiebeweging
- projectiediepzee
- projectiegetijden
- projectiegiraf
- projectiekleinedieren
- projectieluchtdieren
- projectieolifant
- projectiewaterplaats
exhibits:
- apenberg
- berenhoek
- diepzee
- diepzeestenen
- getijdenzone
- kameelenco
- kattenberg
- kust
- olifantenco
- openwater
- schemerzone
- vissengang
- vogeldal
- waterplaats
- zoetwater
decors:
faciliteiten:
- infopuntleven
attachments:
- title: Vlekkenplan zee (pdf)
  url: https://files.museum.naturalis.nl/s/gBX5AfX3yb6pKDw
- title: Vlekkenplan land (pdf)
  url: https://files.museum.naturalis.nl/s/9ZR8WQrsPJq5BYR
- title: Vlekkenplan (vwx)
  url: https://files.museum.naturalis.nl/s/aEwG3PYG3HC4yDW
- title: Leven totaal 3D (dwfx/autocad)
  url: https://files.museum.naturalis.nl/s/t5LjaDNJiqsSNWG
- title: Kabelschema totaal (pdf)
  url: https://files.museum.naturalis.nl/s/J8cEQYNLRL32R22
- title: Kabelschema totaal (vwx)
  url: https://files.museum.naturalis.nl/s/jrZGdZgnPpHjH2N
- title: ID sheets (pdf)
  url: https://files.museum.naturalis.nl/s/Hzk6HfRTrdam3ea
- title: Definitief ontwerp (pdf)
  url: https://files.museum.naturalis.nl/s/9ZR8WQrsPJq5BYR
- title: Leven functies (pdf)
  url: https://files.museum.naturalis.nl/s/w6sKARA2zmoF3WZ
- title: Leven Definitief Ontwerp Samenvatting (pdf)
  url: https://files.museum.naturalis.nl/s/FDksDddw9TrLPFX
- title: Afzetpaal podia bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/AKdYb4Kqb25mDPG
- title: Afzetpaal gewijzigd 150mm bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/NEf3SymD6stLzQL
- title: leven_constructie_vergelijking_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/KdAfmm2go6aiomJ
- title: leven_hoogwerker_stempelplaatsen_plattegrond (pdf)
  url: https://files.museum.naturalis.nl/s/oPeRgQ4ao4rynmq
- title: leven_staalconstructie_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/8zeiKoR2sjXxwLB
- title: leven_staalconstructie_01_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/zgrBTXHp2gMLfAj
- title: leven_houtconstructie_02_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/HAy4aCeeWPRNqWC
- title: leven_staal_houtconstructie_03_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/ZYww3KwjBwaYrzy
- title: leven_lightsheets_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/tfKDXSN7Q29gSSP
- title: leven_looppad_model_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/nTaKeS4Mb5EoKAf
- title: leven_schoonmaaklooppaden_noraplan_instructie (pdf)
  url: https://files.museum.naturalis.nl/s/t79SR5Y6GmjS55Z
- title: leven_noodgang_detail_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/kEwi5n5fRKELgga
- title: leven_onderhoud_geoliedepodia_instructie (pdf)
  url: https://files.museum.naturalis.nl/s/m9HcyteXyG7fZod
- title: leven_podia_afzetting_luchtdeel_plattegrond (pdf)
  url: https://files.museum.naturalis.nl/s/2Z5yoQkWgwagTyj
- title: leven_podia_afzetting_onderwater_plattegrond (pdf)
  url: https://files.museum.naturalis.nl/s/FnbEzBY7nRKjBbE
- title: leven_totaal_opslag_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/4KQFd3GCXwKty9s
- title: leven_totaal03_bouwtekening (pfd)
  url: https://files.museum.naturalis.nl/s/ZXRbAeSknn7qYXH
- title: leven_totaal06_bouwtekening (pfd)
  url: https://files.museum.naturalis.nl/s/Jo7N8iqNTpdMDYA
- title: leven_trapconstructie1_bouwtekening (pfd)
  url: https://files.museum.naturalis.nl/s/WRRG6wSGgRMdXj6
- title: leven_trapconstructie2_bouwtekening (pfd)
  url: https://files.museum.naturalis.nl/s/obpE5gSsgYkRFaH
- title: leven_verlichting_decorspots_ stelbaarhoog_bouwtekening (pfd)
  url: https://files.museum.naturalis.nl/s/YnbGLqJFJQXq5pf
- title: leven_verlichting_inbouwLED_ 45gradendetail_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/bgzmiR8cmGB4pC9
- title: leven_verlichting_inbouwLED_bouwtekening (pfd)
  url: https://files.museum.naturalis.nl/s/FG5xJcikmPCF8m5
- title: leven_vloeren_05_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/GSGCRFxk3cjdFqF
- title: leven_wanden_plafonds_06_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/zSHgsmaTWMCnDxq
- title: leven_interactives_projectieopstellingen_luchtdeel_plattegrond (pdf)
  url: https://files.museum.naturalis.nl/s/Ss6ERZnXQqrN9AA
- title: leven_interactives_schermframe_groot_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/tcL2jkHN2ZHpQW6
- title: leven_interactives_schermframe_klein_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/RES6GTWbCnW7NFX
- title: leven_interactives_schermframe1_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/bBrWA32ZFoH9WH4
- title: leven_interactives_schermframe1_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/HmXpBLYmzFmzpfT
- title: Lichtarmaturen (pdf)
  url: https://files.museum.naturalis.nl/s/2N2n99JCPqNjxtf

- title: NAT LX AsBuilt 02 Leven 20200525 (pdf)
  url: https://files.museum.naturalis.nl/s/CjWsA4eEsZz4bpe
- title: NAT LX AsBuilt 02 Leven overzicht op Data-aansluiting (pdf)
  url: https://files.museum.naturalis.nl/s/E4op23AAHNDYLqJ
- title: NAT LX AsBuilt 02 Leven overzicht op DMX nummer (pdf)
  url: https://files.museum.naturalis.nl/s/kj8fABsM3iXnb3q
- title: NAT LX AsBuilt 02 Leven overzicht op Groepnummer (pdf)
  url: https://files.museum.naturalis.nl/s/5j5eKQ4HmFWipm9
- title: NAT LX AsBuilt 02 Leven overzicht op Posities (pdf)
  url: https://files.museum.naturalis.nl/s/rbC4qsZbkfKdrem
- title: NAT LX AsBuilt 02 Leven overzicht op Unit nummer (pdf)
  url: https://files.museum.naturalis.nl/s/kDCMiqRAMakp5Wx
- title: NAT LX AsBuilt 02 Leven (xls)
  url: https://files.museum.naturalis.nl/s/K9QPfXs6dcsJRsn
- title: NAT LX AsBuilt 02 Leven (lw6)
  url: https://files.museum.naturalis.nl/s/iX6eZP4joMfjx6F
- title: NAT LX AsBuilt 02 Leven (vwx)
  url: https://files.museum.naturalis.nl/s/W4wJdwqbinz9LYn
- title: NAT LX AsBuilt 02 Leven (xml)
  url: https://files.museum.naturalis.nl/s/A2mfrDwNfW3pRFW
---

![leven](https://files.museum.naturalis.nl/s/ekQNSfJYcgsedce/preview)

Bekijk [hier het
vlekkenplan van het deel onder water](https://files.museum.naturalis.nl/s/gBX5AfX3yb6pKDw)
(het zee-deel) en bekijk [hier het vlekkenplan boven water](https://files.museum.naturalis.nl/s/9ZR8WQrsPJq5BYR) (het
land-deel).

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de tentoonstelling toe -->
De tentoonstelling laat de enorme diversiteit in de natuur zien, zij het dat de
nadruk ligt op dieren. Planten laten zich - gedroogd of verkleurd als ze zijn -
moeilijk levendig tentoonstellen en zullen dan ook met name in filmprojecties
te zien zijn. Daarin vormen ze onderdeel van een ecosysteem en laten ze een
samenhang zien met andere organismen. In samenwerking met Educatie is een
aantal groeperingen gemaakt, van objecten die een bepaalde relatie hebben op
het gebied van vorm en functie; verleiding, afweer, camouflage en dergelijke.
Ook kleine dieren komen aan bod; deze worden op een verrassende manier in
clusters tentoongesteld. Al met al ligt de nadruk op de levende natuur. Stenen
zijn (behalve in film) slechts een klein onderdeel van de tentoonstelling,
omdat deze een museale opstelling behoeven, terwijl we in deze tentoonstelling
juist geen vitrines willen.

## Technische omschrijving

![zee](https://files.museum.naturalis.nl/s/s4mRPHjAjnBsfBK/preview)

De drie verschillende habitats, Water, Land en Lucht zijn belangrijk. Niet
alleen als ‘natuurlijke’ omgeving voor de collectie, maar ook voor de
oriëntatie van de bezoeker. Het ruimtelijk concept bestaat uit een abstract
landschap dat de bezoeker lineair door de verschillende habitats begeleid. Het
pad en het landschap hebben dezelfde uitstraling, waardoor de bezoeker deel
gaat uitmaken van het landschap. Het landschap heeft reliëf en rotspartijen
waardoor de bezoeker niet altijd overzicht heeft over de totale ruimte.
Zodoende kunnen we dramatische momenten creëren. Het pad loopt tijdens de
wandeling langzaam omhoog, waardoor de bezoeker uiteindelijk een verdieping
hoger uitkomt dan waar hij begonnen is. De overgang water-land is abrupt en
visueel spectaculair omdat deze overgang voor ons als mens grote impact heeft.
Als de bezoeker tussen de vissen ‘onder water’ loopt, kan hij door het
wateroppervlak heen al enige landdieren (en bezoekers) ontwaren. Als hij zich
later op het land bevindt, ziet hij andere bezoekers onder water staan.

![land](https://files.museum.naturalis.nl/s/tXwXkCJFo7EwLn7/preview)

Om de collectie context en leven te geven is er in de ruimte een aantal grote
projecties geplaatst. Deze films vormen samen met dynamisch licht en geluid een
show van ongeveer 15 minuten. In deze show worden thema’s als verleiding, eten
en gegeten worden en slapen behandeld. Tussen de verschillende thema’s worden
relatief rustige natuurbeelden getoond van bijvoorbeeld een opkomende zon of
een enorme regenbui. Speciale aandacht is er voor de flora, die op een
wandvullende projectie goed tot haar recht komt. De combinatie van beeld,
licht, geluid, landschap en collectie maakt dat de bezoeker ervaart onderdeel
van de natuur te zijn

Het VLAN van leven heeft het subnet 10.129.0.0/16, de showcontroller (CureCore2) heeft het IP adres 10.129.1.1.

## Onderdelen

Een functionele en technische beschrijving van de specifieke onderdelen vind je
op de onderstaande pagina's.

### Show

Meer informatie over de show in deze tentoonstelling vind je hier:

{{< pagelist shows >}}

### Interactives

In de tentoonstelling staan de volgende interactives:

{{< pagelist interactives >}}

### Exhibits

In de tentoonstelling staan de volgende exhibits:

{{< pagelist exhibits >}}

### Decors

De volgende decors maken onderdeel uit van de tentoonstelling:

{{< pagelist decors >}}

### faciliteiten

In de tentoonstelling staan de volgende facaliteiten:

{{< pagelist faciliteiten >}}

## Handleidingen en procedures

* [onderhoud geoliedepodia instructie](https://files.museum.naturalis.nl/s/m9HcyteXyG7fZod)
* [schoonmaaklooppaden noraplan instructie](https://files.museum.naturalis.nl/s/t79SR5Y6GmjS55Z)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues met betrekking tot de gehele
tentoonstelling bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->
* Project: [Sjan Janssen](mailto:Sjan.Janssen@naturalis.nl)
* Technisch: [Technisch Team](mailto:support@naturalis.nl)

### SLA

#### Garantie

Indien en geen documentatie te vinden is over garantiebepalingen, dan geldt de algemene overeengekomen regeling. Hieronder nog een keer de toelichting van Erik Jan Elderman op die regeling uit de overeenkomst.
Deze regeling geldt zowel voor de tentoonstellingsbouwers als voor Atatech:

#### Artikel 7 Garantie

* 7.1      Opdrachtnemer blijft aansprakelijk voor herstel door middel van reparatie of vervanging van het geleverde of onderdelen daarvan die binnen 12 maanden na datum van aflevering
gebreken vertonen. Opdrachtnemer is niet tot herstel gehouden indien hij aantoont dat het
gebrek niet is te wijten aan fouten van Opdrachtnemer of door haar geleverde materialen.

* 7.2      De in dit artikel geregelde garantie van Opdrachtnemer houdt in dat het geleverde van
deugdelijk materiaal en met goed vakmanschap is vervaardigd en de montagewerkzaamheden
vakkundig zijn uitgevoerd. Mochten zich binnen de garantietermijn materiaal-, fabricage- of ontwerpfouten voordoen in het geleverde, dan zal Opdrachtnemer zonder berekening van kosten zorgdragen voor reparatie daarvan. Naast de feitelijke herstelkosten zullen ook eventuele aan dit herstel verbonden kosten als kosten gemoeid met montage en demontage van apparatuur; transportkosten van apparatuur, alsmede reis- en verblijfskosten van personeel van Opdrachtnemer; extra kosten in verband met spoedeisend herstel van storingen, die als garantieverplichting moeten worden verholpen; aantoonbare technische gevolgschade, welke optreedt door een voor garantie in aanmerking komend defect onderdeel, door Opdrachtnemer worden vergoed.

* 7.3      Opdrachtnemer staat ervoor in dat het door hem geleverde blijft functioneren conform het Bestek. Mocht binnen een periode van 3 jaar na datum van ondertekening van het overname-protocol, zoals bepaald in artikel 6, aan de hand van systematisch optreden van identieke storingen, blijken dat het opgeleverde of delen daarvan toch niet blijken te voldoen aan het Bestek, zal Opdrachtnemer alsnog zorgdragen voor aanpassing van het geleverde. Opdrachtnemer zal in dat geval het in het kader van deze overeenkomst geleverde door middel van een (ontwerp)-aanpassing herstellen, zonder dat hier voor Naturalis kosten aan zijn verbonden.

Opleverdata verschillen per zaal en per leverancier, maar de meeste zalen/de meeste AVH zijn opgeleverd in juni/juli/augustus 2019. Let op: als binnen die garantietermijn een item vervangen wordt, gaat de garantie van 12 maanden voor dat onderdeel opnieuw in. Oplevering van vervangen onderdelen moet dus goed worden geadministreerd.

### Bouwer

* Techniek: [Ata Tech](https://ata-tech.nl)
* Decor, Lightsheets, Projectieschermen: [Kloosterboer](http://kloosterboer-decor.nl)
* MDF decor van de scheg en getijdenzone: [MOprojects GmbH](http://moprojects.de)
* Films/projecies en audio: [Shosho](http://www.shosho.nl/)
* Show: Rutger van Dijk ([SemMika](https://www.semmika.nl/))
* Lichttechniek: Tim Blom ([Blom Art & ights](mailto:blomartt@live.nl))
* Preparaat drinkende giraffe & collectie montage: [Inside Out Animals](http://www.insideoutanimals.com/)
* Montage collectie: [De Omslag / Jaap Bosma](http://www.de-omslag.nl/)

### Ontwerper

* Tentoonstelling: [Kossmann.DeJong](https://www.kossmanndejong.nl/)
* Licht: [Marc Heinz](https://www.marcheinz.com)
* Films: [Shosho](http://www.shosho.nl/)
* Show: Rutger van Dijk ([SemMika](https://www.semmika.nl/))
* Rechten filmbeelden: [Getty Images (Netherlands) B.V.](mailto:mahmud.saeid@gettyimages.com)

## Bijlagen

{{< pagelist bijlagen >}}
