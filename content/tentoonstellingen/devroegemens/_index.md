---
title: "devroegemens"
date: 2019-07-24T13:48:49Z
draft: false
status:
unid: dae16819-e870-4598-90c4-a4b5b375925b
shows:
- showvroegemens
interactives:
- vroegemensportaal
- filmdubois
- schaduwspel
exhibits:
- homoniden
- schatkamermens
decors:
faciliteiten:
attachments:
- title: definitiefontwerp (pdf)
  url: https://files.museum.naturalis.nl/f/52947
- title: definitiefontwerp vlekkenplan (pdf)
  url: https://files.museum.naturalis.nl/s/PKwcgMnnsQfFYqS
- title: kabelschema av (pdf)
  url: https://files.museum.naturalis.nl/s/42o4Cs5ADcpsCNy
- title: kabelschema av (vwx)
  url: https://files.museum.naturalis.nl/s/mndfQ2LTtNGCXPH
- title: onderhoudsvoorschriften (pdf)
  url: https://files.museum.naturalis.nl/s/ZEa3tnKnfFSobwi
- title: vlekkenplan av (pdf)
  url: https://files.museum.naturalis.nl/s/KzaTsnZYS3pGmRj
- title: vlekkenplan av (vwx)
  url: https://files.museum.naturalis.nl/s/eg4B9GFNdkCqYCc
- title: devroegemens_zaalplattegrond (pdf)
  src: https://files.museum.naturalis.nl/s/NmCNNn6b9E8qXrz
- title: devroegemens_entreeportaal en tribune_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/mztPKAbi32dTnwi

- title: NAT LX AsBuilt 07 de Vroege Mens 20200512 (pdf)
  url: https://files.museum.naturalis.nl/s/b2q8w9NdtZjQ77w
- title: NAT LX AsBuilt 07 de Vroege Mens overzicht op Data aansluiting (pdf)
  url: https://files.museum.naturalis.nl/s/xtKbBR4xSwHGp9t
- title: NAT LX AsBuilt 07 de Vroege Mens overzicht op DMX nummer (pdf)
  url: https://files.museum.naturalis.nl/s/caTzpCwq6HE35PA
- title: NAT LX AsBuilt 07 de Vroege Mens overzicht op Groepnummer (pdf)
  url: https://files.museum.naturalis.nl/s/gPQJ6s4mRBorYW9
- title: NAT LX AsBuilt 07 de Vroege Mens overzicht op Posities (pdf)
  url: https://files.museum.naturalis.nl/s/8jcJ9E53pAiLyWR
- title: NAT LX AsBuilt 07 de Vroege Mens overzicht op Unit nummer (pdf)
  url: https://files.museum.naturalis.nl/s/BYKQq7djpsyPzRD
- title: NAT LX AsBuilt 07 de Vroege Mens (xls)
  url: https://files.museum.naturalis.nl/s/WxiLoXwDptBYTYX
- title: NAT LX AsBuilt 07 de Vroege Mens (lw6)
  url: https://files.museum.naturalis.nl/s/kW6XzAaQeKmJkf9
- title: NAT LX AsBuilt 07 de Vroege Mens_2 (vwx)
  url: https://files.museum.naturalis.nl/s/qrFASo9TykkgKJg
- title: NAT LX AsBuilt 07 de Vroege Mens_2 (xml)
  url: https://files.museum.naturalis.nl/s/tbWeETeHCbZRxZF

---

<!-- Voeg een overzichtsafbeelding toe -->
[![vlekkenplan](https://files.museum.naturalis.nl/s/3Z7dPxWqfYAY3Aq/preview)](https://files.museum.naturalis.nl/s/PKwcgMnnsQfFYqS)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de tentoonstelling toe -->

Naturalis bezit een van de belangrijkste fossielen op het gebied van menselijke
evolutie: de resten van Homo erectus en de bekraste schelp, gevonden door de
Nederlandse onderzoeker Eugène Dubois.

Deze vaste presentatie vertelt het verhaal van Dubois’ zoektocht en toont deze
onbetwiste topstukken als een schat. Een reconstructie van Homo erectus door
Kennis & Kennis maken de -voor de meeste bezoekers- moeilijk ‘leesbare’
fossielen toegankelijk en schijnen licht op dit sleutelfiguur uit de menselijke
evolutie.

* Typering: Museum
* Formaat: 164 m²
* Hoogte: 6 meter
* Ligging: Nieuwbouw laag 4
* Icoon: Dubois fossielen
* Lengte bezoekerservaring: 10 minuten
* Bezoekerscapaciteit:
  * per moment 75
  * per uur: 300

Na een compacte entreezone (interactive [vroegemensportaal]( ../../interactives/vroegemensportaal/_index.md)) bestaat de zaal uit vier delen:

1. een AV presentatie van ca. vijf minuten over Dubois en zijn zoektocht naar
   'the missing link' (interactive [filmdubois](../../interactives/filmdubois/_index.md))
1. een schatkamer met alleen de vier topstukken van Dubois: schedelkapje, kies,
   dijbeen en schelp; gepresenteerd met compacte uitleg en link (peppers’
   ghost?) naar silhouet Homo erectus, om botten in context te plaatsen
   (exhibit [schatkamermens](../../exhibits/schatkamermens/_index.md)
1. een realistisch beeld van Homo erectus. Centraal geplaatst. Licht.
   Daaromheen spelen met schaduwen, je bent zelf deel van de stamboom
   (interactive [schaduwspel](../../interactives/schaduwspel/_index.md))
1. een compacte presentatie van afgietsels van andere “key”-fossils die ons
   beeld van de menselijke evolutie bepalen (exhibit [homoniden](../../exhibits/homoniden/_index.md))

## Technische omschrijving

<!-- Voeg hier een blokschema van de hele tentoonstelling toe -->

Technisch is deze zaal niet zo ingewikkeld.  Deze zaal bestaat een enkele
projectie en een film bij de ingang die laat zien hoe lang de bezoeker
nog moet wachtwen voordat de volgende ronde begint. Dit wordt gedaan
met twee Intel Nuc's, een projector en een beeldscherm. De rest van de
zaal wordt verlicht met een aantal spots en een eenvoudige lichtshow.

Het VLAN van devroegemens heeft het subnet 10.134.0.0/16, de showcontroller (CureCore2) heeft het IP adres 10.134.1.1.

## Onderdelen

Een functionele en technische beschrijving van de specifieke onderdelen vind je
op de onderstaande pagina's.

### Show

Meer informatie over de show in deze tentoonstelling vind je hier:

{{< pagelist shows >}}

### Interactives

In de tentoonstelling staan de volgende interactives:

{{< pagelist interactives >}}

### Exhibits

In de tentoonstelling staan de volgende exhibits:

{{< pagelist exhibits >}}

### Decors

De volgende decors maken onderdeel uit van de tentoonstelling:

{{< pagelist decors >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

[Onderhoudsvoorschriften](https://files.museum.naturalis.nl/s/ZEa3tnKnfFSobwi)
van Hypsos.

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues met betrekking tot de gehele
tentoonstelling bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

* Projectleider: [Matthijs Vegter](mailto:matthijs.vegter@naturalis.nl)
* Techniek: [Technisch Team](mailto:support@naturalis.nl)

### SLA

#### Garantie

Indien en geen documentatie te vinden is over garantiebepalingen, dan geldt de algemene overeengekomen regeling. Hieronder nog een keer de toelichting van Erik Jan Elderman op die regeling uit de overeenkomst.
Deze regeling geldt zowel voor de tentoonstellingsbouwers als voor Atatech:

#### Artikel 7 Garantie

* 7.1      Opdrachtnemer blijft aansprakelijk voor herstel door middel van reparatie of vervanging van het geleverde of onderdelen daarvan die binnen 12 maanden na datum van aflevering
gebreken vertonen. Opdrachtnemer is niet tot herstel gehouden indien hij aantoont dat het
gebrek niet is te wijten aan fouten van Opdrachtnemer of door haar geleverde materialen.

* 7.2      De in dit artikel geregelde garantie van Opdrachtnemer houdt in dat het geleverde van
deugdelijk materiaal en met goed vakmanschap is vervaardigd en de montagewerkzaamheden
vakkundig zijn uitgevoerd. Mochten zich binnen de garantietermijn materiaal-, fabricage- of ontwerpfouten voordoen in het geleverde, dan zal Opdrachtnemer zonder berekening van kosten zorgdragen voor reparatie daarvan. Naast de feitelijke herstelkosten zullen ook eventuele aan dit herstel verbonden kosten als kosten gemoeid met montage en demontage van apparatuur; transportkosten van apparatuur, alsmede reis- en verblijfskosten van personeel van Opdrachtnemer; extra kosten in verband met spoedeisend herstel van storingen, die als garantieverplichting moeten worden verholpen; aantoonbare technische gevolgschade, welke optreedt door een voor garantie in aanmerking komend defect onderdeel, door Opdrachtnemer worden vergoed.

* 7.3      Opdrachtnemer staat ervoor in dat het door hem geleverde blijft functioneren conform het Bestek. Mocht binnen een periode van 3 jaar na datum van ondertekening van het overname-protocol, zoals bepaald in artikel 6, aan de hand van systematisch optreden van identieke storingen, blijken dat het opgeleverde of delen daarvan toch niet blijken te voldoen aan het Bestek, zal Opdrachtnemer alsnog zorgdragen voor aanpassing van het geleverde. Opdrachtnemer zal in dat geval het in het kader van deze overeenkomst geleverde door middel van een (ontwerp)-aanpassing herstellen, zonder dat hier voor Naturalis kosten aan zijn verbonden.

Opleverdata verschillen per zaal en per leverancier, maar de meeste zalen/de meeste AVH zijn opgeleverd in juni/juli/augustus 2019. Let op: als binnen die garantietermijn een item vervangen wordt, gaat de garantie van 12 maanden voor dat onderdeel opnieuw in. Oplevering van vervangen onderdelen moet dus goed worden geadministreerd.

### Bouwer

* Bouwer: [Hypsos](https://hypsos.com/soesterberg/)
* Reconstructie: [Kennis & Kennis](https://kenniskennis.com)
* Techniek: [Ata Tech](https://ata-tech.nl)
* Mounting collectie: [Marianne Inkelaar](https://nl.linkedin.com/in/marianne-inkelaar-93095b19)
* Bewerking schedels (schilderwerk) [Manimal works](https://www.manimalworks.com/)
* Model Homo erectus: [Kennis&Kennis](https://www.kenniskennis.com)

### Ontwerper

* Ontwerp: [Designwolf](http://designwolf.nl/contact/)
* Film: [Redrum](https://redrumbureau.com)
* Pepper's ghost: Frank Vermeij
* Lichtontwerk: [Licht Joost de Beij](http://www.licht-joostdebeij.nl/)

## Bijlagen

{{< pagelist bijlagen >}}
