---
title: "dekudde"
draft: true
status:
unid: 
shows:
- showzaal10
interactives:
- A.2 Podium + Watskeburt
- A.3 Vegetatie
- B.2 Tricerascope
- C.1.2 Tuinieren
- C.1.5 Eten & Scheten
- C.2.2 Wild aantrekkelijk
- C.2.3 Groei
- C.2.4 Hoe oud
- C.2.6 Graffiti
- C.3.3 Samen sta je sterk
- C.3.4 Au!
- C.4.1 Schaduwtheater
exhibits:
- A.1 Introductie
- B.4 Foto hotspots
- C.1.3 Tanden wisselen
- C.2.5 Een ei hoort erbij
- C.3.2 Hoorns
decors:
- C.1.6 Zitplekken
attachments:
- title: 2024-09-25_Service manual NL_Naturalis- Triceratops
  url: https://files.museum.naturalis.nl/s/LFtrgzXqqtLbR6k
---

![Plattegrond](./Plattegrond_de_Kudde.png "Plattegrond")

## Functionele omschrijving



## Technische omschrijving

<!-- Voeg hier een blokschema van de hele tentoonstelling toe -->

Het VLAN van zaal10 heeft het subnet 10.143.0.0/16, de showcontroller (Pharos) heeft het IP adres 10.143.1.1.

## Onderdelen

Een functionele en technische beschrijving van de specifieke onderdelen vind je in verschillende mappen:
- [Tekeningen](https://files.museum.naturalis.nl/f/117380).
- [Elektrische schema's](https://files.museum.naturalis.nl/f/117379).
- [Hardware specs](https://files.museum.naturalis.nl/f/117381)

### Show

Meer informatie over de show in deze tentoonstelling vind je hier:

{{< pagelist shows >}}

### Interactives

In de tentoonstelling staan de volgende interactives:

{{< pagelist interactives >}}

### Exhibits

In de tentoonstelling staan de volgende exhibits:

{{< pagelist exhibits >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

Op dit moment zijn er geen known issues met betrekking tot de gehele
tentoonstelling bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

* Projectleider: [Matthijs Vegter](mailto:matthijs.vegter@naturalis.nl)
* Technisch beheer: [Technisch Team](mailto:support@naturalis.nl)

### SLA

#### Garantie

Indien en geen documentatie te vinden is over garantiebepalingen, dan geldt de algemene overeengekomen regeling. Hieronder nog een keer de toelichting van Erik Jan Elderman op die regeling uit de overeenkomst.
Deze regeling geldt zowel voor de tentoonstellingsbouwers als voor Atatech:

#### Artikel 7 Garantie

* 7.1     Opdrachtnemer blijft aansprakelijk voor herstel door middel van reparatie of vervanging van het geleverde of onderdelen daarvan die binnen 12 maanden na datum van aflevering
gebreken vertonen. Opdrachtnemer is niet tot herstel gehouden indien hij aantoont dat het
gebrek niet is te wijten aan fouten van Opdrachtnemer of door haar geleverde materialen.

* 7.2 De in dit artikel geregelde garantie van Opdrachtnemer houdt in dat het geleverde van
deugdelijk materiaal en met goed vakmanschap is vervaardigd en de montagewerkzaamheden
vakkundig zijn uitgevoerd. Mochten zich binnen de garantietermijn materiaal-, fabricage- of ontwerpfouten voordoen in het geleverde, dan zal Opdrachtnemer zonder berekening van kosten zorgdragen voor reparatie daarvan. Naast de feitelijke herstelkosten zullen ook eventuele aan dit herstel verbonden kosten als kosten gemoeid met montage en demontage van apparatuur; transportkosten van apparatuur, alsmede reis- en verblijfskosten van personeel van Opdrachtnemer; extra kosten in verband met spoedeisend herstel van storingen, die als garantieverplichting moeten worden verholpen; aantoonbare technische gevolgschade, welke optreedt door een voor garantie in aanmerking komend defect onderdeel, door Opdrachtnemer worden vergoed.

* 7.3 Opdrachtnemer staat ervoor in dat het door hem geleverde blijft functioneren conform het Bestek. Mocht binnen een periode van 3 jaar na datum van ondertekening van het overname-protocol, zoals bepaald in artikel 6, aan de hand van systematisch optreden van identieke storingen, blijken dat het opgeleverde of delen daarvan toch niet blijken te voldoen aan het Bestek, zal Opdrachtnemer alsnog zorgdragen voor aanpassing van het geleverde. Opdrachtnemer zal in dat geval het in het kader van deze overeenkomst geleverde door middel van een (ontwerp)-aanpassing herstellen, zonder dat hier voor Naturalis kosten aan zijn verbonden.

Opleverdata verschillen per zaal en per leverancier, maar de meeste zalen/de meeste AVH zijn opgeleverd in juni/juli/augustus 2019. Let op: als binnen die garantietermijn een item vervangen wordt, gaat de garantie van 12 maanden voor dat onderdeel opnieuw in. Oplevering van vervangen onderdelen moet dus goed worden geadministreerd.

### Bouwer

* Tentoonstellingsbouw en drukwerk: [Bruns](https://www.bruns.nl/)
* AV Hardware: [Ata Tech](https://ata-tech.nl)
* Lichttechniek: [Tim Blom](mailto:blomartt@live.nl)
* Programmeren licht: Rutger van Dijk ([SemMika](https://www.semmika.nl/))
* Model triceratops: [Minimal Works](https://www.manimalworks.com/)
* Animatronic: [Berkelaar MRT](https://www.berkelaarmrt.com/) en [Rolf RFX](https://rfxprops.com/)
* Model nest, montage afgietsels: Marnel Scherrenberg ([Sensu Lato](https://www.linkedin.com/company/sensulato/))
* Automata's: Donald van der Burg
* Elektronica: Frank Vermeij, Vermeij Elektronica
* Vertaling: [Tim Mitchell](https://www.linkedin.com/in/tim-mitchell-0171a017/)
* Hijswerken: [Witteveen](https://witkraan.nl/)

### Ontwerper

* Tentoonstellingsontwerp: [Designwolf](https://designwolf.nl/) en [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)
* Lichtontwerp: [Tom Verheijen](https://tomverheijen.nl/)
* Interactive software: [Yipp](https://yipp.nl/)
* Montage fossiele skeletten: [Inside Out Animals](https://www.insideoutanimals.com/)
* Projectleiding: [Matthijs Vegter](mailto:matthijs.vegter@naturalis.nl)

## Bijlagen

{{< pagelist bijlagen >}}
