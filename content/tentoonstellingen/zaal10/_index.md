---
title: "zaal10"
draft: false
status:
unid: c4c38367-2419-46a5-bd80-8b4e6d091376
shows:
- showonschatbaar
interactives:
- onschatbaar
attachments:
- title: 200 jaar DO vs aanpassingen uitbreiding (pdf)
  url: https://files.museum.naturalis.nl/s/FbazPK2K5ndPnep
- title: 20200914 Rapport Naturalis Zaal10 Plafond (pdf)
  url: https://files.museum.naturalis.nl/s/mNDMmA7GxJDjo2o
- title: NAT LX AsBuilt 10 Onschatbare waarde V4 (pdf)
  url: https://files.museum.naturalis.nl/s/wDcJSJbdWgiDyWK
- title: Lichtarmaturen_Infra 200 jaar Naturalis (xlsx)
  url: https://files.museum.naturalis.nl/s/wEJj5tw2xzdSg8T
- title: NAT_10_200 Jaar V4 (vwx)
  url: https://files.museum.naturalis.nl/s/JJKoFyRFS3A2LKr
- title: 1461 Grvkl LK-N (pdf)
  url: https://files.museum.naturalis.nl/s/toyfszkTWTyqgAa
- title: 1461-01 E 01-1 (pdf)
  url: https://files.museum.naturalis.nl/s/S3R2yff4gcHtydD
- title: 1461-01 E 01-2 (pdf)
  url: https://files.museum.naturalis.nl/s/GZT8XgaCisjNQQ3
- title: 20190121_E-TEK_TREX_V27_bind (dwg)
  url: https://files.museum.naturalis.nl/s/KmG987GyfSWL28f
- title: 1461-01 E vloer (dwg)
  url: https://files.museum.naturalis.nl/s/3zwpej52FXbxzLa
- title: 1461 Opzet LK-N (dwg)
  url: https://files.museum.naturalis.nl/s/iBp2yLqHqt8s4fr
---

![onschatbaar](./onschatbaar.png)

## Functionele omschrijving

## Technische omschrijving

Het VLAN van zaal10 heeft het subnet 10.143.0.0/16, de showcontroller (Pharos) heeft het IP adres 10.143.1.1.

## Onderdelen

### Show

Meer informatie over de show in deze tentoonstelling vind je hier:

{{< pagelist shows >}}

### Interactives

In de tentoonstelling staan de volgende interactives:

{{< pagelist interactives >}}

### Exhibits

In de tentoonstelling staan de volgende exhibits:

{{< pagelist exhibits >}}

### Faciliteiten

In de tentoonstelling staan de volgende faciliteiten:

{{< pagelist faciliteiten >}}

### Decors

De volgende decors maken onderdeel uit van de tentoonstelling:

{{< pagelist decors >}}

## Handleidingen en procedures

## Known issues

Op dit moment zijn er geen known issues met betrekking tot de gehele
tentoonstelling bekend.

## Afspraken en verantwoordelijkheden

* [Rapport Naturalis Zaal10 Plafond](https://files.museum.naturalis.nl/s/mNDMmA7GxJDjo2o)

### Contact

### SLA

#### Garantie

Indien en geen documentatie te vinden is over garantiebepalingen, dan geldt de algemene overeengekomen regeling. Hieronder nog een keer de toelichting van Erik Jan Elderman op die regeling uit de overeenkomst.
Deze regeling geldt zowel voor de tentoonstellingsbouwers als voor Atatech:

#### Artikel 7 Garantie

* 7.1      Opdrachtnemer blijft aansprakelijk voor herstel door middel van reparatie of vervanging van het geleverde of onderdelen daarvan die binnen 12 maanden na datum van aflevering
gebreken vertonen. Opdrachtnemer is niet tot herstel gehouden indien hij aantoont dat het
gebrek niet is te wijten aan fouten van Opdrachtnemer of door haar geleverde materialen.

* 7.2      De in dit artikel geregelde garantie van Opdrachtnemer houdt in dat het geleverde van
deugdelijk materiaal en met goed vakmanschap is vervaardigd en de montagewerkzaamheden
vakkundig zijn uitgevoerd. Mochten zich binnen de garantietermijn materiaal-, fabricage- of ontwerpfouten voordoen in het geleverde, dan zal Opdrachtnemer zonder berekening van kosten zorgdragen voor reparatie daarvan. Naast de feitelijke herstelkosten zullen ook eventuele aan dit herstel verbonden kosten als kosten gemoeid met montage en demontage van apparatuur; transportkosten van apparatuur, alsmede reis- en verblijfskosten van personeel van Opdrachtnemer; extra kosten in verband met spoedeisend herstel van storingen, die als garantieverplichting moeten worden verholpen; aantoonbare technische gevolgschade, welke optreedt door een voor garantie in aanmerking komend defect onderdeel, door Opdrachtnemer worden vergoed.

* 7.3      Opdrachtnemer staat ervoor in dat het door hem geleverde blijft functioneren conform het Bestek. Mocht binnen een periode van 3 jaar na datum van ondertekening van het overname-protocol, zoals bepaald in artikel 6, aan de hand van systematisch optreden van identieke storingen, blijken dat het opgeleverde of delen daarvan toch niet blijken te voldoen aan het Bestek, zal Opdrachtnemer alsnog zorgdragen voor aanpassing van het geleverde. Opdrachtnemer zal in dat geval het in het kader van deze overeenkomst geleverde door middel van een (ontwerp)-aanpassing herstellen, zonder dat hier voor Naturalis kosten aan zijn verbonden.

Opleverdata verschillen per zaal en per leverancier, maar de meeste zalen/de meeste AVH zijn opgeleverd in juni/juli/augustus 2019. Let op: als binnen die garantietermijn een item vervangen wordt, gaat de garantie van 12 maanden voor dat onderdeel opnieuw in. Oplevering van vervangen onderdelen moet dus goed worden geadministreerd.

### Bouwer

* Decor: [Kloosterboer](http://kloosterboer-decor.nl)
* Techniek: [Technisch Team](mailto:technischteam@naturalis.nl)
* Licht: [Tim Blom](mailto:blomartt@live.nl)
* Showcontrol: Rutger van Dijk ([SemMika](https://www.semmika.nl/))

### Ontwerper

## Bijlagen

{{< pagelist bijlagen >}}
