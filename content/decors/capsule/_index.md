---
title: "capsule"
date: 2019-07-24T13:49:47Z
draft: true
status:
componenten:
- vivitek-dh4661z
- volfoni-smartcrystalcinemaneo
- brightsign-xd1034
tentoonstellingen:
experiences:
- rexperience
attachments:
- title: afbeeldingen (folder)
  src: https://files.museum.naturalis.nl/s/bsjwZ5RCnSBDYR7
- title: rexperience_capsule_bagagebak_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/sS7cjAqrPg6eNQN
- title: rexperience_capsule_bagagerekframe_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/FJ92yMrxxLbe2tp
- title: rexperience_capsule_bekledingschuifdeur_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/38yW5JN5s7ELfip
- title: rexperience_capsule_constructieprojectoren_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/FFEpgPAZcgw2WTF
- title: rexperience_capsule_frame1_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/YkXACeTz6aWfsZ3
- title: rexperience_capsule_frame2totenmet5_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/6TTbfYB8jGcDcmt
- title: rexperience_capsule_geluidwerendewand_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/NReT7dM26AiCnP4
- title: rexperience_capsule_overzichtfacettenboven_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/tcpi5iRDkPso7df
- title: rexperience_capsule_overzichtfacettenonder_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/WdMRYMxFriZQQHQ
- title: rexperience_capsule_principe_opbouwgeluidswanden_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/JZr6nbcDzjzsEkm
- title: rexperience_capsule_schenkelsoverzicht_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/aaX4EfJGsGg6qL3
- title: rexperience_capsule_staalconstructieenspeakers_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/a3WB7qNJHnLzs2b
- title: rexperience_capsule_verhoogdevloer_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/XDcj3bw5t4HgZKs
- title: rexperience_capsule_vloerconstructie_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/H9g38r28KyWG8Aq
- title: rexperience_e_installatie_netwerk_dmx_audio_capsule_aansluitschema (pdf)
  url: https://files.museum.naturalis.nl/s/4bFKiGDNdmkZ73m   
- title: rexperience_e_installatie_netwerk_dmx_audio_ooghal (pdf)
  url: https://files.museum.naturalis.nl/s/RLAXEiPYyNejjFy
---

![capsule](./capsule.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de decor toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Dit decor bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
decor toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
