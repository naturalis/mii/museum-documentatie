---
title: "cascowarenhuis"
date: 2019-07-24T13:49:45Z
draft: true
status:
componenten:
tentoonstellingen:
- deverleiding
attachments:
- title: cascowarenhuis (direcotory)
  src: https://files.museum.naturalis.nl/s/2BCKTKyQeE9xm3W
---

![Voorbeeld - Gordijn](https://files.museum.naturalis.nl/s/nGpzKxtD5k7fYyk/preview)

## Functionele omschrijving

Decor van De Verleiding. Wanden, luifels, gordijn ophanginstallatie, plinten en borden.

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Dit decor bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
decor toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

 * Decor: [Bruns](https://bruns.nl)

### Ontwerper

 * Decor: [Bruns](https://bruns.nl)
 * Inhoud:

## Bijlagen

{{< pagelist bijlagen >}}
