---
title: "warenhuiskassa"
date: 2019-07-24T13:49:47Z
draft: true
status:
componenten:
tentoonstellingen:
- deverleiding
attachments:
- title: 190417_IKWILJE_DO_kassa_G.pdf
  url: https://files.museum.naturalis.nl/s/n3JN49Bm4KGgGJQ
- title: 17326.4.8.5_2.001A Kassa vitrine 20190703.pdf
  url: https://files.museum.naturalis.nl/s/KjwsAQRJSCZipCZ
- title: deverleiding_kassa_plattegrond (pdf)
  url: https://files.museum.naturalis.nl/s/sokipJwCtybLd3j
---

![warenhuiskassa](./warenhuiskassa.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de decor toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Dit decor bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
decor toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

* Decor: [Bruns](https://bruns.nl)

### Ontwerper

* Decor: [Bruns](https://bruns.nl)
* Inhoud:


## Bijlagen

{{< pagelist bijlagen >}}
