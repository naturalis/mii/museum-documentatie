---
title: "ijstijddepot"
date: 2019-07-24T13:49:45Z
status:
componenten:
tentoonstellingen:
 - deijstijd
attachments:
- title: deijstijd_ijstijddepot_belasting_per_stelling (xls)
  src: https://files.museum.naturalis.nl/s/xb6JadNokccRDdr
- title: deijstijd_ijstijddepot_constructieberekening_stellingen (pdf)
  url: https://files.museum.naturalis.nl/s/WGLDDkJi5WyzE2i
- title: deijstijd_ijstijddepot_labels_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/MtqBokdn2aBwj3c
- title: deijstijd_ijstijddepot_stelling_plattegrond_1 (pdf)
  url: https://files.museum.naturalis.nl/s/W9D6nPb6oG5DeDa
- title: deijstijd_ijstijddepot_stelling_plattegrond_2 (pdf)
  url: https://files.museum.naturalis.nl/s/4Gwj6AmRBesKCaL
---

![deijstijd_depot](deijstijd_depot.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de decor toe -->


## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
decor toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

* Decor: [Bruns](https://www.bruns.nl)

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
