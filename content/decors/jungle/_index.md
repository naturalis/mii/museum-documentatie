---
title: "jungle"
date: 2019-07-24T13:49:45Z
draft: false
status:
componenten:
- buttkicker-bklfe
- jbl-ac2826
- martinaudio-cdd5
- qsc-kw181
- citc-aquamaxhazer
- antari-af3effectsfan
- pharos-tpsbb
tentoonstellingen:
experiences:
- rexperience
attachments:
- title: rexperience_jungle_beweegbaretak_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/g6Ea2fxgQcJybpD
- title: rexperience_jungle_constructieondervleugel_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/r2p6xjmP3fAFQMe
- title: rexperience_jungle_framehekwerk_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/nddxGgEQsfbekGr
- title: rexperience_jungle_meshafmetingen_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/5JC3qeDjMkA8o6L
- title: rexperience_jungle_plafondoverzichtlimbo_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/qDgbnNDDMTRFGC3
- title: rexperience_jungle_plafondplaten_overzicht_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/Zn5eMynByfWwoa7
- title: rexperience_jungle_plattegrondblokken_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/q8MxF8YrMf8n2zJ
- title: rexperience_jungle_portalbox_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/JFKTjKAt2d5bfyY
- title: rexperience_jungle_positiebewegendemonitoren_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/gmJ3y2xHzTK6EJT
- title: rexperience_jungle_vlakompilaaronderrotsen_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/rmKL6xQsx4Z5AiW
- title: rexperience_jungle_vloerblokkenposities_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/Y4awpAi4iacZxtd
- title: rexperience_jungle_vloerplatenposities_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/BQXcnaToHjT5YXJ
- title: rexperience_e_installatie_netwerk_dmx_audio_jungle_aansluitschema (pdf)
  url: https://files.museum.naturalis.nl/s/qRsqn4snakAsD2T
---

![jungle](./jungle.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de decor toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Dit decor bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
decor toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
