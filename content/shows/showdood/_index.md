---
title: "showdood"
date: 2019-07-24T13:49:18Z
draft: true
status:
unid: 4258fd6e-109e-4782-a6f8-ed299ce23f6e
componenten:
- hager-svn311
- meanwell-hdr1512
- srs-dsr10n
- visualproductions-cuecore2
tentoonstellingen:
- dedood
attachments:
- title: Cuecore2 configuratie bestand showdood-ctl-1
  src: https://files.museum.naturalis.nl/s/Wo7XAKiNcCB2JkY
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de show toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze show bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

De [RDM Splitter](https://docs.museum.naturalis.nl/latest/componenten/visualproductions-rdmsplitter/) boven vogelskeletten in de goot aan het plafond heeft een kapot poortje. Poortje OUT2 is stuk.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
show toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer shows (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper shows (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
