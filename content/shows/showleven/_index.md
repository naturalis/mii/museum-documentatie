---
title: "showleven"
date: 2019-07-24T13:49:19Z
draft: true
status:
unid: 67e56f6f-6822-4b06-9d9f-341895c27b28
componenten:
- crestron-audext100
- dapaudio-d7889
- dapaudio-pra82
- esi-gigaporthdplus
- hager-svn311
- intel-nuc8i3beh
- meanwell-hdr1512
- qsc-adc4tbk
- visualproductions-cuecore2
tentoonstellingen:
- leven
- leven
attachments:
- title: 20190906_specifiek_kabelschema_2_showleven_v07.pdf
  src: https://files.museum.naturalis.nl/s/4jm5yFEd9RzsMnX
- title: 20190906_luidsprekerplan_2_showleven_v06.pdf
  src: https://files.museum.naturalis.nl/s/wTmYnXpbPRDsznE
- title: Cuecore2 configuratiebestand showleven-ctl-1
  src: https://files.museum.naturalis.nl/s/bjdN7qXpzXrfYKq
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de show toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze show bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
show toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer shows (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper shows (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
