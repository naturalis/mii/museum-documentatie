---
title: "showverleiding"
date: 2019-07-24T13:49:19Z
draft: true
status:
unid: 512465ef-d74c-4688-8a90-949cbb14813f
componenten:
- hager-svn311
- meanwell-hdr1512
- srs-dsr10n
- visualproductions-cuecore2
tentoonstellingen:
- deverleiding
attachments:
- title: vloeraansluitingen_deverleiding (folder met foto's)
  src: https://files.museum.naturalis.nl/s/4WKLPbSRgNFZjds
- title: Cuecore2 configuratiebestand showverleiding-ctl-1
  src: https://files.museum.naturalis.nl/s/mPr9GWxrq7e88B6
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de show toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze show bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
show toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer shows (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper shows (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
