---
title: "showevolutie"
draft: true
status:
componenten:
- hager-svn311
- meanwell-hdr1512
- pharos-lpc1
- srs-dsr10n
tentoonstellingen:
- evolutie
attachments:
- title: Show en Oersteen combinatie
  src: https://files.museum.naturalis.nl/s/sbACMrMS2DTyM85
- title: Pharos configuratie bestand showevolutie-ctl-1
  src: https://files.museum.naturalis.nl/s/mWE3JqxszwEkbZp
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->
![schema](https://files.museum.naturalis.nl/s/sbACMrMS2DTyM85/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de show toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze show bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
show toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer shows (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper shows (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
