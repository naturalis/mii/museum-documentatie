---
title: "The Close App"
date:
draft: true
status:
attachments:
---
## Dienstverlening

Storingen worden in het weekend gemeld bij de dagco. De dagco kan besluiten dat de storing dusdanig is dat hij zsm verholpen moet worden. In dat geval brengt de dagco de aanwezige TT medewerker op de hoogte via de porto (of telefonisch als deze al naar huis is). Het TT neemt contact op met de onderstaande contactpersonen.

## Contactpersonen

* Aart-Jan Klein
* CRM Specialist
* (+31) 6 11880868
* aartjan@thecloseapp.com

* Remco Armee
* Chief Operating Officer
* (+31) 6 21105809
* remco@thecloseapp.com

## Overige info

[Concept overdrachtsdocument](https://docs.google.com/document/d/1vQ6lR9Y0lrAZtRlKtJlQrjsVd-RVoltKopKAuptYyps/edit?usp=sharing)
