---
title: "rexperience"
date: 2019-07-24T13:49:25Z
draft: true
status:
unid: 2792398c-f00f-4e1f-9140-cf8cd69b9042
shows:
- showrexperience
interactives:
- knaagdierontvangsthal
- monitorenontvangsthal
- stoelentijdcapsule
- monitorenjungle
- trexanimatronic
- bewakingrexperience
- projectiejungle
exhibits:
decors:
- vertrekhal
- ontsmettingszone
- wachtruimte
- capsule
- jungle
- wormhole
faciliteiten:
attachments:
- title: rexperience_handleidingen_algemenehandleiding_technischteam (docx)
  src: https://files.museum.naturalis.nl/s/E83raKcnBf69oaS
- title: rexperience_handleidingen_algemenegebruikershandleiding (docx)
  url: https://files.museum.naturalis.nl/s/Bzdg5Frbos7RnKi
- title: rexperience_handleidingen_inspectiepuntendecor (doc)
  url: https://files.museum.naturalis.nl/s/rzpdKK3ggF9eKXD
- title: rexperience_handleidingen_inspectiepuntendecor (xls)
  url: https://files.museum.naturalis.nl/s/iSYakb45MZ3BSeA
- title: rexperience_handleidingen_schoonmaakdecor (doc)
  url: https://files.museum.naturalis.nl/s/9ArdZoswxPXas8H
- title: rexperience_handleidingen_stappenlijstproceduresgebruiker (xls)
  url: https://files.museum.naturalis.nl/s/SQR7cSaTtRrns6L
- title: rexperience_beschrijving_gebouwtechnischeinstallaties (pdf)
  url: https://files.museum.naturalis.nl/s/ryX379kdaBNANTP
- title: rexperience_brandveiligheid_koppeling_bmi_showcontrol (pdf)
  url: https://files.museum.naturalis.nl/s/FXsgjxw44MRAFLw
- title: rexperience_materialenstaat_wanden_en_deuren (pdf)
  url: https://files.museum.naturalis.nl/s/g4n6TzzzwKRwCWg
- title: rexperience_onderhoudskalender_externen (pdf)
  url: https://files.museum.naturalis.nl/s/NcZE7mfMqTmczqi
- title: rexperience_e_installatie_netwerk_dmx_audio_datametingen
  url: https://files.museum.naturalis.nl/s/Y43BPzMzCGX4EJs
- title: rexperience_e_installatie_netwerk_dmx_audio_dmx_deel1.2 (pdf)
  url: https://files.museum.naturalis.nl/s/zgJAYFA6ZzqWzJJ
- title: rexperience_e_installatie_netwerk_dmx_audio_dmx_deel2.2 (pdf)
  url: https://files.museum.naturalis.nl/s/CYiQ4T27e9SZtPN
- title: rexperience_e_installatie_netwerk_dmx_audio_e_tekeningendeel1 (pdf)
  url: https://files.museum.naturalis.nl/s/E6Nnm9aeJi4pN4o
- title: rexperience_e_installatie_netwerk_dmx_audio_e_tekeningendeel2 (pdf)
  url: https://files.museum.naturalis.nl/s/W5Y23TxcQMMH6fe
- title: rexperience_e_installatie_netwerk_dmx_audio_eldoled_aansluitschema (pdf)
  url: https://files.museum.naturalis.nl/s/8HtWW7BSNbFRQ4m
- title: rexperience_e_installatie_netwerk_dmx_audio_network_ip_lijst (xls)
  url: https://files.museum.naturalis.nl/s/37bRD5sgRx33E4P
- title: rexperience_e_installatie_netwerk_dmx_audio_rack_indeling (pdf)
  url: https://files.museum.naturalis.nl/s/Lt59295bgnaiEek
- title: rexperience_e_installatie_netwerk_dmx_audio_serverrack j3_audio (pdf)
  url: https://files.museum.naturalis.nl/s/Z35jcssz7QJZtsX
- title: rexperience_e_installatie_netwerk_dmx_audio_serverrack_j1_lichtenpatch (pdf)
  url: https://files.museum.naturalis.nl/s/ZAR9abdG8mZjiQg
- title: rexperience_e_installatie_netwerk_dmx_audio_serverrack_j2_beeldensturing (pdf)
  url: https://files.museum.naturalis.nl/s/atgyKckqEcJFTYB
- title: rexperience_e_installatie_netwerk_dmx_audio_technieklijst vermogensberekening (xls)
  url: https://files.museum.naturalis.nl/s/KR36rKCKWxnJ4kA
- title: rexperience_plattegrond_algemeen (pdf)
  url: https://files.museum.naturalis.nl/s/a48MBQTzGqCYaDe
- title: rexperience_plattegrond_beveiliging (pdf)
  url: https://files.museum.naturalis.nl/s/KGMBzXcBd6aLQF5
- title: rexperience_plattegrond_cameraposities (pdf)
  url: https://files.museum.naturalis.nl/s/99b9EypZgGjqpQR
- title: rexperience_plattegrond_genummerd (pdf)
  url: https://files.museum.naturalis.nl/s/zmZxiaGpbA6sBBc
- title: rexperience_plattegrond_opstortenvloer_inmeting (pdf)
  url: https://files.museum.naturalis.nl/s/sRjHBz7i5JoimK6
- title: rexperience_wandentekening (pdf)
  url: https://files.museum.naturalis.nl/s/LkycgNke4yjxz3B
- title: rexperience_serverruimte_vloer_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/icRYT3JFEw364o3
- title: rexperience_sleutels (pdf)
  url: https://files.museum.naturalis.nl/s/gxMK2KoYiFoJ5w9
- title: rexperience_technischeinstallaties_deursystemen_beveiligingsinfo (pdf)
  url: https://files.museum.naturalis.nl/s/kP7omp4LFNXsCcm
- title: rexperience_technischeinstallaties_treax_schuifdeur_aansluitschema (pdf)
  url: https://files.museum.naturalis.nl/s/mEZi9ttiRemiQfP
- title: rexperience_technischeinstallaties_treax_schuifdeur_beveiliging
  url: https://files.museum.naturalis.nl/s/fEKpZYK8WAJAwaA
- title: rexperience_e_installatie_netwerk_dmx_audio_aansluitschemaschuifdeur (pdf)
  url: https://files.museum.naturalis.nl/s/QdrnwK9gQnbkDnq
- title: rexperience_materiaallijst_algemeen (xls)
  url: https://files.museum.naturalis.nl/s/2LkAoXcxdkxdG2M
- title: rexperience_licht_plafondverlichting_tekening (pdf)
  url: https://files.museum.naturalis.nl/s/23JGYzjcz9QaKs9
- title: rexperience_licht_armaturenlijst (pdf)
  url: https://files.museum.naturalis.nl/s/6SzdfL6AkS46Agy
- title: rexperience_licht_armaturenlijst2 (pdf)
  url: https://files.museum.naturalis.nl/s/fbmR4oBi5KHJFS2
- title: rexperience_licht_dmxhookup (pdf)
  url: https://files.museum.naturalis.nl/s/SeiBne3XR9NWwj3
- title: rexperience_licht_driverhookup (pdf)
  url: https://files.museum.naturalis.nl/s/Bx82zPccXSdZzn5
- title: rexperience_licht_instrumentschema (pdf)
  url: https://files.museum.naturalis.nl/s/i7RDaKMmzWwL5ef
- title: rexperience_licht_plafondverlichting_dmxadresshookup (pdf)
  url: https://files.museum.naturalis.nl/s/XRBcywMS7zc4yN6
- title: rexperience_licht_plafondverlichting_drivergroups (pdf)
  url: https://files.museum.naturalis.nl/s/RmQGmGMqZJXpx33
- title: rexperience_licht_plafondverlichting_totaal (pdf)
  url: https://files.museum.naturalis.nl/s/Mfm3X2xSqEDeXM8
- title: rexperience_licht_portalboxexport (pdf)
  url: https://files.museum.naturalis.nl/s/xgsLR24xAjFi8er
---

![rexperience_plattegrond](./rexperience_plattegrond.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de experience toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema van de hele experience toe -->

Het VLAN van de rexperience heeft het subnet 10.138.0.0/16. Hij heeft 2 showcontrollers (Pharos). De Main controller (10.138.1.55) is voor de algemene besturing: alles initieel goed zetten, reageren op de Pharos touchscreens zoals deuren open, etc.
De Main controller heeft een zwarte achtergrond in de web-interface om verwarring te voorkomen.

De Show controller (10.138.1.56) draait de show. Deze verdeling is gemaakt omdat het allemaal teveel werd voor één Pharos.
De Show controller heeft, zoals gewoonlijk, een witte achtergrond in de web-interface.

## Onderdelen

Een functionele en technische beschrijving van de specifieke onderdelen vind je
op de onderstaande pagina's.

### Show

Meer informatie over de show in deze experience vind je hier:

{{< pagelist shows >}}

### Interactives

In de experience staan de volgende interactives:

{{< pagelist interactives >}}

### Exhibits

In de experience staan de volgende exhibits:

{{< pagelist exhibits >}}

### Decors

De volgende decors maken onderdeel uit van de experience:

{{< pagelist decors >}}

## Handleidingen en procedures

* [rexperience_handleidingen_algemenehandleiding_technischteam](https://files.museum.naturalis.nl/s/E83raKcnBf69oaS)
* [rexperience_handleidingen_algemenegebruikershandleiding](https://files.museum.naturalis.nl/s/Bzdg5Frbos7RnKi)
* [rexperience_handleidingen_inspectiepuntendecor doc](https://files.museum.naturalis.nl/s/rzpdKK3ggF9eKXD)
* [rexperience_handleidingen_inspectiepuntendecor xls](https://files.museum.naturalis.nl/s/iSYakb45MZ3BSeA)
* [rexperience_handleidingen_schoonmaakdecor](https://files.museum.naturalis.nl/s/9ArdZoswxPXas8H)
* [rexperience_handleidingen_stappenlijstproceduresgebruiker](https://files.museum.naturalis.nl/s/SQR7cSaTtRrns6L)

## Known issues en tips

1. De sleutel achter de boom in de jungle krijgt het lampje niet uit: Er staat nog een noodknop ingedrukt ergens.
  (geen issue maar wel common mistake)
1. Geluid doet het niet: De Macmini verzorgt al het geluid met Qlab. Controleer de Macmini. Dat kan via VNC of in de systeemkast:

* Draait Qlab en de meest recente project
* Zijn alle geluidskaarten aanwezig:
  * 24Ao # 1
  * 24Ao # 2
  * 24Ao # 3

  Zo niet, herstart de betreffende Motu 24Ao # en herstart Qlab

1. Bij de animatronics doet "Homing" het niet: "Reset" eerst, dan "Enable Drives" dan "Homing", je ziet de Trex bewegen op
  het bewakingsbeeld
1. Bij "Buiten Gebruik" heb je kans dat de rode knop nog ingedrukt is
1. Audio loopt niet synchroon met beeld: herstart Qlab en "zet alles klaar"
1. Bij geen audio in een hele zaal: Motu's herstarten en Macmini herstarten

### Koeling

Roel van Unen over de koeling:

Er is zowel ventilatie, als koeling in de capsule.
We blazen verse lucht in onder de vloer van de capsule. Deze lucht wordt extra gekoeld door een kanaalkoeler die ook onder de vloer ligt. (Ik hoop niet dat we daar ooit problemen mee hebben, want ik heb geen serviceluik in de vloer kunnen ontdekken.)
Deze lucht komt door de driehoekige roosters voor in de capsule naar boven.
Boven de console aan het dak achter in de capsule zit een afzuiging.

De totale hoeveelheid verse lucht voor de gehele rexperience is beperkt. We hebben deze zo goed mogelijk over de verschillende ruimtes verdeeld. In theorie zou het net genoeg moeten zijn als we het geplande aantal bezoekers er doorheen krijgen. Maar ik weet ook dat theorie en praktijk niet altijd het zelfde is. Dus het best spannend hoe het gaat als we op volle kracht draaien. De totale hoeveelheid lucht kunnen we niet vergroten. maar we kunnen straks nog wel een beetje spelen met de verdeling over de verschillende ruimtes. Maar dat moeten we in de praktijk ervaren als de rexperience eenmaal in gebruik is.

## Afspraken en verantwoordelijkheden

* [rexperience_beschrijving_gebouwtechnischeinstallaties](https://files.museum.naturalis.nl/s/ryX379kdaBNANTP)
* [rexperience_brandveiligheid_koppeling_bmi_showcontrol](https://files.museum.naturalis.nl/s/FXsgjxw44MRAFLw )
* [rexperience_materialenstaat_wanden_en_deuren](https://files.museum.naturalis.nl/s/g4n6TzzzwKRwCWg)
* [rexperience_technischeinstallaties_deursystemen_beveiligingsinfo](https://files.museum.naturalis.nl/s/kP7omp4LFNXsCcm)

### Contact

* Projectleider: [Matthijs Vegter](mailto:matthijs.vegter@naturalis.nl)
* Project Management: [Jeroen Schellekens](https://nl.linkedin.com/in/joep-schellekens-a2b41917)
* Techniek: [Technisch Team](mailto:support@naturalis.nl)

### SLA

* [rexperience_onderhoudskalender_externen](https://files.museum.naturalis.nl/s/NcZE7mfMqTmczqi)

### Bouwer

* Decorbouw, technisch ontwerp en -realisatie: [Kloosterboer](http://kloosterboer-decor.nl)
* Soundtrack: Gerry Arling
* Animatronics en bewegende tak.: [Wiebe Berkelaar MRT](https://www.berkelaarmrt.com/)
* D Box bewegende theaterstoelen: [D-Box](https://www.d-box.com/en/contact/)
* AV Hardware: [Atatech](https://www.ata-tech.nl/)
* AV Hardware: [VHS](mailto:bob@vhs-bv.nl)
* Programmeren geluid: [Dennis Slot](mailto:dennis@studiodennisslot.com)
* Programmeren showcontrol: Rutger van Dijk [Semmika](https://www.semmika.nl/)
* Armaturen licht: [Q Cat](https://www.qcat.nl/)
* Armaturen licht: De Cirkel
* Lichttechniek: [Tim Blom](mailto:blomartt@live.nl)
* Animaties: [JellyFish London](https://jellyfishpictures.co.uk/)
* 3D brillen: [de Wijs apparatenbouw](mailto:info@dewijs-3d.com)
* Deursystemen: [Treax](https://www.treax.nl/)
* Leeftijdsadviezen: Peter Nikken
* Attractiekeuring: [TUV](https://www.tuv.nl/nl/diensten/keuring-en-inspectie/)
* E installatie en bekabeling: [Rijndorp](https://www.rijndorp.com/)
* Luchttechniek: Grimbergen luchtechniek
* Compressor: [Pneunet](https://pneunet.nl/)
* Props (bankjes, monteursbenodigdheden): Jaap Bosma

### Ontwerper

* Advies Conceptontwikkeling: Karel Willemen
* Decor-, content en grafish ontwerp: [Daniel Ament](https://www.danielament.com)
* Sound design: [Hens Zimmerman](https://henszimmerman.audio/)
* Lichtontwerp: Allard Ockx

## Bijlagen

{{< pagelist bijlagen >}}
