---
title: "Online Spotlight Studio Handleiding"
date: 2019-07-08T09:37:18+02:00
draft: false
status:
attachments:
---

## Inleiding

Deze handleiding is bedoeld voor technici om de Online Spotlight livestreams te ondersteunen.

De organisator nodigt via Google Agenda een Sciencelink, een wetenschapper en een technicus uit om deel te nemen aan een “Test online spotlight” en een “Opstarten livestream & online spotlight”. Het eerste contact loopt via de Google Meet functies van deze uitnodigingen. Tijdens dit contact worden zaken afgestemd, zoals het uitwisselen van Skype-namen en het bespreken van de presentatie.

De test afspraak is bedoeld om de openbare livestream genoeg voor te bereiden dat er daarna geen technische wijzigingen hoeven plaats te vinden en de uitvoer van de Online Spotlight probleemloos verloopt.

De openbare livestream wordt door afdeling Communicatie aangemaakt, aangevuld met een titel, beschrijving, een afbeelding en hashtags. De besloten test stream wordt aangemaakt door de technicus binnen zijn/haar eigen @naturalis.nl YouTube-kanaal.

## Voorbereiding

### Software
OBS Studio
https://obsproject.com/nl

### NDI plugin voor OBS
https://github.com/Palakis/obs-ndi/releases

### Skype
https://www.skype.com/nl/get-skype/

{{% notice warning %}}
Nooit de software updaten tussen de test en de uitvoer!
{{% /notice %}}

## Hardware

Laptop met Windows, macOS besturings systeem.
Linux is nog niet getest.

## Accounts

### Skype
Voor technicus:	[eigen account]

voor wetenschapper:	[eigen account] óf
Username: Live_Science
Password: l1vesc1ence

voor Sciencelink:	[eigen account]

### Youtube

De test stream voer je uit binnen jouw eigen @naturalis.nl YouTube-kanaal (youtube heeft 24 uur nodig om jouw eigen account te verifiëren).

Om de Online Spotlight live te streamen dien je in te kunnen loggen op het Naturalis-kanaal met manager rechten. Deze rechten worden toegewezen door Communicatie en dienen bij hen aangevraagd te worden.

## Youtube Studio

Account wisselen
Ga naar **youtube.com** en klik op de **profielafbeelding** rechtsboven en vervolgens op de pijl rechts naast **Van account wisselen**.

![](./1.png)

In het Accounts venster kun je kiezen op welk kanaal je zult livestreamen.

![](./2.png)

Als het Naturalis-kanaal niet verschijnt dan zijn er nog geen manager-rechten toegewezen aan jouw persoonlijke Naturalis account.

## Test stream aanmaken

Ga naar **youtube.com** (wissel zo nodig naar jouw persoonlijke Naturalis account), klik op het **Camera-icoon** en vervolgens op **Live gaan**

![](./3.png)

Je komt nu in het Studio gedeelte

Kies voor stream voor NU en druk op **Start**

![](./4.png)

Kies vervolgens voor streaming software en druk op **Start**

![](./5.png)

Er verschijnt nu een interface voor Stream bewerken

Verander **Openbaar** naar **Niet vermeld**

![](./6.png)

Selecteer bij doelgroep: **Nee, de video is niet gemaakt voor kinderen**

![](./7.png)

Druk vervolgens op **Opslaan**

De live regiekamer van je stream verschijnt nu

Rechtsboven staan drie icoontjes waarvan de linker het **Delen-icoon** (pijl naar rechts)

Klik op deze pijl en druk op **Kopiëren**

![](./8.png)

Open de uitnodiging van **techniek test online spotlight** in Google Agenda en klik op het **pijl-icoon** rechtsonder

![](./9.png)

Kies vervolgens voor **Add note** en plak hier de eerder gekopieerde link om de organisator en andere genodigden toegang te geven tot de test stream en deze mee kan kijken

![](./10.png)

## Skype verbinden

###Nieuwe groeps-chat aanmaken

Open **Skype**
Log in
Klik bij **Nieuw chatgesprek** op **Nieuw groeps-chat** en nodig de sciencelink en wetenschapper uit.

![](./11.png)

Start een videogesprek door op het **camera-icoon** te klikken

![](./12.png)

###Instellingen
Om de beelden van skype direct naar OBS te sturen dien je NDI aan te hebben staan. Dit doe je door:

![](./13.png)

Instellingen te openen en bij **Bellen** op **Geavanceerd** te klikken

![](./14.png)
en voor NDI gebruik toestaan te kiezen
![](./15.png)
Skype watermerk kies voor Top right
![](./16.png)

Tijdens de uitvoer dient de technisch operator skype camera en microfoon te muten en de speakers uit te schakelen.

## OBS instellen

###Instellingen downloaden

Download eerst je OBS scene files
https://drive.google.com/file/d/1ZIRTREovlEKc-sm0M-yEH3DegAj0wlif/view?usp=sharing
![](./17.png)
Pak de zip file uit

### Laad je profiel in OBS

Ga in OBS naar **profiel** door in het venster linksboven **importeer** te kiezen bij het kopje profiel

![](./18.png)

selecteer de map Profiel_ddmmjjjj en deze staat nu in OBS

### Laad je scene verzameling  in OBS

Ga in OBS naar **sceneverzameling** door in het venster linksboven scene verzameling te kiezen en kies voor **importeer**

![](./19.png)

selecteer op de **drie puntjes** rechtsonder verzamelingspad en open het .JSON bestand uit de OBS scene files map en kies voor **importeer**

![](./20.png)

### Beeld van Wetenschapper wijzigen

Dubbelklik op **Wetenschapper** in het Bronnen-venster

![](./21.png)
Klik op het **pijl-icoon** om het Skype-beeld van de wetenschapper te selecteren (er moet wel skype verbinding zijn)
![](./22.png)
Hetzelfde geldt voor het wijzigen van het beeld van de Sciencelink.

Stel ook de **latency mode** in op **low**

![](./22a.png)

### Naam en titel van sprekers wijzigen

Dubbelklik op **Naam Sciencelink** in het Bronnen-venster en wijzig de naam in het Tekst-venster
![](./23.png)

Hetzelfde geldt voor **Naam Wetenschapper** en **Wetenschapper Titel**

### Begin- en Eindslide wijzigen

Download de beginslide-afbeelding uit de [Thumbnail Drive Map](https://drive.google.com/open?id=1Cqdzh2Yi9Pbg--L7P-XOZku0U31AOlc5)

Selecteer **Begin** in het Scéne-venster, vervolgens **Beginslide** in het Bronnen-venster, kies voor **Bladeren** en open de gedownloade afbeelding

![](./24.png)

Hetzelfde geldt voor **Einde** in het Scéne-venster en vervolgens **Eindslide** in het Bronnen-venster



## Streamen
### Live gaan

Keer terug naar het studio gedeelte op Youtube

Kopieer de **Streamsleutel** en check of de **DVR instelling** uitgeschakeld is

![](./25.png)

Open **OBS Studio**
Klik op **Instellingen** in het Controls-venster

![](./26.png)

Selecteer de **Stream** en plak de Streamsleutel in het veld **Streamkey**

![](./27.png)

Klik **Kruisje** voor het afsluiten en antwoord **ja**

![](./28.png)

Klik op **Stream Starten** in het Controls-venster

![](./29.png)

Zodra de verbinding goed is zal op de Youtube studio gedeelte de tekst “Het lijkt erop dat je klaar bent. Klik hier om te beginnen met streamen.” Klik dan op **LIVE GAAN**

![](./30.png)

## Bedienen

Voor het bedienen van de uitzending is de rechter interface (programma) het live beeld wat naar youtube gaat.
Op het linker beeld (voorbeeld) kun je de volgende scène alvast klaar zetten en met overgang naar de live voorstelling zetten.
De scène keuzes zijn links onder te selecteren. De scène van keuze verschijnt in het voorbeeldvenster.

![](./31.png)

Hou er rekening mee dat Skype alle audio kanalen meestuurt in elke video stream. Dit houd in dat je alle video-steams moet muten behalve de Sciencelink anders krijg je echo's.
![](./31a.png)


De volgorde van scènes van een Online Spotlight is altijd Begin -> Sprekers - > Einde.

## Afsluiten

Klik op **STREAM BEËINDIGEN** in het Youtube studio gedeelte

![](./32.png)
