---
title: "Redeployment"
date: 2019-07-08T09:37:18+02:00
draft: false
status:
attachments:
---

Dit zijn de stappen om een bestaande NUC opnieuw te deployen. De stappen zijn
hetzelfde wanneer je een defecte NUC vervangt door een nieuwe.

1. Open
   [AWX](https://awx.museum.naturalis.nl/#/templates)
1. Start het 'Update en configureer switchpoorten en deploy interactive' sjabloon; of wanneer de netwerkpoort al goed staan (bij vervanging) 'Deploy interactives'
1. Tags en variablen kan je overslaan met 'Next'
1. Geef de naam op van de computer, (zie screenshot)
1. Geef aan of je de machine opnieuw wil 'commissionen' zodat MAAS een nieuwe
   inventarisatie van de hardware uitvoert.
1. Geef aan of je de machine, ongeacht of de computer al eens is geïnstalleerd,
   opnieuw wilt 'deployen' (voorzien van een besturingsysteem).
1. Bij beide 'no' installeerd AWX alleen de content opnieuw en draait Ubuntu updates
1. Klik op 'Next' 'Ja' en, als je het zeker weet, op 'Launch'.
1. Er start nu een 'Job' die, afhankelijk van de interactive, zo'n 12 minuten
   duurt. Na afloop zal de computer zijn voorzien van een besturingsysteem
   en de software en configuratie die bij de interactive hoort.

Wanneer de deploy Failed bij "maas-control : Delete existing machine" dan had Maas het op dat moment te druk
om de machine uit zijn database te verwijder. Nogmaals de deploy draaien zal dit w.s. wel oplossen.

![redeploy](redeploy.png)
