---
title: "Assets invoeren in TOPdesk"
date: 2019-07-08T09:37:18+02:00
draft: false
status:
attachments:
---

## Algemene informatie

Assets worden gedocumenteerd in TOPdesk. Om een goede documentatie erop na te houden is het belangrijk om uniform te documenteren. Vandaar deze handleiding.

### Inhoud

## Omschrijving

Deze handleiding beschrijft hoe je een nieuwe asset invoert in TOPdesk.
Een asset is in het nederlands een bedrijfsmiddel. Het kan een apparaat zijn zoals een NUC, beeldscherm of speaker. Maar ook een tentoonstelling, show of decor.

Er is ook een hiërarchie in TOPdesk: een apparaat hangt aan een interactive, show, exhibit of faciliteit. Een interactive, show, exhibit of faciliteit hang weer aan een tentoonstelling of experience. De diepte van de hiërarchie bestaat meestal uit drie lagen: zaal > interactive > component.

```bash
Museum
 |-- Experience: japanstheater
 |   |-- Interactive: bewakingjapanstheater
 |          |-- Speaker: bewakingjapanstheater-spk-2
 |-- Tentoonstelling: atrium
 |   |-- Faciliteit: ticketzuil4
 |          |-- Beeldscherm: ticketzuil4-bld-1
 |-- Tentoonstelling: dedood
 |   |-- Interactive: timelaps
 |          |-- Projector: timelaps-prj-1
 ```

Deze hiërarchie maak je door middel van relationships. Een relatie maak door een asset 'onderdeel te maken van ...'.

## Benodigdheden

1. Toegang tot TOPdesk
1. Zoveel mogelijk informatie over de asset

### Personen

Dit kan je in je ééntje doen.

### Kennis

Geen.

### Onderdelen



### Gereedschap

1. Computer/Laptop

## Instructies

1. Log-in in TOPdesk en ga naar [Asset Overview](https://naturalis.topdesk.net/tas/secure/assetmgmt/overview.html).
1. Maak een nieuwe asset aan en kies de juiste type (per type zijn er verschillende velden gedefinieerd)
1. Vul zo volledig mogelijk in:
  - In het geval van o.a. zaal, component of interactive is er waarschijnlijk een documentatie pagina. Copy-paste de link
  - Bij een asset met netwerk aansluiting is het belangrijk om het IP adres, MAC adres en de switch poort goed te documenteren
  - Link een onderdeel aan zijn ouder, niet andersom. Wij gebruiken "onderdeel van" en niet "Link as ...."

## Tests

1. Vergelijk je asset pagina met een vergelijkbaar onderdeel

## Bijlagen

{{< pagelist bijlagen >}}
