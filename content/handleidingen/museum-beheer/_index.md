---
title: "Technisch museum beheer"
date: 2019-09-11T13:37:18+02:00
draft: false
status:
attachments:
---

Dit document is bedoeld voor beheerders van het museum en leden van het
technisch team die nog niet alles weten van de techniek, maar toch dingen
willen kunnen doen om (eenvoudige) problemen op te lossen. Dit gaat dan vooral
om problemen die regelmatig voor komen bij het starten en het stoppen van het
museum en technische ongemakken gedurende de dag.

Voordat je aan de slag gaat met het museum moet je eerst via je computer
inloggen op de VPN van het museum en je moet toegang hebben tot de
groep [Technisch Team in Bitwarden](https://vault.bitwarden.com/#/).
Anders zijn veel van de applicaties en sites die in dit document worden
beschreven niet bruikbaar of benaderbaar.

Behandelde onderwerpen:

* [Monitoren](#monitoren)
* [Museum bedienen via awx](#museum-bedienen-via-awx)
* [Museum aan](#museum-aan)
* [Tentoonstelling aan](#tentoonstelling-aan)
* [Filmpje beweegt niet of geen geluid](#filmpje-beweegt-niet-of-geen-geluid)
* [Geen beeld bij een filmpje](#geen-beeld-bij-een-filmpje)
* [Blauw beeld bij een filmpje](#blauw-beeld-bij-een-filmpje)
* [Computer vastgelopen en sensu geeft connectie fout](#computer-vastgelopen-en-sensu-geeft-een-connectie-fout)
* [Schattenjacht helmpjes met problemen](#schattenjacht-helmpjes-met-problemen)
* [Schattenjacht controller met problemen](#schattenjacht-controller-met-problemen)
* [Nisverlichting moet uit](#nisverlichting-moet-uit)
* [Werkverlichting moet uit](#werkverlichting-moet-uit)
* [Werkverlichting moet aan](#werkverlichting-moet-aan)
* [Museum uit](#museum-uit)
* [Tentoonstelling uit](#tentoonstelling-uit)
* [Computer aan of uit](#computer-aan-of-uit)
* [Projector aan of uit](#projector-aan-of-uit)
* [Help mijn probleem staat er niet bij](#help-mijn-probleem-staat-er-niet-bij)
* [Vragen of opmerkingen](#vragen-of-opmerkingen)

## Monitoren

Om het monitoren makkelijk te maken hebben we een aantal hulpmiddelen tot onze
beschikking waar je gebruik van kan maken.

Allereerst is het belangrijk om via 'Uchiwa / Sensu' in de gaten te houden of
bepaalde machines en apparaten en de software op die apparaten nog blijven
functioneren.

* [sensu.naturalis.nl](https://sensu.naturalis.nl/#/events?status=2)

![Sensu Museum monitoring](sensu-museum.png)

Hierbij is het belangrijk dat je vooral naar de CRITICAL meldingen
kijkt. Let op dat je de lijst sorteert op tijd en dat je de
_gesilenced_ meldingen uitfiltert.

* [screenshots.museum.naturalis.nl](http://screenshots.museum.naturalis.nl/#_)

Iedere minuut wordt er van interactives in het museum een screenshot gemaakt.
Met deze tool kan je zien of een interactive, game of kiosk nog draait. In het
zoekveldje kan je een naam of deel van de naam van een interactive invoeren om
alleen die ene te zien. Door te klikken op het plaatje kan je een grotere
versie van het screenshot bekijken.

![Screenshot scrubber](screenshot-scrubber.png)

Ook kan je hier teruggaan in de tijd. Door de datum te kiezen en met de
_scrubber_ te slepen kan je screenshots tot 14 dagen eerder opzoeken. Handig
voor als de inhoud van bijvoorbeeld een spelletje lijkt te zijn vastgelopen.

* [chat.naturalis.io](https://chat.naturalis.io)

Dit is de mattermost chat omgeving van het museum hier kan je discussies over
bepaalde zalen en interactives volgen. Daarnaast is hier ook het sensu kanaal
te vinden waar ook de belangrijkste meldingen van sensu automatisch worden
ingeschoten. Maak vooral gebruik van de zoekfunctie als je vermoed dat een
bepaald probleem vaker is voorgekomen. Vaak worden hier mogelijke oplossingen
aangedragen. Het kan ook geen kwaad om problemen in de chat te melden. Bij
voorkeur in het kanaal van de bepaalde zaal waar het probleem zich voordoet.

![Dashboard handscanner](dashboard-handscanner.png)

* [dashboard.naturalis.nl](https://dashboard.naturalis.nl/d/YBWYyuFWz/museum)

Dit het Grafana/Prometheus dashboard. Hier kan je van iedere computer in het
museum zien hoe hij functioneert. Ook zijn hier bijvoorbeeld gegevens over de
helmpjes op te vragen. Wachtwoord is te vinden in Bitwarden.

* [TOPdesk asset management](https://naturalis.topdesk.net/tas/secure/assetmgmt/overview.html)

In asset management van TOPdesk staat alle informatie over alle onderdelen van
het museum.  Dit geldt ook voor de interactives, computers, beeldschermen,
projectoren en andere componenten. Bij twijfel, of als je bepaalde informatie
zoekt, _altijd TOPdesk raadplegen_.

![TOPdesk asset management](topdesk-schema.png)

Daarnaast TOPdesk vanzelfsprekend ook gebruikt om issues mee af te handelen.
Net als bij gewoonlijke support kunnen issues die worden gemeld over het
museum ook worden ingediend via een [formulier in het zelf help
gedeelte](https://naturalis.topdesk.net/tas/public/ssp/content/serviceflow?unid=03110973fd0d47a9a9a291f917887876).
Al deze issues worden ontvangen en via het [kanban
board](https://naturalis.topdesk.net/tas/secure/agileboard/#/board/d807eba2-1fd8-4118-8f29-93861e920f84)
afgehandeld.

## Museum bedienen via AWX

Om het museum te bedienen en om dingen uit en aan te zetten maken we gebruik
van [AWX](https://awx.museum.naturalis.nl/). Dit is de plek waar je het museum
kan uit- en aanzetten en bepaalde onderdelen opnieuw kan starten of stoppen.
Hier kan je ook zien of bepaalde acties succesvol zijn uitgevoerd. Dit wordt
aangegeven met *rode* en *groene* resultaat kleuren.

![awx dashboard](awx-dashboard.png)

Om workflows of job templates te starten moet je naar de
[templates](https://awx.museum.naturalis.nl/#/templates). Hier kan je jobs en
workflows zoeken op naam.

Om een workflow of job te starten kan je op het raketje klikken of in de
detailweergave op *launch* drukken, waarna bij sommige jobs een vraag komt om
meer informatie (naam van machine bijvoorbeeld, of een groep machines). Hierna
wordt de job klaargezet voor start. Dit kan soms even op zich laten wachten,
vooral wanneer een andere taak nog bezig is.

![awx jobs list](awx-jobs.png)

Soms duurt het even voordat een bepaalde taak wordt uitgevoerd in AWX. Grote
taken worden namelijk niet tegelijkertijd uitgevoerd maar op volgorde. Je kan
zien waar AWX mee bezig is door te klikken op de [jobs
lijst](https://awx.museum.naturalis.nl/#/jobs?job_search=page_size:20;order_by:-finished;not__launch_type:sync).
De taak die actief is heeft een _pulserend groen_ balletje voor de regel. Als
je op de regel zelf klikt kan je de activiteit van die taak nader bestuderen.

![awx logging](awx-logging.png)

Dit geldt ook voor taken die zijn voltooid. Voltooide taken/jobs die niet
succesvol zijn, zijn gemarkeerd met een _rood_ balletje. In de detailweergave
is aan de rechterkant van het scherm te zien welke taken er zijn uitgevoerd en
welke daarvan niet succesvol zijn geweest. Dit hoeft overigens niet altijd een
probleem te zijn.

## Museum aan

Als je het museum wilt aanzetten dan is het een goed idee om eerst even te
controleren of de opdracht niet al
[gescheduled](https://awx.museum.naturalis.nl/#/schedules) staat of al bezig is
in de [jobs
lijst](https://awx.museum.naturalis.nl/#/jobs?job_search=page_size:20;order_by:-finished;not__launch_type:sync).
In principe wordt het museum dagelijks nog met de hand gestart, maar soms kan
iemand 'm al hebben klaar gezet. In dat geval moet je het museum *niet* met de
hand starten, of eerst de taak annuleren en dan als nog met de hand doen.

_Start museum_ is een workflow dat wil zeggen dat het bestaat uit andere jobs en
workflows. Dit is in het
[schema](https://awx.museum.naturalis.nl/#/templates/workflow_job_template/10/workflow-maker?template_search=page_size:20;order_by:name;type:workflow_job_template,job_template;search:start;search:museum)
goed te zien. Deze workflows worden achter elkaar uitgevoerd. Iedere stap gaat
meestal goed, maar soms kunnen door timing problemen ontstaan die je tijdens
een ronde door het museum moet proberen te verhelpen. Neem altijd een laptop
mee, zorg ervoor dat je op het VPN zit en dat je bij alle systemen kan (log bij
voorkeur alvast in).

## Tentoonstelling aan

Soms is een tentoonstelling of een deel niet aangegaan. Of is het verstandig de
hele procedure opnieuw te starten. Dit kan via [start tentoonstelling
workflow](https://awx.museum.naturalis.nl/#/templates/workflow_job_template/76?template_search=page_size:20;order_by:name;type:workflow_job_template,job_template;search:start).
Als je deze workflow lanceert kan je kiezen voor een bepaalde tentoonstelling
of experience. Alleen deze wordt dan gestart. De workflow bestaat zelf weer
[uit andere jobs en
workflows](https://awx.museum.naturalis.nl/#/templates/workflow_job_template/76/workflow-maker?template_search=page_size:20;order_by:name;type:workflow_job_template,job_template;search:start).

![awx workflow](awx-workflow.png)

## Filmpje beweegt niet of geen geluid

Dit probleem wordt vaak veroorzaakt doordat een show niet is gestart of dat het
bericht om de show te starten te vroeg is gestuurd of niet is ontvangen. Het
makkelijkste is dit op te lossen met de [start
show](https://awx.museum.naturalis.nl/#/templates/job_template/88?template_search=page_size:20;order_by:name;type:workflow_job_template,job_template;search:start)
workflow.

![start show](awx-startshow.png)

Na lanceren wordt gevraagd om welke show je wil starten. Als de workflow is
uitgevoerd is gaat de show vaak wel van start.

Mocht de specifieke interactive toch niet zijn gestart dan kan je ook nog per
interactive een herstart uitvoeren. Dit kan met de job template [restart
interactive](https://awx.museum.naturalis.nl/#/templates/job_template/104?template_search=page_size%3A20%3Border_by%3Aname%3Btype%3Aworkflow_job_template%2Cjob_template%3Bsearch%3Arestart),
na het invoeren van de computernaam wordt alleen de applicatie op die computer
herstart zonder de hele computer opnieuw te starten.

## Geen beeld bij een filmpje

Dit kan door meerdere redenen komen:

* filmpje is nog niet gestart (begint met zwart beeld), zie hierboven
* de projector is niet gestart
* de interactive zelf is niet goed opgestart

Als de projector niet is gestart zou dit ook zichtbaar moeten zijn als melding
in [sensu](https://sensu.naturalis.nl/#/events?status=2). In dit geval is er
sprake van een CRITICAL op een *ping check* van die desbetreffende projector.

![sensu critical ping](sensu-critical.png)

Of je kan het zien aan het lichtje op de projector zelf. In dat geval kan je de
[projector starten of herstarten in
awx](https://awx.museum.naturalis.nl/#/templates?template_search=page_size:20;order_by:name;type:workflow_job_template,job_template;search:project).

Vaak zie je het beeld van de projector daarna verschijnen en ook het beeld van
de computer. Mocht dit toch nog resulteren in een blauw beeld moet je de
computer zelf opnieuw starten.

## Blauw beeld bij een filmpje

Soms is de verbinding met de projector niet goed opgekomen. De projector moet
vaak eerst worden gestart daarna de computer om een goede verbinding tot stand
te brengen. Dit doe je door in AWX eerst de [computer te
stoppen](https://awx.museum.naturalis.nl/#/templates/job_template/86?template_search=page_size:20;order_by:name;type:workflow_job_template,job_template;search:computer).
En daarna de [computer weer te
starten](https://awx.museum.naturalis.nl/#/templates/job_template/82?template_search=page_size:20;order_by:name;type:workflow_job_template,job_template;search:computer).

Mocht het probleem hiermee niet zijn opgelost kan je de projector en de
computer opnieuw met AWX uitzetten. Dan eerst de projector starten en daarna de
computer.

## Computer vastgelopen en sensu geeft connectie fout

Heel af en toe lopen computers vast. Dit is terug te zien in
[sensu](https://sensu.naturalis.nl/#/events?status=2) met de mededeling `No
keepalive sent from client for 999 seconds (>=180)`.  Dit is meestal een zware
vastloper en hier kan je via AWX helaas niets aan doen. Bij dit soort gevallen
zit er niets anders op dan de machine zelf fysiek uit en aan te zetten in de
SER.

## Schattenjacht helmpjes met problemen

Als je in het spel schattenjacht in zaal aarde gebruik maakt van de helmpjes
dan moet de helm in ieder geval:

1. licht aan en een stem wanneer de helm wordt opgepakt
1. richten op bepaalde stenen in de schatkamer en hoor welke steen het is
1. in het
   [dashboard](https://dashboard.naturalis.nl/d/Z_l3ZCOWz/schattenjacht?orgId=1)
   moeten de gegevens van de helmpjes zichtbaar zijn

In de eerste geval gaat het om het helmpje zelf. Als het licht niet aan gaat
dan is of de batterij leeg of het helmpje fysiek stuk. In dat geval moet de
helm eerst aan de lader worden gehangen in het hok naast het Japans theater.
Echt kapotte helmen moeten eerst worden onderzocht en gerepareerd.

![schattenjacht helmpjes](dashboard-helmpjes.png)

Als de helm wel werkt, maar er worden geen stenen herkend dan is er mogelijk
iets mis met de verbinding met de showcontroller. Dit moet ook zichtbaar zijn
in Sensu.

Tot slot kan de bluetooth hub van de helmpjes zijn uitgevallen dit zijn ESP32
machientjes die aan het netwerk hangen. Deze kunnen worden herstart via AWX of
door de ethernet kabel opnieuw in te pluggen.

## Schattenjacht controllers met problemen

Het schattenjacht spel kent twee controllers die via de housekeeping
server allerlei informatie over het spel naar de monitoring sturen. Deze
controllers hebben de neiging om er nog wel eens uit te liggen. Dit is te
zien in het **sensu** dashboard. Dit probleem is vrij eenvoudig te
verhelpen in awx. Kies het job template **Reset POE device**, start
de job en kies de naam van het device dat je wil herstarten. Hierna
zal het apparaat weer gaan functioneren.

## Nisverlichting moet uit

Soms is de nisverlichting ten onrechte aan. Dit kan op dit moment nog niet via
AWX worden afgehandeld.

{{% notice warning %}}
@todo: moeten we hier nog een workflow voor maken?
{{% /notice %}}

## Werkverlichting moet uit

Na het starten van het museum blijft de werkverlichting vaak aanstaan om de
schoonmakers de tijd te geven het museum schoon te maken. Rond 10:00 moet deze
verlichting uit zijn. Dit kan in AWX met de [schakel werklicht museum
uit](https://awx.museum.naturalis.nl/#/templates/job_template/102?template_search=page_size:20;order_by:name;type:workflow_job_template,job_template;search:werklicht)
template.

Als je slechts van een bepaalde zaal het werklicht wil uitschakelen kan dit met
[stop
werklicht](https://awx.museum.naturalis.nl/#/templates/job_template/99?template_search=page_size:20;order_by:name;type:workflow_job_template,job_template;search:werklicht).

## Werkverlichting moet aan

Soms moet de werkverlichting van een bepaalde zaal juist weer aan. Dit kan met
de [start
werklicht](https://awx.museum.naturalis.nl/#/templates/job_template/100?template_search=page_size:20;order_by:name;type:workflow_job_template,job_template;search:werklicht)
job template. Na het opgeven van de zaal wordt het werklicht in die betreffende
zaal aangeschakeld.

## Museum uit

Aan het einde van de dag moet het museum ook weer worden uitgeschakeld.  Daar
is in AWX de [stop
museum](https://awx.museum.naturalis.nl/#/templates/workflow_job_template/90?template_search=page_size:20;order_by:name;type:workflow_job_template,job_template;search:museum)
workflow die [achter elkaar de zalen
uitschakeld](https://awx.museum.naturalis.nl/#/templates/workflow_job_template/90/workflow-maker?template_search=page_size:20;order_by:name;type:workflow_job_template,job_template;search:museum).

Letop! Deze kan al zijn
[gescheduled](https://awx.museum.naturalis.nl/#/schedules), of [al
draaien](https://awx.museum.naturalis.nl/#/jobs?job_search=page_size:20;order_by:-finished;not__launch_type:sync),
dus is het zaak om eerst te kijken of dit het geval is.

## Tentoonstelling uit

Als je niet het hele museum in een keer wil uitschakelen of er is een zaal die niet
goed is uitgegaan kan je ook een [tentoonstelling per zaal
uitschakelen](https://awx.museum.naturalis.nl/#/templates/workflow_job_template/85?template_search=page_size:20;order_by:name;type:workflow_job_template,job_template;search:tentoon).
Na het activeren van deze activiteit moet je opgeven welke tentoonstelling je
wil uitschakelen.

## Computer aan of uit

In AWX kan je ook computers individueel of per groep aanzetten met [start
computers](https://awx.museum.naturalis.nl/#/templates/job_template/82?template_search=page_size:20;order_by:name;type:workflow_job_template,job_template;search:start;search:computer)
en uitzetten met [stop
computers](https://awx.museum.naturalis.nl/#/templates/job_template/86?template_search=page_size:20;order_by:name;type:workflow_job_template,job_template;search:computer).

## Projector aan of uit

Dit geldt ook voor projectoren. Die kan je zowel aanschakelen met [start
projectors](https://awx.museum.naturalis.nl/#/templates/job_template/81?template_search=page_size:20;order_by:name;type:workflow_job_template,job_template;search:projector)
als uitschakelen met [stop
projectors](https://awx.museum.naturalis.nl/#/templates/job_template/87?template_search=page_size:20;order_by:name;type:workflow_job_template,job_template;search:projector).

## Help mijn probleem staat er niet bij

Of als je het probleem niet met bovenstaande stappen krijgt opgelost.
Nooit in paniek raken.

Noteer het tijdstip waarop je het probleem hebt geconstateerd en beschrijf zo
precies mogelijk de symptomen. Controleer alle monitoring gegevens en de
stappen die je hebt ondernomen om het op te lossen.

Neem contact op met iemand van het technisch team die het systeem heeft helpen
bouwen of zet een interactive of projector tijdelijk *uit*, zodat er op een
later moment wel naar gekeken kan worden. Maak een issue aan of meldt aan
via [het
formulier](https://naturalis.topdesk.net/tas/public/ssp/content/serviceflow?unid=03110973fd0d47a9a9a291f917887876).

## Vragen of opmerkingen

Deze handleiding is waarschijnlijk niet compleet. Mocht je nog vragen of
opmerkingen hebben, zet ze in de [chat](https://chat.naturalis.io),
iemand van het technisch team zal dit dan met je bespreken en duidelijker maken
in de documentatie.
