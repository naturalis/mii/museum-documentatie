---
title: "Google Drive locaties"
date: 2019-07-08T09:37:18+02:00
draft: false
status:
attachments:
---

## Algemene informatie

Er zweven wat documenten rond op de Google Drive die interessant zijn maar waar we
het bestaan niet van weten of vergeten zijn waar ze te vinden zijn.

We hebben natuurlijk liever documenten op onze eigen contentserver maar deze
bestanden worden bijvoorbeeld veranderd door andere afdelingen zodat wij een
oudere versie op de contentserver hebben staan. Vandaar dat ik besloten heb om
ze toch zo te documenteren.

## Zaal tekeningen

* [Overzicht en nummering zalen](https://docs.google.com/spreadsheets/d/1l6hu_3QlfZqYrP4A1FpMCqkEZDcGi0bdYCpZoE8BR2Q/edit?usp=sharing). Links in ROOD doet het niet meer.
* [Dezelfde tekeningen, maar dan de originele locatie](https://drive.google.com/drive/folders/1-G4iwcl-UY2KEkMCItKxzHNd2MTA-8FV?usp=sharing_eip&ts=604f37f3)

## StudioLab 2.0

* [StudioLab 2.0 locatie afdeling breed](https://drive.google.com/drive/folders/1ZAO7-bDWNnE56LwQOEYBAyMQSPOuouLb?usp=sharing_eip&ts=602cd823)
* [StudioLab 2.0 locatie voor TT](https://drive.google.com/drive/folders/162LY3Ze--8gUl6A1iLq9zlvPnXC_POGf)
