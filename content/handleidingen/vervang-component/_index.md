---
title: "Vervang Component"
date: 2020-03-05T14:57:03+01:00
draft: false
status:
attachments:
---

## Algemene informatie


### Inhoud

## Omschrijving

Deze handleiding beschrijft hoe je een component vervangt. Voor het vervangen van een NUC zie [Vervang NUC](https://docs.museum.naturalis.nl/latest/handleidingen/vervang-nuc/).

## Benodigdheden

1. Toegang tot: TOPdesk, Gitlab en AWX
1. Asset naam oude component (bijvoorbeeld bewakingjapanstheater-ctl-1)
1. In TOPdesk (Asset managment) ingevoerd vervangend component (is dat nog niet gebeurd zie handleiding [Assets invoeren in TOPdesk](https://docs.museum.naturalis.nl/latest/handleidingen/assets-invoeren-in-topdesk) )

### Personen

Dit kan je in je ééntje doen. Maar met z'n tweeen is wel zo gezellig.

### Kennis

Kennis van TOPdesk, AWX en Gitlab.

### Onderdelen


### Gereedschap

1. Computer/Laptop

## Instructies

### TOPdesk

1. Zoek beide assets op in de Assets Overview van TOPdesk, we gaan nu een paar wijzigingen maken. Het is handig om eerst alle wijzigingen te doen voordat je ze saved. Dit scheelt dubbele namen
1. Neem bij de vervangende asset de volgende waarden over van de te vervangen asset:
* naam
* configuratie status
* IP adres
* aansluitpunt
* Netwerk velden
1. Verander de naam van de te vervangen asset in zijn oude naam (die kan je vinden in de history: rechts onder vind je dat zijn naam is veranderd van bijvoorbeeld nn-ctl-48, dit is zijn oude naam) en verander zijn configuratie status naar bijvoorbeeld defect
1. Save beide pagina's

### AWX

Een component met een netwerk aansluiting hoort ook in ansbile beschreven te staan. De ansible playbook waar de hosts in beschreven worden is [ansible-museum](https://gitlab.com/naturalis/mii/museum/ansible-museum). Deze is niet algemeen beschikbaar.

Een vervangend component heeft een ander MAC adres, deze moet je wijzigen in zijn host_vars (inventories/production/host_vars).

Nu kan je in AWX de Template "Update en configureer device" draaien. Deze zal de switch poort in het juiste VLAN zetten en de device zijn 'vaste' DHCP IP nummer toekennen.

## Tests

Wanneer de asset zijn juiste IP nummer krijgt heeft alles gewerkt.
Wanneer dat niet zo is zou je stap voor stap terug kunnen gaan en controlleren of er iets mis is geconfigureerd:

- Staat het IP nummer, MAC adres en switch poort goed geconfigureerd in de host_var
- Staat de asset goed in de hosts file
- Heeft AWX de laatste versie van de repo opgehaald
- Is je git push goed gegaan, en staat je wijziging het al in de master branch
- Staat het IP nummer, MAC adres en switch poort goed geconfigureerd in TOPdesk

## Bijlagen

{{< pagelist bijlagen >}}
