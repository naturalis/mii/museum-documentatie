---
title: "Updaten Watchpax Players"
date: 2020-01-17T15:05:54+01:00
draft: false
status:
attachments:
---

In het museum worden op twee plaatsen [Dataton Watchpax](../../componenten/dataton-watchout) players gebruikt:

* [projectiewaterplaats](../../interactives/projectiewaterplaats) in
  de zaal Leven
* [cirkelprojectie](../..interactives/cirkelprojectie) in de zaal
  De Dood

In deze handleiding lees je hoe je de content op deze players update.

## Voorbereidingen

Voor het updaten van de players moet je gebruik maken van de Watchout software
(Windows software) met bijbehorende USB dongle.

De versie van de [Watchout software](../../componenten/dataton-watchout) die je gebruikt om content up te daten moet overeenkomen met de versie die
op de Watchpax players draait. De in het museum gebruikte versie is 6.3.1. Deze
versie is [te downloaden van de content
server](https://files.museum.naturalis.nl/s/zkmXQfEaH87me5p) en staat eveneens
geïnstalleerd op de gemeenschappelijk laptop van het Technisch Team.

De USB dongle met de bijbehorende licentie is te vinden in de sleutelkast in
het technisch team hok.

Om vanaf je computer de players te kunnen programmeren dien je tot slot te
verbinden met hetzelfde netwerk waarin de players staan. Dit kan door
verbinding te maken met het draadloze netwerk dat bij de zaal hoort
(bijvoorbeeld SSID 'Leven').

Let op: het updaten van de content is hinderlijk voor de bezoekerservaring. Doe
dit dus buiten openingstijden of zet ten minste de bijbehorende projectoren uit
of op zwart.

## Updaten content

De Watchpax players hebben ruwweg twee manieren om te draaien: zelfstandig (al
dan niet aangestuurd door een showcontroller) of op basis van live bediening.
In het museum draaien de players zelfstandig en luisteren ze via Artnet / DMX
naar triggers van de showcontrollers.

Om de content te updaten en daarna de players aanstuurbaar te maken via de
showcontrollers ga je als volgt te werk:

1. Zet het showbestand met de bijbehorende Content folder op de
   USB-stick. Indien deze er al op stonden, overschrijf dan de mediabestanden
   met een nieuwe versie en met dezelfde naam. Je kunt deze downloaden uit de
   map van de interactive op de content server.
1. De USB stick en content server hebben een versie beheer d.m.v. datum-mapjes.
1. Verbind de laptop met het netwerk van de tentoonstelling.
1. Steek de licentie dongle in je laptop.
1. Open Watchout op de laptop. Mocht Windows via een popup vragen naar Firewall
   instellingen, zorg dan dat Watchout op zowel privé als publieke netwerken
   volledige toegang heeft.
1. Open in Watchout het betreffende showbestand uit de eerste stap:
   Klik op Window > Media > Selecteer 1.mp4 en druk op enter
   (of rechtermuistoets). Vervang door de nieuwe content via Browse.
1. Klik op het Stage venster om de software de controle te geven over de
   players.
1. Hernoen niet de showfile. Deze hoort "waterplaats het leven" te heten. In de
   Watchpack wordt deze show namelijk opgestart.
1. Let op de layers in de main timeline window: 1.mp4 hoort in Layer 21 en 2.mp4
   hoort in Layer 20.
1. Kies in het menu voor 'Stage > Update'. Watchout gaat als het goed is nu de
   content op de players updaten.
1. Voor de zekerheid sla je nu een kopie op op de USB-stick: File > Save A Copy
   Noem hem: "waterplaat het leven datum".
1. Zodra alle content is geupdate, selecteer het Stage venster en klik op
   "Display 1". Kies dan in het menu voor 'Stage > Manage Display Computer >
    Remote Access'. Er verschijnt nu een apart venster.
1. Sluit nu de Watchout software en kies in het remote access venster voor
   'File > Re-launch'. De controle van de software over de players komt hiermee
   te vervallen, zodat de players weer naar externe triggers van
   de showcontroller luisteren.
