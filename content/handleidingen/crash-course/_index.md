---
title: "Crash Course museum debugging"
date: 2019-07-08T09:37:18+02:00
draft: false
status:
attachments:
---

## Prerequesites

* ansible-museum repo installeren <https://gitlab.com/naturalis/mii/museum/ansible-museum>
* develop - voor nieuwe ontwikkelingen
* master - voor productie
* ansible galaxy installatie voor roles/requirements.txt
* roles zijn geversioned!

Om de boel te versnellen maken we gebruik van mitogen.

## Git tricks

Voor schonere repo history.

```bash
git checkout develop
git pull --rebase
git push
git checkout master
git merge develop
git pull --rebase
```

## AWX

Het project staat op de master branch van de repo. In de inventory
staan alle variabelen van een host gedefinieerd. Die moeten wel in
sync zijn met je eigen repo.

## Playbooks

De Ansible configuratie bestaat uit playbooks die zelf weer uit plays bestaat,
die op zichzelf weer uit meerdere rollen kan bestaan.

### Job Template

Is een aanroep van een ansible-playbook waar je zelf bepaalde parameters zoals
tags en limits kan toevoegen.

### Workflow template

Een combinatie van Job templates.

### Strategy

Standaard is de afwerking van een playbook 'linear' waarbij iedere taak
synchroon wordt uitgevoerd. Standaard wordt mitogen-linear gebruikt.

### Survey

Is een vraag ingebouwd om parameters te vereisen bij een playbook.

### Artifacts

Sommige variabelen worden in het playbook dynamisch gezet.

### Host patterns

Je kan patterns gebruiken om joins te maken van hosts met bijvoorbeeld
`interactive:&deverleiding`.

## Belangrijke rollen van een interactive

### Vlan

Een host variabele bestand bestaat uit in ieder geval:

* type
* ansible_host
* network_interfaces

Bij een nieuwe host moet de switch en de switch_port binnen network_interfaces
gezet worden. De default wordt gehaald uit de default_port_config van een
group_vars (zie bijvoorbeeld
deverleiding).

### Base Deployment

Typische taken in een deployment:

* Maas wijst een vlan toe
* Doe 'Comission and deploy machine'
* Power on: Stuurt een Wake-On-Lan (als hij niet reageert moet de machine met
  de hand gestart)
* Wake-On-Lan kan ook met de hand in `/etc/maas/wol/hostname.domain.sh` op de
  maas-master
* Install Naturalis Base  (alleen de essentials)
* Install Interactive Base  (de essentials van alle interactives)
* En vervolgens rollen naar type games, mediaplayer of kiosk

### Services en systemd

Iedere interactive heeft system services. De belangrijkste hierin is de
`interactive-user.target`. Een target bevat dependencies  in `Requires`.

* user@1111.service

Start de user context voor systemd.

* getty@tty1.service

Start een standaard login voor de interactive user. Waarbij diverse applicaties
via standaard profiles gestart.

* `.zprofile` - start X
* `.xinitrc` - start `start.sh`
* `start.sh` - Zet de resolutie en diverse parameters. Genereerd  een
  achtergrond. Zet environment en die start via systemctl
  `interactive-user.target`.

De service van de interactive user zijn te controleren met (inloggen als
interactive):

```bash
systemctl --user status
```

```mermaid
sequenceDiagram
  participant systemd
  participant user@1111.target
  participant getty@tty1.service
  participant interactiveuser.target
  participant .zprofile
  participant .xinitrc
  participant start.sh
  participant display
  participant screen_background
  systemd->>user@1111.target: start
  activate user@1111.target
  user@1111.target->>getty@tty1.service: start
  activate getty@tty1.service
  getty@tty1.service->>interactiveuser.target: start
  activate interactiveuser.target
  interactiveuser.target->>.zprofile: start Xorg Xserver
  .zprofile->>.xinitrc: Execute
  .xinitrc->>start.sh: Exectute
  activate start.sh
  start.sh->>display: Set resolutions
  display-->>start.sh:
  start.sh->>screen_background: Set image
  screen_background-->>start.sh:
  start.sh-->>interactiveuser.target: started
  start.sh-->>getty@tty1.service: started
  deactivate start.sh
  deactivate getty@tty1.service
  deactivate interactiveuser.target
  deactivate user@1111.target
```

### Audio

`pulseaudio.service` - Zet de PulseAudio goed.

Alle audio wordt gedaan met PulseAudio. Om de audio te checken kan je gebruik
maken van `pulsemixer`.

### Logging

Systemd services loggen naar journald en die zijn te lezen met `journalctl`.
Typische commandos die in de praktijk veel gebruikt worden:

```bash
journalctl -u NetworkManager -f
```

Of als interactive user:

```bash
journalctl --user -u vnc
```

Laat zien welke boots er geweest zijn:

```bash
journalctl --list-boots
```

Maak een lijst van alle dependencies van een target:

```bash
systemctl --user list-dependencies interactive.target
```

## Interactives

Er zijn drie soorten interactives met ieder hun eigen plays:

* games
* mediaplayers
* kiosks

Iedere interactive host hoort bij een of meerdere groepen.

## Andere assets

Naast andere interactives kennen we ook soorten assets, zoals:

* projectoren
* arduinos
* esp32
* switches

Ook deze kunnen via Ansible worden bedient.

## Roles

De roles van de Ansible repository staat in roles/requirements.yml hiermee
wordt via ansible-galaxy alle afhankelijke projecten en rollen worden
geïnstalleerd. Dat kan met het commando:

```bash
ansible-galaxy install -r roles/requirements.yml --roles-path ./roles/
```

In `roles/requirements.yml` wordt altijd naar specifieke getagde versies van
Ansible rollen verwezen. Om een rol te updaten ga je als volgt te werk:

1. Tag de lokaal uitgecheckte repository van de role:

   ```bash
   git tag 0.1.2
   ```

1. Push de tag naar de remote repository:

   ```bash
   git push origin 0.1.2
   ```

1. Pas de versie aan in de `develop` branch van de `ansible-museum` repo.
1. Commit en merge de wijziging naar de master branch:

   ```bash
   git commit -m 'Gebruik versie 0.1.2' roles/requirements.yml
   git pull --rebase
   git push
   git checkout master
   git merge develop
   git pull --rebase
   git push
   ```

1. Draai op AWX het 'Update museum inventory' template

Iedere rol heeft default variabelen die terug te vinden zijn in `defaults/main.yml`
dit bestand geeft de default waarden, maar ook de velden die ingesteld kunnen
worden per rol.

### Mediaplayer

Op iedere mediaplayer draaien 1 of meer mpv instances.

Na een lang vooronderzoek hebben we gekozen voor mpv omdat het platform aan
de meeste van onze eisen voldoet. Alleen het synchroon spelen van films
zoals de projectie in de dood hebben we niet aangedurft.

We doen ook dingen die niet kunnen op andere commerciele oplossingen:

* knop en plaatje
* unicast udp koppeling
* multicast udp koppeling
* meerdere spelers op een machine (audio per kanaal of meerdere kanalen)
* gebruik maken van externe dacs

Al deze varianten zijn instelbaar met de host_vars of group_vars voor de
mediaplayer role.

De rol van mediaspelers zit in ansible-mpv. Belangrijke onderdelen in de
rol zijn:

* mpv_instances - Geeft aan welke players er draaien op een mediaplayer machine.

De content van de mpv player wordt gehaald van de content server en bestaat uit
drie playlists (show, emergency en test).

* mpv_controllers - Dit zijn de udp services (unicast en multicast) die per player kunnen bedienen.

De udp configuratie ontvangt bepaalde commandos die een script van commandos naar
mpv stuurt.

### Games

### Kiosks
