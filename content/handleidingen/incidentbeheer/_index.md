---
title: "Incidentbeheer"
date: 2019-09-30T13:51:57+02:00
draft: false
status:
attachments:
---

Het technisch team gebruikt Topdesk voor de administratie en afhandeling van
storingsmeldingen. Specifiek maken we gebruik van het Kanban board in Topdesk.
De rolverdeling bij en wijze van de verwerking van de meldingen staat hieronder
opgesomd.

Als regulier lid van het team zie je toe op de juiste verwerking van meldingen.
Specifiek heb je vanuit deze rol de volgende taken:

* Loop de lijst 'unprioritized' door en voeg aan meldingen met de naam 'Melding
  museum' een titel toe die een samenvatting geeft van het probleem.
* Zorg dat deze melding aan de juiste functionele eenheid zijn gekoppeld.
* Geef een reactie richting de aanmelder en vraag daarbij om aanvullende
  informatie.
* Zet zaken met een (zeer) hoge impact door naar de lijst Todo.
* Zet met de kernleden aan het einde van de dag de issues op To Do klaar voor
  de volgende dag.
* Zet meldingen op de lijst 'In progress' op de naam van een behandelaar
  (indien deze bekend is) of terug op de lijst 'To Do' en op naam Technisch
  Team.
* Loop de lijst Waiting na en doe waar nodig navraag naar de status en sleep
  Opgelost (gereed) meldingen naar Done indien de melding echt is opgelost of
  naar To Do als er nog actie is vereist.

Of je nu regulier of ad hoc lid bent van het team, zodra je aan de slag gaat
met het oplossen van meldingen, ga je als volgt te werk:

* Pak meldingen op uit de lijst To Do en zet deze op je eigen naam en
  verschuif deze naar In Progress.
* Is de melding opgelost, zet de status op Opgelost (gereed) en meldt dit terug
  naar de aanmelder.
* Kom je niet klaar met de melding omdat je niet voldoende tijd hebt zet de
  melding weer op Technisch team in de lijst To Do.
* Wacht de oplossing van de melding op input van de aanmelder zet de melding op
  Wacht op klant.
* Wacht de oplossing van de melding op actie van een leverancier zet de melding
  op Wacht op leverancier.
