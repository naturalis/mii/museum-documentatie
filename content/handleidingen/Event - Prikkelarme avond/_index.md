---
title: "Event - Prikkelarme avond"
date: 
draft: false
status:
attachments:
---

# Event - Prikkelarme avond

Voor een prikkelarme avond moet de audio in het museum aangepast worden en erna moet de audio weer op het normale niveau ingesteld worden. Dit is geregeld met een tweetal playbooks in https://gitlab.com/naturalis/mii/museum/ansible-museum. Beide playbooks zijn via AWX te starten met onderstaande jobs en kan worden uitgevoerd door Support en Beveiliging.

## Start prikkelarme avond
Het "start" playbook heeft 3 functies:
- het reduceren van het audio niveau op specifieke interactives
- het muten van een hele zaal
- het muten van specifieke interactives

Het audio niveau van interactives kan aangepast worden met de variabele `lowstimulus_audio_volume`. Dus bijvoorbeeld `lowstimulus_audio_volume: 40%`

[lowtimulus_start.yaml](https://gitlab.com/naturalis/mii/museum/ansible-museum/-/blob/main/playbooks/museum/lowstimulus_start.yml)


## Stop prikkelarme avond
Het "stop" playbook heeft 3 functies:
- het resetten van het audio niveau op specifieke interactives die zachter gezet zijn
- het unmuten van een hele zaal
- het unmuten van specifieke interactives

[lowtimulus_stop.yaml](https://gitlab.com/naturalis/mii/museum/ansible-museum/-/blob/main/playbooks/museum/lowstimulus_sttop.yml)

# Huidige settings

- Zaal Leven en interactives Sexystories, Letsdance en Film Dubois: Audio reduceren tot opgegeven niveau
- Zaal de Dood, Dinotijd, Evolutie en IJstijd (behalve kijkers): muten
- Interactives camperhawaii, camperjapan, greenporn, vogelsvoeren: muten

# links
Ticket voor realiseren playbooks: https://gitlab.com/naturalis/mii/museum/ansible-museum/-/issues/14