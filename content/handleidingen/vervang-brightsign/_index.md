---
title: "Vervang Brightsign"
draft: false
status:
attachments:
---

Wanneer vastgesteld wordt dat een Brightsign defect is en vervangen moet worden ga je als volgt te
werk:

## Voorbereiden nieuwe Brightsign

Wanneer een Brightsign nieuw is geleverd moet deze eerst in TOPdesk worden geadministreerd en vervolgens in BrightAuthor Connected worden aangemeld en van content worden voorzien. Nadat deze voorbereidingen uitgevoerd zijn, kan de mediaplayer worden gewisseld.

### Voorbereidingen

Voor het uitvoeren van alle onderstaande handelingen heb je een aantal tools
nodig.

* TOPdesk account
* BSN.Cloud account
* BrightAuthor Connected applicatie
* Gitlab account met schrijfrechten op de ansible-museum repository.
* NextCloud account met toegang tot de museum fileserver, met name de [Brightsign map](https://files.museum.naturalis.nl/s/BSdD2K9zTXfB5Gq).

### TOPdesk

1. Zoek in TOPdesk met Module 'Assets overview' en op basis van de naam en van de te vervangen Brightsign de bijbehorende assetkaart.
2. Zoek eveneens de assetkaart van de nieuwe Brightsign, ditmaal op basis van het serienummer.
3. Neem de velden outlet, IP adres, aansluitpunt, outlet en beoogde naam over van de huidige Brightsign naar de nieuwe Brightsign en verwijder deze bij de huidige Brightsign.
4. Neem de relationship van de huidige Brightsign over op de nieuwe Brightsign en verwijder deze bij de huidige Brightsign door te klikken op 'Unlink'.
5. Vervang de naam bij de huidige Brightsign (bijv. van `waternesten-cmp-1` naar `nn-cmp-151`). Je kunt het oorspronkelijk nn nummer van de asset vinden door in History te filteren op alleen 'Card modifications'.

### BrightAuthor Connected

1. Open de BrightAuthor Connected applicatie en log in met je BSN.Cloud account.
2. Ga naar Provision, klik op Add Player en vul het serienummer in, de naam van de mediaplayer en zaal [naam van de zaal] (bijv. zaal Leven) in het venster Player Description.
3. Zorg dat zowel de BrightSign als de SD-kaart helemaal leeg zijn.
4. Sluit de BrightSign en je laptop aan op hetzelfde lokale netwerk.
5. Sluit een monitor en de stroomadapter aan op de Brightsign.
3. Zoek in de NextCloud fileserver naar de standaard presentatie `[type]-template.bpfx` die overeenkomt met de nieuwe type BrightSign en download deze.
4. Zoek in de NextCloud fileserver naar de contentmap van de te vervangen mediaplayer en download de content.
5. Open de standaard presentatie en wissel de video uit met de gedownloade content.
6. Sla deze nieuwe presentatie op onder de naam [interactive][jaar][maand][dag] en upload deze naar de contentmap van de mediaplayer op de fileserver.
7. Kies Publish Schedule, slecteer de nieuwe Brigthsign of voeg een Networked Player toe en vul het IP adres in die op de mediaplayer toont.
8. Selecteer Active All Day Every Day en klik vervolgens op Publish.

## Mediaplayer vervangen

### Ansible inventory

1. Open de `master` branch van de [ansible-museum repository](https://gitlab.com/naturalis/mii/museum/ansible-museum) .
2. Klik op Edit kies de Web IDE.
3. Zoek onder `host_vars` het bestand van de mediaplayer die vervangen moet worden en wijzig het mac adres.
4. Ga naar Source Control, vul een commit message in en klik op Commit and push to `main`.
5. Ga naar de [Projecten](https://awx.museum.naturalis.nl/#/projects?project_search=page_size:20;order_by:name) pagina van Ansible AWX en klik op `De nieuwste SCM-herziening ophalen`.
6. Maak een melding voor Ict Infra om de IP reservering in Maas aan te passen. Noem daarbij de naam van de mediaplayer, het oude en het nieuwe mac adres.

Wanneer je bevestiging hebt dat de IP reservering in Maas is gelukt, kun je de nieuwe Brightsign aansluiten op de plek van de oude mediaplayer.

### Fysiek wisselen mediaplayer

Zorg er voor dat de nieuwe Brightsign wordt voorzien van het label met de naam van de
computer (bijv. `waternesten-cmp-1`) en verwijder dit label van de oude mediaplayer.

Haal de huidige mediaplayer weg en sluit de nieuwe, vervangende Brightsign aan op de fysieke plek van de oude mediaplyer. Controleer voor de zekerheid ook of de outlet of switchpoort waarop de Brightsign wordt aangesloten overeenkomt met de informatie op de assetkaart in TOPdesk.

## Bijlagen

{{< pagelist bijlagen >}}
