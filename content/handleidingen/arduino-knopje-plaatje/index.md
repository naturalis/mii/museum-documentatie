---
title: "Knopje plaatje (Arduino LOIS)"
date: 2022-04-04T12:56:53+02:00
draft: true
status:
attachments:
- title:
  src:
- title:
  url:
---

## Algemene informatie

<!-- In het onderstaande document geven we aan wat de gewenste document indeling is
voor een handleiding en wat we hoe beschreven willen zien. -->

### Inhoud

## Omschrijving

In deze handleing word uitgelegd hoe je [LOIS](https://docs.museum.naturalis.nl/latest/componenten/naturalis-lois/) voorziet van de juiste Arduino code voor het simpel aansturen van een interactive.

## Benodigdheden

### Sofware

PlatformIO (+ code editor)
[PlatformIO](https://platformio.org/) is een handige tool voor het werken met Arduino. PlatformIO gebruik je in combinatie met een code editor. [VSCode](https://code.visualstudio.com/) is een krachtig crossplaform code editor.

Maar er zijn natuurlijk ook andere opties die ook gebruikt kunnen worden in PlatformIO:
[Vim](https://opensource.com/article/19/3/getting-started-vim)
[Atom](https://atom.io/)

In de onderstaande instructies gaan we aan de slag met VSCODE in combinatie met PlatformIO.

GIT
Download [hier](https://git-scm.com/download/win) of
installeer met [Chocolatey](https://community.chocolatey.org/packages/git).

### Hardware

[LOIS](https://docs.museum.naturalis.nl/latest/componenten/naturalis-lois/)

### Personen

<!-- Hier wordt aangegeven met hoeveel mensen de handeling kan worden verricht. Hou
hierbij ook rekening met de ARBO regels die op dit gebied van toepassing zijn. -->

### Kennis

<!-- Welke voorkennis is nodig voor het uitvoeren van de handelingen en waar is deze
te vinden. -->

### Onderdelen

<!-- Een overzicht van de onderdelen die nodig zijn bij het uitvoeren van de
handelingen indien nodig. -->

### Gereedschap

<!-- Een overzicht van het gereedschap dat nodig is voor het uitvoeren van de
instructies. -->

## Instructies

Tools:

Installeer een code editor die compatibel in met [PlatformIO](https://platformio.org/), bijvoorbeeld [VSCode](https://code.visualstudio.com/).

Open VSCode en klik in de linker balk op Extensions (icoontje bestaat uit 4 blokjes). Zoek vervolgens op platformio. Bovenaan staat nu als het goed is PlatformIO IDE. Installeer deze door op install te klikken.

Code:

Alle code die we gebruiken in het museum is te vinden op gitlab: <https://gitlab.com/naturalis/mii>. Elke folder die begint met ```arduino-``` bevat Arduino code die we kunnen aanpassen of hergebruiken.

De repo [arduino-lois](https://gitlab.com/naturalis/mii/arduino-lois) bevat de basis code die gebruikt kan worden voor het simpel aansturen van een interactive op basis van simpele keyboard inputs in de vorm van letter. Deze code wordt gebruikt in combinatie met een [Arduino Leonardo](https://docs.museum.naturalis.nl/latest/componenten/arduino-leonardo/) en emuleren we eigenlijk een toetsenbord waardoor een developer ook zonder een knop en microcontroller de applicatie op de interactive kan testen. Een aantal repo's zoals arduino-flipperkast en arduino-spermarace zijn hier afgeleiden van. Dit omdat hier geen standaard knoppen worden gebruikt.

<!---
De repo [arduino-soe](https://gitlab.com/naturalis/mii/arduino-soe) bevat de code voor het versturen van UDP commando's naar de showcontrollers over het netwerk middels de druk op een knop.  

Met arduino-lois en arduino-soe vormen dus de basis en zijn in de meeste gevallen geschikt voor het aansturen van een specifieke interactive of de showcontroller. 

Maar er zijn ook voorbeelden die gebruik maken van complexere sensoren, zie bijvoorbeeld [arduino-ertsenelecto]https://gitlab.com/naturalis/mii/arduino-ertsenelectro), [arduino-timelapse](https://gitlab.com/naturalis/mii/arduino-timelapse) of [arduino-handscanner](https://gitlab.com/naturalis/mii/arduino-handscanner).

--->

We gaan er voor de verdere uitleg vanuit dat je simpelweg een knop wilt toevoegen waarmee je een filmpje of audio-fragment op een video player mee wilt kunnen starten.

Ga naar de repo [arduino-lois](https://gitlab.com/naturalis/mii/arduino-lois) en click rechts op de blauwe knop clone. Kopiër de url bij "Clone with HTTPS" of gebruik deze link ```https://gitlab.com/naturalis/mii/arduino-lois.git```.

Ga in Windows Powershell naar de directory op je locale werkstation (bijvoorbeeld laptop) waar je de code wilt hebben staan en type daar:

```bash
git clone https://gitlab.com/naturalis/mii/arduino-lois.git
```

Ga vervolgens naar de directory

```bash
cd  arduino-lois
```

Start VSCODE

```bash
code . 
```

Als het goed is opent VSCode nu

## Tests

<!-- Hier wordt aangegeven welke tests er kunnen worden uitgevoerd om te testen of de
instucties ook het gewenste resultaat hebben opgeleverd.-->

## Bijlagen

{{< pagelist bijlagen >}}
