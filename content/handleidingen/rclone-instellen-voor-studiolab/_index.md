---
title: "Samba en Rclone instellen voor StudioLab 2.0"
date:
draft: true
status:
attachments:
---


## Samba

De Streaming PC gaat zijn videodata opnemen op de NUC studiolab-cmp-5.
Deze nu heeft een samba service draaien waar de Streaming PC connectie mee kan maken.

De Streaming PC en de NUC hebben een 1-op-1 10GbE verbinding met als netwerk instelling: 172.16.66.0/24.

### Login gegevens:
* share: `\\172.16.66.3\videodata`
* user: videouser
* password: zie bitwarden

Vervolgens wordt elke nacht de videodata verplaatst naar Google Drive.

{{% notice warning %}}
Let op! Dit is dus geen backup! Data wordt verwijderd van de NUC.
{{% /notice %}}

## Rclone

Dat verplaatsen wordt gedaan door [Rclone](https://rclone.org/).
Om de NUC automatisch te laten inloggen moet je wel wat voorbereiden.
Door `rclone config` te draaien en in te vullen crëeer je een configfile `~/.congig/rclone/rclone.conf`.
Die kan je koppelen aan een Google user account met een OAuth2 token flow of met een [service account](https://rclone.org/drive/#1-create-a-service-account-for-example-com). Wij hebben een service account:

* Google Project: [studiolab-recordings-001](https://console.cloud.google.com/iam-admin/settings?project=studiolab-recordings-001&orgonly=true&supportedpurview=organizationId)
* Google Service account: [service@studiolab-recordings-001.iam.gserviceaccount.com](https://console.cloud.google.com/iam-admin/serviceaccounts/details/101497894352427603717?project=studiolab-recordings-001&supportedpurview=project)
* Google Drive Share drive: [Studiolab recordings](https://drive.google.com/drive/folders/0APl49bprc3eTUk9PVA)

Rclone krijgt een JSON file en een rclone config file om met het service account in te kunnen loggen.

De data kan je nu verplaaten van `/data` naar de googledrive: met:

`rclone move /data googledrive:/`

Dit commando wordt elke nacht om 01:00 door systemd gedraaid.
