---
title: "Pharos"
draft: false
shows:
 - showlivescience
 - showaarde
 - showdinotijd
 - showjapanstheater
 - showrexperience
componenten:
 - pharos-tpsbb
 - pharos-lpc1
 - pharos-lpc2
 - figure53-qlab
attachments:
 - title: Handleiding Pharos Designer 2 (pdf)
   url: https://files.museum.naturalis.nl/s/5ETKJmw7KjxyRHH
---

Dit zijn de handleidingen die gaan over de configuratie en bediening van
de Pharos showcontroller.

## Componenten

Deze handleiding heeft betrekking op de volgende componenten:

{{< pagelist componenten >}}

Er zijn drie verschillende Pharos componenten in gebruik. De [LPC1](../../componenten/pharos-lpc1) ondersteunt 512 DMX kanalen, de [LPC2](../../componenten/pharos-lpc2) 1024. De [TPS](../../componenten/pharos-tpsbb) staat voor Touch Panel
Station en kan gebruikt worden om een showcontroller met programmeerbare
knoppen te bedienen. Dit laatste component wordt gebruikt in [studiolab](../../interactives/studiolab) en [bewakingjapanstheater](../../interactives/bewakingjapanstheater).

Bij de Pharos componenten zonder display kan je aan de LEDjes op het Pharos
logo zien of het apparaat nog functioneerd. Als die LEDjes aan zijn dan draait
hij normaal.

Beide typen Pharos LPC kunnen worden gevoed door middel van zowel een
poweradapter als via Power over Ethernet (PoE). Bij de bouw van het museum
bleek dat het simultane gebruik van beide typen voeding tot problemen leidde.
Alle Pharos showcontrollers worden om die reden gevoed door middel van PoE.

## Shows

Dit type showcontroller wordt gebruikt in de volgende shows:

{{< pagelist shows >}}

Belangrijke punten die te maken hebben met deze shows:

* Alle shows zijn geprogrammeerd door Rutger van Dijk van
  [SemMika](https://www.semmika.nl) behalve de Rexperience die wordt
  geprogrammeerd door [Kloosterboer](http://www.kloosterboer-decor.nl).
* Instellingen in Dinotijd zijn keuzes gemaakt door de lichtontwerper van die
  zaal.
* In Livescience worden twee showcontrollers gebruikt. Eén voor het aansturen
  van het licht en de ander voor alleen studiolab. Om die reden kan het zijn
  dat voor een wijziging in die zaal aanpassingen zijn vereist in twee
  showcontrollers.
* Rutger heeft in juni 2020 alle Pharos showcontrollers de firmware geupdate naar
  versie 2.7.5.
* Shows worden geprogrammeerd door middel van het programma Pharos Designer. De
  gebruikte versie van deze software moet corresponderen met de firmware op
  de showcontroller, anders werkt het niet.
* De configuratie bestanden per showcontroller zijn te vinden op de content server.


## Configuratie

De gebruikelijke aanpak bij het configureren van de shows door Rutger van Dijk
is door bij het indelen van scenes gebruik te maken van de volgende groepen:

* A: **Algemeen** - De standaard instellingen van de lampen (zonder show).
* B: **Basisstanden** - De show instellingen.
* C: **Test** - Testinstellingen van werking van bepaalde onderdelen in de show.

Scenes in dezelfde groep kunnen niet tegelijkertijd worden geactiveerd of
gebruikt.

Binnen een groep begint Rutger altijd te tellen bij 1. Als er dus een nieuw
cluster van functionele onderdelen begint dan begint dat dus ook meestal bij 1
of 101 of 201, etc.

In reports kun je overzichten opvragen van de configuratie van de showcontroller.
Op die manier kan je zien welke groepen, objecten en andere onderdelen binnen
een show zijn geconfigureerd.

Bij de indeling van triggers worden kleurcodes gebruikt om de acties goed van
elkaar te kunnen onderscheiden.

* Rood: systeemtriggers
* Oranje: externe uitsturing door FEMKE en knoppen
* Geel: testtriggers
* Groen: tijdtriggers
* Cyaan: alles wat uit touchscreen wordt getriggerd
* Paars: nested taaklijsten

Per show/zaal kunnen de timelines verschillen. Zo is de ene show veel
statischer van karakter dan de ander.

Om bepaalde onderdelen te testen of tijdelijk uit of juist aan te zetten kan je
gebruik maken van de zogeheten _park_ stand, als deze staat ingesteld wordt
iedere andere instelling overruled.

### Japans theater

Het Japans Theater heeft een vrij uitzonderlijke show. Behalve het licht en de
mediaplayers bepaalt de showcontroller in het Japans Theater ook de werking en
beweging van het poppentheater. Iedere Arduino en aangestuurde motor van iedere
pop wordt hier in de showcontroller van DMX waarden voorzien. Deze DMX waarden
worden met behulp van een Lua script omgezet naar seriële commando's. Dit
gebeurt iedere 0,2 seconden.

### QLab

In het Japans Theater wordt ook gebruik gemaakt van een QLab installatie op een
[Mac Mini](../../componenten/apple-macmini2018). Deze zorgt voor
geluidshow in Japans Theater. De communicatie tussen Pharos en QLab gebeurt op
basis van het [OSC protocol](https://en.wikipedia.org/wiki/Open_Sound_Control).

### Aarde

Aarde loopt helemaal op timeline, variërend tussen een lichte en donkere stand.
Daarnaast is er voor elke [schattenjacht](../../interactives/schattenjacht) helm een tijdlijn geprogrammeerd. Om die
reden is in deze zaal gekozen voor een Pharos showcontroller in plaats van een
Cuecore2.

### Dinotijd

De show in de zaal Dinotijd bestaat uit:

* Eén lichtstand voor de gehele zaal
* Het bij de start van de show activeren van de zaalbrede soundscape
* Het onafhankelijk aansturen van de videoprojecties
* De aansturing van de lampen en audioplayer van [dinovoetstappen](../../interactives/dinovoetstappen).

### Atrium

Ook in het Atrium wordt de verlichting in het plafond gestuurd met een Pharos
LPC. Deze kan worden bediend met een TPS achter de servicebalie. De lampen in
het Atrium kunnen nu nog niet van kleur veranderen, maar wel helder wit of
juist zachter geel. In de toekomst zouden deze lampen kunnen worden vervangen
door RGB lampen.
