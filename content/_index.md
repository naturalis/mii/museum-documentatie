---
title: "Museum Naturalis"
date: 2019-07-08T10:00:00+02:00
draft: false
tentoonstellingen:
- atrium
- auditorium
- deaarde
- dedood
- deijstijd
- deverleiding
- devroegemens
- dinotijd
- evolutie
- leven
- livescience
- zaal10
experiences:
- japanstheater
- rexperience
---

## Documentatie museum Naturalis

Dit is de museum documentatie van het nieuwe museum van Naturalis. Het doel van
deze documentatie is om het beheer van alle techniek in het museum te
ondersteunen. De doelgroep van de documentatie bestaat om die reden in de eerste
plaats uit leden van het technisch team. Maar ook publieksbegeleiders kunnen
profijt hebben van beschrijvingen op de site.

## Indeling

De documentatie op deze site bestaat uit twee delen.

### Algemeen nut

In het eerste deel staan alle handleidingen en standaarden die van algemeen nut
zijn. Handleidingen bieden je de informatie om beheertaken (zoals het
installeren van een computer) uit te voeren. Op de pagina's over de standaarden
die met name ook tijdens de bouw van belang waren staat uitgelegd hoe in
het museum wordt gedocumenteerd en aan welke technische voorwaarden onderdelen
in het museum moeten voldoen.

### Specifieke onderdelen

In het tweede, specifieke deel staan alle onderdelen [die we in het museum onderscheiden](./standaarden/documentatie/naamgeving#datastructuur) gedocumenteerd. Het uitgangspunt hierbij is dat specifieke informatie
telkens maar op één plek staat: bijzonderheden van een component dat in twee
interactives wordt gebruikt vind je op de pagina van het betreffende component.

Bekijk hier de documentatie van de tentoonstellingen:

{{< pagelist tentoonstellingen >}}

En die van de experiences:

{{< pagelist experiences >}}

## Work in progress

Het documenteren van een museum is - en blijft voorlopig - work in progress.
Als er zaken ontbreken op een pagina wordt dit aangegeven met een
informatieblok als de onderstaande:

{{< pagequality >}}

* Er stond geen koud bier klaar bij het documenteren van deze pagina
* Waar is Titus?
* Wat heeft Joep nu weer gedaan?
* David z'n goedkeuring - grom - krijgt het sowieso niet (er is altijd wel een
  Markdown syntax probleem).
* Hebben jullie ook een glitteronesie bij je voor vananavond?

{{< /pagequality >}}
