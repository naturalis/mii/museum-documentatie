---
title: "geboortetaart"
date: 2019-07-24T13:47:31Z
draft: true
status:
unid: 4e4e9bdd-f4e5-4d46-aa01-5963c0bc3a96
componenten:
tentoonstellingen:
- deverleiding
attachments:
- title: geboortetaart_bouwtekening_meubel.pdf
  src: https://files.museum.naturalis.nl/s/2eBZAeyXRCabLpP
- title: geboortetaart_bouwtekening_fonteinlamp.pdf
  src: https://files.museum.naturalis.nl/s/EyBbpndtwpwLipE
- title: geboortetaart_technischetekening_fonteinlamp.pdf
  src: https://files.museum.naturalis.nl/s/83jNZy5Xy52ytdS
---

![geboortetaart](./geboortetaart.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
