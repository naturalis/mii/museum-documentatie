---
title: "etalagegeur"
date: 2019-07-24T13:47:29Z
draft: true
status:
unid: 47319fb6-8652-4fdf-aef1-508cddcea8fe
componenten:
tentoonstellingen:
- deverleiding
attachments:
- title: etalagegeur_bouwtekening.pdf
  src: https://files.museum.naturalis.nl/s/X2YJLmnkbs2YPkb
- title: etalagegeur_technischetekening.pdf
  src: https://files.museum.naturalis.nl/s/kcf3o9gJ9nsK5tG
- title: verleiding_interactive-etalagegeur_titelbord_bouwtekening (png)
  src: https://files.museum.naturalis.nl/s/2dPJ4dLys2DDRPN
- title: safety data sheets (directory)
  src: https://files.museum.naturalis.nl/s/8r3YF7RjGKBPRRX
---

![geur](./geur.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

* [safety data sheets](https://files.museum.naturalis.nl/s/8r3YF7RjGKBPRRX)

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
