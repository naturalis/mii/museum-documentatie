---
title: "edmontosaurus"
date: 2019-07-24T13:47:29Z
draft: true
status:
unid: 4e159576-a3b9-440e-8cfc-3a211ab1c6af
componenten:
- canon-xeedwux6600z
- dapaudio-pr82
- ecler-eca120
- intel-nuc8i3beh
- smartmetals-0022070
- kramer-tp590txr
tentoonstellingen:
- dinotijd
attachments:
- title: edmontosaurus_bouwtekening.pdf
  src: https://files.museum.naturalis.nl/s/bnbNfWTiMrWSs4r
- title: edmontosaurus_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/LwgFEeBgMbTHyzm
- title: edmontosaurus_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/szydaHNRikCkysL
- title: edmontosaurus_projectieopstelling.pdf
  src: https://files.museum.naturalis.nl/s/6TowSyc6A4Hgf32
- title: edmontosaurus_projectieopstelling.vwx
  src: https://files.museum.naturalis.nl/s/fGB9KAPpHxGbPCq
- title: edmontosaurus_projectorophanging.pdf
  src: https://files.museum.naturalis.nl/s/bNfQ5y8oCiZ4qSN
- title: edmontosaurus_projectorophanging.vwx
  src: https://files.museum.naturalis.nl/s/bFeHxktstY3qPen
- title: edmontorsaurus_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/J9EwLceMMPW7Q8W
  src:
---

![edmontosaurus](./edmontosaurus.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
