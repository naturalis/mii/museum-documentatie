---
title: "tekentafellivescience"
date: 2019-07-24T16:05:10Z
draft: false
status:
unid: a31bb107-6444-4333-aac1-bdeb03bd010e
componenten:
tentoonstellingen:
- livescience
attachments:
- title: Tekenkamer tafel
  src: https://files.museum.naturalis.nl/s/HnAarZBTania8T4
- title: livescience_exhibits_tekentafel_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/nCDcwx9q8aBFAFZ

---

![tekentafel](https://files.museum.naturalis.nl/s/HnAarZBTania8T4/preview)

## Functionele omschrijving

Kijk mee terwijl wetenschappelijke illustratoren van Naturalis aan het werk zijn, bewonder hun
prachtige tekeningen (vroeger en nu) van levensechte planten en dieren of probeer het zelf.

Wetenschappelijke illustraties van planten en dieren spelen een onmisbare rol in het onderzoek
van Naturalis. Ze laten precies die details zien die nodig zijn en zijn veel nauwkeuriger dan een
foto.

## Technische omschrijving

* Mobiele Tekentafel als werkplek voor wetenschappelijk illustrator.

### Contact

* [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)
* [Manon Laterveer - de Beer](mailto:Manon.Laterveer-debeer@naturalis.nl)

### Bouwer

* [Bruns](https://bruns.nl)

## Bijlagen

{{< pagelist bijlagen >}}
