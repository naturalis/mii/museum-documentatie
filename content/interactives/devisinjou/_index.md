---
title: "devisinjou"
date: 2023-02-26T16:26:25Z
draft: true
status:
unid: 
componenten:
- iiyama-tf2738mscb2
- zotaczbox-magnusone
- atlona-atomeexkitlt
- thetamp-ta50
- visaton-fr10
tentoonstellingen:
- evolutie
experiences:
attachments:
- title: evolutie_interactive_devisinjou_meubel_bouwtekening.pdf
  src: https://files.museum.naturalis.nl/s/Pw64XYTLCx7NzfP
- title: evolutie_interactive_devisinjou_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/e4irfZ25KqmNYXS
- title: evolutie_interactive_devisinjou_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/PsxbodXDZjeHa3n
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

![definitiefontwerp](./devisinjou_ontwerp.png "definitief ontwerp")

## Functionele omschrijving

Röntgenapparaat (niet echt) waarin je botten en spieren ziet bewegen en je de tijd kun terugspoelen om je hand te zien veranderen in vinnen (morphen).

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

De configuratie van de Intel Nuc wordt verzorgd met behulp van ansible
configuratiemanagement. De configuratie is terug te vinden in de
[ansible configuratie van het museum](https://gitlab.com/naturalis/mii/museum/ansible-museum).

De applicatie werkt precies zoals andere interactive installaties en maakt
gebruik van de
[interactive playbook](https://gitlab.com/naturalis/mii/ansible-interactive).
De content komt van de content server en staat in
[content map van evolutie](https://files.museum.naturalis.nl/f/92431).

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

Voor het beheren en bedienen van dit soort interactives. Gebruik
de algemene handleiding
[Technisch museum beheer](https://docs.museum.naturalis.nl/latest/handleidingen/museum-beheer/).

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
