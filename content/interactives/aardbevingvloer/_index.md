---
title: "aardbevingvloer"
date: 2019-07-24T13:47:25Z
draft: true
status:
unid: 234f6e12-e620-4de1-bd90-904214e4afce
componenten:
tentoonstellingen:
experiences:
- japanstheater
attachments:
- title:
  src:
- title:
  url:
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

De aardbevingvloer heeft meerdere onderdelen in de regelkast onder de vloer:
- Kissbox (PoE UDP naar stroopjes)
- Siemens Logos (programeerbare relais, heeft het ‘aardbeving programma’ en geeft aan wat de frequentieregelaar moet doen)
- Frequentieregelaar (geeft frequentie-spanning naar motor, verschillende frequentie is verschillende snelheid).


Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
