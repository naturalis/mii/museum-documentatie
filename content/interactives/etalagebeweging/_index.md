---
title: etalagebeweging
date: 2019-12-05T13:47:25+01:00
draft: true
status:
unid: b2842727-1bab-413c-9a8c-0a76b883fb2e
componenten:
tentoonstellingen:
- deverleiding
attachments:
- title: etalagebeweging_bouwtekening.pdf
  src: https://files.museum.naturalis.nl/s/5YZCKKz68kyKXiy
- title: etalagebeweging_technischetekening.pdf
  src: https://files.museum.naturalis.nl/s/nytybnp6zqZLYWe
- title: etalagebeweging_technischetekening (pdf)
  src: https://files.museum.naturalis.nl/s/fit58Y8djFsmNCx
---

![beweging](./beweging.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
