---
title: "oersteen"
date: 2023-02-26T16:26:25Z
draft: true
status:
unid: 
componenten:
- apple-macmini2020
- genelec-8030cp6
- genelec-7050cpm
- presonus-nsb88
tentoonstellingen:
- evolutie
experiences:
attachments:
- title: Oersteen Technisch naslagwerk
  url: https://files.museum.naturalis.nl/s/S4rxRgpCMwwaEDc
- title: evolutie_interactive_oersteen_houtbouw_tekening.pdf
  src: https://files.museum.naturalis.nl/s/62D3QN8z4BNeNJC
- title: evolutie_interactive_oersteen_daansluitingdigitubes_tekening.pdf
  src: https://files.museum.naturalis.nl/s/DMdLkm3tN2zoWb7
- title: evolutie_interactive_oersteen_eaansluitingdigitubes_tekening.pdf
  src: https://files.museum.naturalis.nl/s/8NyEdqzcTx69FJT
- title: evolutie_interactive_oersteen_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/YP25rDR4icqHWLS
- title: evolutie_interactive_oersteen_digitubes_patchlijst.pdf
  src: https://files.museum.naturalis.nl/s/bFpDB7YKdfSbJnB
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

![definitiefontwerp](./oersteen_ontwerp.png "definitief ontwerp")

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

De content komt van de content server en staat in
[content map van evolutie](https://files.museum.naturalis.nl/f/92427).

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

Voor het beheren en bedienen van dit soort interactives. Gebruik
de algemene handleiding
[Technisch museum beheer](https://docs.museum.naturalis.nl/latest/handleidingen/museum-beheer/).

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

- title: evolutie_interactive_oersteen_slawiwo_19_12_2022.pdf
  src: https://files.museum.naturalis.nl/f/96370

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
