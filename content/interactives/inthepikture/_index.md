---
title: "inthepikture"
date: 2019-07-24T13:47:33Z
draft: true
status:
unid: 0c2f5e3e-7c48-4e98-8a35-cd656c27e29b
componenten:
tentoonstellingen:
- deverleiding
attachments:
- title: inthepikture_bouwtekening_vitrine.pdf
  src: https://files.museum.naturalis.nl/s/MbQk4rrznix8Lr3
- title: inthepikture_bouwtekening_vitrinelade.pdf
  src: https://files.museum.naturalis.nl/s/bdi979Di3XpTKxy
- title: inthepikture_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/EyjxTqwEjRd2QyE
---

![inthepikture](./inthepikture.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
