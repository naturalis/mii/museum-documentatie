---
title: "projectieolifant"
date: 2019-07-24T16:03:33Z
draft: true
status:
unid: 02cb0c19-972e-450a-8bd6-b8bd5182ab12
componenten:
- canon-lens
- epson-ebpu2010b
- intel-nuc8i3beh
- kramer-tp590txr
- smartmetals-0022070
- dapaudio-pra82
tentoonstellingen:
- leven
attachments:
- title: projectieolifant_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/kyyKWRoSDXyHxW6
- title: projectieolifant_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/mkHrDocPXZzDnoB
- title: projectieolifant_projectieopstelling.pdf
  src: https://files.museum.naturalis.nl/s/KXNTp78we8jzJnw
- title: projectieolifant_projectieopstelling.vwx
  src: https://files.museum.naturalis.nl/s/ECeWoRGjmZ7QrXK
- title: projectieolifant_projectorophanging.pdf
  src: https://files.museum.naturalis.nl/s/iWqXjpjWj5P9wH8
- title: projectieolifant_projectorophanging.vwx
  src: https://files.museum.naturalis.nl/s/PCBfLwWSqGZ8RKy
---

![olifant](./olifant.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
