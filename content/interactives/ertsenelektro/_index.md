---
title: "ertsenelektro"
date: 2019-07-24T13:47:29Z
draft: true
status:
unid: 2999188a-855a-461a-a397-5b1a7679df7b
componenten:
tentoonstellingen:
- deaarde
attachments:
- title: arduino-erstenelectro (Gitlab)
  src: https://gitlab.com/naturalis/mii/arduino-ertsenelectro
- title: handleidingen ertsenelectro (Google drive)
  src: https://drive.google.com/open?id=15WqDqqjRgBgVZr5yYzBRfNwSSEcY_3tv
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->
[![ontwerp](https://files.museum.naturalis.nl/s/f3FrE6k4MBYoEZ2/preview)](https://files.museum.naturalis.nl/s/f3FrE6k4MBYoEZ2)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

De ertsen electro is een interactive waarbij het publiek gevraagd wordt een
combinatie tussen een metaal en het bijhorende erts te maken.

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

De combinatie wordt gemaakt door zowel een erts als een metaal aan te raken.

Verschillende metalen van een motorfiets zijn van elkaar (elektrisch)
geïsoleerd en via kabels verbonden met de hardware. Bij dit spel gebruiken we 5
verschillende metalen/ertsen:

* ijzer
* koper
* chroom
* lood
* aluminium

De meting van de combinaties gebeurt middels capacitieve koppeling. De
afzonderlijk metalen onderdelen worden om de beurt gedurende 1 milliseconde van
een 12 volt potentiaal voorzien.(puls) Als een persoon een metaal aanraakt zal
zijn/haar lichaam het elektrisch potentiaal van dat metaal volgen. Als deze
persoon ook nog een erts aanraakt dan kunnen de potentiaal sprongetjes met
behulp van een elektrode die aan de achterkant van het erts is aangebracht
worden waargenomen. Het erts geleidt de stroom niet. Dit werkingsprincipe wordt
capacitieve koppeling genoemd.

Zie voor een verdere beschrijving de [arduino-ertsenelectro repository op
Gitlab](https://gitlab.com/naturalis/mii/arduino-ertsenelectro)

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
