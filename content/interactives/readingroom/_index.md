---
title: "readingroom"
draft: false
status:
unid: 6a93f98b-8cbf-43c9-a6b9-0a05f3049818
componenten:
- philips-55bdl4050d
- neutrik-nahdmiw
- vesa-4270
tentoonstellingen:
- livescience
attachments:
- title: Kabelschema (pdf)
  url: https://files.museum.naturalis.nl/s/d5FarF9yHNqPmnk
- title: Kabelschema (vwx)
  url: https://files.museum.naturalis.nl/s/5QnjKw8fnB4LePc
- title: IDsheet Definitief Ontwerp
  url: https://files.museum.naturalis.nl/s/KgzcNkwiSw4K7DA
- title: ReadingRoom aanzicht schema
  url: https://files.museum.naturalis.nl/s/As8GpWnPeBTa6Z4
- title: lifescience_interactives_readingroom_boekenkast_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/zzZaHKRzTBqZA2J
- title: livescience_interactives_readingroom_bankje_wanden_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/iSFa6foPnec3jHW
---

[![Reading Room](https://files.museum.naturalis.nl/s/As8GpWnPeBTa6Z4/preview)](https://files.museum.naturalis.nl/s/As8GpWnPeBTa6Z4)

## Functionele omschrijving

Een open, multifunctionele, media-luwe, intieme ontmoetingsplek waar in alle
rust gesprekken kunnen plaatsvinden tussen bezoekers, medewerkers en relaties
(Naturalisstakeholders). Hier voelt iedereen zich welkom om zelf of samen met
anderen iets te doen (bv. workshops, activiteiten, rondneuzen) of te bespreken
(bv. napraten met de onderzoeker na een ScienceTalk in Studiolab, bijeenkomsten
amateurverenigingen), los van de meer levendige omgeving van de rest van
LiveScience. Werk/leestafel, zitjes, boekenkasten,objecten,
foto’s/tekeningen/schilderijen, tv scherm (in een kast), ontdekkingskisten
e.d.-Presentaties vinden zittend plaats, in een intieme sfeer. Geen
collegezaal-achtige setting.

## Technische omschrijving

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

### Contact

* [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)
* [Manon Laterveer - de Beer](mailto:Manon.Laterveer-debeer@naturalis.nl)

### Ontwerper

* [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)
* [Manon Laterveer - de Beer](mailto:Manon.Laterveer-debeer@naturalis.nl)

### Bouwer

* [Bruns](https://www.bruns.nl)

## Bijlagen

{{< pagelist bijlagen >}}
