---
title: "projectiegetijden"
date: 2019-07-24T16:03:32Z
draft: true
status:
unid: fc41dbb7-8b26-4bc8-ab11-a86deea7064a
componenten:
- canon-lens
- epson-ebpu1007b
- intel-nuc8i3beh
- kramer-tp590txr
- smartmetals-0022070
tentoonstellingen:
- leven
attachments:
- title: projectiegetijden_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/JzpteAP4jf2EWKq
- title: projectiegetijden_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/YH6DJCYYngMnPPr
- title: projectiegetijden_projectieopstelling.pdf
  src: https://files.museum.naturalis.nl/s/fg5WPfNpcDFNYDK
- title: projectiegetijden_projectieopstelling.vwx
  src: https://files.museum.naturalis.nl/s/zs4qYPNNwFq9aP8
- title: projectiegetijden_projectorophanging.pdf
  src: https://files.museum.naturalis.nl/s/WiiJTWmSLTWGDrs
- title: projectiegetijden_projectorophanging.vwx
  src: https://files.museum.naturalis.nl/s/peZgAwpLx4EaGTK
---

![getijden](./getijden.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
