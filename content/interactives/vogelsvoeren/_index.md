---
title: "vogelsvoeren"
date: 2019-07-24T16:03:37Z
draft: true
status:
unid: 8cdf5cdb-4a30-4a1d-bfd0-f52d40ccc889
componenten:
- intel-nuc8i3beh
- lepai-2020plus
- meanwell-gsm60b12p1j
- visaton-dome
- visaton-frs8
tentoonstellingen:
- deverleiding
attachments:
- title: vogelsvoeren_bouwtekening_meubel.pdf
  src: https://files.museum.naturalis.nl/s/2BWra7FJTijQCBi
- title: vogelsvoeren_bouwtekening_vogelnest.pdf
  src: https://files.museum.naturalis.nl/s/39Esq2f985N5H5R
- title: vogelsvoeren_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/NxsSLDCqgmD7PBo
- title: vogelsvoeren_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/GKdYe8Qc5s6egbb
- title: vogelsvoeren_technischetekening.pdf
  src: https://files.museum.naturalis.nl/s/xJEsMApWYSJHTTC
---

![vogelsvoeren](./vogelsvoeren.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
