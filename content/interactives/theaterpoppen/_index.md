---
title: "theaterpoppen"
date: 2019-07-24T16:03:35Z
draft: true
status:
unid: d578dfff-a01d-40cb-8f46-2c916442dd65
componenten:
tentoonstellingen:
experiences:
- japanstheater
attachments:
- title: Manual poppentheater (directory)
  src: https://files.museum.naturalis.nl/s/27fFdZ5MjxHrWP5
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

De montoren worden aangestuurd door RS485 (serieel), soort RS232 maar dan gebalanceerd voor langere kabels.
Pharos heeft een scriptje gemaakt die DMX omzet in die seriele RS485 taal, dus wij sturen de motoren aan via DMX, Pharos vertaalt dat met dat scriptje naar serieel en dat laat de motoren bewegen. Het scriptje is te vinden in de Showfile.

Je kan dus niet snel waardes wijzigen (zoals dimmen) want dat zijn teveel stapjes, in de show behoor je gewoon 1 waarde op te geven (bijvoorbeeld 1000) en dan loopt de motor naar positie 1000.

4 Regelaartjes gekregen voor de motoren van de poppen (die liggen nu in de grot):

Vervangen doe je zo:
er zit een pot-metertje op daar moet je de spanning van instellen.
meet eerst een regelaatje voor dezelfde soort motor: multimeter op spanning zetten gelijkstroom: rood op de potmeter, en de andere op de massa. (zwarte is de massa (de min))

de spanning die je meet moet je maal twee doen (dus als je 0,8 V meet betekend dat dat je 1,6 V naar de motor stuurt).
Let op! Er zijn verschillende modellen met verschillende spanningen die ze moeten krijgen. Google op het motor nummer voor meer details.


## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
