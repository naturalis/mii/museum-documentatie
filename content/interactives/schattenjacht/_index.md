---
title: "schattenjacht"
date: 2019-07-25T08:24:16Z
status:
unid: c73befbd-b0c2-43ac-8fd4-4b87e8fa15b8
componenten:
 - naturalis-helmetbluetoothhub
 - naturalis-rs422-bridge
 - esp32-poe-iso
tentoonstellingen:
 - deaarde
attachments:
- title: Schatkamer plattegrond
  src: https://files.museum.naturalis.nl/s/BnakPfQzDPjBPMD
- title: Schattenjacht ontwerp
  src: https://files.museum.naturalis.nl/s/LdoxkfHAA3xgjA9
- title: Kopie Schatkamer Schattenjacht (oud document)
  src: https://files.museum.naturalis.nl/s/oYTaWiAcoWaPzyZ
- title: Helmet exporter
  url: https://gitlab.com/naturalis/mii/helmet_exporter
- title: Bluetooth Hub gitlab repository
  url: https://gitlab.com/naturalis/mii/helmet-bluetooth-hub
- title: Arduino schattenjacht infrarood gitlab repository
  url: https://gitlab.com/naturalis/mii/arduino-schattenjacht-ir
- title: deaarde_interactives_schattenjacht_audio_documentatie (docx)
  src: https://files.museum.naturalis.nl/s/8AdW4BL692NtrGK
- title: deaarde_interactive_schattenjacht_reparatieverslag-helmen_2020_documentatie (docx)
  src: https://files.museum.naturalis.nl/s/fNKQw4HpbA2Xkew
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

De schattenjacht is een spel dat in de schatkamer van de Aarde zaal wordt
gespeeld. Met speciaal geprepareerde helmen wordt de bezoeker uitgedaagd om 14
“schatten” te zoeken.

In de vitrine in schatkamer van de Aardezaal  zijn 14 objecten voorzien van een
ir-led die een code uitzendt.  De schattenjacht-helmen kunnen door middel van
de Infrarood-signalen deze objecten vinden

> Specifiek gericht op kinderen Pak een helmpje bij de
> entree van de schatkamer en zet hem op. Kruip, sluip, loop
> door de schatkamer, en kijk voorzichtig en goed rond. Er
> liggen 10 bijzondere schatten uit de grond in de
> schatkamer, maar we zeggen niet waar.  Het lampje op de
> helm is nodig om de stenen te verlichten op het moment dat
> je ze vindt. Sommige zullen zelfs oplichten!  Het helmpje
> houdt bij of je alle schatten al gevonden hebt!

7 spelers, 6 jaar of ouder. 15 helmpjes in gebruik

## Technische omschrijving

Dit spel bestaat uit helmpjes, met daarin een
esp32/bluetooth component die communiceert met en centrale
esp32 hub en de showcontroller. Naar hub wordt de status van
ieder helmpje gestuurd met data over temperatuur van de
onderdelen, maar ook de laadstatus van iedere batterij.

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

De mineralen met infrarood licht zijn:

 * Toermalijn 1
 * Prehniet 2
 * Bariet 3
 * Fluoriet 4
 * Zwavel 5
 * Goud 6
 * Willemiet 7
 * Apatiet 8
 * Sodaliet 9
 * Bergkristal 10
 * Pyriet 11
 * Aragoniet 12
 * Calciet 13
 * Okeniet 14

Zie [de code](https://gitlab.com/naturalis/mii/arduino-schattenjacht-ir/blob/master/src/main.cpp#L28) voor een actuele lijst.

De 14 infrarood codes worden gegenereerd door de IR transmitter. De software
van dit apparaat wordt hier beschreven.

De software loopt op een Arduino Nano. Deze is uitgerust met een Atmel
ATmega328  processor. Naar huidige (2019) maatstaven is dit een bescheiden
systeem maar het is krachtig genoeg om de software van de IR transmitter uit te
voeren.

## Configuratie

De helmpjes functioneren autonoom en worden via de source
code ingesteld. De helmpjes
[registreren infrarood lichtsignalen](https://gitlab.com/naturalis/mii/arduino-schattenjacht-ir)
van bepaalde stenen in de exhibit en sturen via bluetooth json
records met status informatie naar de hub en de showcontroller.
De hub communiceert met een server waar de json records via een zogeheten
node-exporter worden aangeboden aan Prometheus.

De configuratie van deze centrale server gebeurt in de code van de hub
[ExporterHost en ExporterPort](https://gitlab.com/naturalis/mii/helmet-bluetooth-hub/blob/master/src/esp32-bluetooth-bridge.ino#L56).

Op dit moment draait de
[exporter](https://gitlab.com/naturalis/mii/helmet_exporter)
op de [housekeeping](https://gitlab.com/naturalis/mii/housekeeping) server.

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

Volume van de stem in het helmpje is soms wat zacht. Dit kan per helmpje met een pod metertje worden ingesteld.

Infrarood lichtjes worden soms slecht gezien door het helmpje. Heeft te maken met
hoek van inval van het licht. Bij kleine kinderen werkt het vaak beter.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

De hardware en software van de helmpjes en de hub zijn
oorspronkelijk gebouwd door Frank Vermeij. De software is
terug te vinden op Gitlab en wordt beheert door het
technisch team van het museum (SD en Infra).

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

Frank Verweij

### Ontwerper

Frank Verweij

## Bijlagen

{{< pagelist bijlagen >}}
