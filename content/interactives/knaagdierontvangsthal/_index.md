---
title: "knaagdierontvangsthal"
date: 2019-07-24T16:03:28Z
draft: true
status:
unid: c3090768-70cf-4d7e-a975-de578e5fe0e0
componenten:
- intel-nuc8i3beh
- lepai-2020plus
- iiyama-tf1015mcb1
- visaton-frs8
tentoonstellingen:
experiences:
- rexperience
attachments:
- title: rexperience_vertrekhal_knaagdiercircuitboard_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/MFBfbGzTDSesD3L
---

![knaagdier](./knaagdier.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
