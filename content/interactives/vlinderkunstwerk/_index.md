---
title: "vlinderkunstwerk"
draft: true
status:
componenten:
tentoonstellingen:
- livescience
attachments:
- title: 241126_Origin of Imagination by Dominic Harris - O&M
  src: https://files.museum.naturalis.nl/f/117490
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

![front view](./front%20view.jpg)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->
![Kabelschema](https://files.museum.naturalis.nl/f/117492)

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

Het vlinderkunstwerk kan handmatig aan- of uit geschakeld worden door middel van de Pharos TPC (Touch Panel Controller) op het regiemeubel van het Studiolab. Met de knop "Log In Admin" en de code 2 4 6 8, verschijnt het Systeem-scherm. De knop "Vlinderscherm" geeft toegang tot de knoppen Aan, Standby en Uit. Deze knoppen geven de volgende commando's aan de remote access computer (dhs-ori-04):
- Aan = DHS-STARTUP
- Standby = DHS-PAUSE (pressed) of DHS-CONTINUE (released)
- Uit = DHS-SHUTDOWN

Een uitgebreide uitleg van de commando's staat beschreven op pagina 33 van de [OPERATION & MAINTENANCE MANUAL](https://files.museum.naturalis.nl/f/117490)

### TROUBLESHOOTING: HOW TO TURN THE ARTWORK ON

The artwork can be turned on with the command DHS-STARTUP, which is triggered in the Pharos controller at the lighting desk of the LiveScience area.

This command executes in order:
- WakeOnLAN to Image Processing Server (4 minutes)
- WakeOnLAN to Render Server (2 Minutes)
- Restart the scanner computers (2 Minutes)
- Turn on Buttons and scanner screens (instantly)
- Turn on VideoWall (Instantly)

### TROUBLESHOOTING: THE ARTWORK DOESN’T TURN ON

First, check if the videowall is actually off or is presenting the dark grey background, setup in the computer.

If it’s dark grey (visible and will produce noticeable banding when looking at it), it may be caused by the render application that either has crashed or has not started. Execute a Shutdown command, wait 5 minutes, and execute the Startup command again. If it doesn’t fix the issue, contact Dominic Harris Studio for further support.

When it is off, it is completely black, and it will require the startup command.

Check in the rack room:
- Check if the remote access computer (dhs-ori-04) is turned on. If the computer is off for any reason, the commands will not work. Turn on the computer, wait for two minutes, and send the Startup command again.
- Check if the Image Processing server (dhs-ori-01) is turned on. If it’s not on, try to turn it on manually. If the light in the button doesn’t turn on, report it to Dominic Harris Studio to proceed with a server replacement.
- Check if the Render Server server (dhs-ori-02) is turned on. If it’s not on, try to turn it on manually. If the light in the button doesn’t turn on, report it to Dominic Harris Studio to proceed with a server replacement.
- Check the Colorlight Z3 controller and see if the Blackout button is active. Try pressing the button and check if the videowall turns on.

If none of these steps resolve the issue, please contact Dominic Harris Studio for further support.

### TROUBLESHOOTING: WHAT TO DO IN CASE OF POWER FAILURE

All the small form factor computers are setup to startup on power, while the servers starts via Wake on LAN.

In case of a power failure in the rack room:
- Turn on the Colorlight Z3 controller by pressing the power button, wait for a minute until the screen shows fully operational.
- Check if the remote access computer (dhs-ori-04) is turned on. If the computer is off for any reason, the commands will not work. Turn on the computer, wait for two minutes, and send the Startup command again.

In case of a power failure in the video area:
- Send a shutdown command and wait for 5 minutes until it ends.
- Issue the Startup command and wait 8 minutes to finish the startup process
- The scanners don’t start automatically after a power failure, so once the scanning computers finish to boot, they need to be turned on, manually, by pressing the Start Scan button in the scanner base.

In case of further problems, please contact Dominic Harris Studio for further support.

### TROUBLESHOOTING: ONE SIDE OF THE SCREEN DISPLAYS VIDEO BUT THE OTHER SHOWS BLACK

This can happen on the rare occasion that the video signal doesn’t synchronize properly.
Sending a Shutdown, waiting 5 minutes, and sending a Startup command should resolve the issue.

In case of further problems, please contact Dominic Harris Studio for further support.

### TROUBLESHOOTING: THE SYSTEM IS WORKING, BUT ONE SCANNER CONTINUES TO FAIL

Check the colour of the LED light at the scanner base.
If there is no light (it should be always blue):
- Close any error message on the computer screen
- Press the start button on the scanner base
- You will see a quick calibration of the scanner head, and the LED
should be turned blue and will be ready to scan.
If the light is blinking in amber, it means that the scanner needs to
recalibrate. To do this:
- Close any error message on the computer screen
- Press the stop button on the scanner base, for three seconds
- Press the start button on the scanner base
- You will see a quick calibration of the scanner head, and the LED
should be turned blue and will be ready to scan.
In case of further problems, please contact Dominic Harris Studio for further
support.

### TROUBLESHOOTING: TOUCH INTERACTION IS NOT WORKING PROPERLY

Check that no objects are physically interrupting the scanner laser area.
Sending a Shutdown, waiting 5 minutes, and sending a Startup command
should fix it.
In case of further problems, please contact Dominic Harris Studio for further
support.


## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->
Dominic Harris: +44 20 8969 3960
Studio Dominic Harris: +44 20 8969 3960
Gorka Cortázar:	+44 7340 386165

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
