---
title: "diergroepen"
date: 2023-02-26T16:26:25Z
draft: true
status:
unid: 
componenten:
tentoonstellingen:
- evolutie
attachments:
- title: evolutie_interactive_diergroepen_meubel_bouwtekening.pdf
  src: https://files.museum.naturalis.nl/s/sNn6e4dMr95t825
- title: evolutie_interactive_diergroepen_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/swyjsP5jne9Hgrq
- title: evolutie_interactive_diergroepen_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/msdaSNfq2x995EX
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

![definitiefontwerp](./diergroepen_ontwerp.png "definitief ontwerp")

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

Voor het beheren en bedienen van dit soort interactives. Gebruik
de algemene handleiding
[Technisch museum beheer](https://docs.museum.naturalis.nl/latest/handleidingen/museum-beheer/).

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
