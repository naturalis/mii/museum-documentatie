---
title: "projectiegiraf"
date: 2019-07-24T16:03:32Z
draft: true
status:
unid: 6b0c6361-247a-4235-b65a-ac84c04a4132
componenten:
- canon-lens
- epson-ebpu1007b
- intel-nuc8i3beh
- kramer-tp590txr
- smartmetals-0022070
tentoonstellingen:
- leven
attachments:
- title: projectiegiraf_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/6HAgKXMyJcxsfBs
- title: projectiegiraf_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/ARKjedrcxYKHMxH
- title: projectiegiraf_projectieopstelling.pdf
  src: https://files.museum.naturalis.nl/s/rkawEyCT6dajRrH
- title: projectiegiraf_projectieopstelling.vwx
  src: https://files.museum.naturalis.nl/s/24AsHCXaZZ58AsP
- title: projectiegiraf_projectorophanging.pdf
  src: https://files.museum.naturalis.nl/s/XN72gDWrCCgxNNM
- title: projectiegiraf_projectorophanging.vwx
  src: https://files.museum.naturalis.nl/s/5snxL4mCo4NrGfF
---

![giraf](./giraf.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
