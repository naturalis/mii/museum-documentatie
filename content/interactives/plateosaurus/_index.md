---
title: "plateosaurus"
date: 2019-07-24T16:03:31Z
draft: true
status:
unid: bb7f38f3-4dd5-4783-9d79-df7428685385
componenten:
- canon-xeedwux6600z
- dapaudio-pr82
- ecler-eca120
- intel-nuc8i3beh
- smartmetals-0022070
- kramer-tp590txr
tentoonstellingen:
- dinotijd
attachments:
- title: plateosaurus_bouwtekening.pdf
  src: https://files.museum.naturalis.nl/s/pZbdH9oNQYnXYYA
- title: plateosaurus_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/mJ4d8siZPYyKyNZ
- title: plateosaurus_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/fHd4HHrD6gdGTNC
- title: plateosaurus_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/Ns2Y568C5KqBo2J
- title: plateosaurus_projectieopstelling.pdf
  src: https://files.museum.naturalis.nl/s/ykNQoQkMDMFyoa8
- title: plateosaurus_projectieopstelling.vwx
  src: https://files.museum.naturalis.nl/s/SYFQa6GQ6sHwRfB
- title: plateosaurus_projectorophanging.pdf
  src: https://files.museum.naturalis.nl/s/oBabCdZmds2X5ky
- title: plateosaurus_projectorophanging.vwx
  src: https://files.museum.naturalis.nl/s/DTMGtkGpiLip8BH
---

![plateosaurus](./plateosaurus.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
