---
title: "kijkersfriesland"
date: 2019-07-24T16:03:28Z
draft: true
status:
unid: 9055f8d8-ec4f-429f-8292-834d0a160c26
componenten:
- intel-nuc8i7beh
- visaton-amp22ln
- dewijs-1804d55x
- screenspacelab-ijstijdkijkers
tentoonstellingen:
- deijstijd
attachments:
- title: kijkersfriesland_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/b7PQyrwn9wBdzzK
- title: kijkersfriesland_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/SmCzSZa7diTG8j3
- title: deijstijd_ijstijdkijkers_bouwtekening.pdf
  src: https://files.museum.naturalis.nl/s/TcYXg6zcnHjctAg
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

In de group_var ijstijdkijkers.yml hebben we de aanpassing gemaakt: arduino: no
Niet vergeten deze weer terug te zetten wanneer de ijstijdkijkers weer teruggeplaatst worden.


## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

- AV hardware en software [ScreenSpace Lab](mailto:marijn@refocusvfx.com)
- De Wijs kijker [de Wijs apparatenbouw](mailto:info@dewijs-3d.com)
- Animaties en audio ijstijdkijkers [Redrum](http://www.redrumbureau.com)

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
