---
title: "trexanimatronic"
date: 2019-07-24T16:03:36Z
draft: false
status:
unid: bb11ceec-33e9-4114-9359-dd7a92594409
componenten:
- meyersound-mm4xp
- jbl-control19cs
tentoonstellingen:
experiences:
- rexperience
attachments:
- title: rexperience_e_installatie_netwerk_dmx_audio_animatronics_aansluitschema (pdf)
  src: https://files.museum.naturalis.nl/s/2EoriKQSPmEYXLr
- title: rexperience_e_installatie_netwerk_dmx_audio_animatronics_elektrischetekeningen (pdf)
  url: https://files.museum.naturalis.nl/s/gw9PCe9HpoctEFA
- title: rexperience_technischeinstallaties_animatronics_trex_rail_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/RCxDwKPCxEFbqwy
- title: rexperience_handleidingen_animatronics_bediening (pdf)
  url: https://files.museum.naturalis.nl/s/PLoW8FM9ceHAXBG
- title: rexperience_handleidingen_animatronics_instructiesvoorveiligheid_veiligheidssystemen (pdf)
  url: https://files.museum.naturalis.nl/s/2RmZxmFKPNTHJ8D
- title: rexperience_handleidingen_animatronics_schoonmakenenonderhoud (pdf)
  url: https://files.museum.naturalis.nl/s/fNx83548eDmx9j3

---

![rex](./rex.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

* [rexperience_handleidingen_animatronics_bediening](https://files.museum.naturalis.nl/s/PLoW8FM9ceHAXBG)
* [rexperience_handleidingen_animatronics_instructiesvoorveiligheid_veiligheidssystemen](https://files.museum.naturalis.nl/s/2RmZxmFKPNTHJ8D)
* [rexperience_handleidingen_animatronics_schoonmakenenonderhoud](https://files.museum.naturalis.nl/s/fNx83548eDmx9j3)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
