---
title: "spermarace"
date: 2019-07-24T16:03:34Z
draft: true
status:
unid: daf84c08-4263-4510-8019-d7a7a41f1de4
componenten:
- philips-55bdl4050d
- arduino-leonardo
- ecler-eca120
- intel-nuc8i7beh
- qsc-adc4tbk
- vesa-4270
- advancedphotonix-nsl19m51
tentoonstellingen:
- deverleiding
attachments:
- title: spermarace_bouwtekening.pdf
  src: https://files.museum.naturalis.nl/s/GGZ3JraN5w5jkQ8
- title: spermarace_elektroschema.pdf
  src: https://files.museum.naturalis.nl/s/j8P9adT9TMACiea
- title: spermarace_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/YJdgRRLsyGtDF3b
- title: spermarace_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/a7tRikq6mMGNARz
- title: spermarace_technischetekening.pdf
  src: https://files.museum.naturalis.nl/s/RbjpD6PPiqrKe7n
- title: verleiding_interactives_spermarace_specs_sensors (docx)
  src: https://files.museum.naturalis.nl/s/9WNKCjJxyW74iPG
- title: verleiding_interactives_spermarace_reparatieverslag_mrt2020_sensoren (docx)
  src: https://files.museum.naturalis.nl/s/rEj6bkH3XZifDxi
---

![spermarace](./spermarace.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

[Gitlab code](https://gitlab.com/naturalis/mii/arduino-spermarace)

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

Titus 03-06-2021:

Door hardware wijziging, o.a. toevoegen weerstand zat er een delay in de afloop naar nul. Deze loopt nu via een curve. Daarom moest de trash hold value van 0 naar 20. Hij reageert nu als de waarde onder de 20 duikt. Dit kan desgewenst nog veranderd worden naar 30 of 40 als wenselijk is dat hij gevoeliger wordt afgesteld. Maar ik zou niet onder de waarde 10 gaan zitten.

Ik heb twee regels code toegevoegd voor calibratie. Gebruik:

In het ino bestand op de nu de "//" voor de twee regels in de loop verwijderen.
Commando pio run --target upload uitvoeren.
En vervolgens pio device monitor
Nu zie je realtime de waarde die binnenkomen, mochten deze te snel gaan dan kan je de delay verhogen (deze wel naderhand terugzetten).

Aan de hand van de waargenomen waarde kun je vast stellen wat de gewenste trashhold waarde is. Het is aanbevolen die na hardware wijzigingen altijd even te checken en empirisch vast te stellen.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
