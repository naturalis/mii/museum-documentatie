---
title: "dinovoetstappen"
date: 2019-07-24T13:47:29Z
draft: true
status:
unid: 99cb3430-224b-4cc6-8603-6f2f1c93f4ab
componenten:
- crestron-audext100
- dapaudio-pra82
- esi-gigaporthdplus
- intel-nuc8i3beh
tentoonstellingen:
- dinotijd
attachments:
- title: dinovoetstappen_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/mrWkSWWdWQkNoGa
- title: dinovoetstappen_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/TrWptep9cK6sJom
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
