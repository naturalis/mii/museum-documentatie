---
title: "vroegemensportaal"
draft: false
status:
unid: cc2cc261-dbb0-4ef5-810e-1760d0351956
componenten:
- intel-nuc8i3beh
- philips-bdl3230ql00
- vesa-4270
tentoonstellingen:
- devroegemens
attachments:
- title: asbuilt (pdf)
  src: https://files.museum.naturalis.nl/s/iqLFKfEWSdxBZsq
- title: schoonmaakvoorschrift (pdf)
  src: https://files.museum.naturalis.nl/s/ApknczEP22CpLAd
- title: kabelschema (pdf)
  url: https://files.museum.naturalis.nl/s/5amNzESLYpe6nRz
- title: kabelschema (vwx)
  src: https://files.museum.naturalis.nl/s/ToprAPwTAHDgXe7
- title: definitiefontwerp (pdf)
  url: https://files.museum.naturalis.nl/s/SdAM43kpS8BT2jX
---

![Screenshot](https://files.museum.naturalis.nl/s/ocjGcx9E8AjzHnT/preview)
<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

Compacte entreezone van de zaal, laat de bezoeker zien hoe lang het nog
duurt voordat de show weer begint.

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

Dit deel van de zaal bestaat uit een beeldscherm met daaraan een Nuc die
een filmpje laat zien met een terugtellende klok.

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

[Schoonmaakvoorschrift](https://files.museum.naturalis.nl/s/ApknczEP22CpLAd)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

* [Matthijs Vegter](mailto:matthijs.vegter@naturalis.nl)
* [Technisch Team](mailto:support@naturalis.nl)

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

* [Hypsos](https://hypsos.com/soesterberg/)

### Ontwerper

* Ontwerp: [Designwolf](http://designwolf.nl/contact/)
* Film: [Redrum](https://redrumbureau.com)

## Bijlagen

{{< pagelist bijlagen >}}
