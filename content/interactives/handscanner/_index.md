---
title: "handscanner"
date: 2019-07-24T13:47:32Z
draft: true
status:
unid: 1c439e55-ccdf-4af7-a798-ce705180debf
componenten:
- arduino-leonardo
- dapaudio-pr32
- intel-nuc8i3beh
- lepai-2020plus
- meanwell-gsm60b12p1j
- omnimount-100wc
- optoma-ml750st
tentoonstellingen:
- dedood
attachments:
- title: handscanner_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/JknsYrQmW4LdaFE
- title: handscanner_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/zqk4qprHWe5ZF8k
- title: handscanner_projectieopstelling.pdf
  src: https://files.museum.naturalis.nl/s/N9QTN4j9PXi6oCM
- title: handscanner_projectieopstelling.vwx
  src: https://files.museum.naturalis.nl/s/3Wbcwdwrem4eQD4
- title: handscanner_projectorophanging.pdf
  src: https://files.museum.naturalis.nl/s/A5piJcPp5do7Ef6
- title: handscanner_technischetekening.pdf
  src: https://files.museum.naturalis.nl/s/P6y3FTiLAQ7F2xY
- title: dood_interactive_handscanner_documentatie (docx)
  src: https://files.museum.naturalis.nl/s/XR8wZ9SWiw5xr3S
- title: dood_interactive_handscanner_qt113_sensor _documentatie (pdf)
  src: https://files.museum.naturalis.nl/s/3trnoHsSX2ssHmL
---

![handscanner](./handscanner.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

Handscanner-cmp-1 hangt redelijk vaak. Voordat dit gerepareerd is heeft Robert
een knop in de SER aan de serverkast geplakt waarmee je de stroom kan uit en aan
zetten. Nu hoef je niet de wand-deur niet elke keer op te tillen.

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

* Animatie: [Shosho](http://www.shosho.nl/)

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
