---
title: "filmdubois"
draft: false
status:
unid: eebdab22-4a35-41f0-ad9b-69a211b67fdd
componenten:
- smartmetals-0021065
- crestron-audext100
- canon-lens
- dapaudio-drx10ba
- dapaudio-pra82
- intel-nuc8i3beh
- epson-ebl635su
tentoonstellingen:
- devroegemens
attachments:
- title: definitiefontwerp tribune (pdf)
  src: https://files.museum.naturalis.nl/s/iKPWYtgTezBHRPy
- title: kabelschema av (pdf)
  url: https://files.museum.naturalis.nl/s/a9ZpaWXPEgkPcCF
- title: kabelschema av (vwx)
  src: https://files.museum.naturalis.nl/s/HFkjXDfDb5aQ6tC
- title: onderhoudsvoorschrift tribune
  url: https://files.museum.naturalis.nl/s/wQWQWCksTXWxsDF
- title: projectieopstelling (pdf)
  src: https://files.museum.naturalis.nl/s/D9iNRo5aRGEyWNk
- title: projectieopstelling (vwx)
  url: https://files.museum.naturalis.nl/s/rSKPtmyAB7mxmcC
- title: projectorophanging (pdf)
  src: https://files.museum.naturalis.nl/s/YQFw74YWD9EAgLP
- title: projectorophanging (vwx)
  url: https://files.museum.naturalis.nl/s/qtSL836ZAZDgtLo
---

![screenshot](https://files.museum.naturalis.nl/s/PjDCw6fzpsXCANR/preview)

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

Een AV presentatie van ca. vijf minuten over Dubois en zijn zoektocht naar 'the missing link'.

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

[Onderhoudsvoorschrift tribune](https://files.museum.naturalis.nl/s/wQWQWCksTXWxsDF)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

- Projectleider: [Matthijs Vegter](mailto:matthijs.vegter@naturalis.nl)
- Techniek: [Technisch Team](mailto:support@naturalis.nl)

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

[Hypsos](https://hypsos.com/soesterberg/)

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

- Ontwerp: [Designwolf](http://designwolf.nl/contact/)
- Film: [Redrum](https://redrumbureau.com)


## Bijlagen

{{< pagelist bijlagen >}}
