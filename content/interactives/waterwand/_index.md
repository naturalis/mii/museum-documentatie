---
title: "waterwand"
date: 2019-07-24T16:03:37Z
draft: true
status:
unid: f9c17eb0-a525-4632-a064-107118acb756
componenten:
- christie-14013310801
- christie-dwu630gs
- intel-nuc8i3beh
- smartmetals-0022070
- kramer-tp590txr
tentoonstellingen:
- dinotijd
attachments:
- title: waterwand_bouwtekening_meubel.pdf
  src: https://files.museum.naturalis.nl/s/jiq578f3R9naLtC
- title: waterwand_bouwtekening_muur.pdf
  src: https://files.museum.naturalis.nl/s/PEdpaHaZR3E9Zeq
- title: waterwand_bouwtekening_vitrine.pdf
  src: https://files.museum.naturalis.nl/s/9McbkNKLJgpn7CZ
- title: waterwand_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/9zAQp2qqPEgjYrc
- title: waterwand_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/XknLPEMbTMN2f54
- title: waterwand_projectieopstelling.pdf
  src: https://files.museum.naturalis.nl/s/QzrAcMAS9aWYdyi
- title: waterwand_projectieopstelling.vwx
  src: https://files.museum.naturalis.nl/s/oCaiP7oQ3qiQdFT
- title: waterwand_projectorophanging.pdf
  src: https://files.museum.naturalis.nl/s/GgbL6sBFKc9r6cP
- title: waterwand_projectorophanging.vwx
  src: https://files.museum.naturalis.nl/s/FxxDjggyRo2QrdA
- title: detailtekeningen (directory)
  src: https://files.museum.naturalis.nl/s/2p27k4XRDi3ZxBK
- title: dino_exhibit_waterwand_totaal (pdf)
  src: https://files.museum.naturalis.nl/s/KJ5PHic55J7zZrF
- title: dino_exhibits_waterwand_overzicht (pdf)
  src: https://files.museum.naturalis.nl/s/ediLn2AsEyXdEnc
- title: dino_decor_waterwand_ichtyosaurussen_staalconstructie_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/RMKwH5RyxGbRya2
- title: dino_exhibit_waterwand_a_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/RRKjd4JYagboWde
- title: dino_exhibit_waterwand_b_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/dMEHSyZeG757bT4
- title: dino_exhibit_waterwand_bevestigingaanwand_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/eFLeGBgoYBQHLnf
- title: dino_exhibit_waterwand_c_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/m8rfq5r9iLfDCXz
- title: dino_exhibit_waterwand_e_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/9c3KyT3RT5oyKcz
- title: dino_exhibit_waterwand_e_constructieveberekening_staalconstructie_ichtyosaurus (pdf)
  src: https://files.museum.naturalis.nl/s/cMRfgCHmEXnMdjm
- title: dino_exhibit_waterwand_hoekstuk_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/4Wcj7gR9CA543w3
- title: dino_exhibit_vitrines_afmetingen (pdf)
  src: https://files.museum.naturalis.nl/s/eMtX3bnsNYTkXSq
---

![waterwand](./waterwand.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
