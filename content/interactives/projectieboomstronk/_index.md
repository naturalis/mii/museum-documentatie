---
title: "projectieboomstronk"
date: 2023-02-26T16:26:25Z
draft: true
status:
unid: 
componenten:
- vivitek-du4771z
- intel-nuc8i3beh
- digitus-ds55508
tentoonstellingen:
- evolutie
experiences:
attachments:
- title: evolutie_Interactive_projectieboomstronk_glas.pdf.pdf
  src: https://files.museum.naturalis.nl/s/F3me5YRByj4Y3FK
- title: evolutie_interactive_projectieboomstronk_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/RY5kkm3BecTJ3xi
- title: evolutie_interactive_projectieboomstronk_ontwerptekening.pdf
  src: https://files.museum.naturalis.nl/s/T356zQnCMJd7e6m
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

![definitiefontwerp](./projectieboomstronk_ontwerp.png "definitief ontwerp")

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

De configuratie van de Intel Nuc wordt verzorgd met behulp van ansible
configuratiemanagement. De configuratie is terug te vinden in de
[ansible configuratie van het museum](https://gitlab.com/naturalis/mii/museum/ansible-museum).

De applicatie werkt precies zoals andere interactive installaties en maakt
gebruik van de
[interactive playbook](https://gitlab.com/naturalis/mii/ansible-interactive).
De content komt van de content server en staat in
[content map van evolutie](https://files.museum.naturalis.nl/f/92434).

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

Voor het beheren en bedienen van dit soort interactives. Gebruik
de algemene handleiding
[Technisch museum beheer](https://docs.museum.naturalis.nl/latest/handleidingen/museum-beheer/).

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
