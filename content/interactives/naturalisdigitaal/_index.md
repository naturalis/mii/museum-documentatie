---
title: "naturalisdigitaal"
draft: true
status:
unid: 32e1205d-dfe8-41fb-a6a1-a4626f168cca
componenten:
- ecler-eca120
- intel-nuc8i7beh
- kaiser-541
- iiyama-tf5538uhscb1ag
- qsc-acs4tbk
tentoonstellingen:
- livescience
attachments:
- title: Kabelschema (pdf)
  url: https://files.museum.naturalis.nl/s/L5HYQMWzM7fBgBt
- title: Kabelschema (vwx)
  url: https://files.museum.naturalis.nl/s/LSg6J7KDSdcpNkP
- title: definitiefontwerp (pdf)
  url: https://files.museum.naturalis.nl/s/RHbQoJHAgBZjbf3
- title: livescience_interactives_naturalisdigitaal_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/HiSj4Ty3j864kCn
---

![Screenshot](screenshot.jpg)

## Functionele omschrijving

<<<<<<< HEAD
Naturalis Digitaal is een groot touchscreen in LiveScience waarop een algemene introductie te zien
is van de werkzaamheden binnen het instituut Naturalis. Aan de hand van een korte film wordt de
verbinding gelegd tussen het museum en de werkzaamheden en doelstellingen van de rest van het instituut.
De bezoeker kan deze interactive bedienen via een touchscreen.
=======
Naturalis Digitaal is een groot touchscreen in LiveScience waarop een algemene
introductie te zien is van de werkzaamheden binnen het instituut Naturalis. Aan
de hand van een korte film wordt de verbinding gelegd tussen het museum en de
werkzaamheden en doelstellingen van de rest van het instituut.  De bezoeker kan
deze interactive bedienen via een touchscreen.
>>>>>>> 1e8c700e6067c2b1ffae3b8527d10391e94bd894

## Technische omschrijving

Technisch bestaat deze interactive uit een beeldscherm en een
Intel Nuc waarop een versie van linux draait. Deze machine is
zo ingericht dat daarop een HTML5 applicatie draait waarin
de dynamische presentatie wordt getoond.

{{<mermaid>}}
graph TD;
    A[Bezoeker] --> |bedient| B
    B[Touchscreen] --> |interface| E[Html5 applicatie]
    C[Intel Nuc] --> |toon beeld| B[TouchScreen]
    C[Intel Nuc] --> |draait| D[Linux]
    D[Linux] --> |draait| E[Unity applicatie]
{{</mermaid>}}

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

De computer waarop deze interactive draait is:

 * naturalisdigitaal-cmp-1

## Configuratie


De configuratie van de Intel Nuc wordt verzorgd met behulp van ansible
configuratiemanagement. De configuratie is terug te vinden in de
[inventory van het museum](https://gitlab.com/naturalis/mii/museum/ansible-museum).

De applicatie werkt precies zoals andere interactive installaties en maakt
gebruik van de
[interactive playbook](https://gitlab.com/naturalis/mii/ansible-interactive).
<<<<<<< HEAD
De content komt van de content server en staat in
[content map van livescience](https://files.museum.naturalis.nl/s/DAi58MgdBXb27xc).
=======
De content komt van de content server en staat in
[content map van livescience](https://files.museum.naturalis.nl/s/nmW3gYRnR9EC9JZ).
>>>>>>> 1e8c700e6067c2b1ffae3b8527d10391e94bd894


### Contact

 * [Technisch Team](mailto:support@naturalis.nl)
 * [Joran Peeks van iQMedia](mailto:joran.peeks@iqmedia.nl)
 * [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)


### Bouwer

 * [IQ Media](https://iqmedia.nl)

### Ontwerper

 * [IQ Media](https://iqmedia.nl)

## Tentoonstelling

{{< pagelist tentoonstellingen >}}

## Bijlagen

{{< pagelist bijlagen >}}
