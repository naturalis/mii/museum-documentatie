---
title: "datingquiz"
date: 2019-07-24T13:47:28Z
draft: true
status:
unid: 908ad97b-03a8-4d4c-9052-6ba4eeb1772c
componenten:
- iiyama-tf2234mcb5x
- intel-nuc8i7beh
- lepai-2020plus
- meanwell-gsm60b12p1j
- vistaton-frs8
tentoonstellingen:
- deverleiding
attachments:
- title: datingquiz_bouwtekening.pdf
  src: https://files.museum.naturalis.nl/s/QDNmLQwNqjtmAFK
- title: datingquiz_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/Nj5GfGmjR8gN9N3
- title: datingquiz_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/iMbkwpFbBx2piCY
---

![datingquiz](./datingquiz.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
