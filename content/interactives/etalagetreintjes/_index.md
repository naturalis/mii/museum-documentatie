---
title: etalagetreintjes
date: 2019-12-05T13:51:46+01:00
draft: false
status:
unid: ba12b23b-d2c7-46c1-b4bc-0ba427694e2b
componenten:
- esp32-PoE-ISO
tentoonstellingen:
- deverleiding
attachments:
- title: 190224_VERLEIDING_DO_E_etalgekado.pdf
  url: https://files.museum.naturalis.nl/s/DQcnC82pRy9GEcA
- title: 17326.4.3.4_1 Etalage Cadeaus.pdf
  url: https://files.museum.naturalis.nl/s/gZbLmwNryD9XzCZ
- title: 17326.4.3.4_1.001A Verleiding Samenstelling 20181211.pdf
  url: https://files.museum.naturalis.nl/s/JNTsPHtktmds4oS
- title: verleiding_exhibit_etalagetreintjes_bouwtekening (png)
  src: https://files.museum.naturalis.nl/s/9oaQTrPbxrepWJ6
---

![treintjes](./treintjes.png)

## Functionele omschrijving

Etalagetreintjes is een vitrine in deverleiding. Hierin rijden twee
paar treintjes die gaan rijden wanneer iemand voor de vitrine staat.
Tegelijkertijd wordt door de showcontroller een bepaalde lichtshow
getoond.

## Technische omschrijving

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

De esp32 ontvangt een signaal van de bewegingsensor voor aan
de virtrine. Deze werkt als een druk op de knop. De esp32 stuurt
commandos naar de showcontroller:

- ETALAGEBEWEGING_AAN
- ETALAGEBEWEGING_UIT

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

* Decor: [Bruns](https://bruns.nl)

### Ontwerper

* Decor: [Bruns](https://bruns.nl)
* Inhoud:

## Bijlagen

{{< pagelist bijlagen >}}
