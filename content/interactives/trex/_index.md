---
title: "trex"
date: 2019-07-24T16:03:36Z
draft: true
status:
unid: 36cbae04-9669-47f9-9760-5c3e520fd888
componenten:
- canon-xeedwux6600z
- dapaudio-pr82
- ecler-eca120
- intel-nuc8i3beh
- smartmetals-0022070
- kramer-tp590txr
tentoonstellingen:
- dinotijd
attachments:
- title: trex_bouwtekening.pdf
  src: https://files.museum.naturalis.nl/s/rrAPfTBSMjyp8iQ
- title: trex_definitiefontwerp_a.pdf
  src: https://files.museum.naturalis.nl/s/ziHZWJxc93x89N7
- title: trex_definitiefontwerp_b.pdf
  src: https://files.museum.naturalis.nl/s/fMsbxpbYCPmBk5J
- title: trex_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/GJf5SWK6XfbexYZ
- title: trex_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/M4ZjnfXxtB96r4Z
- title: trex_projectieopstelling.pdf
  src: https://files.museum.naturalis.nl/s/EJX2nY9onioms3q
- title: trex_projectieopstelling.vwx
  src: https://files.museum.naturalis.nl/s/TB3nqgdSzHHtxpf
- title: trex_projectorophanging.pdf
  src: https://files.museum.naturalis.nl/s/WxQWpJtCTzz5Amb
- title: trex_projectorophanging.vwx
  src: https://files.museum.naturalis.nl/s/GgkQBkjwjWqEcD5
---

[trex](./trex.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
