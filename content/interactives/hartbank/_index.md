---
title: "hartbank"
date: 2019-07-24T13:47:32Z
draft: true
status:
unid: 6e08e79e-d4c4-443b-a3bb-f3da9785ac63
componenten:
- arduino-leonardo
- dapaudio-amp104
- stoplisten-gorillaaudiohandset
- intel-nuc8i3beh
- neutrik-np2x
tentoonstellingen:
- deverleiding
attachments:
- title: hartbank_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/Ds2LGEBytQMzHTd
- title: hartbank_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/6B4keNE8NXN2kp4
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
