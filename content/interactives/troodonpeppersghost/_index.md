---
title: "troodonpeppersghost"
date: 2019-07-24T16:03:36Z
draft: true
status:
unid: 3aa50f0a-27e4-439a-95b8-5e8aa9400495
componenten:
- intel-nuc8i3beh
- philips-bdl3230ql00
tentoonstellingen:
- dinotijd
attachments:
- title: troodonpeppersghost_bouwtekening.pdf
  src: https://files.museum.naturalis.nl/s/623jEbHizHCZCHp
- title: troodonpeppersghost_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/G42nmzGYb7NegEc
- title: troodonpeppersghost_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/xMWygpa7aynpAdg
- title: troodonpeppersghost_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/jjscgPnj2cJnfMx
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
