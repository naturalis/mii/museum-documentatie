---
title: "orgel"
date: 2019-07-24T16:03:31Z
draft: true
status:
unid: 87fe2959-e0ca-4044-8c85-90a1d7a66dab
componenten:
- akai-mpx16
- doepfer-ctm64
- ecler-eca120
- kingston-sdcit8gb
- qsc-adc4tbk
- visualproductions-cuecore2
tentoonstellingen:
- deverleiding
attachments:
- title: orgel_bouwtekening_samenstelling.pdf
  src: https://files.museum.naturalis.nl/s/Xx6EM6jJNxnQmyk
- title: orgel_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/MszHj4CBrz2j9Wr
- title: orgel_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/BD4kS2itE6WbGWM
- title: orgel_technischetekening.pdf
  src: https://files.museum.naturalis.nl/s/3bp5LKSEotwYnpE
- title: orgel_bouwtekening_samenstelling.pdf
  src: https://files.museum.naturalis.nl/s/Xx6EM6jJNxnQmyk
---

![orgel](./orgel.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
