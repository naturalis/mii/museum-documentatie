---
title: "projectieluchtdieren"
date: 2019-07-24T16:03:32Z
draft: true
status:
unid: 24f42c96-25fe-43da-894c-c4a6f5e92f24
componenten:
- canon-lens
- epson-ebpu1007b
- intel-nuc8i3beh
- kramer-tp590txr
- smartmetals-0022070
tentoonstellingen:
- leven
attachments:
- title: projectieluchtdieren_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/KB6kP6YZnTpGdfd
- title: projectieluchtdieren_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/JkR6jjBgJQYsLro
- title: projectieluchtdieren_projectieopstelling.pdf
  src: https://files.museum.naturalis.nl/s/as75fN9aLbKdzHF
- title: projectieluchtdieren_projectieopstelling.vwx
  src: https://files.museum.naturalis.nl/s/es6APrZD44YwXPN
- title: projectieluchtdieren_projectorophanging.pdf
  src: https://files.museum.naturalis.nl/s/fGWHyE9LRW5ikZ5
- title: projectieluchtdieren_projectorophanging.vwx
  src: https://files.museum.naturalis.nl/s/8bCSiS38HfKeYPH
---

![luchtdieren](./luchtdieren.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
