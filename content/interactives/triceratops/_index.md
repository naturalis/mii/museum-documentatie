---
title: "triceratops"
date: 2019-07-24T16:03:36Z
draft: true
status:
unid: f1346e60-5b3d-4d0f-8584-76b44af7197f
componenten:
- canon-xeedwux6600z
- dapaudio-pr82
- ecler-eca120
- intel-nuc8i3beh
- smartmetals-0022070
- kramer-tp590txr
tentoonstellingen:
- dinotijd
attachments:
- title: triceratops_bouwtekening.pdf
  src: https://files.museum.naturalis.nl/s/jf6syYqoG75btt7
- title: triceratops_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/aG2Tj3RkrHsKdEB
- title: triceratops_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/o34A7F9QfoxK8aA
- title: triceratops_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/c2w5ZYcQ9AEbALJ
- title: triceratops_projectieopstelling.pdf
  src: https://files.museum.naturalis.nl/s/mn6z3RgEM6H5ABM
- title: triceratops_projectieopstelling.vwx
  src: https://files.museum.naturalis.nl/s/Zx6MfC8XXFao8We
- title: triceratops_projectorophanging.pdf
  src: https://files.museum.naturalis.nl/s/NBemRP9QjSQQD5e
- title: triceratops_projectorophanging.vwx
  src: https://files.museum.naturalis.nl/s/36xP8cNZSYjHYad
---

![triceratops](./triceratops.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
