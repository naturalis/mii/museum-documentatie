---
title: "gameover"
date: 2019-07-24T13:47:31Z
draft: true
status:
unid: f891882b-27f1-465c-a568-25199c8630d2
componenten:
- iiyama-tf5538uhscb1ag
- intel-nuc8i3beh
tentoonstellingen:
- dedood
attachments:
- title: gameover_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/nmzn9TSoHLqHGSQ
- title: gameover_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/2K2WnxJxT5zzjXe
- title: gameover_displayophanging.pdf
  src: https://files.museum.naturalis.nl/s/TrLo5pz6R7aeKKx
- title: gameover_displayophanging.vwx
  src: https://files.museum.naturalis.nl/s/D3XKJWkSLz9G5fc
---

![gameover](./gameover.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
