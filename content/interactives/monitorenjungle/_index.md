---
title: "monitorenjungle"
date: 2019-07-24T16:03:30Z
draft: false
status:
unid: c5d315c3-03a5-4e61-b18d-ad5af27cf724
componenten:
- intel-nuc8i3beh
- iiyama-tf3238mscb1ag
- kramer-tp590r
tentoonstellingen:
experiences:
- rexperience
attachments:
- title: rexperience_jungle_zwevendemonitor_as_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/fnbqBfrAsgXnRDE
- title: rexperience_jungle_zwevendemonitor_as3 (pdf)
  url: https://files.museum.naturalis.nl/s/7tLRDwAqTnHo7zY
- title: rexperience_jungle_zwevendemonitor_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/scC9A3cDCEBN2T3
- title: rexperience_jungle_zwevendemonitor2_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/BkEDb5D3CdE2KyQ
- title: rexperience_jungle__zwevendemonitoren_wandbevestiging_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/xKaKjfSiKJy7QqM
- title: rexperience_jungle_zwevendemonitor_wandbevestiging_10graden_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/Gdw9dTWASkyaPeW
- title: rexperience_jungle_zwevendemonitorombouw_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/rs42kgBt7GMFTZL
- title: rexperience_e_installatie_netwerk_dmx_audio_zwevendemonitors_aansluitschema (pdf)
  src: https://files.museum.naturalis.nl/s/jbRrKXWkxDZQrtn
---

![zwevendemonitor](./zwevendemonitor.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
