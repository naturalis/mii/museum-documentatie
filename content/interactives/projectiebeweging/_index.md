---
title: "projectiebeweging"
date: 2019-07-24T16:03:31Z
draft: true
status:
unid: f67e73ff-5334-4be9-b57b-cc06ed4ac5ec
componenten:
- canon-lens
- epson-ebpu1007b
- intel-nuc8i3beh
- kramer-tp590txr
- smartmetals-0022445
tentoonstellingen:
- leven
attachments:
- title: projectiebeweging_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/Z5JYRFYAeLpTnBM
- title: projectiebeweging_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/bbcbks6M88nCi3G
- title: projectiebeweging_projectieopstelling.pdf
  src: https://files.museum.naturalis.nl/s/XBnwHNya8tR2iNg
- title: projectiebeweging_projectieopstelling.vwx
  src: https://files.museum.naturalis.nl/s/KDYgPpxdAMkqoxz
- title: projectiebeweging_projectorophanging.pdf
  src: https://files.museum.naturalis.nl/s/tMbnE8DGSRjPkG3
- title: projectiebeweging_projectorophanging.vwx
  src: https://files.museum.naturalis.nl/s/6wHaz9aZdZcTjxX
---

![beweging](./beweging.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
