---
title: "projectiekleinedieren"
date: 2019-07-24T16:03:32Z
draft: true
status:
unid: 184d44a7-40f5-4229-ae2c-39dc45a20d9a
componenten:
- canon-lens
- canon-xeedwux6600z
- intel-nuc8i3beh
- kramer-tp590txr
- smartmetals-0022070
tentoonstellingen:
- leven
attachments:
- title: projectiekleinedieren_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/7aHoH59qSHfsCx5
- title: projectiekleinedieren_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/Tmb8exmHckpacKS
- title: projectiekleinedieren_projectieopstelling.pdf
  src: https://files.museum.naturalis.nl/s/42cZMW7Qy5HBqdz
- title: projectiekleinedieren_projectieopstelling.vwx
  src: https://files.museum.naturalis.nl/s/XtPoprG23fqZKxA
- title: projectiekleinedieren_projectorophanging.pdf
  src: https://files.museum.naturalis.nl/s/RXtcHaA2Mocbfia
- title: projectiekleinedieren_projectorophanging.vwx
  src: https://files.museum.naturalis.nl/s/Kcr7nX4dsKz46KQ
---

![kleinedieren](./kleinedieren.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
