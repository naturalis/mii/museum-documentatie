---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
author: Ruud Altenburg
draft: false
---
<!--
Hier moet een korte inleiding komen over de applicatie. En hieronder staan
diverse kopjes om aan te geven welke informatie per applicatie bekend zou
moeten zijn. De auteur mag naar eigen inzicht deze kopjes verwijderen of
aanpassen, het is maar een suggestie.
-->


## Functionele omschrijving

<!-- Geef hier een functionele omschrijving van de applicatie -->

## Technische omschrijving

<!-- Geef hier een functionele omschrijving van de applicatie -->

## Componenten

<!-- uit welke onderdelen bestaat de applicatie -->

## Configuratie

<!-- welke specifieke configuratie kent de applicatie -->

## Installaties

<!-- verwijs naar installaties -->

## Handleidingen en procedures

<!-- verwijs naar google drive documenten/mappen of andere locaties -->

## Verwijzing documentatie

<!-- verwijs naar google drive documenten/mappen of andere locaties -->

## Known issues

<!-- Voeg hier known issues toe -->

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
eenheid toe -->

