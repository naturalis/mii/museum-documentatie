#--- BUILDER -----------------------------------------------------
FROM ubuntu:24.04 AS builder
ENV HUGO_VERSION='0.140.2'
COPY . /var/tmp/build
RUN apt-get update && \
    apt-get upgrade --yes && \
    apt-get install --yes --no-install-recommends \
        git \
        wget \
        rsync \
        ca-certificates && \
    wget --quiet https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-amd64.deb && \
    dpkg -i hugo_${HUGO_VERSION}_Linux-amd64.deb && \
    bash /var/tmp/build/build-for-docker.sh

#--- DOCUMENTATION SERVER -------------------------
FROM nginx:latest
WORKDIR /root/
COPY nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /var/www/docs/ /usr/share/nginx/html/
