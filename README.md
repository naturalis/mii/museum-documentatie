# Naturalis Software Development

This documentation is intended for all technical staff of Naturalis
who deal with the software developed by Naturalis itself.
It is intended to assist employees in understanding the
technical operation of the software. 
Using this technical documentation it should be easier to find solutions to
incidents and problems.

## Install

This documentation can be read and viewed via gitlab, as well as installed locally.

Install hugo (on Mac):

```bash
brew install hugo
```

or for Linux users:

```bash
apt-get install hugo
```

Windows users can follow the
[instruction](https://gohugo.io/getting-started/installing/)
of the hugo project.

Or download one of the [binaries](https://github.com/gohugoio/hugo/releases).

Clone the documentation (do not forget to include the submodule repositories):

```bash
git clone --recurse-submodules https://gitlab.com/naturalis/sd/documentatie
```

Or update the submodules:

```bash
git submodule update --recursive
```


Start the hugo server:

```bash
hugo -D server
```

Open your browser [http://127.0.0.1:1313/](http://127.0.0.1:1313).

Running Hugo has the advantage that you can see the images and
can use the search engine in the theme.

## Using docker

Or if you can't get the most recent version of Hugo installed. Run it via a docker container:

```
docker run -ti -v ${PWD}:/src -p 127.0.0.1:1313:1313 registry.gitlab.com/pages/hugo:0.114.1 hugo server --bind 0.0.0.0
```
 
And if you don't feel like running your own version, use the docker image already built.

 1. First create a [personal access token](https://gitlab.com/profile/personal_access_tokens) (preferably API) store it in your bitwarden
 2. Login: `docker login registry.gitlab.com -u your.username -p` enter the token as password
 3. Run the latest version: `docker run -p 1313:80 registry.gitlab.com/naturalis/sd/documentation:master`
 4. Open your browser [http://127.0.0.1:1313/](http://127.0.0.1:1313).

